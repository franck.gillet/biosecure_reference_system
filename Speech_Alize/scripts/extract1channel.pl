#!/usr/bin/perl -w

# variables d'environnement
$debug       = $ENV{'DEBUG'};


#############################################################
#          extract1channel.pl			           #
#
#  Dec 2005
#
#  Fonction:  separer les canaux
#  
#  Appel:     extract1channel.pl liste donnees extension executable sortie
#             liste = nom de la liste a utiliser (noms des fichiers sans extension) avec indication pour chaque fichier le canal � utilis� 
#             (ex. all_train.list,F1_train.list,...) 
#	      donnees = repertoire o� se trouvent les donn�es audio
#	      extension = extension des donnees audio (ex. sph)	
#	      executable = chemin vers l'executable w_edit (ex. /home/nist/bin)
#	      sortie = repertoire d'enregistrement des donnees traitees
#
#  Entree:    Liste des fichiers:  
#             $listname
#	      Donnes a partager:
#	      $data_orig
#	      Extension:
#	      $data_ext
#	      Executable:
#	      $progs_nist
#	      Sortie:$data_exit
#	      
#
#  Sortie:    Resultats le fichier du canal voulu
#             
#                                                           #
#############################################################

$ARGC= @ARGV;
$ARGC == 5 or die ("wrong number of arguments\n");

print "param.pl [@ARGV]\n" if (defined $debug);
# parametre d'entree: 
# chemin absolu vers la liste a utiliser
$listname = $ARGV[0];
print "\tListe utilis�e:\t$listname\n" if (defined $debug);

# nom du repertoire des donnees audio
$data_orig = $ARGV[1];
print "\tDonnees utilisees:\t$data_orig\n" if (defined $debug);

# extension des donnees audio
$data_ext = $ARGV[2];
print "\tExtension:\t$data_ext\n" if (defined $debug);

# chemin vers executable w_edit
$progs_nist = $ARGV[3];
print "\tExecutable:\t$progs_nist\n" if (defined $debug);

# repertoire d'enregistrement des donnees traitees
$data_exit = $ARGV[4];
print "\tSortie:\t$data_exit\n" if (defined $debug);

# repertoires
$fromdir = "$data_orig";
$todir   = "$data_exit";

# executable
$w_edit  = "$progs_nist/w_edit";

open LIST,$listname or die " cannot open $listname \n";
@items=<LIST>;
close LIST;

foreach $item(@items)
{
    #oter le dernier caractere si il s'agit d'un separateur
    chomp $item;
    
    #enlever l'annee du nom du fichier	
    ($year, $tmp) = split(/\//,$item);
    #separer l'identifiant du cannal
    ($name, $channel) = split(/\./,$tmp);
    #($dir=$name)=~s/\/[^\/]+$//;

    #  creation du repertoire s'il n'existe pas
	$outdir="$todir";
    if(!-d $outdir){
	$comm="mkdir -p $outdir";
	$exitval=system($comm);
	die ("Non-zero exit from $comm") if ($exitval);
	print "$comm\n" if (defined $debug);
    }
    
    # specially process NIST compressed format

  
   if($channel eq "A"){
   
    $comm="$w_edit -o pcm -c 1 $fromdir/$name\.$data_ext $todir/$name\.$channel\.$data_ext";
    $exitval=system("$comm");
    die ("Non-zero exit from $comm") if ($exitval);
    print "$comm\n" if (defined $debug); 
   
   }
   elsif($channel eq "B"){
    $comm="$w_edit -o pcm -c 2 $fromdir/$name\.$data_ext $todir/$name\.$channel\.$data_ext";
    $exitval=system("$comm");
    die ("Non-zero exit from $comm") if ($exitval);
    print "$comm\n" if (defined $debug); 
   }


}
