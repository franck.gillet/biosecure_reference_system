###########################################################
####### Reference System based on 2D face modality ########
###########################################################
###################### BioSecure ##########################
################# Bogazici University #####################
###########################################################
######### first official version 1.0 - 10/01/2008 #########
###########################################################

What can you find here?
-----------------------

- the BioSecure reference system (source code, xml files 
and script) used to make a full 2D face recognition 
experiment.
- a documentation which explains how to use the system.


First step
----------

To begin, I advise you to read the document stored in 
/doc directory.


Short description of each directory
-----------------------------------

- BioSecure2D_5: source files of the reference system:
	- src: source code
	- bin: folder where the binaries will be stored 
after the compilation and installation of the source code
	- files: input files required by the ref. system
	- marker_coordinates: marker coordinates files 
required for the normalization step
	- protocol: target and test filename lists in the 
XML format

- doc: documentation which explains how to install and 
use the reference system.

- download: reference system in the form of compressed 
file (tar.gz) to download it.

- results: intermediate results and final scores (obtained 
on BANCA database in accordance with P protocol). These 
files can be used by the user who does not want to make a 
full experiment from A to Z.

- scripts: scripts used to run a full experiment with the 
reference system in accordance with the P protocol from 
BANCA.

- tar_file: OpenCV compressed file.

- trials: lists of trials in accordance with the P protocol.

Questions
---------

If you have questions about the use of the reference system, 
you can contact the next person:
Aurelien Mayoue: aurelien.mayoue@int-evry.fr
