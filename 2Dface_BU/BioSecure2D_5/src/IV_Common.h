/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if !defined(IV_COMMON_HEADER)
#define IV_COMMON_HEADER

#include <stdio.h>
#include <time.h>
#include <float.h>
#include <math.h>
#include <limits.h>

#include <cv.h>
#include <highgui.h>

#include <vector>
#include <string>
#include <list>
#include <iostream>
#include <algorithm>

#include "tinyxml.h"
#include "FaceData.h"

#define DEBUG_RECOGNIZER_CODE 0
#define MAX_PATH 1000

//---------------------- COMMONS --------------------------------------

typedef struct TempMarkersInfo
{
	CvPoint2D32f points[2];
	int markerIdx[2];
}
TempMarkersInfo;

typedef struct CropParams
{
	int cropWidth;
	int cropHeight;
	TempMarkersInfo tmpMrkrsInfo;
	char* imgType;
	char* markersFileName;
	char* prmsFileName;
	char* imageStorageFolder;
	char* outputLocation;
	char* maskImgFileName;
	IplImage* maskImg;
}
CropParams;

typedef struct CroppedImagesData
{
	CvMat* croppedImgs;
	std::vector<std::string> fileName;
}
CroppedImagesData;

typedef struct PCAParams
{
	std::string dimensionReduction;
	int numEigenvectors;
	float energyRatio;
}
PCAParams;

/*! \struct IVPrmtrSettings
		\brief Structure used for storing the IV system parameters settings
*/
class IVPrmtrSettings
{
public:
	std::string prmtrsName;
  std::string method;
	PCAParams pcaParams;
	std::string similarityMeasure;
	std::string imageFilesLocation;
	std::string imageFilesType;
	std::string maskImageFile;
	std::string trainSessionProtocolFile;
	std::string targetSessionProtocolFile;
	std::string testSessionProtocolFile;

void load_score( const char* pFileName )
	{
		TiXmlDocument doc( pFileName );
		if ( !doc.LoadFile( ) ) return;

		TiXmlHandle hDoc( &doc );
		TiXmlElement* pElem;
		TiXmlHandle hRoot( 0 );

		// block : Root parameters name
		{
			pElem = hDoc.FirstChildElement().Element();

			// should always have a valid root but handle gracefully if it does
			if ( !pElem ) return;

			prmtrsName = pElem->Value();

			// save this for later
			hRoot = TiXmlHandle( pElem );
		}

		// block : METHOD
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "METHOD" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->method = pText;
			}
		}

		// block : PCA_PARAMETERS
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "PCA_PARAMETERS" ).FirstChild().Element();

			if ( !pElem ) return;

			// Element : dimension reduction criterion
      pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->pcaParams.dimensionReduction = pText;
			}

			pElem = pElem->NextSiblingElement();
      
			if ( !pElem ) return;

			// Element : number of eigenvectors
      pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				sscanf( pText, "%d", &this->pcaParams.numEigenvectors );
			}

			pElem = pElem->NextSiblingElement();
      
			if ( !pElem ) return;

			// Element : energy ratio
      pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				sscanf( pText, "%f", &this->pcaParams.energyRatio );
			}
		}

		// block : SIMILARITY_MEASURE
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "SIMILARITY_MEASURE" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->similarityMeasure = pText;
			}
		}

		// block : IMAGE_FILES_LOCATION
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "IMAGE_FILES_LOCATION" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->imageFilesLocation = pText;
			}
		}

		// block : IMAGE_FILES_TYPE
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "IMAGE_FILES_TYPE" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->imageFilesType = pText;
			}
		}

		// block : MASK_IMAGE_FILE
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "MASK_IMAGE_FILE" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->maskImageFile = pText;
			}
		}
		
	}


void load_test( const char* pFileName, const char* pTargetVectorsFileName , const char* pTestVectorsFileName )
	{
		TiXmlDocument doc( pFileName );
		if ( !doc.LoadFile( ) ) return;

		TiXmlHandle hDoc( &doc );
		TiXmlElement* pElem;
		TiXmlHandle hRoot( 0 );

		this->targetSessionProtocolFile = pTargetVectorsFileName;
		this->testSessionProtocolFile = pTestVectorsFileName;

		// block : Root parameters name
		{
			pElem = hDoc.FirstChildElement().Element();

			// should always have a valid root but handle gracefully if it does
			if ( !pElem ) return;

			prmtrsName = pElem->Value();

			// save this for later
			hRoot = TiXmlHandle( pElem );
		}

		// block : METHOD
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "METHOD" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->method = pText;
			}
		}

		// block : PCA_PARAMETERS
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "PCA_PARAMETERS" ).FirstChild().Element();

			if ( !pElem ) return;

			// Element : dimension reduction criterion
      pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->pcaParams.dimensionReduction = pText;
			}

			pElem = pElem->NextSiblingElement();
      
			if ( !pElem ) return;

			// Element : number of eigenvectors
      pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				sscanf( pText, "%d", &this->pcaParams.numEigenvectors );
			}

			pElem = pElem->NextSiblingElement();
      
			if ( !pElem ) return;

			// Element : energy ratio
      pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				sscanf( pText, "%f", &this->pcaParams.energyRatio );
			}
		}

		// block : SIMILARITY_MEASURE
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "SIMILARITY_MEASURE" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->similarityMeasure = pText;
			}
		}

		// block : IMAGE_FILES_LOCATION
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "IMAGE_FILES_LOCATION" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->imageFilesLocation = pText;
			}
		}

		// block : IMAGE_FILES_TYPE
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "IMAGE_FILES_TYPE" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->imageFilesType = pText;
			}
		}

		// block : MASK_IMAGE_FILE
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "MASK_IMAGE_FILE" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->maskImageFile = pText;
			}
		}
		
	}


void load_save( const char* pFileName, const char* pTrainingVectorsFileName)
	{
		TiXmlDocument doc( pFileName );
		if ( !doc.LoadFile( ) ) return;

		TiXmlHandle hDoc( &doc );
		TiXmlElement* pElem;
		TiXmlHandle hRoot( 0 );

		this->trainSessionProtocolFile = pTrainingVectorsFileName;

		// block : Root parameters name
		{
			pElem = hDoc.FirstChildElement().Element();

			// should always have a valid root but handle gracefully if it does
			if ( !pElem ) return;

			prmtrsName = pElem->Value();

			// save this for later
			hRoot = TiXmlHandle( pElem );
		}

		// block : METHOD
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "METHOD" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->method = pText;
			}
		}

		// block : PCA_PARAMETERS
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "PCA_PARAMETERS" ).FirstChild().Element();

			if ( !pElem ) return;

			// Element : dimension reduction criterion
      pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->pcaParams.dimensionReduction = pText;
			}

			pElem = pElem->NextSiblingElement();
      
			if ( !pElem ) return;

			// Element : number of eigenvectors
      pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				sscanf( pText, "%d", &this->pcaParams.numEigenvectors );
			}

			pElem = pElem->NextSiblingElement();
      
			if ( !pElem ) return;

			// Element : energy ratio
      pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				sscanf( pText, "%f", &this->pcaParams.energyRatio );
			}
		}

		// block : SIMILARITY_MEASURE
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "SIMILARITY_MEASURE" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->similarityMeasure = pText;
			}
		}

		// block : IMAGE_FILES_LOCATION
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "IMAGE_FILES_LOCATION" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->imageFilesLocation = pText;
			}
		}

		// block : IMAGE_FILES_TYPE
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "IMAGE_FILES_TYPE" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->imageFilesType = pText;
			}
		}

		// block : MASK_IMAGE_FILE
		{
			const char* pKey = NULL;
			const char* pText = NULL;

			pElem = hRoot.FirstChild( "MASK_IMAGE_FILE" ).Element();

			if ( !pElem ) return;

			pKey = pElem->Value();
			pText = pElem->GetText();

			if ( pKey && pText )
			{
				this->maskImageFile = pText;
			}
		}
		
	}

	IVPrmtrSettings() 
	{
		// Set default values
		method = std::string("PCA");
		pcaParams.dimensionReduction = std::string("energy");
		pcaParams.energyRatio = 0.99F;
		pcaParams.numEigenvectors = 10;
		similarityMeasure = std::string("whitened_cosine_angle");
    imageFilesLocation = std::string("./");
		trainSessionProtocolFile = std::string("train_protocol.xml");
		targetSessionProtocolFile = std::string("target_protocol.xml");
		testSessionProtocolFile = std::string("test_protocol.xml");
	};

};

/*!
\fn				void RandShuffle( CvMat* mat )
\brief		used for shuffling the given array values
\param		mat CvMat pointer to the input array to be shuffled
*/
void RandShuffle( CvMat* mat );

/*!
\fn				void HistEq( IplImage* src, IplImage* dst, IplImage* mask )
\brief		used for histogram equalizing the given image
\param		src IplImage pointer to the input image to be histogram equalized
\param		dst IplImage pointer to the histogram equalized output image
\param		mask IplImage pointer to single bit image to determine te valid pixels
to be included in the histogram equalization process
*/
void HistEq( IplImage* src, IplImage* dst, IplImage* mask );

/*!
\fn				void GetNormalizedImageVector( IplImage* srcImg, CvMat* imgVector, IplImage* mskImg )
\brief		used for normalizing the input image and converting it to a vector array
\param		srcImg IplImage pointer to the input image to be normalized
\param		imgVector CvMat pointer to the normalized output image pixel values
\param		mskImg IplImage pointer to single bit image to determine te valid pixels
to be included in the normalization process
*/
void GetNormalizedImageVector( IplImage* srcImg, CvMat* imgVector, IplImage* mskImg );

/*!
\fn				void GetNormalizedImage( CvMat* imgVector, IplImage* normalizedImg, IplImage* mskImg )
\brief		used for normalizing the input image and converting it to a vector array
\param		srcImg IplImage pointer to the input image to be normalized
\param		imgVector CvMat pointer to the normalized output image pixel values
\param		mskImg IplImage pointer to single bit image to determine te valid pixels
to be included in the normalization process
*/
void GetNormalizedImage( CvMat* imgVector, IplImage* normalizedImg, IplImage* mskImg );

/*!
\fn				void GetNormalizedMirrorImageVector( IplImage* srcImg, CvMat* imgVector, IplImage* mskImg )
\brief		used for normalizing and mirroring the input image vertically and converting it to a vector array
\param		srcImg IplImage pointer to the input image to be normalized
\param		imgVector CvMat pointer to the normalized output image pixel values
\param		mskImg IplImage pointer to single bit image to determine te valid pixels
to be included in the normalization process
*/
void GetNormalizedMirrorImageVector( IplImage* srcImg, CvMat* imgVector, IplImage* mskImg );

/*! \brief Fills the image vector by valid pixel values defined by maskImage

		\param srcImg pointer to the source image which stores the image values.
		\param imgVector pointer to the vector array which stores the valid pixel values.
		\param maskImage pointer to the mask image which stores the mask information.
*/
void GetImageVector(IplImage* srcImg, CvMat* imgVector, IplImage* mskImg);

/*!
\fn				void FindSimilarityTransformation( CvMat* referenceSet, CvMat* featureSet, CvMat* transform )
\brief		used for finding the best similarity transformation in the minimum-squared error sense
which takes the featureSet point set and transforms it to the referenceSet point set
\param		referenceSet target point set
\param		featureSet input point set
\param		transform 2x3 CvMat transform array pointer which describes an similarity transformation
*/
void FindSimilarityTransformation( CvMat* referenceSet, CvMat* featureSet, CvMat* transform );

/*!
\fn				void FindAffineTransformation( CPointSet referenceSet, CPointSet featureSet, CvMat* transform )
\brief		used for finding the best affine transformation in the minimum-squared error sense
which takes the featureSet point set and transforms it to the referenceSet point set
\param		referenceSet target point set
\param		featureSet input point set
\param		transform 2x3 CvMat transform array pointer which describes an affine transform
*/
void FindAffineTransformation( CvMat* referenceSet, CvMat* featureSet, CvMat* transform );

void FindAlignmentTransform( CvMat* referenceSet, CvMat* featureSet, CvMat* transform );

bool FindReferenceFaceTemplate4Procrustes( CropParams* crpPrms, CvMat* markerData, CvMat* referenceMarkers, CvMat* refInvTrnsfrm );

/*!
\fn				void Procrustes( CvMat* markerData, CropParams* crpPrms )
\brief		used for finding the necessary transformation for each image using the input parameters
given by the crpPrms parameter.
\param		markerData face feature marker coordinates data matrix pointer
\param		crpPrms cropping paaremeters storage structure pointer
*/
CvMat* Procrustes( CvMat* markerData, CropParams* crpPrms );

/*!
\fn				int GetInputDimension( IplImage* maskImage )
\brief		used for finding the number of dimension of the image vectors used in the training set
given the mask image file name which describes the valid pixel location of the training and test images
\param		maskImage IplImage pointer to single bit image to determine te valid pixels
*/
int GetInputDimension( IplImage* maskImage );

/*!
\fn				void MakeUnitNorm(CvMat *x)
\brief		used for scaling an input vector such that it has a unit norm.
*/
void MakeUnitNorm(CvMat *x);

/*!
\fn				void ShowVectorImage( CvMat* imgVector )
\brief		Shows the input image vector in a display window, used for debug purposes only.
\param		imgVector pointer to the CvMat array which holds the image vector to be displayed
*/
void ShowVectorImage( CvMat* imgVector );

/*!
\fn				void ReadMarkerData( CropParams* crpPrms )
\brief		Reads the marker data and returns its matrix pointer.
\param		crpPrms pointer to the CropParams structure which hold the information about cropping
*/
CvMat* ReadMarkerData( CropParams* crpPrms );

/*!
\fn				void CreatePositiveCroppedImages( CropParams* crpPrms, CvMat* markerData, CvMat* transformArray )
\brief		Crops the images and returns them in the matrix pointer.
\param		crpPrms pointer to the CropParams structure which hold the information about cropping
\param		markerData face feature marker coordinates data matrix pointer
\param		transformArray pointer to the matrix pointer which holds the values of the transformation arrays.
*/
CroppedImagesData* CreatePositiveCroppedImages( CropParams* crpPrms, CvMat* markerData, CvMat* transformArray );

/*!
\fn				bool SaveCroppedImages( CvMat* croppedImgs, CropParams* crpPrms )
\brief		Saves the cropped images by using crpPrms information in a single binary or separate images.
\param		croppedImgs pointer to matrix which holds the cropped images in the rows.
\param		crpPrms pointer to the CropParams structure which hold the information about cropping.
*/
bool SaveCroppedImages( CroppedImagesData* croppedData, CropParams* crpPrms );

/*!
\fn				CvMat* LoadCroppedImages( const char* fileName )
\brief		Loads the cropped images and the returns the pointer to its matrix.
\param		fileName pointer to the fileName storage
*/
CvMat* LoadCroppedImages( const char* fileName );

/*!
\fn				isLessThan(const double &v1, const double &v2)
\brief		Comparison function to be used to sort by word length.
\param		v1 reference to the first value
\param		v2 reference to the second value
*/
bool IsLessThan(const double &v1, const double &v2);

#endif
