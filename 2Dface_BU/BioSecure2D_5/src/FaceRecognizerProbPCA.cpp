/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "FaceRecognizerProbPCA.h"

CRecognizerProbPCA::CRecognizerProbPCA( FaceData& trainData )
:m_trainData(trainData)
{
	m_M = 0;
	m_N = m_trainData.m_dataMatrix->cols;
	m_K = m_trainData.m_dataMatrix->rows;
	m_rho = 0;

	m_Phi = NULL;
	m_eigVals = NULL;
	m_covIntraMat = NULL;
	m_diffDataMat = NULL;
}

CRecognizerProbPCA::CRecognizerProbPCA( std::string& modelFileName )
{
	m_M = 0;
	m_N = 0;
	m_K = 0;
	m_rho = 0;

	m_Phi = NULL;
	m_eigVals = NULL;
	m_covIntraMat = NULL;
	m_diffDataMat = NULL;

	LoadModel( modelFileName );
}

CRecognizerProbPCA::CRecognizerProbPCA( )
{
	m_M = 0;
	m_N = 0;
	m_K = 0;
	m_rho = 0;

	m_Phi = NULL;
	m_eigVals = NULL;
	m_covIntraMat = NULL;
	m_diffDataMat = NULL;

}

CRecognizerProbPCA::CRecognizerProbPCA(std::string& modelFileName, FaceData& targetData )
:m_targetData(targetData)
{
	m_M = 0;
	m_N = m_targetData.m_dataMatrix->cols;
	m_K = m_targetData.m_dataMatrix->rows;
	m_rho = 0;

	m_Phi = NULL;
	m_eigVals = NULL;
	m_covIntraMat = NULL;
	m_diffDataMat = NULL;

	LoadModel( modelFileName );

}

CRecognizerProbPCA::~CRecognizerProbPCA( )
{
	if ( m_eigVals != NULL )
		cvReleaseMat( &m_eigVals );
	if ( m_Phi != NULL )
		cvReleaseMat( &m_Phi );
	if ( m_covIntraMat != NULL )
		cvReleaseMat( &m_covIntraMat );
	if ( m_diffDataMat != NULL )
		cvReleaseMat( &m_diffDataMat );
}

bool CRecognizerProbPCA::ComputeDiffDataMatrix( )
{
	if ( m_diffDataMat != NULL )
	{
		cvReleaseMat( &m_diffDataMat );
		m_diffDataMat = NULL;
	}

	int i, j;
	int countDiffImg = 0;
	CvMat imgVecA, imgVecB, rowVec;

	CvMat* diffImg = cvCreateMat( 1, m_N, CV_64FC1 );

	// Coount Difference Images
	countDiffImg = 0;
	for (i=0; i < m_K; i++)
	{
		for (j=0; j < m_K; j++)
		{
			if ( (i == j) || (m_trainData.m_actualId[i] != m_trainData.m_actualId[j]) )
				continue;

			countDiffImg++;
		}
	}

	// Create difference image data matrix
	m_diffDataMat = cvCreateMat( countDiffImg, m_N, CV_64FC1 );

	// Compute difference image data matrix
	countDiffImg = 0;
	for (i=0; i < m_K; i++)
	{
		cvGetRow( m_trainData.m_dataMatrix, &imgVecA, i );
		for (j=0; j < m_K; j++)
		{
			if ( (i == j) || (m_trainData.m_actualId[i] != m_trainData.m_actualId[j]) )
				continue;

			cvGetRow( m_trainData.m_dataMatrix, &imgVecB, j );

			cvSub( &imgVecA, &imgVecB, diffImg );

			cvGetRow( m_diffDataMat, &rowVec, countDiffImg );
			cvCopy( diffImg, &rowVec );

			countDiffImg++;
		}
	}

	cvReleaseMat( &diffImg );

	return true;
}

bool CRecognizerProbPCA::ComputeCovIntraMatrix( )
{
	if ( m_covIntraMat != NULL )
	{
		cvReleaseMat( &m_covIntraMat );
		m_covIntraMat = NULL;
	}

	if ( m_diffDataMat == NULL )
	{
		return false;
	}

	int i, j;

	// Calculate covaricance matrix
	if ( m_N > m_diffDataMat->rows )
	{
    m_covIntraMat = cvCreateMat( m_diffDataMat->rows, m_diffDataMat->rows, CV_64FC1 );
		cvMulTransposed( m_diffDataMat, m_covIntraMat, 0, NULL );
	}
	else
	{
		m_covIntraMat = cvCreateMat( m_N, m_N, CV_64FC1 );
		cvMulTransposed( m_diffDataMat, m_covIntraMat, 1, NULL );
	}

	// Scale covariance matrix of difference image vectors
	for (i=0; i < m_covIntraMat->rows; i++)
	{
		for (j=0; j < m_covIntraMat->cols; j++)
		{
			double matVal = CV_MAT_ELEM( *m_covIntraMat, double, i, j );
			CV_MAT_ELEM( *m_covIntraMat, double, i, j ) = matVal/(double) m_diffDataMat->rows;
		}
	}

	return true;
}

int CRecognizerProbPCA::ComputeM( CvMat* eigVals, double energy_ratio )
{
	int i;
	double csum = 0, avg_enrgy = 0;

	for (i=0; i < eigVals->rows; i++)
		avg_enrgy += CV_MAT_ELEM( *eigVals, double, i, 0 );

	csum = 0;
	i = 0;
	double energy_thr = energy_ratio*avg_enrgy;
	while ( csum < energy_thr )
	{
		csum += CV_MAT_ELEM( *eigVals, double, i, 0 );
		i++;
	}

	return i;
}

bool CRecognizerProbPCA::ComputeProjMat( int _M )
{
	m_M = _M;

	if ( m_covIntraMat == NULL )
		return false;

	int i;

	CvRect rect;
	CvMat temp_mat, temp_vec;

	CvMat* I = cvCloneMat( m_covIntraMat );
	CvMat* W = cvCreateMat(I->cols,1,CV_64FC1);
	CvMat* U = cvCreateMat(I->cols,I->cols,CV_64FC1);
	CvMat* V = cvCreateMat(I->cols,I->cols,CV_64FC1);

	if ( m_N > m_covIntraMat->cols )
	{
		cvSVD( I, W, U, V, CV_SVD_MODIFY_A | CV_SVD_U_T );

		rect.x = rect.y = 0;
		rect.height = m_M; rect.width = U->cols;
		cvGetSubRect( U, &temp_mat, rect );

		CvMat* PhiL = cvCloneMat( &temp_mat );
		CvMat* PhiT = cvCreateMat( m_M, m_N, CV_64FC1);
		m_Phi = cvCreateMat( m_N, m_M, CV_64FC1);

		cvMatMulAdd( PhiL, m_diffDataMat, 0, PhiT );
		for (i=0; i < PhiT->rows; i++)
		{
			cvGetRow( PhiT, &temp_vec, i );
			MakeUnitNorm( &temp_vec );
		}
		cvTranspose( PhiT, m_Phi );

		cvReleaseMat(&PhiL);
		cvReleaseMat(&PhiT);
	}
	else
	{
		cvSVD( I, W, U, V, CV_SVD_MODIFY_A );

		rect.x = rect.y = 0;
		rect.height = V->rows; rect.width = m_M;
		cvGetSubRect( V, &temp_mat, rect );

		m_Phi = cvCloneMat(&temp_mat);
	}

	cvReleaseMat( &I );
	cvReleaseMat( &U );
	cvReleaseMat( &V );

	// Compute Rho
	m_rho = 0.;
	if ( W->rows == m_M )
		m_rho = 0.;
	else
	{
		for (i=m_M; i < W->rows; i++)
			m_rho += CV_MAT_ELEM( *W, double, i, 0 );
		m_rho /= double (W->rows-m_M);
	}

	if ( m_eigVals != NULL )
		cvReleaseMat( &m_eigVals );

	m_eigVals = cvCreateMat( 1, m_M, CV_64FC1 );
	for (i=0; i < m_M; i++)
	{
		double eig_val = CV_MAT_ELEM( *W, double, i, 0 );
		CV_MAT_ELEM( *m_eigVals, double, 0, i ) = eig_val;
	}

	cvReleaseMat( &W );

	return true;
}

bool CRecognizerProbPCA::ComputeProjMat( double  energyRatio )
{
	if ( m_covIntraMat == NULL )
		return false;

	int i;

	CvRect rect;
	CvMat temp_mat, temp_vec;

	CvMat* I = cvCloneMat( m_covIntraMat );
	CvMat* W = cvCreateMat(I->cols,1,CV_64FC1);
	CvMat* U = cvCreateMat(I->cols,I->cols,CV_64FC1);
	CvMat* V = cvCreateMat(I->cols,I->cols,CV_64FC1);

	if ( m_N > m_covIntraMat->cols )
	{
		cvSVD( I, W, U, V, CV_SVD_MODIFY_A | CV_SVD_U_T );

		m_M = ComputeM(W, energyRatio);

		rect.x = rect.y = 0;
		rect.height = m_M; rect.width = U->cols;
		cvGetSubRect( U, &temp_mat, rect );

		CvMat* PhiL = cvCloneMat( &temp_mat );
		CvMat* PhiT = cvCreateMat( m_M, m_N, CV_64FC1);
		m_Phi = cvCreateMat( m_N, m_M, CV_64FC1);

		cvMatMulAdd( PhiL, m_diffDataMat, 0, PhiT );
		for (i=0; i < PhiT->rows; i++)
		{
			cvGetRow( PhiT, &temp_vec, i );
			MakeUnitNorm( &temp_vec );
		}
		cvTranspose( PhiT, m_Phi );

		cvReleaseMat(&PhiL);
		cvReleaseMat(&PhiT);
	}
	else
	{
		cvSVD( I, W, U, V, CV_SVD_MODIFY_A );

		m_M = ComputeM(W, energyRatio);

		rect.x = rect.y = 0;
		rect.height = V->rows; rect.width = m_M;
		cvGetSubRect( V, &temp_mat, rect );

		m_Phi = cvCloneMat(&temp_mat);
	}

	cvReleaseMat( &I );
	cvReleaseMat( &U );
	cvReleaseMat( &V );

	// Compute Rho
	m_rho = 0.;
	if ( W->rows == m_M )
		m_rho = 0.;
	else
	{
		for (i=m_M; i < W->rows; i++)
			m_rho += CV_MAT_ELEM( *W, double, i, 0 );
		m_rho /= double (W->rows-m_M);
	}

	if ( m_eigVals != NULL )
		cvReleaseMat( &m_eigVals );

	m_eigVals = cvCreateMat( 1, m_M, CV_64FC1 );
	for (i=0; i < m_M; i++)
	{
		double eig_val = CV_MAT_ELEM( *W, double, i, 0 );
		CV_MAT_ELEM( *m_eigVals, double, 0, i ) = eig_val;
	}

	cvReleaseMat( &W );

	return true;
}

double CRecognizerProbPCA::MLPCADistance( CvMat* x )
{
	int i;
	double dist;
	double projsum = 0.;
	double ResErr;
	CvMat* y;
	CvMat* xtilta;

	xtilta = cvCloneMat(x);
	y = cvCreateMat( 1, m_M, CV_64FC1 );

	cvMatMulAdd( xtilta, m_Phi, 0, y );

	if ( m_rho == 0 )
	{
		for (i=0; i < m_M; i++)
			projsum += (y->data.db[i]*y->data.db[i])/m_eigVals->data.db[i];
		dist = projsum;
	}
	else
	{
		ResErr = cvDotProduct(xtilta,xtilta)-cvDotProduct(y,y);
		for (i=0; i < m_M; i++)
			projsum += (y->data.db[i]*y->data.db[i])/m_eigVals->data.db[i];
		dist = projsum + (ResErr/m_rho);
	}

	cvReleaseMat(&y);
	cvReleaseMat(&xtilta);

	return dist;
}

bool CRecognizerProbPCA::Train( int m_M )
{
	bool check = true;

	// Set parameters
	m_N = m_trainData.m_dataMatrix->cols;
	m_K = m_trainData.m_dataMatrix->rows;

	check = ComputeDiffDataMatrix();

	if ( !check )
		return false;

	check = ComputeCovIntraMatrix();

	if ( !check )
		return false;

	check = ComputeProjMat( m_M );

	return check;
}

bool CRecognizerProbPCA::Train( double energyRatio )
{
	bool check = true;

	// Set parameters
	m_N = m_trainData.m_dataMatrix->cols;
	m_K = m_trainData.m_dataMatrix->rows;

	check = ComputeDiffDataMatrix();

	if ( !check )
		return false;

	check = ComputeCovIntraMatrix();

	if ( !check )
		return false;

	check = ComputeProjMat( energyRatio );

	return check;
}

static int CmpFunc( const void* _a, const void* _b, void* )
{
	double a = *((double*)_a);
  double b = *((double*)_b);
	return ((a < b) ? -1 : ((a > b) ? 1:0));
}

double CRecognizerProbPCA::MeasureSimilarity( CvMat* imgA, CvMat* imgB )
{
	int i, j, idx;
	double distance;
	CvMat rowVecA, rowVecB;
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSeq* distanceVals = cvCreateSeq( CV_SEQ_ELTYPE_GENERIC, sizeof(CvSeq), sizeof(double), storage );

	CvMat* diffImg = cvCreateMat( 1, imgA->cols, CV_64FC1 );

	for (i=0; i < imgA->rows; i++)
	{
		cvGetRow( imgA, &rowVecA, i );
		for (j=0; j < imgB->rows; j++)
		{
      cvGetRow( imgB, &rowVecB, i );
			cvSub( &rowVecA, &rowVecB, diffImg );
      distance = MLPCADistance( diffImg );
			cvSeqPush( distanceVals, &distance );
		}
	}
	cvSeqSort( distanceVals, CmpFunc, NULL );

	// Return minimum value of distance values
	idx = 0;

	// Return median value of distance values
	//idx = cvRound( (double) distanceVals->total/2 );

	distance = *((double*)cvGetSeqElem( distanceVals, idx ));

	cvReleaseMemStorage( &storage );
	cvReleaseMat( &diffImg );

	return distance;
}

bool CRecognizerProbPCA::Test( FaceData& testData,
															 std::string& testScoresOutFileName, 
															 std::string& testROCValuesFileName )
{
	int i, j, n;
	int numAttacks = 0;
	int numTrueAccess = 0;
  CvMat rowVec, rowVecTrgt;
  CvMat* imgTest = cvCreateMat( 1, m_N, CV_64FC1 );
  CvMat* imgTrgt = NULL;
	std::vector<int> idList;
	std::vector<double> scores;
	std::string exampleIds;
	std::string separator(":");

	if( !m_targetData.m_dataMatrix )
  {
    std::cerr << "Target data matrix is not available for testing." << std::endl;
    exit(0);
  }

	TiXmlDocument doc;  
 	TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
	doc.LinkEndChild( decl );  
 
	TiXmlElement * root = new TiXmlElement( "match_data" );  
	doc.LinkEndChild( root );  

	TiXmlComment * comment = new TiXmlComment();
	comment->SetValue( "Test Scores for BioSecure IV System." );  
	root->LinkEndChild( comment );  

	scores.clear();

	for (i=0; i < testData.m_dataMatrix->rows; i++)
	{
		// Load test image vector
		cvGetRow( testData.m_dataMatrix, &rowVec, i );
		cvCopy( &rowVec, imgTest );

		// Get PCA coefficients of example ID image vectors
		GetTargetIDList4Subject( idList, testData.m_claimedId[i] );
		int numClaimedIdExamples = (int) idList.size();
		if( numClaimedIdExamples == 0 )
		{
			std::cout << "There is no example for the claimed ID " << testData.m_claimedId[i] << "." << std::endl;
			doc.SaveFile( testScoresOutFileName );
			exit(0);
		}

		exampleIds.clear();
		for ( j=0; j < numClaimedIdExamples-1; j++)
    {
			exampleIds += (m_targetData.m_fileName[idList[j]] + separator);
    }
		exampleIds += m_targetData.m_fileName[idList[j]];

		imgTrgt = cvCreateMat( numClaimedIdExamples, m_N, CV_64FC1 );

		for ( j=0; j < imgTrgt->rows; j++)
    {
			cvGetRow( m_targetData.m_dataMatrix, &rowVec, idList[j] );
	    cvGetRow( imgTrgt, &rowVecTrgt, j );
	    cvCopy( &rowVec, &rowVecTrgt );
    }

	  // Now measure similarity between two sets of images
	  double similarity = MeasureSimilarity( imgTest, imgTrgt );

		// Save score values
		scores.push_back( similarity );

		TiXmlElement* claimResult = new TiXmlElement( "claimresult" );  
		root->LinkEndChild( claimResult );
		claimResult->SetAttribute( "real_id", testData.m_actualId[i] );
		claimResult->SetAttribute( "claimed_id", testData.m_claimedId[i] );
		claimResult->SetAttribute( "access_file_name", testData.m_fileName[i] );
		claimResult->SetDoubleAttribute( "score", similarity );
		claimResult->SetAttribute( "claimed_id_examples", exampleIds );

    cvReleaseMat( &imgTrgt );

		if ( testData.m_claimedId[i] != testData.m_actualId[i] )
			numAttacks++;
		else
			numTrueAccess++;

		double percentage = (100.0 * i) / (double) testData.m_dataMatrix->rows;

		fprintf( stderr, "WRITING LOG VALUES OF TEST SAMPLES %3d%%\r", (int) percentage );
		fflush( stderr );
	}

	doc.SaveFile( testScoresOutFileName );

	// Now compute ROC pair values
	stable_sort( scores.begin(), scores.end(), IsLessThan );

	std::vector<double>::iterator scoreIterator;

	double scoreMinVal = 0;
	double scoreMaxVal = 0;
	double scoreVal = 0;
	for ( scoreIterator = scores.begin(); scoreIterator != scores.end(); scoreIterator++)
	{
		scoreVal = *scoreIterator;
		//std::cout << scoreVal << std::endl;

		if ( (scoreVal < scoreMinVal) || (scoreIterator == scores.begin()) )
			scoreMinVal = scoreVal;

		if ( (scoreVal > scoreMaxVal) || (scoreIterator == scores.begin()) )
			scoreMaxVal = scoreVal;
	}

	int numIntervals = 256;
	double delta = (scoreMaxVal-scoreMinVal)/(double) numIntervals;

	double falseAlarmRate = 0;
	double falseRejectRate = 0;
	double thresholdVal = 0;
	double wer = 0;
	double minWer = 0;
	double r = 1;
	double minWerThreshold, minWerFRR, minWerFAR;

	std::vector<double> farStrg;
	std::vector<double> frrStrg;

	farStrg.clear();
	frrStrg.clear();

  //std::cout << "Writing ROC values for Test Data ..." << testROCValuesFileName << std::endl;
  FILE* inStream = fopen(testROCValuesFileName.c_str(), "wt");
	if(!inStream)
	{
		std::cerr << "Can not open ROC values saving file " << testROCValuesFileName
	<< " for writing." << std::endl;
		exit(-1);
	}

	for ( n=0; n < numIntervals; n++)
	{
		thresholdVal = scoreMinVal+n*delta;

		falseAlarmRate = 0;
		falseRejectRate = 0;

		// Check each test data sample for the threshold
		for (i=0; i < testData.m_dataMatrix->rows; i++)
		{
			// Load test image vector
			cvGetRow( testData.m_dataMatrix, &rowVec, i );
			cvCopy( &rowVec, imgTest );

			// Get PCA coefficients of example ID image vectors
			GetTargetIDList4Subject( idList, testData.m_claimedId[i] );
			int numClaimedIdExamples = (int) idList.size();
			if( numClaimedIdExamples == 0 )
			{
				std::cout << "There is no example for the claimed ID " << testData.m_claimedId[i] << "." << std::endl;
				doc.SaveFile( testScoresOutFileName );
				exit(0);
			}

			imgTrgt = cvCreateMat( numClaimedIdExamples, m_N, CV_64FC1 );

			for ( j=0; j < imgTrgt->rows; j++)
			{
				cvGetRow( m_targetData.m_dataMatrix, &rowVec, idList[j] );
				cvGetRow( imgTrgt, &rowVecTrgt, j );
				cvCopy( &rowVec, &rowVecTrgt );
			}

			// Now measure similarity between two sets of images
			double similarity = MeasureSimilarity( imgTest, imgTrgt );

			if ( (similarity < thresholdVal) && (testData.m_claimedId[i] != testData.m_actualId[i]) )
				falseAlarmRate++;

			if ( (similarity > thresholdVal) && (testData.m_claimedId[i] == testData.m_actualId[i]) )
				falseRejectRate++;

			cvReleaseMat( &imgTrgt );
		}
		
		falseAlarmRate /= (double) numAttacks;
		falseRejectRate /= (double) numTrueAccess;

		falseAlarmRate *= 100;
		falseRejectRate *= 100;

		wer = (falseRejectRate+r*falseAlarmRate)/(1+r);

		if ( (wer < minWer) || (n == 0) )
		{
			minWer = wer;
			minWerThreshold = thresholdVal;
			minWerFRR = falseRejectRate;
			minWerFAR = falseAlarmRate;
		}

		// Save performance values
		farStrg.push_back( falseAlarmRate );
		frrStrg.push_back( falseRejectRate );

		fprintf( inStream, "%lf %lf %lf\n", falseAlarmRate, falseRejectRate, thresholdVal );

		double percentage = (100.0 * n) / (double) numIntervals;

		fprintf( stderr, "WRITING ROC VALUES OF TEST SAMPLES %3d%%\r", (int) percentage );
		fflush( stderr );
	}

	fprintf( stderr, "                                                \r" );
	fflush( stderr );

	std::cout << "MINIMUM WER THRESHOLD : " << minWerThreshold << std::endl; 
	std::cout << "MINIMUM WER FAR       : " << minWerFAR << std::endl; 
	std::cout << "MINIMUM WER FRR       : " << minWerFRR << std::endl; 

	fclose( inStream );

	cvReleaseMat( &imgTest );


	return true;
}

bool CRecognizerProbPCA::WriteTargetVectors( IVPrmtrSettings& settings, std::string& targetVectorsOutFileName )
{
	int i, j;

  FaceData& targetData = GetTargetData();

	if( !targetData.m_dataMatrix )
  {
    std::cerr << "Test Data matrix is not available." << std::endl;
    exit(0);
  }

	TiXmlDocument doc;  
 	TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
	doc.LinkEndChild( decl );  
 
	TiXmlElement * root = new TiXmlElement( "target_vectors_session" );  
	doc.LinkEndChild( root );  

	root->SetAttribute( "database_id", targetData.m_databaseID );
	root->SetAttribute( "protocol_id", targetData.m_protocolID );
	root->SetAttribute( "vector_dimension", m_N );

	TiXmlComment * comment = new TiXmlComment();
	comment->SetValue( "Target Vectors Data." );  
	root->LinkEndChild( comment );

	char field[MAX_PATH];
	char fileLocation[256];
	for (i=0; i < targetData.m_dataMatrix->rows; i++)
	{
		TiXmlElement* targetVector = new TiXmlElement( "target_vector" );  
		root->LinkEndChild( targetVector );

		sprintf( fileLocation, "%s/%s.%s", settings.imageFilesLocation.c_str(), targetData.m_fileName[i].c_str(), settings.imageFilesType.c_str() );

		targetVector->SetAttribute( "target_id", fileLocation );
		
		//for (j=0; j < targetData.m_dataMatrix->cols; j++)
		//{
		//	double val = CV_MAT_ELEM( *(targetData.m_dataMatrix), double, i, j );
		//  sprintf( field, "v_%d", j );
		//	targetVector->SetDoubleAttribute( field, val );
		//}

		//if( i % 10 == 0 )
		//{
		//		fprintf( stderr, "WRITING TARGET VECTORS %3d%%\r", (int) ( 100.0 * ((double) i / (double) targetData.m_dataMatrix->rows) ) );
		//		fflush( stderr );
		//}

	}

 	doc.SaveFile( targetVectorsOutFileName );

	//fprintf( stderr, "FINISHED TARGET VECTORS WRITING.       \n" );
	//fflush( stderr );

	return true;
}

bool CRecognizerProbPCA::WriteTestVectors( IVPrmtrSettings& settings, FaceData& testData, std::string& testVectorsOutFileName )
{
	int i, j;

	if( !testData.m_dataMatrix )
  {
    std::cerr << "Test Data matrix is not available." << std::endl;
    exit(0);
  }

	TiXmlDocument doc;  
 	TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
	doc.LinkEndChild( decl );  
 
	TiXmlElement * root = new TiXmlElement( "test_vectors_data" );  
	doc.LinkEndChild( root );  

	TiXmlComment * comment = new TiXmlComment();
	comment->SetValue( "Test Vectors Data." );  
	root->LinkEndChild( comment );

	root->SetAttribute( "database_id", testData.m_databaseID );
	root->SetAttribute( "protocol_id", testData.m_protocolID );
	root->SetAttribute( "vector_dimension", m_N );

	char field[MAX_PATH];
	char fileLocation[256];
	for (i=0; i < testData.m_dataMatrix->rows; i++)
	{
		TiXmlElement* testVector = new TiXmlElement( "test_vector" );  
		root->LinkEndChild( testVector );

		sprintf( fileLocation, "%s/%s.%s", settings.imageFilesLocation.c_str(), testData.m_fileName[i].c_str(), settings.imageFilesType.c_str() );
		testVector->SetAttribute( "test_id", fileLocation );
		
		//for (j=0; j < testData.m_dataMatrix->cols; j++)
		//{
		//	double val = CV_MAT_ELEM( *(testData.m_dataMatrix), double, i, j );
		//  sprintf( field, "v_%d", j );
		//	testVector->SetDoubleAttribute( field, val );
		//}

		//if( i % 10 == 0 )
		//{
		//		fprintf( stderr, "WRITING XML FILE %3d%%\r", (int) ( 100.0 * ((double) i / (double) testData.m_dataMatrix->rows) ) );
		//		fflush( stderr );
		//}
	}

 	doc.SaveFile( testVectorsOutFileName );

	//fprintf( stderr, "FINISHED TEST VECTORS WRITING.       \n" );
	//fflush( stderr );

	return true;
}

TiXmlElement* CRecognizerProbPCA::GetTargetElement( TiXmlHandle& hDoc, int& vectorDimension )
{
	TiXmlElement* pElem;
	TiXmlHandle hRoot( 0 );

	std::string session_type;
	std::string databaseID;
	std::string protocolID;

	const char *pName = NULL;

	pElem = hDoc.FirstChildElement().Element();

	// should always have a valid root but handle gracefully if it does
	if ( !pElem ) return false;

	session_type = pElem->Value();

	pName = pElem->Attribute("database_id");
	if (pName) databaseID = pName;
	
	pName = pElem->Attribute("protocol_id");
	if (pName) protocolID = pName;

	pElem->QueryIntAttribute( "vector_dimension", &vectorDimension );

	// save this for later
	hRoot = TiXmlHandle( pElem );

	pElem = hRoot.FirstChild( "target_vector" ).Element();

	return pElem;
}

TiXmlElement* CRecognizerProbPCA::GetTestElement( TiXmlHandle& hDoc, int& vectorDimension )
{
	TiXmlElement* pElem;
	TiXmlHandle hRoot( 0 );

	std::string session_type;
	std::string databaseID;
	std::string protocolID;

	const char *pName = NULL;

	pElem = hDoc.FirstChildElement().Element();

	// should always have a valid root but handle gracefully if it does
	if ( !pElem ) return false;

	session_type = pElem->Value();

	pName = pElem->Attribute("database_id");
	if (pName) databaseID = pName;
	
	pName = pElem->Attribute("protocol_id");
	if (pName) protocolID = pName;

	pElem->QueryIntAttribute( "vector_dimension", &vectorDimension );

	// save this for later
	hRoot = TiXmlHandle( pElem );

  pElem = hRoot.FirstChild( "test_vector" ).Element();

	return pElem;
}

bool CRecognizerProbPCA::WriteScores( std::string& targetVectorsFileName, std::string& testVectorsFileName, 
																		  std::string& scoresOutFileName, const char* pMaskFileName )
{
	int i;
	int targetVectorDimension = 0;
	int testVectorDimension = 0;

	TiXmlDocument targetDoc( targetVectorsFileName );
	if ( !targetDoc.LoadFile( ) ) return false;

	TiXmlHandle hTargetDoc( &targetDoc );

	TiXmlDocument testDoc( testVectorsFileName );
	if ( !testDoc.LoadFile( ) ) return false;

	TiXmlHandle hTestDoc( &testDoc );

	TiXmlElement* pElemTarget = GetTargetElement( hTargetDoc, targetVectorDimension );
	TiXmlElement* pElemTest = GetTestElement( hTestDoc, testVectorDimension );

	TiXmlElement* pFirstElemTarget = pElemTarget;
	TiXmlElement* pFirstElemTest = pElemTest;

	if ( targetVectorDimension != testVectorDimension )
  {
    std::cerr << "Dimension of target and test vectors is not equal." << std::endl;
    return false;
  }

	// New XML Score document
	TiXmlDocument doc;  
 	TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
	doc.LinkEndChild( decl );  
 
	TiXmlElement * root = new TiXmlElement( "scores_session" );  
	doc.LinkEndChild( root );  

	TiXmlComment * comment = new TiXmlComment();
	comment->SetValue( "Scores Data." );  
	root->LinkEndChild( comment );

	root->SetAttribute( "target_vectors_file", targetVectorsFileName );
	root->SetAttribute( "test_vectors_file", testVectorsFileName );

	std::string vectorIdx;
	const char *pNameTargetId = NULL;
	const char *pNameTestId = NULL;
	char stringTemp[MAX_PATH];

  IplImage* mskImg = cvLoadImage( pMaskFileName, 0);

	if ( !mskImg )
	{
		printf( "Can not read mask image file %s.\n", pMaskFileName );
		cvReleaseImage( &mskImg );
		return false;
	}

	int dimension = GetInputDimension( mskImg );

	CvMat* testVector = cvCreateMat( 1, dimension, CV_64FC1);
	CvMat* targetVector = cvCreateMat( 1, dimension, CV_64FC1);

	CvSize imgSize;
	CvSize maskSize = cvGetSize( mskImg );

	for( pElemTest; pElemTest; pElemTest=pElemTest->NextSiblingElement())
	{
		pNameTestId = pElemTest->Attribute("test_id");
		if ( !pNameTestId )
		{
			printf( "Can not read attribute <test_id> in %s file.\n", testVectorsFileName );
			return false;
		}

		// Read test vector
		IplImage* normalizedTestImg = cvLoadImage( pNameTestId, 0 );

		if ( !normalizedTestImg )
		{
			printf( "Can not read image file %s.\n", pNameTestId );
			cvReleaseImage( &normalizedTestImg );
			return false;
		}

		imgSize = cvGetSize( normalizedTestImg );
		if ( (imgSize.width != maskSize.width) || (imgSize.height != maskSize.height) )
		{
			printf( "Image size does not match with the mask image size.\n" );
			return false;
		}

		GetImageVector( normalizedTestImg, testVector, mskImg );

		// Loop over the target vectors for each test vector
		for( pElemTarget; pElemTarget; pElemTarget=pElemTarget->NextSiblingElement())
		{
			pNameTargetId = pElemTarget->Attribute("target_id");
			if ( !pNameTargetId )
			{
				printf( "Can not read attribute <target_id> in %s file.\n", targetVectorsFileName );
				return false;
			}

			// Read target vector
			IplImage* normalizedTargetImg = cvLoadImage( pNameTargetId, 0 );

			if ( !normalizedTargetImg )
			{
				printf( "Can not read image file %s.\n", pNameTargetId );
				cvReleaseImage( &normalizedTargetImg );
				return false;
			}

			imgSize = cvGetSize( normalizedTargetImg );
			if ( (imgSize.width != maskSize.width) || (imgSize.height != maskSize.height) )
			{
				printf( "Image size does not match with the mask image size.\n" );
				return false;
			}

			GetImageVector( normalizedTargetImg, targetVector, mskImg );

			// Compute similarity
			double similarity = MeasureSimilarity( testVector, targetVector );

			TiXmlElement* result = new TiXmlElement( "scoreresult" );  
			root->LinkEndChild( result );
			result->SetAttribute( "test_file_name", pNameTestId );
			result->SetAttribute( "target_file_name", pNameTargetId );
			result->SetDoubleAttribute( "score", similarity );


			cvReleaseImage( &normalizedTargetImg );
		}

		pElemTarget = pFirstElemTarget;

		cvReleaseImage( &normalizedTestImg );
	}

 	doc.SaveFile( scoresOutFileName );

	cvReleaseMat( &testVector );
	cvReleaseMat( &targetVector );
	cvReleaseImage( &mskImg );

	return true;
}

bool CRecognizerProbPCA::SaveModel(const std::string&	modelFileName)
{
  FILE* inStream = fopen(modelFileName.c_str(), "wb");
  if(!inStream)
    {
      std::cerr << "Can not open model saving file " << modelFileName
		<< " for writing." << std::endl;
      exit(-1);
    }

		if ( m_trainData.m_dataMatrix == NULL )
    {
      fprintf( stderr, "Training data is not available.\n" );
      fclose( inStream );
      return false;
    }

	if ( m_Phi == NULL )
	{
		fprintf( stderr, "Eigenvectors are not available.\n" );
		fclose( inStream );
		return false;
	}

	if ( m_eigVals == NULL )
	{
		fprintf( stderr, "Eigenvalues are not available.\n" );
    fclose( inStream );
		return false;
	}

	int i, j;

	// Write number of dimensions of the training samples
	fwrite( &m_N, sizeof(int), 1, inStream );

	// Write number of Eigen vectors
	fwrite( &m_M, sizeof(int), 1, inStream );

	// Write eigenvectors
	for (i=0; i < m_N; i++)
	{
		for (j=0; j < m_M; j++)
		{
			double value = CV_MAT_ELEM( *m_Phi, double, i, j );
			fwrite( &value, sizeof(double), 1, inStream );
		}
	}

	// Write eigenvalues
	for (j=0; j < m_M; j++)
	{
		double value = CV_MAT_ELEM( *m_eigVals, double, 0, j );
		fwrite( &value, sizeof(double), 1, inStream );
	}

	// Write average residual energy
	fwrite( &m_rho, sizeof(double), 1, inStream );

	fclose( inStream );
	return true;
}

bool CRecognizerProbPCA::LoadModel(const std::string&	modelFileName)
{
  FILE* inStream = fopen( modelFileName.c_str(), "rb" );
  if(!inStream)
    {
      std::cerr << "Can not open model saving file " << modelFileName <<
	" for reading." << std::endl;
      exit(-1);
    }

	if ( m_Phi != NULL )
	{
		cvReleaseMat( &m_Phi );
		m_Phi = NULL;
	}

	if ( m_eigVals != NULL )
	{
		cvReleaseMat( &m_eigVals );
		m_Phi = NULL;
	}

	if ( m_covIntraMat != NULL )
	{
		cvReleaseMat( &m_covIntraMat );
		m_covIntraMat = NULL;
	}

	if ( m_diffDataMat != NULL )
	{
		cvReleaseMat( &m_diffDataMat );
		m_diffDataMat = NULL;
	}

	int i, j;
	double value = 0;

	// Write number of dimensions of the training samples
	fread( &m_N, sizeof(int), 1, inStream );

	// Write number of Eigen vectors
	fread( &m_M, sizeof(int), 1, inStream );

	// Allocate memory
	m_Phi = cvCreateMat( m_N, m_M, CV_64FC1 );
	m_eigVals = cvCreateMat( 1, m_M, CV_64FC1 );

	// Read eigenvectors
	for (i=0; i < m_N; i++)
	{
		for (j=0; j < m_M; j++)
		{
			fread( &value, sizeof(double), 1, inStream );
			CV_MAT_ELEM( *m_Phi, double, i, j ) = value;
		}
	}

	// Read eigenvalues
	for (j=0; j < m_M; j++)
	{
		fread( &value, sizeof(double), 1, inStream );
		CV_MAT_ELEM( *m_eigVals, double, 0, j ) = value;
	}

	// Write average residual energy
	fread( &m_rho, sizeof(double), 1, inStream );

	fclose( inStream );
	return true;

}
