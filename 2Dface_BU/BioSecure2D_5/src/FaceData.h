/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if !defined(FACE_DATA_HEADER)
#define FACE_DATA_HEADER

#include "IV_Common.h"

/*!
  \defgroup FaceData Structure used for storing the face data information
*/

/*!
  \ingroup FaceData
	\brief Structure used for storing the face data information

	FaceData class is used for storing image data information. The object created with 
	this class is passed as a parameter for the recognition method initialization.
*/

class FaceData
{
public:
	int m_numSubjects;
	CvMat* m_dataMatrix;
	std::string m_sessionType;
	std::string m_databaseID;
	std::string m_protocolID;

	std::vector<std::string> m_actualId;
	std::vector<std::string> m_claimedId;
	std::vector<std::string> m_fileName;
	std::vector<std::string> m_result;

	IplImage* m_mskImg;

	//! \brief constructer for the FaceData class
	FaceData();

	//! \brief A void destructer for the class to release all the memory allocation done by the class
	~FaceData();

	/*! \brief returns dimension of the image vector by counting the valid pixels defined by maskImage

			\param maskImage pointer to the mask image which stores the mask information.
	*/
	int GetInputDimension( IplImage* maskImage );

	/*! \brief Load the images defined by the parameters settings file.

			\param pImagesDirName Directory of images to be read and normalized.
			\param pImagesFileType character array which contains the extension type of the images.
			\param pProtocolFileName character array which contains the protocol used in the training and testing.
			\param pMaskFileName character array which contains full path of the mask image.
	*/
	bool Load( const char* pImagesDirName, const char* pImagesFileType, const char* pProtocolFileName, const char* pMaskFileName );

};

#endif
