/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <time.h>
#include <float.h>
#include <math.h>
#include <limits.h>

#include <string>
#include <iostream>
#include <cstdlib>

#include <cv.h>
#include <highgui.h>

#include "IV_Common.h"
#include "FaceRecognizerPCA.h"
#include "FaceRecognizerProbPCA.h"

int main( int argc, char* argv[] )
{
	int i;
	std::string inputFile;
	std::string TrainingProtocolFileName;
	std::string outFile;

  if( argc == 1 )
  {
    printf( "Usage: %s\n  -params <IV_System_Parameters_File> -inTrain <Training_Protocol> -out  <Model_File_Name>", argv[0] );
		printf( "Program produces model data file using given input parameter by the -params switch.\n" );
		printf( "-params <IV_System_Parameters_File> : IV system parameters given by <IV_System_Parameters_File> in XML format.\n" );
		printf( "-inTrain <Training_Protocol> : Training protocol file in XML format.\n" );
		printf( "-out <Model_File_Name>: outputs model data file, <Model_File_Name>, in binary format.\n" );
    return 0;
  }

  for( i = 1; i < argc; i++ )
  {
    if( !strcmp( argv[i], "-params" ) )
    {
			inputFile = argv[++i];
    }
		else
		{
			if ( !strcmp( argv[i], "-inTrain" ) )
			{
				TrainingProtocolFileName = argv[++i];
			}
			else
			{
				if ( !strcmp( argv[i], "-out" ) )
				{
				outFile = argv[++i];
				}
			}
		}
	}

	IVPrmtrSettings settings;

	settings.load_save( inputFile.c_str(),TrainingProtocolFileName.c_str());

	FaceData trnData;

	trnData.Load( settings.imageFilesLocation.c_str(), 
								settings.imageFilesType.c_str(),
								settings.trainSessionProtocolFile.c_str(),
								settings.maskImageFile.c_str() );

	if ( settings.method == "Maximum_Likelihood_PCA" )
	{
		CRecognizerProbPCA probPCARec( trnData );

		if ( settings.pcaParams.dimensionReduction == "number_of_eigenvectors" )
		{
			probPCARec.Train( settings.pcaParams.numEigenvectors );
		}
		else
		{
			if ( settings.pcaParams.dimensionReduction == "energy" )
			{
				probPCARec.Train( settings.pcaParams.energyRatio );
			}
		}

    // Save model file  
		probPCARec.SaveModel( outFile );
	}
	else
	{
		if ( settings.method == "PCA" )
		{
			CRecognizerPCA pcaRec( trnData );

			if ( settings.pcaParams.dimensionReduction == "number_of_eigenvectors" )
			{
				pcaRec.Train( settings.pcaParams.numEigenvectors );
			}
			else
			{
				if ( settings.pcaParams.dimensionReduction == "energy" )
				{
					pcaRec.Train( settings.pcaParams.energyRatio );
				}
			}

			// Save model file  
			pcaRec.SaveModel( outFile );
		}
	}	

	return 0;
}

