/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if !defined(FACE_RECOGNIZER_PCA_HEADER)
#define FACE_RECOGNIZER_PCA_HEADER

#include "IV_Common.h"

/*!
  \defgroup RecognizerPCA PCA Based Recognition Class
*/

/*!
  \ingroup RecognizerPCA
	\brief Recognizer class using PCA based feature vectors with some	distance measures

	CRecognizerPCA class is constructed with FaceData class. It is
	trained by the Train method using the given trainData and save the
	trained PCA model with SaveModel method. To test the system it is
	constructed with second constructor with model file name, target data class, xml test
	output log file name.
*/

class CRecognizerPCA
{
public:

	//! \brief First constructer for the class by loading the train data
	CRecognizerPCA( FaceData& trainData );

	//! \brief First constructer for the class by loading the model
	CRecognizerPCA( std::string& modelFileName );

	//! \brief First constructer for the class by loading the model
	CRecognizerPCA( );

	//! \brief Second constructer for the class by loading model data file and target data to test the PCA model.
	CRecognizerPCA( std::string& modelFileName, FaceData& targetData );

	//! \brief A void destructer for the class to release all the memory allocation done by the class
	~CRecognizerPCA( );

	/*! \enum DistanceType
			\brief Distance type enumarators for choosing the distance measure usedin the testing
	*/
	enum DistanceType
	{
		//! \brief Distance type enumarator representing L1 norm distance measure between the PCA coefficients
		DISTANCE_L1,

		//! \brief Distance type enumarator representing L2 norm distance measure between the PCA coefficients
		DISTANCE_L2,

		//! \brief Distance type enumarator representing L1 norm distance measure between the whitened PCA coefficients with their corresponding standard deviations
		DISTANCE_WL1,

		//! \brief Distance type enumarator representing L2 norm distance measure between the whitened PCA coefficients with their corresponding standard deviations
		DISTANCE_WL2,

		//! \brief Distance type enumarator representing cosine angle distance measure between the whitened PCA coefficients with their corresponding standard deviations
		DISTANCE_WCOS
	};

	/*! \brief Returns the number of train samples
	*/
	inline int GetNumTrnSamples()
	{
		return m_trainData.m_dataMatrix->rows;
	}

	/*! \brief Returns the dimension size of train data samples
	*/
	inline int GetDimension()
	{
		return m_N;
	}

	/*! \brief Returns number of subjects of the train data set

			\param idx sample index value
	*/
	inline int GetNumTrainSubjects( )
	{
		return m_trainData.m_numSubjects;
	}

	/*! \brief Returns the subject ID of the train data sample for the given data index value

			\param idx sample index value
	*/
	inline std::string GetTrainSampleSubjectID( int idx )
	{
		return m_trainData.m_actualId[idx];
	}

	/*! \brief Returns the subject ID of the target data sample for the given data index value

			\param idx sample index value
	*/
	inline std::string GetTargetSampleSubjectID( int idx )
	{
		return m_targetData.m_actualId[idx];
	}

	/*! \brief Returns the train data reference
	*/
	inline FaceData& GetTrainData()
	{
		return m_trainData;
	}

	/*! \brief Returns the target data reference
	*/
	inline FaceData& GetTargetData()
	{
		return m_targetData;
	}

	/*! \brief Returns the train data PCA coefficients pointer
	*/
	inline CvMat* GetTargetPCACoeffs()
	{
		return m_trgtPCACfs;
	}

	/*! \brief Returns the train data file name string
	*/
	inline std::string GetTrainFileName( int trnIdx )
	{
		return m_trainData.m_fileName[trnIdx];
	}

	/*! \brief Returns the number of samples in the given data set for the given subject ID

			\param subjID subject ID
	*/
	inline int GetNumTrainSamples4Subject( std::string& subjID )
	{
		// Fill image set A
		int countSet = 0;

		if (!m_trainData.m_dataMatrix) return countSet;

		for ( int k=0; k < m_trainData.m_dataMatrix->rows; k++)
		{
			if ( m_trainData.m_actualId[k] == subjID )
			{
				countSet++;
			}
		}

		return countSet;
	}

	/*! \brief Returns the sample indices in the target data set for the given subject ID

			\param subjID idList reference to be filled up
			\param subjID subject ID
	*/
	inline void GetTargetIDList4Subject( std::vector<int>& idList, std::string& subjID )
	{
		// Fill image set A
		idList.clear();

		if( !m_targetData.m_dataMatrix )
		{
			std::cerr << "Target data matrix is not available." << std::endl;
			exit(0);
		}

		for ( int k=0; k < m_targetData.m_dataMatrix->rows; k++)
		{
			if ( m_targetData.m_actualId[k] == subjID )
			{
				idList.push_back( k );
			}
		}
	}

	/*! \brief Returns the number of samples in the given face data for the given subject ID

			\param subjID subject ID value
	*/
	inline int GetNumSamples4Subject( FaceData& faceData, std::string& subjID )
	{
		// Fill image set A
		int countSet = 0;

		if (!faceData.m_dataMatrix) return countSet;

		for ( int k=0; k < faceData.m_dataMatrix->rows; k++)
		{
			if ( faceData.m_actualId[k] == subjID )
			{
				countSet++;
			}
		}

		return countSet;
	}

	/*! \brief Training method used for obtaining training parameters using first m_M number of eigenvectors

			\param M Number of PCA vectors used for training
	*/
	bool Train( int M );

	/*! \brief Training method used for obtaining training parameters using first m_M number of eigenvectors
			corresponding to the given energyRatio.

			\param energyRatio Ratio of sum of the selected eigenvalues used for training to the whole eigenvalues sum.
	*/
	bool Train( double energyRatio );


	/*! \brief Computes similarity scores of a given test data.

			\param testData Test data reference.
			\param scores Similarity scores storage reference.
	*/
	bool ComputeSimilarityScores( FaceData& testData, std::vector<double>& scores, DistanceType distanceType );

	/*! \brief ROC values computation method used for obtaining
			the ROC curve value pairs for the given preloaded test data.
			Value pairs are written into the <testOutputFileName> in 
			text format.

			\param testData Test data reference.
			\param testOutFileName ROC value pairs file name.
			\param distanceType Distance type enumarator for similarity measure computation.
	*/
	bool ComputeROCValues4IV( FaceData& testData, std::string& testOutFileName, DistanceType distanceType );

	/*! \brief Writes the projected vectors for the images defined by
			the target protocol file. Since ML PCA method depends on the distribution of the image difference
			vectors the output is just the same as the image vectors defined by the protocol file.

			\param targetVectorsOutFileName String which contains the target vectors output in XML format.
	*/
	bool WriteTargetVectors( std::string& targetVectorsOutFileName );

	/*! \brief Writes the projected vectors for all images defined by
			the test protocol file.Since ML PCA method depends on the distribution of the image difference
			vectors the output is just the same as the image vectors defined by the protocol file.

			\param testData Face data structure which contains the information about the test data.
			\param testVectorsOutFileName String which contains the test vectors output in XML format.
	*/
	bool WriteTestVectors( FaceData& testData, std::string& testVectorsOutFileName );


	/*! \brief Gets the XML element pointer of the given target vectors file name..

			\param hDoc handle to target XML file.
			\param vectorDimension dimension of the vectors given in the <vectorsFileName>.
	*/
	TiXmlElement*  GetTargetElement( TiXmlHandle& hDoc, int& vectorDimension );

	/*! \brief Gets the XML element pointer of the given target vectors file name..

			\param hDoc handle to target XML file.
			\param vectorDimension dimension of the vectors given in the <vectorsFileName>.
	*/
	TiXmlElement*  GetTestElement( TiXmlHandle& hDoc, int& vectorDimension );

	/*! \brief Writes the similarity scores between projected test vectors and target vectors.

			\param targetVectorsFileName XML data file which contains the projected target vectors.
			\param testVectorsFileName XML data file which contains the projected test vectors.
	*/
	bool WriteScores( std::string& targetVectorsFileName, std::string& testVectorsFileName, DistanceType distanceType,
									  std::string& scoresOutFileName );

	/*! \brief Test method used for obtaining for the test result of the currently loaded PCA model.

			\param testData Test data reference.
			\param distanceType Distance type enumarator for similarity measure computation.
			\param testScoresOutFileName XML output log file name.
			\param testROCValuesFileName XML output log file name.
	*/
	bool Test( FaceData& testData, DistanceType distanceType,
						 std::string& testScoresOutFileName, 
						 std::string& testROCValuesFileName );

	/*! \brief Saves the trained PCA parameters into the input file.

			\param modelFileName Model file name.
	*/
	bool SaveModel( std::string&	modelFileName );

	/*! \brief Loads save PCA model parameters into the input file.

			\param modelFileName Model file name.
	*/
	bool LoadModel( std::string& modelFileName);

	/*! \brief Computes similarity values between two normalized image set of vectors and returns
			it.

			\param CvMat* structure pointer corresponding to image set A matrix
			\param CvMat* structure pointer corresponding to image set B matrix
			\param type Distance measurement type enumarator
	*/
  double MeasureSimilarity( CvMat* imgA, CvMat* imgB, DistanceType type );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients using the
	given distance measure enumarator.

	\param xTstRaw CvMat array pointer to the normalized test image vector with the array size (1 x m_N).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
	double Distance1( CvMat* xTstRaw, CvMat* xTrnPCACfs, DistanceType distanceType );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients using the
	given distance measure enumarator.

	\param xTstPCACfs CvMat array pointer to the test image PCA coefficients with vector with the array size (1 x m_M).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
	double Distance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs, DistanceType distanceType );

private:

	//! \brief Sets the number of PCA feature vectors
	int m_M;

	//! \brief Shows the dimension of the training and target image vectors
	int m_N;

	//! \brief Shows the number of train or target images in the FaceData structure
	int m_K;

	//! \brief Shows the residual error with m_M number of PCA feature vectors
	double m_rho;

	//! \brief Points to the CvMat array which holds eigenvectors in the column based order with array size (m_N x m_M)
	CvMat* m_Phi;

	//! \brief Points to the CvMat array which holds eigenvalues in the descending order with array size (1 x m_M)
	CvMat* m_eigVals;

	//! \brief Points to the CvMat array which holds mean vector of the training image vectors with array size (1 x m_N)
	CvMat* m_mu;

	//! \brief Points to the CvMat array which holds the PCA coefficients of the target images with array size (m_K x m_M)
	CvMat* m_trgtPCACfs;

	//! \brief Points to the CvMat array which holds the covariance matrix of the training images with array size (m_N x m_N) or (m_K x m_K)
	CvMat* m_covMat;

	//! \brief FaceData class which holds training data information
	FaceData m_trainData;

	//! \brief FaceData class which holds target data information
	FaceData m_targetData;

	/*! \brief Compute Covariance matrix of the training data matrix m_trnData->dataMatrix.
	*/
  bool ComputeCovMatrix( );

	/*! \brief Computes the number of eigenvectors for the given energy_ratio parameter and set its
	to the member m_M.

	\param eigVals Pointer to CvMat structure used for holding the whole eigenvalues with the array size (1 x m_N)
	\param energy_ratio Ratio of sum of the selected eigenvalues used for training to the whole eigenvalues sum.
	*/
	int ComputeM( CvMat* eigVals, double energy_ratio );

	/*! \brief Computes PCA projection matrix

	The PCA projection matrix contains m_M number of eigenvectors in the column based order
	corresponding to the first largest m_M eigenvalues of the covariance matrix m_covMat. The
	computed projection	matrix is loaded into m_Phi member with array size (m_N x m_M).

	\param M The number of first eigenvectors used in the training
	*/
	bool ComputeProjMat( int M );

	/*! \brief Computes PCA projection matrix

	The PCA projection matrix contains m_M number of eigenvectors in the column based order
	corresponding to the first largest m_M eigenvalues of the covariance matrix m_covMat. The
	computed projection	matrix is loaded into m_Phi member with array size (m_N x m_M).

	\param energyRatio Ratio of sum of the selected eigenvalues used for training to the whole eigenvalues sum.
	*/
	bool ComputeProjMat( double energyRatio );


	/*! \brief Computes Distances between the test image vector and train image PCA coefficients using the
	given L1 distance.

	\param xTstRaw CvMat array pointer to the normalized test image vector with the array size (1 x m_N).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
  double L1Distance1( CvMat* xTstRaw, CvMat* xTrnPCACfs );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients using the
	given L2 distance.

	\param xTstRaw CvMat array pointer to the normalized test image vector with the array size (1 x m_N).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
	double L2Distance1( CvMat* xTstRaw, CvMat* xTrnPCACfs );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients by whitening the PCA vectors
	using their corresponding standard deviations	and computes L1 distance measure between these whitened PCA coefficients.

	\param xTstRaw CvMat array pointer to the normalized test image vector with the array size (1 x m_N).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
	double WL1Distance1( CvMat* xTstRaw, CvMat* xTrnPCACfs );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients by whitening the PCA vectors
	using their corresponding standard deviations	and computes L2 distance measure between these whitened PCA coefficients.

	\param xTstRaw CvMat array pointer to the normalized test image vector with the array size (1 x m_N).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
	double WL2Distance1( CvMat* xTstRaw, CvMat* xTrnPCACfs );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients by whitening the PCA vectors
	using their corresponding standard deviations	and computes thecosine angle distance measure between these whitened PCA
	coefficient vectors.

	\param xTstRaw CvMat array pointer to the normalized test image vector with the array size (1 x m_N).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
	double WCOSDistance1( CvMat* xTstRaw, CvMat* xTrnPCACfs );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients using the
	given L1 distance.

	\param xTstPCACfs CvMat array pointer to the test image PCA coefficients with vector with the array size (1 x m_M).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
  double L1Distance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients using the
	given L2 distance.

	\param xTstPCACfs CvMat array pointer to the test image PCA coefficients with vector with the array size (1 x m_M).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
	double L2Distance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients by whitening the PCA vectors
	using their corresponding standard deviations	and computes L1 distance measure between these whitened PCA coefficients.

	\param xTstPCACfs CvMat array pointer to the test image PCA coefficients with vector with the array size (1 x m_M).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
	double WL1Distance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients by whitening the PCA vectors
	using their corresponding standard deviations	and computes L2 distance measure between these whitened PCA coefficients.

	\param xTstPCACfs CvMat array pointer to the test image PCA coefficients with vector with the array size (1 x m_M).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
	double WL2Distance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs );

	/*! \brief Computes Distances between the test image vector and train image PCA coefficients by whitening the PCA vectors
	using their corresponding standard deviations	and computes thecosine angle distance measure between these whitened PCA
	coefficient vectors.

	\param xTstPCACfs CvMat array pointer to the test image PCA coefficients with vector with the array size (1 x m_M).
	\param xTrnPCACfs CvMat array pointer to the train image PCA coefficients with vector with the array size (1 x m_M).
	*/
	double WCOSDistance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs );
};

#endif
