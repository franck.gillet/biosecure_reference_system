/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "FaceRecognizerPCA.h"

CRecognizerPCA::CRecognizerPCA( FaceData& trainData )
:m_trainData(trainData)
{
	m_M = 0;
	m_N = m_trainData.m_dataMatrix->cols;
	m_K = m_trainData.m_dataMatrix->rows;
	m_rho = 0;

	m_Phi = NULL;
	m_eigVals = NULL;
	m_mu = NULL;
	m_trgtPCACfs = NULL;
	m_covMat = NULL;
}

CRecognizerPCA::CRecognizerPCA( std::string& modelFileName )
{
	m_M = 0;
	m_N = 0;
	m_K = 0;
	m_rho = 0;

	m_Phi = NULL;
	m_eigVals = NULL;
	m_mu = NULL;
	m_trgtPCACfs = NULL;
	m_covMat = NULL;

	LoadModel( modelFileName );
}

CRecognizerPCA::CRecognizerPCA( )
{
	m_M = 0;
	m_N = 0;
	m_K = 0;
	m_rho = 0;

	m_Phi = NULL;
	m_eigVals = NULL;
	m_mu = NULL;
	m_trgtPCACfs = NULL;
	m_covMat = NULL;

}

CRecognizerPCA::CRecognizerPCA( std::string& modelFileName, FaceData& targetData )
:m_targetData(targetData)
{
	m_M = 0;
	m_N = m_targetData.m_dataMatrix->cols;
	m_K = m_targetData.m_dataMatrix->rows;
	m_rho = 0;

	m_Phi = NULL;
	m_eigVals = NULL;
	m_mu = NULL;
	m_trgtPCACfs = NULL;
	m_covMat = NULL;

	LoadModel( modelFileName );
}

CRecognizerPCA::~CRecognizerPCA( )
{
	if ( m_eigVals != NULL )
		cvReleaseMat( &m_eigVals );
	if ( m_Phi != NULL )
		cvReleaseMat( &m_Phi );
	if ( m_mu != NULL )
		cvReleaseMat( &m_mu );
	if ( m_trgtPCACfs != NULL )
		cvReleaseMat( &m_trgtPCACfs );
	if ( m_covMat != NULL )
		cvReleaseMat( &m_covMat );
}

bool CRecognizerPCA::ComputeCovMatrix( )
{
	if ( m_trainData.m_dataMatrix == NULL )
	{
		fprintf( stderr, "Train data is not loaded.\n" );
		exit(0);
	}

	if ( m_mu != NULL )
	{
		cvReleaseMat( &m_mu );
		m_mu = NULL;
	}

	if ( m_covMat != NULL )
	{
		cvReleaseMat( &m_covMat );
		m_covMat = NULL;
	}

	int i, j;
	CvMat trnVec, tmpVec;

	m_mu = cvCreateMat( 1, m_N, CV_64FC1 );
	CvMat* tmpMat = cvCreateMat( m_K, m_N, CV_64FC1 );

	cvSetZero( m_mu );
	for (i=0; i < m_K; i++)
	{
		cvGetRow( m_trainData.m_dataMatrix, &trnVec, i );
		cvAdd( m_mu, &trnVec, m_mu );
	}
	cvScale( m_mu, m_mu, 1./(double) m_K );

	for (i=0; i < m_K; i++)
	{
		cvGetRow( m_trainData.m_dataMatrix, &trnVec, i );
		cvGetRow( tmpMat, &tmpVec, i );
		cvSub( &trnVec, m_mu, &tmpVec );
	}

	// Calculate covariance matrix
	if ( m_N > m_K )
	{
    m_covMat = cvCreateMat( m_K, m_K, CV_64FC1 );
		cvMulTransposed( tmpMat, m_covMat, 0, NULL );
	}
	else
	{
		m_covMat = cvCreateMat( m_N, m_N, CV_64FC1 );
		cvMulTransposed( tmpMat, m_covMat, 1, NULL );
	}
	cvReleaseMat( &tmpMat );

	// Scale covariance matrix of difference image vectors
	for (i=0; i < m_covMat->rows; i++)
	{
		for (j=0; j < m_covMat->cols; j++)
		{
			double matVal = CV_MAT_ELEM( *m_covMat, double, i, j );
			CV_MAT_ELEM( *m_covMat, double, i, j ) = matVal/(double) m_K;
		}
	}

	return true;
}

int CRecognizerPCA::ComputeM( CvMat* eigVals, double energy_ratio )
{
	int i;
	double csum = 0, avg_enrgy = 0;

	for (i=0; i < eigVals->rows; i++)
		avg_enrgy += CV_MAT_ELEM( *eigVals, double, i, 0 );

	csum = 0;
	i = 0;
	double energy_thr = energy_ratio*avg_enrgy;
	while ( csum < energy_thr )
	{
		csum += CV_MAT_ELEM( *eigVals, double, i, 0 );
		i++;
	}

	return i;
}

bool CRecognizerPCA::ComputeProjMat( int _M )
{
	m_M = _M;

	if ( m_covMat == NULL )
		return false;

	if ( m_Phi != NULL )
	{
		cvReleaseMat( &m_Phi );
		m_Phi = NULL;
	}

	if ( m_trgtPCACfs != NULL )
	{
		cvReleaseMat( &m_trgtPCACfs );
		m_trgtPCACfs = NULL;
	}

	int i;

	CvRect rect;
	CvMat temp_mat, temp_vec;

	CvMat* I = cvCloneMat( m_covMat );
	CvMat* W = cvCreateMat(I->cols,1,CV_64FC1);
	CvMat* U = cvCreateMat(I->cols,I->cols,CV_64FC1);
	CvMat* V = cvCreateMat(I->cols,I->cols,CV_64FC1);

	if ( m_N > m_covMat->cols )
	{
		cvSVD( I, W, U, V, CV_SVD_MODIFY_A | CV_SVD_U_T );

		rect.x = rect.y = 0;
		rect.height = m_M; rect.width = U->cols;
		cvGetSubRect( U, &temp_mat, rect );

		CvMat* PhiL = cvCloneMat( &temp_mat );
		CvMat* PhiT = cvCreateMat( m_M, m_N, CV_64FC1);

		m_Phi = cvCreateMat( m_N, m_M, CV_64FC1);

		CvMat trnVec, tmpVec;
		CvMat* tmpMat = cvCreateMat( m_K, m_N, CV_64FC1 );

		for (i=0; i < m_K; i++)
		{
			cvGetRow( m_trainData.m_dataMatrix, &trnVec, i );
			cvGetRow( tmpMat, &tmpVec, i );
			cvSub( &trnVec, m_mu, &tmpVec );
		}
		cvMatMulAdd( PhiL, tmpMat, 0, PhiT );

		for (i=0; i < PhiT->rows; i++)
		{
			cvGetRow( PhiT, &temp_vec, i );
			MakeUnitNorm( &temp_vec );
		}
		cvTranspose( PhiT, m_Phi );

		// Compute train set PCA coefficients
		m_trgtPCACfs = cvCreateMat( m_K, m_M, CV_64FC1 );
		cvMatMulAdd( tmpMat, m_Phi, 0, m_trgtPCACfs );
    cvReleaseMat( &tmpMat );

		cvReleaseMat(&PhiL);
		cvReleaseMat(&PhiT);
	}
	else
	{
		cvSVD( I, W, U, V, CV_SVD_MODIFY_A );

		rect.x = rect.y = 0;
		rect.height = V->rows; rect.width = m_M;
		cvGetSubRect( V, &temp_mat, rect );

		m_Phi = cvCloneMat(&temp_mat);

		CvMat trnVec, tmpVec;
		CvMat* tmpMat = cvCreateMat( m_K, m_N, CV_64FC1 );

		for (i=0; i < m_K; i++)
		{
			cvGetRow( m_trainData.m_dataMatrix, &trnVec, i );
			cvGetRow( tmpMat, &tmpVec, i );
			cvSub( &trnVec, m_mu, &tmpVec );
		}

		// Compute train set PCA coefficients
		m_trgtPCACfs = cvCreateMat( m_K, m_M, CV_64FC1 );
		cvMatMulAdd( tmpMat, m_Phi, 0, m_trgtPCACfs );
    cvReleaseMat( &tmpMat );
	}

	cvReleaseMat( &I );
	cvReleaseMat( &U );
	cvReleaseMat( &V );

	// Compute Rho
	m_rho = 0.;
	if ( W->rows == m_M )
		m_rho = 0.;
	else
	{
		for (i=m_M; i < W->rows; i++)
			m_rho += CV_MAT_ELEM( *W, double, i, 0 );
		m_rho /= double (W->rows-m_M);
	}

	if ( m_eigVals != NULL )
		cvReleaseMat( &m_eigVals );

	m_eigVals = cvCreateMat( 1, m_M, CV_64FC1 );
	for (i=0; i < m_M; i++)
	{
		double eig_val = CV_MAT_ELEM( *W, double, i, 0 );
		CV_MAT_ELEM( *m_eigVals, double, 0, i ) = eig_val;
	}

	cvReleaseMat( &W );

	return true;
}


bool CRecognizerPCA::ComputeProjMat( double  energyRatio )
{
	if ( (m_covMat == NULL) || (m_mu == NULL) )
		return false;

	if ( m_Phi != NULL )
	{
		cvReleaseMat( &m_Phi );
		m_Phi = NULL;
	}

	if ( m_trgtPCACfs != NULL )
	{
		cvReleaseMat( &m_trgtPCACfs );
		m_trgtPCACfs = NULL;
	}

	int i;

	CvRect rect;
	CvMat temp_mat, temp_vec;

	CvMat* I = cvCloneMat( m_covMat );
	CvMat* W = cvCreateMat(I->cols,1,CV_64FC1);
	CvMat* U = cvCreateMat(I->cols,I->cols,CV_64FC1);
	CvMat* V = cvCreateMat(I->cols,I->cols,CV_64FC1);

	if ( m_N > m_covMat->cols )
	{
		cvSVD( I, W, U, V, CV_SVD_MODIFY_A | CV_SVD_U_T );

		m_M = ComputeM(W, energyRatio);

		rect.x = rect.y = 0;
		rect.height = m_M; rect.width = U->cols;
		cvGetSubRect( U, &temp_mat, rect );

		CvMat* PhiL = cvCloneMat( &temp_mat );
		CvMat* PhiT = cvCreateMat( m_M, m_N, CV_64FC1);

		m_Phi = cvCreateMat( m_N, m_M, CV_64FC1);

		CvMat trnVec, tmpVec;
		CvMat* tmpMat = cvCreateMat( m_K, m_N, CV_64FC1 );

		for (i=0; i < m_K; i++)
		{
			cvGetRow( m_trainData.m_dataMatrix, &trnVec, i );
			cvGetRow( tmpMat, &tmpVec, i );
			cvSub( &trnVec, m_mu, &tmpVec );
		}
		cvMatMulAdd( PhiL, tmpMat, 0, PhiT );

		for (i=0; i < PhiT->rows; i++)
		{
			cvGetRow( PhiT, &temp_vec, i );
			MakeUnitNorm( &temp_vec );
		}
		cvTranspose( PhiT, m_Phi );

		// Compute train set PCA coefficients
		m_trgtPCACfs = cvCreateMat( m_K, m_M, CV_64FC1 );
		cvMatMulAdd( tmpMat, m_Phi, 0, m_trgtPCACfs );
    cvReleaseMat( &tmpMat );

		cvReleaseMat(&PhiL);
		cvReleaseMat(&PhiT);
	}
	else
	{
		cvSVD( I, W, U, V, CV_SVD_MODIFY_A );

		m_M = ComputeM(W, energyRatio);

		rect.x = rect.y = 0;
		rect.height = V->rows; rect.width = m_M;
		cvGetSubRect( V, &temp_mat, rect );

		m_Phi = cvCloneMat(&temp_mat);

		CvMat trnVec, tmpVec;
		CvMat* tmpMat = cvCreateMat( m_K, m_N, CV_64FC1 );

		for (i=0; i < m_K; i++)
		{
			cvGetRow( m_trainData.m_dataMatrix, &trnVec, i );
			cvGetRow( tmpMat, &tmpVec, i );
			cvSub( &trnVec, m_mu, &tmpVec );
		}

		// Compute train set PCA coefficients
		m_trgtPCACfs = cvCreateMat( m_K, m_M, CV_64FC1 );
		cvMatMulAdd( tmpMat, m_Phi, 0, m_trgtPCACfs );
    cvReleaseMat( &tmpMat );
	}

	cvReleaseMat( &I );
	cvReleaseMat( &U );
	cvReleaseMat( &V );

	// Compute Rho
	m_rho = 0.;
	if ( W->rows == m_M )
		m_rho = 0.;
	else
	{
		for (i=m_M; i < W->rows; i++)
			m_rho += CV_MAT_ELEM( *W, double, i, 0 );
		m_rho /= double (W->rows-m_M);
	}

	if ( m_eigVals != NULL )
		cvReleaseMat( &m_eigVals );

	m_eigVals = cvCreateMat( 1, m_M, CV_64FC1 );
	for (i=0; i < m_M; i++)
	{
		double eig_val = CV_MAT_ELEM( *W, double, i, 0 );
		CV_MAT_ELEM( *m_eigVals, double, 0, i ) = eig_val;
	}

	cvReleaseMat( &W );

	return true;
}

double CRecognizerPCA::L1Distance1( CvMat* xTstRaw, CvMat* xTrnPCACfs )
{
	int i;
	double dist = 0.;
	CvMat* y;
	CvMat *xtilta;

	xtilta = cvCloneMat( xTstRaw );
	cvSub( xtilta, m_mu, xtilta );

	y = cvCreateMat( 1, m_M, CV_64FC1 );
	cvMatMulAdd( xtilta, m_Phi, 0, y );

	for (i=0; i < m_M; i++)
		dist += fabs(y->data.db[i] - xTrnPCACfs->data.db[i]);

	cvReleaseMat(&y);
	cvReleaseMat(&xtilta);

	return dist;
}

double CRecognizerPCA::L2Distance1( CvMat* xTstRaw, CvMat* xTrnPCACfs )
{
	int i;
	double diff = 0.;
	double dist = 0.;
	CvMat* y;
	CvMat *xtilta;

	xtilta = cvCloneMat( xTstRaw );
	cvSub( xtilta, m_mu, xtilta );

	y = cvCreateMat( 1, m_M, CV_64FC1 );
	cvMatMulAdd( xtilta, m_Phi, 0, y );

	for (i=0; i < m_M; i++)
	{
		diff = y->data.db[i] - xTrnPCACfs->data.db[i];
		dist += diff*diff;
	}

	cvReleaseMat(&y);
	cvReleaseMat(&xtilta);

	return sqrt( dist );
}

double CRecognizerPCA::WL1Distance1( CvMat* xTstRaw, CvMat* xTrnPCACfs )
{
	int i;
	double stdDev = 0.;
	double dist = 0.;
	CvMat* y;
	CvMat *xtilta;

	xtilta = cvCloneMat( xTstRaw );
	cvSub( xtilta, m_mu, xtilta );

	y = cvCreateMat( 1, m_M, CV_64FC1 );
	cvMatMulAdd( xtilta, m_Phi, 0, y );

	for (i=0; i < m_M; i++)
	{
		stdDev = sqrt( m_eigVals->data.db[i] );
		dist += fabs( (y->data.db[i] - xTrnPCACfs->data.db[i])/ stdDev );
	}

	cvReleaseMat(&y);
	cvReleaseMat(&xtilta);

	return dist;
}

double CRecognizerPCA::WL2Distance1( CvMat* xTstRaw, CvMat* xTrnPCACfs )
{
	int i;
	double stdDev = 0.;
	double diff = 0.;
	double dist = 0.;
	CvMat* y;
	CvMat *xtilta;

	xtilta = cvCloneMat( xTstRaw );
	cvSub( xtilta, m_mu, xtilta );

	y = cvCreateMat( 1, m_M, CV_64FC1 );
	cvMatMulAdd( xtilta, m_Phi, 0, y );

	for (i=0; i < m_M; i++)
	{
		stdDev = sqrt( m_eigVals->data.db[i] );
		diff = (y->data.db[i] - xTrnPCACfs->data.db[i]) / stdDev;
		dist += diff*diff;
	}

	cvReleaseMat(&y);
	cvReleaseMat(&xtilta);

	return sqrt( dist );
}

double CRecognizerPCA::WCOSDistance1( CvMat* xTstRaw, CvMat* xTrnPCACfs )
{
	int i;
	double stdDev = 0.;
	double dist = 0.;

	CvMat* yTrn = cvCreateMat( 1, m_M, CV_64FC1 );
	CvMat* yTst = cvCreateMat( 1, m_M, CV_64FC1 );

	CvMat* xtilta = cvCloneMat( xTstRaw );
	cvSub( xtilta, m_mu, xtilta );
	cvMatMulAdd( xtilta, m_Phi, 0, yTst );

	for (i=0; i < m_M; i++)
	{
		stdDev = sqrt( m_eigVals->data.db[i] );
		yTrn->data.db[i] = xTrnPCACfs->data.db[i]/stdDev;
	}

	double normYTrn = sqrt( cvDotProduct( yTrn, yTrn ) );
	double normYTst = sqrt( cvDotProduct( yTst, yTst ) );
	double normYTstYTrn = cvDotProduct( yTst, yTrn );

	dist = normYTstYTrn/(normYTst*normYTrn);

	cvReleaseMat(&yTst);
	cvReleaseMat(&yTrn);
	cvReleaseMat(&xtilta);

	return (-dist);
}

double CRecognizerPCA::Distance1( CvMat* xTstRaw, CvMat* xTrgtPCACfs, DistanceType distanceType )
{
	double distance = 0.;

	switch ( distanceType )
	{
	case DISTANCE_L1:
		distance = L1Distance1( xTstRaw, xTrgtPCACfs );
		break;
	case DISTANCE_L2:
		distance = L1Distance1( xTstRaw, xTrgtPCACfs );
		break;
	case DISTANCE_WL1:
		distance = WL1Distance1( xTstRaw, xTrgtPCACfs );
		break;
	case DISTANCE_WL2:
		distance = WL2Distance1( xTstRaw, xTrgtPCACfs );
		break;
	case DISTANCE_WCOS:
		distance = WCOSDistance1( xTstRaw, xTrgtPCACfs );
		break;
	default:
		distance = WL2Distance1( xTstRaw, xTrgtPCACfs );
		break;
	}

	return distance;
}

double CRecognizerPCA::L1Distance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs )
{
	int i;
	double dist = 0.;
	int M = xTstPCACfs->cols;

	for (i=0; i < M; i++)
		dist += fabs(xTstPCACfs->data.db[i] - xTrnPCACfs->data.db[i]);

	return dist;
}

double CRecognizerPCA::L2Distance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs )
{
	int i;
	double diff = 0.;
	double dist = 0.;
	int M = xTstPCACfs->cols;

	for (i=0; i < M; i++)
	{
		diff = xTstPCACfs->data.db[i] - xTrnPCACfs->data.db[i];
		dist += diff*diff;
	}

	return sqrt( dist );
}

double CRecognizerPCA::WL1Distance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs )
{
	int i;
	double stdDev = 0.;
	double dist = 0.;
	int M = xTstPCACfs->cols;

	for (i=0; i < M; i++)
	{
		stdDev = sqrt( m_eigVals->data.db[i] );
		dist += fabs( (xTstPCACfs->data.db[i] - xTrnPCACfs->data.db[i])/ stdDev );
	}

	return dist;
}

double CRecognizerPCA::WL2Distance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs )
{
	int i;
	double stdDev = 0.;
	double diff = 0.;
	double dist = 0.;
	int M = xTstPCACfs->cols;

	for (i=0; i < M; i++)
	{
		stdDev = sqrt( m_eigVals->data.db[i] );
		diff = (xTstPCACfs->data.db[i] - xTrnPCACfs->data.db[i]) / stdDev;
		dist += diff*diff;
	}

	return sqrt( dist );
}

double CRecognizerPCA::WCOSDistance2( CvMat* xTstPCACfs, CvMat* xTrnPCACfs )
{
	int i;
	double stdDev = 0.;
	double dist = 0.;
	int M = xTstPCACfs->cols;

	CvMat* yTrn = cvCreateMat( 1, m_M, CV_64FC1 );

	for (i=0; i < M; i++)
	{
		stdDev = sqrt( m_eigVals->data.db[i] );
		yTrn->data.db[i] = xTrnPCACfs->data.db[i]/stdDev;
	}

	double normYTrn = sqrt( cvDotProduct( yTrn, yTrn ) );
	double normYTst = sqrt( cvDotProduct( xTstPCACfs, xTstPCACfs ) );
	double normYTstYTrn = cvDotProduct( xTstPCACfs, yTrn );

	dist = normYTstYTrn/(normYTst*normYTrn);

	cvReleaseMat(&yTrn);

	return (-dist);
}

double CRecognizerPCA::Distance2( CvMat* xTstPCACfs, CvMat* xTrgtPCACfs, DistanceType distanceType )
{
	double distance = 0.;

	switch ( distanceType )
	{
	case DISTANCE_L1:
		distance = L1Distance2( xTstPCACfs, xTrgtPCACfs );
		break;
	case DISTANCE_L2:
		distance = L1Distance2( xTstPCACfs, xTrgtPCACfs );
		break;
	case DISTANCE_WL1:
		distance = WL1Distance2( xTstPCACfs, xTrgtPCACfs );
		break;
	case DISTANCE_WL2:
		distance = WL2Distance2( xTstPCACfs, xTrgtPCACfs );
		break;
	case DISTANCE_WCOS:
		distance = WCOSDistance2( xTstPCACfs, xTrgtPCACfs );
		break;
	default:
		distance = WL2Distance2( xTstPCACfs, xTrgtPCACfs );
		break;
	}

	return distance;
}

bool CRecognizerPCA::Train( int m_M )
{
	bool check = true;

	if ( m_trainData.m_dataMatrix == NULL )
	{
		fprintf( stderr, "Train data is empty...!\n" );
		exit(0);
	}

	// Set parameters
	m_N = m_trainData.m_dataMatrix->cols;
	m_K = m_trainData.m_dataMatrix->rows;

	check = ComputeCovMatrix();

	if ( !check )
		return false;

	check = ComputeProjMat( m_M );

	return check;
}

bool CRecognizerPCA::Train( double energyRatio )
{
	bool check = true;

	if ( m_trainData.m_dataMatrix == NULL )
	{
		fprintf( stderr, "Train data is empty...!\n" );
		exit(0);
	}

	// Set parameters
	m_N = m_trainData.m_dataMatrix->cols;
	m_K = m_trainData.m_dataMatrix->rows;

	check = ComputeCovMatrix();

	if ( !check )
		return false;

	check = ComputeProjMat( energyRatio );

	return check;
}

static int CmpFunc( const void* _a, const void* _b, void* )
{
	double a = *((double*)_a);
  double b = *((double*)_b);
	return ((a < b) ? -1 : ((a > b) ? 1:0));
}

double CRecognizerPCA::MeasureSimilarity( CvMat* imgA, CvMat* imgB, DistanceType type )
{
	int i, j, idx;
	double distance;
	CvMat rowVecA, rowVecB;
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSeq* distanceVals = cvCreateSeq( CV_SEQ_ELTYPE_GENERIC, sizeof(CvSeq), sizeof(double), storage );

	for (i=0; i < imgA->rows; i++)
	{
		cvGetRow( imgA, &rowVecA, i );
		for (j=0; j < imgB->rows; j++)
		{
      cvGetRow( imgB, &rowVecB, i );
      distance = Distance2( &rowVecA, &rowVecB, type );
			cvSeqPush( distanceVals, &distance );
		}
	}
	cvSeqSort( distanceVals, CmpFunc, NULL );

	// Return minimum value of distance values
	idx = 0;

	// Return median value of distance values
	//idx = cvRound( (double) distanceVals->total/2 );

	distance = *((double*)cvGetSeqElem( distanceVals, idx ));

	cvReleaseMemStorage( &storage );

	return distance;
}

bool CRecognizerPCA::ComputeSimilarityScores( FaceData& testData, std::vector<double>& scores, DistanceType distanceType )
{
	int i, j;
  CvMat rowVec, rowVecTrgt;
  CvMat* imgTest = cvCreateMat( 1, m_N, CV_64FC1 );
  CvMat* imgTrgt = NULL;
	std::vector<int> idList;

  FaceData& targetData = GetTargetData();

	if( !m_trgtPCACfs )
  {
    std::cerr << "Target PCA coefficients matrix is not available for testing." << std::endl;
    exit(0);
  }

	for (i=0; i < testData.m_dataMatrix->rows; i++)
	{
		// Load test image vector
		cvGetRow( testData.m_dataMatrix, &rowVec, i );
		cvCopy( &rowVec, imgTest );

		// Get PCA coefficients of example ID image vectors
		GetTargetIDList4Subject( idList, testData.m_claimedId[i] );
		int numClaimedIdExamples = (int) idList.size();
		if( numClaimedIdExamples == 0 )
		{
			std::cerr << "There is no example for the claimed ID " << testData.m_claimedId[i] << "." << std::endl;
			exit(0);
		}

		imgTrgt = cvCreateMat( numClaimedIdExamples, m_M, CV_64FC1 );

		for ( j=0; j < imgTrgt->rows; j++)
    {
	    cvGetRow( m_trgtPCACfs, &rowVec, idList[j] );
	    cvGetRow( imgTrgt, &rowVecTrgt, j );
	    cvCopy( &rowVec, &rowVecTrgt );
    }

	  // Now measure similarity between two sets of images
	  double similarity = MeasureSimilarity( imgTest, imgTrgt, distanceType );

		scores.push_back( similarity );
	}

	return true;
}

bool CRecognizerPCA::ComputeROCValues4IV( FaceData& testData, std::string& rocValuesOutFileName, DistanceType distanceType )
{
	std::vector<double> scores;

	ComputeSimilarityScores( testData, scores, distanceType );

	stable_sort( scores.begin(), scores.end(), IsLessThan );

	std::vector<double>::iterator scoreIterator;

	for ( scoreIterator = scores.begin(); scoreIterator != scores.end(); scoreIterator++)
	{
		double scoreValue = *scoreIterator;
	}

	return true;
}

bool CRecognizerPCA::Test( FaceData& testData, DistanceType distanceType,
													 std::string& testScoresOutFileName, 
													 std::string& testROCValuesFileName )
{
	int i, j, n;
	int numAttacks = 0;
	int numTrueAccess = 0;
  CvMat rowVec, rowVecTrgt;
  CvMat* imgTest = cvCreateMat( 1, m_N, CV_64FC1 );
  CvMat* imgTrgt = NULL;
	std::vector<int> idList;
	std::vector<double> scores;
	std::string exampleIds;
	std::string separator(":");

  FaceData& targetData = GetTargetData();

	if( !m_trgtPCACfs )
  {
    std::cerr << "Target PCA coefficients matrix is not available for testing." << std::endl;
    exit(0);
  }

	TiXmlDocument doc;  
 	TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
	doc.LinkEndChild( decl );  
 
	TiXmlElement * root = new TiXmlElement( "match_data" );  
	doc.LinkEndChild( root );  

	TiXmlComment * comment = new TiXmlComment();
	comment->SetValue( "Test Scores for BioSecure IV System." );  
	root->LinkEndChild( comment );  

	scores.clear();

	for (i=0; i < testData.m_dataMatrix->rows; i++)
	{
		// Load test image vector
		cvGetRow( testData.m_dataMatrix, &rowVec, i );
		cvCopy( &rowVec, imgTest );

		// Get PCA coefficients of example ID image vectors
		GetTargetIDList4Subject( idList, testData.m_claimedId[i] );
		int numClaimedIdExamples = (int) idList.size();
		if( numClaimedIdExamples == 0 )
		{
			std::cout << "There is no example for the claimed ID " << testData.m_claimedId[i] << "." << std::endl;
			doc.SaveFile( testScoresOutFileName );
			exit(0);
		}

		exampleIds.clear();
		for ( j=0; j < numClaimedIdExamples-1; j++)
    {
			exampleIds += (targetData.m_fileName[idList[j]] + separator);
    }
		exampleIds += targetData.m_fileName[idList[j]];

		imgTrgt = cvCreateMat( numClaimedIdExamples, m_M, CV_64FC1 );

		for ( j=0; j < imgTrgt->rows; j++)
    {
	    cvGetRow( m_trgtPCACfs, &rowVec, idList[j] );
	    cvGetRow( imgTrgt, &rowVecTrgt, j );
	    cvCopy( &rowVec, &rowVecTrgt );
    }

	  // Now measure similarity between two sets of images
	  double similarity = MeasureSimilarity( imgTest, imgTrgt, distanceType );

		// Save score values
		scores.push_back( similarity );

		TiXmlElement* claimResult = new TiXmlElement( "claimresult" );  
		root->LinkEndChild( claimResult );
		claimResult->SetAttribute( "real_id", testData.m_actualId[i] );
		claimResult->SetAttribute( "claimed_id", testData.m_claimedId[i] );
		claimResult->SetAttribute( "access_file_name", testData.m_fileName[i] );
		claimResult->SetDoubleAttribute( "score", similarity );
		claimResult->SetAttribute( "claimed_id_examples", exampleIds );

    cvReleaseMat( &imgTrgt );

		if ( testData.m_claimedId[i] != testData.m_actualId[i] )
			numAttacks++;
		else
			numTrueAccess++;

		double percentage = (100.0 * i) / (double) testData.m_dataMatrix->rows;

		fprintf( stderr, "WRITING LOG VALUES OF TEST SAMPLES %3d%%\r", (int) percentage );
		fflush( stderr );
	}

	doc.SaveFile( testScoresOutFileName );

	// Now compute ROC pair values
	stable_sort( scores.begin(), scores.end(), IsLessThan );

	std::vector<double>::iterator scoreIterator;

	double scoreMinVal = 0;
	double scoreMaxVal = 0;
	double scoreVal = 0;
	for ( scoreIterator = scores.begin(); scoreIterator != scores.end(); scoreIterator++)
	{
		scoreVal = *scoreIterator;

		if ( (scoreVal < scoreMinVal) || (scoreIterator == scores.begin()) )
			scoreMinVal = scoreVal;

		if ( (scoreVal > scoreMaxVal) || (scoreIterator == scores.begin()) )
			scoreMaxVal = scoreVal;
	}

	int numIntervals = 256;
	double delta = (scoreMaxVal-scoreMinVal)/(double) numIntervals;

	double falseAlarmRate = 0;
	double falseRejectRate = 0;
	double thresholdVal = 0;
	double wer = 0;
	double minWer = 0;
	double r = 1;
	double minWerThreshold, minWerFRR, minWerFAR;

	std::vector<double> farStrg;
	std::vector<double> frrStrg;

	farStrg.clear();
	frrStrg.clear();

  FILE* inStream = fopen(testROCValuesFileName.c_str(), "wt");
	if(!inStream)
	{
		std::cerr << "Can not open ROC values saving file " << testROCValuesFileName
	<< " for writing." << std::endl;
		exit(-1);
	}

	for ( n=0; n < numIntervals; n++)
	{
		thresholdVal = scoreMinVal+n*delta;

		falseAlarmRate = 0;
		falseRejectRate = 0;

		// Check each test data sample for the threshold
		for (i=0; i < testData.m_dataMatrix->rows; i++)
		{
			// Load test image vector
			cvGetRow( testData.m_dataMatrix, &rowVec, i );
			cvCopy( &rowVec, imgTest );

			// Get PCA coefficients of example ID image vectors
			GetTargetIDList4Subject( idList, testData.m_claimedId[i] );
			int numClaimedIdExamples = (int) idList.size();
			if( numClaimedIdExamples == 0 )
			{
				std::cout << "There is no example for the claimed ID " << testData.m_claimedId[i] << "." << std::endl;
				doc.SaveFile( testScoresOutFileName );
				exit(0);
			}

			imgTrgt = cvCreateMat( numClaimedIdExamples, m_M, CV_64FC1 );

			for ( j=0; j < imgTrgt->rows; j++)
			{
				cvGetRow( m_trgtPCACfs, &rowVec, idList[j] );
				cvGetRow( imgTrgt, &rowVecTrgt, j );
				cvCopy( &rowVec, &rowVecTrgt );
			}

			// Now measure similarity between two sets of images
			double similarity = MeasureSimilarity( imgTest, imgTrgt, distanceType );

			if ( (similarity < thresholdVal) && (testData.m_claimedId[i] != testData.m_actualId[i]) )
				falseAlarmRate++;

			if ( (similarity > thresholdVal) && (testData.m_claimedId[i] == testData.m_actualId[i]) )
				falseRejectRate++;

			cvReleaseMat( &imgTrgt );
		}
		
		falseAlarmRate /= (double) numAttacks;
		falseRejectRate /= (double) numTrueAccess;

		falseAlarmRate *= 100;
		falseRejectRate *= 100;

		wer = (falseRejectRate+r*falseAlarmRate)/(1+r);

		if ( (wer < minWer) || (n == 0) )
		{
			minWer = wer;
			minWerThreshold = thresholdVal;
			minWerFRR = falseRejectRate;
			minWerFAR = falseAlarmRate;
		}

		// Save performance values
		farStrg.push_back( falseAlarmRate );
		frrStrg.push_back( falseRejectRate );

		fprintf( inStream, "%lf %lf %lf\n", falseAlarmRate, falseRejectRate, thresholdVal );

		double percentage = (100.0 * n) / (double) numIntervals;

		fprintf( stderr, "WRITING ROC VALUES OF TEST SAMPLES %3d%%\r", (int) percentage );
		fflush( stderr );
	}

	fprintf( stderr, "                                                \r" );
	fflush( stderr );

	std::cout << "MINIMUM WER THRESHOLD : " << minWerThreshold << std::endl; 
	std::cout << "MINIMUM WER FAR       : " << minWerFAR << std::endl; 
	std::cout << "MINIMUM WER FRR       : " << minWerFRR << std::endl; 

	fclose( inStream );

	cvReleaseMat( &imgTest );


	return true;
}

bool CRecognizerPCA::WriteTargetVectors( std::string& targetVectorsOutFileName )
{
	int i, j;

  FaceData& targetData = GetTargetData();

	if( !m_trgtPCACfs )
  {
    std::cerr << "Target PCA coefficients matrix is not available." << std::endl;
    exit(0);
  }

	TiXmlDocument doc;  
 	TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
	doc.LinkEndChild( decl );  
 
	TiXmlElement * root = new TiXmlElement( "target_vectors_session" );  
	doc.LinkEndChild( root );  

	root->SetAttribute( "database_id", targetData.m_databaseID );
	root->SetAttribute( "protocol_id", targetData.m_protocolID );
	root->SetAttribute( "vector_dimension", m_M );

	TiXmlComment * comment = new TiXmlComment();
	comment->SetValue( "Target Vectors Data." );  
	root->LinkEndChild( comment );

	char field[MAX_PATH];
	for (i=0; i < m_trgtPCACfs->rows; i++)
	{
		TiXmlElement* targetVector = new TiXmlElement( "target_vector" );  
		root->LinkEndChild( targetVector );
		targetVector->SetAttribute( "target_id", targetData.m_fileName[i] );
		
		for (j=0; j < m_trgtPCACfs->cols; j++)
		{
			double val = CV_MAT_ELEM( *m_trgtPCACfs, double, i, j );
		  sprintf( field, "v_%d", j );
			targetVector->SetDoubleAttribute( field, val );
		}
	}

 	doc.SaveFile( targetVectorsOutFileName );

	return true;
}

bool CRecognizerPCA::WriteTestVectors( FaceData& testData, std::string& testVectorsOutFileName )
{
	int i, j;

	if( !testData.m_dataMatrix )
  {
    std::cerr << "Test Data matrix is not available." << std::endl;
    exit(0);
  }

	CvMat rowVec;
	CvMat* xTest = cvCreateMat( 1, m_N, CV_64FC1 );
	CvMat* xTilta = cvCreateMat( 1, m_N, CV_64FC1 );
	CvMat* y = cvCreateMat( 1, m_M, CV_64FC1 );

	TiXmlDocument doc;  
 	TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
	doc.LinkEndChild( decl );  
 
	TiXmlElement * root = new TiXmlElement( "test_vectors_data" );  
	doc.LinkEndChild( root );  

	TiXmlComment * comment = new TiXmlComment();
	comment->SetValue( "Test Vectors Data." );  
	root->LinkEndChild( comment );

	root->SetAttribute( "database_id", testData.m_databaseID );
	root->SetAttribute( "protocol_id", testData.m_protocolID );
	root->SetAttribute( "vector_dimension", m_M );

	char field[MAX_PATH];
	for (i=0; i < testData.m_dataMatrix->rows; i++)
	{
		// Load test image vector
		cvGetRow( testData.m_dataMatrix, &rowVec, i );
		cvCopy( &rowVec, xTest );

		// Get PCA coefficients of test ID image vectors
		cvSub( xTest, m_mu, xTilta );
		cvMatMulAdd( xTilta, m_Phi, 0, y );

		TiXmlElement* testVector = new TiXmlElement( "test_vector" );  
		root->LinkEndChild( testVector );
		testVector->SetAttribute( "test_id", testData.m_fileName[i] );
		
		for (j=0; j < y->cols; j++)
		{
			double val = CV_MAT_ELEM( *y, double, 0, j );
      sprintf( field, "v_%d", j );
			testVector->SetDoubleAttribute( field, val );
		}
	}

 	doc.SaveFile( testVectorsOutFileName );

	cvReleaseMat( &xTest );
	cvReleaseMat( &xTilta );
	cvReleaseMat( &y );

	return true;
}

TiXmlElement* CRecognizerPCA::GetTargetElement( TiXmlHandle& hDoc, int& vectorDimension )
{
	TiXmlElement* pElem;
	TiXmlHandle hRoot( 0 );

	std::string session_type;
	std::string databaseID;
	std::string protocolID;

	const char *pName = NULL;

	pElem = hDoc.FirstChildElement().Element();

	// should always have a valid root but handle gracefully if it does
	if ( !pElem ) return false;

	session_type = pElem->Value();

	pName = pElem->Attribute("database_id");
	if (pName) databaseID = pName;
	
	pName = pElem->Attribute("protocol_id");
	if (pName) protocolID = pName;

	pElem->QueryIntAttribute( "vector_dimension", &vectorDimension );

	// save this for later
	hRoot = TiXmlHandle( pElem );

  pElem = hRoot.FirstChild( "target_vector" ).Element();

	return pElem;
}

TiXmlElement* CRecognizerPCA::GetTestElement( TiXmlHandle& hDoc, int& vectorDimension )
{
	TiXmlElement* pElem;
	TiXmlHandle hRoot( 0 );

	std::string session_type;
	std::string databaseID;
	std::string protocolID;

	const char *pName = NULL;

	pElem = hDoc.FirstChildElement().Element();

	// should always have a valid root but handle gracefully if it does
	if ( !pElem ) return false;

	session_type = pElem->Value();

	pName = pElem->Attribute("database_id");
	if (pName) databaseID = pName;
	
	pName = pElem->Attribute("protocol_id");
	if (pName) protocolID = pName;

	pElem->QueryIntAttribute( "vector_dimension", &vectorDimension );

	// save this for later
	hRoot = TiXmlHandle( pElem );

  pElem = hRoot.FirstChild( "test_vector" ).Element();

	return pElem;
}

bool CRecognizerPCA::WriteScores( std::string& targetVectorsFileName, std::string& testVectorsFileName, DistanceType distanceType,
																  std::string& scoresOutFileName )
{
	int i;
	int targetVectorDimension = 0;
	int testVectorDimension = 0;

	TiXmlDocument targetDoc( targetVectorsFileName );
	if ( !targetDoc.LoadFile( ) ) return false;

	TiXmlHandle hTargetDoc( &targetDoc );

	TiXmlDocument testDoc( testVectorsFileName );
	if ( !testDoc.LoadFile( ) ) return false;

	TiXmlHandle hTestDoc( &testDoc );

	TiXmlElement* pElemTarget = GetTargetElement( hTargetDoc, targetVectorDimension );
	TiXmlElement* pElemTest = GetTestElement( hTestDoc, testVectorDimension );

	TiXmlElement* pFirstElemTarget = pElemTarget;
	TiXmlElement* pFirstElemTest = pElemTest;

	if ( targetVectorDimension != testVectorDimension )
	{
		std::cerr << "Dimension of target and test vectors is not equal." << std::endl;
		return false;
	}

	// New XML Score document
	TiXmlDocument doc;  
 	TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
	doc.LinkEndChild( decl );  
 
	TiXmlElement * root = new TiXmlElement( "scores_session" );  
	doc.LinkEndChild( root );  

	TiXmlComment * comment = new TiXmlComment();
	comment->SetValue( "Scores Data." );  
	root->LinkEndChild( comment );

	root->SetAttribute( "target_vectors_file", targetVectorsFileName );
	root->SetAttribute( "test_vectors_file", testVectorsFileName );

	CvMat* targetVector = cvCreateMat( 1, targetVectorDimension, CV_64FC1 );
	CvMat* testVector = cvCreateMat( 1, testVectorDimension, CV_64FC1 );

	std::string vectorIdx;
	const char *pNameTargetId = NULL;
	const char *pNameTestId = NULL;
	char stringTemp[MAX_PATH];

	for( pElemTest; pElemTest; pElemTest=pElemTest->NextSiblingElement())
	{
		pNameTestId = pElemTest->Attribute("test_id");
		if ( !pNameTestId )
		{
			printf( "Can not read attribute <test_id> in %s file.\n", testVectorsFileName );
			return false;
		}

		// Read test vector
		double value = 0;
		for (i=0; i < testVectorDimension; i++)
		{
			sprintf( stringTemp, "v_%d", i );
			vectorIdx = stringTemp;			
			pElemTest->QueryDoubleAttribute( vectorIdx, &value );
			testVector->data.db[i] = value;
		}

		// Loop over the target vectors for each test vector
		for( pElemTarget; pElemTarget; pElemTarget=pElemTarget->NextSiblingElement())
		{
			pNameTargetId = pElemTarget->Attribute("target_id");
			if ( !pNameTargetId )
			{
				printf( "Can not read attribute <target_id> in %s file.\n", targetVectorsFileName );
				return false;
			}

			// Read target vector
			double value = 0;
			for (i=0; i < targetVectorDimension; i++)
			{
				sprintf( stringTemp, "v_%d", i );
				vectorIdx = stringTemp;			
				pElemTarget->QueryDoubleAttribute( vectorIdx, &value );
				targetVector->data.db[i] = value;
			}

			// Compute similarity
			double similarity = MeasureSimilarity( testVector, targetVector, distanceType );

			TiXmlElement* result = new TiXmlElement( "scoreresult" );  
			root->LinkEndChild( result );
			result->SetAttribute( "test_file_name", pNameTestId );
			result->SetAttribute( "target_file_name", pNameTargetId );
			result->SetDoubleAttribute( "score", similarity );
		}

		pElemTarget = pFirstElemTarget;
	}

 	doc.SaveFile( scoresOutFileName );

	cvReleaseMat( &testVector );
	cvReleaseMat( &targetVector );

	return true;
}

bool CRecognizerPCA::SaveModel( std::string&	modelFileName )
{
  std::cout << "Writing model file: " << modelFileName << std::endl;
  FILE* inStream = fopen(modelFileName.c_str(), "wb");
  if(!inStream)
    {
      std::cerr << "Can not open model saving file " << modelFileName
		<< " for writing." << std::endl;
      exit(-1);
    }

  if (m_trainData.m_dataMatrix == NULL)
    {
      std::cerr << "Training data is not available." << std::endl;
      fclose(inStream);
      return false;
    }

  if (m_Phi == NULL)
    {
      std::cerr << "Eigenvectors are not available." << std::endl;
      fclose( inStream );
      return false;
    }

  if (m_mu == NULL)
    {
      std::cerr << "Mean vector is not available." << std::endl;
      fclose( inStream );
      return false;
    }

  if (m_eigVals == NULL)
    {
      std::cerr << "Eigenvalues are not available." << std::endl;
      fclose( inStream );
      return false;
    }

  int i, j;

  // Write number of dimensions of the training samples
  fwrite(&m_N, sizeof(int), 1, inStream);

  // Write number of Eigen vectors
  fwrite(&m_M, sizeof(int), 1, inStream);

  // Write mean vector values
  for (j = 0; j < m_N; j++)
    {
      double value = CV_MAT_ELEM(*m_mu, double, 0, j);
      fwrite(&value, sizeof(double), 1, inStream);
    }

  // Write eigenvectors
  for (i = 0; i < m_N; i++)
    for (j = 0; j < m_M; j++)
      {
	double value = CV_MAT_ELEM( *m_Phi, double, i, j );
	fwrite(&value, sizeof(double), 1, inStream);
      }

  // Write eigenvalues
  for (j = 0; j < m_M; j++)
    {
      double value = CV_MAT_ELEM( *m_eigVals, double, 0, j );
      fwrite(&value, sizeof(double), 1, inStream);
    }

  fclose( inStream );
  return true;
}

bool CRecognizerPCA::LoadModel( std::string& modelFileName)
{
	FILE* inStream = fopen(modelFileName.c_str(), "rb" );
	if(!inStream)
	  {
	    std::cerr << "Can not open model saving file " << modelFileName
		      << " for reading." << std::endl;
	    exit(-1);
	  }

	if ( m_trgtPCACfs != NULL )
	{
		cvReleaseMat( &m_trgtPCACfs );
		m_trgtPCACfs = NULL;
	}

	if ( m_Phi != NULL )
	{
		cvReleaseMat( &m_Phi );
		m_Phi = NULL;
	}

	if ( m_mu != NULL )
	{
		cvReleaseMat( &m_mu );
		m_mu = NULL;
	}

	if ( m_eigVals != NULL )
	{
		cvReleaseMat( &m_eigVals );
		m_Phi = NULL;
	}

	int i, j;
	double value = 0;

	// Write number of dimensions of the training samples
	fread( &m_N, sizeof(int), 1, inStream );

	// Write number of Eigen vectors
	fread( &m_M, sizeof(int), 1, inStream );

	m_mu = cvCreateMat( 1, m_N, CV_64FC1 );
	m_Phi = cvCreateMat( m_N, m_M, CV_64FC1 );
	m_eigVals = cvCreateMat( 1, m_M, CV_64FC1 );

	// Read mean vector values
	for (j=0; j < m_N; j++)
	{
		fread( &value, sizeof(double), 1, inStream );
		CV_MAT_ELEM( *m_mu, double, 0, j ) = value;
	}

	// Read eigenvectors
	for (i=0; i < m_N; i++)
	{
		for (j=0; j < m_M; j++)
		{
			fread( &value, sizeof(double), 1, inStream );
			CV_MAT_ELEM( *m_Phi, double, i, j ) = value;
		}
	}

	// Read eigenvalues
	for (j=0; j < m_M; j++)
	{
		fread( &value, sizeof(double), 1, inStream );
		CV_MAT_ELEM( *m_eigVals, double, 0, j ) = value;
	}

	// Compute train data PCA coefficients
	if ( m_K != 0 )
	{
		CvMat trnVec, tmpVec;
		CvMat* tmpMat = cvCreateMat( m_K, m_N, CV_64FC1 );
		m_trgtPCACfs = cvCreateMat( m_K, m_M, CV_64FC1 );

		for (i=0; i < m_K; i++)
		{
			cvGetRow( m_targetData.m_dataMatrix, &trnVec, i );
			cvGetRow( tmpMat, &tmpVec, i );
			cvSub( &trnVec, m_mu, &tmpVec );
		}

		cvMatMulAdd( tmpMat, m_Phi, 0, m_trgtPCACfs );
		cvReleaseMat( &tmpMat );
	}

	fclose( inStream );
	return true;
}
