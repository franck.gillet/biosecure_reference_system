/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <time.h>
#include <float.h>
#include <math.h>
#include <limits.h>

#include <string>
#include <iostream>
#include <cstdlib>

#include <cv.h>
#include <highgui.h>

#include "IV_Common.h"
#include "FaceRecognizerPCA.h"
#include "FaceRecognizerProbPCA.h"

int main( int argc, char* argv[] )
{
	int i;
	std::string inputFile;
	std::string modelFile;
	std::string targetVectorsFile;
	std::string testVectorsFile;
	std::string scoresOutFile;

  if( argc == 1 )
  {
    printf( "Usage: %s\n  -params <IV_System_Parameters_File> -model <Model_File_Name> -outLog <Test_Log_File> -outROC <Test_ROC_File>", argv[0] );
		printf( "Program produces xml test output using given input parameters by the -params and -model switches.\n" );
		printf( "-params : IV system parameters given by <IV_System_Parameters_File> in XML format.\n" );
		printf( "-model	 : Model file name produced by the SaveModel program.\n" );
		printf( "-inTarget : Target vectors file in XML format.\n" );
		printf( "-inTest : Test vectors file in XML format.\n" );
		printf( "-inOut : Scores output file in XML format.\n" );
    return 0;
  }

  for( i = 1; i < argc; i++ )
  {
    if( !strcmp( argv[i], "-params" ) )
    {
			inputFile = argv[++i];
    }
		else
		{
			if ( !strcmp( argv[i], "-model" ) )
			{
				modelFile = argv[++i];
			}
			else
			{
				if ( !strcmp( argv[i], "-inTarget" ) )
				{
					targetVectorsFile = argv[++i];
				}
				else
				{
					if ( !strcmp( argv[i], "-inTest" ) )
					{
						testVectorsFile = argv[++i];
					}
					else
					{
						if ( !strcmp( argv[i], "-inOut" ) )
						{
							scoresOutFile = argv[++i];
						}
					}
				}
			}
		}
	}

	IVPrmtrSettings settings;

	settings.load_score( inputFile.c_str() );

	if ( settings.method == "Maximum_Likelihood_PCA" )
	{
                CRecognizerProbPCA probPCARec( modelFile );

	        probPCARec.WriteScores( targetVectorsFile, testVectorsFile, scoresOutFile, settings.maskImageFile.c_str() );
	}
	else
	{
		if ( settings.method == "PCA" )
		{
                        //CRecognizerPCA pcaRec( modelFile );

			//CRecognizerPCA pcaRec;
                        if ( settings.similarityMeasure == "L1_Norm" )
			{	
				CRecognizerPCA pcaRec;
				pcaRec.WriteScores( targetVectorsFile, testVectorsFile, pcaRec.DISTANCE_L1, scoresOutFile );
			}
			else
			{
        if ( settings.similarityMeasure == "L2_Norm" )
				{
					CRecognizerPCA pcaRec;
					pcaRec.WriteScores( targetVectorsFile, testVectorsFile, pcaRec.DISTANCE_L2, scoresOutFile );
				}
				else
				{
					if ( settings.similarityMeasure == "whitened_L1_Norm" )
					{
						CRecognizerPCA pcaRec( modelFile );
						pcaRec.WriteScores( targetVectorsFile, testVectorsFile, pcaRec.DISTANCE_WL1, scoresOutFile );
					}
					else
					{
						if ( settings.similarityMeasure == "whitened_L2_Norm" )
						{	
							CRecognizerPCA pcaRec( modelFile );
							pcaRec.WriteScores( targetVectorsFile, testVectorsFile, pcaRec.DISTANCE_WL2, scoresOutFile );
						}
						else
						{
							if ( settings.similarityMeasure == "whitened_cosine_angle" )
							{	
								CRecognizerPCA pcaRec( modelFile );
								pcaRec.WriteScores( targetVectorsFile, testVectorsFile, pcaRec.DISTANCE_WCOS, scoresOutFile );
							}
						}
					}
				}

			}
		}
	}	

	return 1;
}

