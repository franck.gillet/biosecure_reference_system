/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if !defined(FACE_RECOGNIZER_PROBPCA_HEADER)
#define FACE_RECOGNIZER_PROBPCA_HEADER

#include "IV_Common.h"

/*!
  \defgroup RecognizerProbPCA Probabilistic PCA Based Recognition Class
*/

/*!
  \ingroup RecognizerProbPCA
	\brief Recognizer class using probabilistic Mahalanobis distance measure

	CRecognizerProbPCA class is constructed with FaceData class. It is
	trained by the Train method using the given trainData and save the
	trained PCA model with SaveModel method. To test the system it is
	constructed with second constructor with model file name, and target
	data and tested by the Test method using the inputs of Test Data and
	xml test output log file name.
*/

class CRecognizerProbPCA
{
public:

	//! \brief First constructer for the class by loading the train data
	CRecognizerProbPCA( FaceData& trainData );

	//! \brief First constructer for the class by loading train data
	CRecognizerProbPCA( std::string& modelFileName );

        CRecognizerProbPCA( );

	//! \brief Second constructer for the class by loading model data file and target data to test the PCA model.
	CRecognizerProbPCA( std::string& modelFileName, FaceData& targetData );

	//! \brief A void destructor for the class to release all the memory allocation done by the class
	~CRecognizerProbPCA( );

	/*! \brief Returns the number of train samples
	*/
	inline int GetNumTrnSamples()
	{
		return m_trainData.m_dataMatrix->rows;
	}

	/*! \brief Returns the dimension size of train data samples
	*/
	inline int GetDimension()
	{
		return m_N;
	}

	/*! \brief Returns number of subjects of the train data set

			\param idx sample index value
	*/
	inline int GetNumTrainSubjects( )
	{
		return m_trainData.m_numSubjects;
	}

	/*! \brief Returns the actual subject ID of the train data sample for the given data index value

			\param idx sample index value
	*/
	inline std::string GetTrainSampleSubjectID( int idx )
	{
		return m_trainData.m_actualId[idx];
	}

	/*! \brief Returns the actual subject ID of the target data sample for the given data index value

			\param idx sample index value
	*/
	inline std::string GetTargetSampleSubjectID( int idx )
	{
		return m_targetData.m_actualId[idx];
	}

	/*! \brief Returns the train data reference
	*/
	inline FaceData& GetTrainData()
	{
		return m_trainData;
	}

	/*! \brief Returns the target data reference
	*/
	inline FaceData& GetTargetData()
	{
		return m_targetData;
	}

	/*! \brief Returns the train data file name string
	*/
	inline std::string GetTrainFileName( int trnIdx )
	{
		return m_trainData.m_fileName[trnIdx];
	}

	/*! \brief Returns the number of samples in the given data set for the given subject ID

			\param subjID subject ID
	*/
	inline int GetNumTrainSamples4Subject( std::string& subjID )
	{
		// Fill image set A
		int countSet = 0;

		if (!m_trainData.m_dataMatrix) return countSet;

		for ( int k=0; k < m_trainData.m_dataMatrix->rows; k++)
		{
			if ( m_trainData.m_actualId[k] == subjID )
			{
				countSet++;
			}
		}

		return countSet;
	}

	/*! \brief Returns the sample indices in the target data set for the given subject ID

			\param subjID idList reference to be filled up
			\param subjID subject ID
	*/
	inline void GetTargetIDList4Subject( std::vector<int>& idList, std::string& subjID )
	{
		// Fill image set A
		idList.clear();

		if( !m_targetData.m_dataMatrix )
		{
			std::cerr << "Target data matrix is not available." << std::endl;
			exit(0);
		}

		for ( int k=0; k < m_targetData.m_dataMatrix->rows; k++)
		{
			if ( m_targetData.m_actualId[k] == subjID )
			{
				idList.push_back( k );
			}
		}
	}

	/*! \brief Returns the number of samples in the given face data for the given subject ID

			\param subjID subject ID value
	*/
	inline int GetNumSamples4Subject( FaceData& faceData, std::string& subjID )
	{
		// Fill image set A
		int countSet = 0;

		if (!faceData.m_dataMatrix) return countSet;

		for ( int k=0; k < faceData.m_dataMatrix->rows; k++)
		{
			if ( faceData.m_actualId[k] == subjID )
			{
				countSet++;
			}
		}

		return countSet;
	}

	/*! \brief Training method used for obtaining training parameters using first m_M number of eigenvectors

			\param M Number of eigenvectors used for training
	*/
	bool Train( int M );

	/*! \brief Training method used for obtaining training parameters using first m_M number of eigenvectors
			corresponding to the given energyRatio.

			\param energyRatio Ratio of sum of the selected eigenvalues used for training to the whole eigenvalues sum.
	*/
	bool Train( double energyRatio );


	/*! \brief Saves the trained PCA parameters into the input file.

			\param modelFileName Model file name.
	*/
	bool SaveModel(const std::string&	modelFileName );


	/*! \brief Loads save PCA model parameters into the model parameters of the class.

			\param modelFileName Model file name.
	*/
	bool LoadModel(const std::string&	modelFileName);

	/*! \brief Computes ML PCA Distances given the difference vector between the test and target images.

	\param x Vector pointer to difference image vector with the array size (1 x m_N).
	*/
  double MLPCADistance( CvMat* x );

	/*! \brief Writes the projected vectors for the images defined by
			the target protocol file. Since ML PCA method depends on the distribution of the image difference
			vectors the output is just the same as the image vectors defined by the protocol file.

			\param targetVectorsOutFileName String which contains the target vectors output in XML format.
	*/
	bool WriteTargetVectors( IVPrmtrSettings& settings, std::string& targetVectorsOutFileName );

	/*! \brief Writes the projected vectors for all images defined by
			the test protocol file.Since ML PCA method depends on the distribution of the image difference
			vectors the output is just the same as the image vectors defined by the protocol file.

			\param testData Face data structure which contains the information about the test data.
			\param testVectorsOutFileName String which contains the test vectors output in XML format.
	*/
	bool WriteTestVectors( IVPrmtrSettings& settings, FaceData& testData, std::string& testVectorsOutFileName );

	/*! \brief Gets the XML element pointer of the given target vectors file name..

			\param hDoc handle to target XML file.
			\param vectorDimension dimension of the vectors given in the <vectorsFileName>.
	*/
	TiXmlElement*  GetTargetElement( TiXmlHandle& hDoc, int& vectorDimension );

	/*! \brief Gets the XML element pointer of the given target vectors file name..

			\param hDoc handle to target XML file.
			\param vectorDimension dimension of the vectors given in the <vectorsFileName>.
	*/
	TiXmlElement*  GetTestElement( TiXmlHandle& hDoc, int& vectorDimension );

	/*! \brief Writes the similarity scores between projected test vectors and target vectors.

			\param targetVectorsFileName XML data file which contains the projected target vectors.
			\param testVectorsFileName XML data file which contains the projected test vectors.
	*/
	bool WriteScores( std::string& targetVectorsFileName, std::string& testVectorsFileName, 
									  std::string& scoresOutFileName, const char* pMaskFileName );

	/*! \brief Test method used for obtaining for the test result of the currently loaded PCA model.

			\param testData Test data reference.
			\param distanceType Distance type enumarator for similarity measure computation.
			\param testScoresOutFileName XML output log file name.
			\param testROCValuesFileName XML output log file name.
	*/
	bool Test( FaceData& testData, std::string& testScoresOutFileName, std::string& testROCValuesFileName );

	/*! \brief Computes similarity values between two normalized image set of vectors and returns
			it.

			\param CvMat* structure pointer corresponding to image set A matrix
			\param CvMat* structure pointer corresponding to image set B matrix
	*/
  double MeasureSimilarity( CvMat* imgA, CvMat* imgB );

private:
	//! \brief Sets number of PCA feature vectors
	int m_M;

	//! \brief Shows the dimension of the training and testing image vectors
	int m_N;

	//! \brief Shows the number of training images in the TrainData structure and usd for training
	int m_K;

	//! \brief Shows the residual error with m_M number of PCA feature vectors
	double m_rho;

	//! \brief Points to the CvMat array which holds eigenvectors in the column based order with array size (m_N x m_M)
	CvMat* m_Phi;

	//! \brief Points to the CvMat array which holds eigenvalues in the descending order with array size (1 x m_M)
	CvMat* m_eigVals;

	//! \brief Points to the CvMat array which holds the intra personal image vector differences of the training image vectors.
	CvMat* m_diffDataMat;

	/*!
		\brief Points to the CvMat array which holds the covariance matrix of m_diffDataMat.
		Its size is array size (m_N x m_N) or (m_K x m_K).
	*/
	CvMat* m_covIntraMat;

	//! \brief Points to the TrainData structure which holds training data information
	FaceData m_trainData;

	//! \brief FaceData class which holds target data information
	FaceData m_targetData;

	//! \brief Compute Covariance intra personal difference matrix, m_diffDataMat.
	bool ComputeDiffDataMatrix( );

	//! \brief Compute Covariance matrix of m_diffDataMat.
	bool ComputeCovIntraMatrix( );

	/*! \brief Computes the number of eigenvectors for the given energy_ratio parameter and set its
	to the member m_M.

	\param eigVals Pointer to CvMat structure used for holding the whole eigenvalues with the array size (1 x m_N)
	\param energy_ratio Ratio of sum of the selected eigenvalues used for training to the whole eigenvalues sum.
	*/
	int ComputeM( CvMat* eigVals, double energy_ratio );

	/*! \brief Computes PCA projection matrix

	The PCA projection matrix contains m_M number of eigenvectors in the column based order
	corresponding to the first largest m_M eigenvalues of the covariance matrix m_covMat. The
	computed projection	matrix is loaded into m_Phi member with array size (m_N x m_M).

	\param M The number of first eigenvectors used in the training
	*/
	bool ComputeProjMat( int M );

	/*! \brief Computes PCA projection matrix

	The PCA projection matrix contains m_M number of eigenvectors in the column based order
	corresponding to the first largest m_M eigenvalues of the covariance matrix m_covMat. The
	computed projection	matrix is loaded into m_Phi member with array size (m_N x m_M).

	\param energyRatio Ratio of sum of the selected eigenvalues used for training to the whole eigenvalues sum.
	*/
	bool ComputeProjMat( double energyRatio );
};


#endif
