/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "FaceData.h"

FaceData::FaceData()
{
	m_numSubjects = 0;
	m_fileName.clear();
	m_actualId.clear();
	m_claimedId.clear();
	m_dataMatrix = NULL;
	m_mskImg = NULL;
}

FaceData::~FaceData()
{
	m_numSubjects = 0;
	
	m_fileName.clear();
	m_actualId.clear();
	m_claimedId.clear();

	if ( !m_dataMatrix )
		cvReleaseMat( &m_dataMatrix );

	if ( !m_mskImg )
		cvReleaseImage( &m_mskImg );
}

int FaceData::GetInputDimension( IplImage* maskImage )
{
	int x, y;
	int numValidPixels = 0;
	uchar maskVal;

	for (y = 0; y < maskImage->height; y++)
	{
		for (x = 0; x < maskImage->width; x++)
    {
      maskVal = ((uchar*)(maskImage->imageData + maskImage->widthStep * y))[x];

      if (maskVal != 0)
        numValidPixels++;
    }
	}

	return numValidPixels;
}
	
bool FaceData::Load( const char* pImagesDirName, const char* pImagesFileType, 
										 const char* pProtocolFileName, const char* pMaskFileName )
{
	if ( !pMaskFileName )
		return false;

	TiXmlDocument doc( pProtocolFileName );
	if ( !doc.LoadFile( ) ) return false;

	TiXmlHandle hDoc( &doc );
	TiXmlElement* pElem;
	TiXmlElement* pElemChild;
	TiXmlHandle hRoot( 0 );

	// block : Root parameters name
	{
		const char *pName = NULL;

		pElem = hDoc.FirstChildElement().Element();

		// should always have a valid root but handle gracefully if it does
		if ( !pElem ) return false;

		m_sessionType = pElem->Value();

		pName = pElem->Attribute("database_id");
		if (pName) m_databaseID = pName;
		
		pName = pElem->Attribute("protocol_id");
		if (pName) m_protocolID = pName;

		// save this for later
		hRoot = TiXmlHandle( pElem );
	}

  
	// block : enrol or claim
	{
		char fileLocation[256];
		const char *pName = NULL;
		const char *pNameResult = NULL;
		const char *pNameActualId = NULL;
		const char *pNameClaimedId = NULL;

		if ( m_sessionType == "claimsession" )
		{
      pElem = hRoot.FirstChild( "claim" ).Element();
		}
		else
		{
      if ( m_sessionType == "enrolsession" )
			{
				pElem = hRoot.FirstChild( "enrol" ).Element();
			}
		}       

		int numDataFiles = 0;
		m_numSubjects = 0;
		m_fileName.clear();
		m_actualId.clear();
		for( pElem; pElem; pElem=pElem->NextSiblingElement())
		{
			if ( m_sessionType == "claimsession" )
			{
				pNameResult = pElem->Attribute("result");
				if ( !pNameResult )
				{
					printf( "Can not read attribute <result> in %s file.\n", pProtocolFileName );
					return false;
				}

				pNameActualId = pElem->Attribute("actual_id");
				if ( !pNameActualId )
				{
					printf( "Can not read attribute <actual_id> in %s file.\n", pProtocolFileName );
					return false;
				}

				pNameClaimedId = pElem->Attribute("claimed_id");
				if ( !pNameActualId )
				{
					printf( "Can not read attribute <claimed_id> in %s file.\n", pProtocolFileName );
					return false;
				}

				//printf( "actual_id= %s, claimed_id= %s\n", pNameActualId, pNameClaimedId );

				if ( pNameResult && pNameActualId && pNameClaimedId )
				{
					m_numSubjects++;
				}
			}
			else
			{
				if ( m_sessionType == "enrolsession" )
				{
					pNameActualId = pElem->Attribute("id");

					if ( !pNameActualId )
					{
						printf( "Can not read attribute <id> in %s file.\n", pProtocolFileName );
						return false;
					}

					m_numSubjects++;
				}
			}

			pElemChild = pElem->FirstChildElement( "faceid" );
			for( pElemChild; pElemChild; pElemChild=pElemChild->NextSiblingElement() )
			{
				const char *pKey=pElemChild->Value();
				const char *pText=pElemChild->GetText();
				if ( pKey && pText ) 
				{
					m_fileName.push_back( pText );
					m_actualId.push_back( pNameActualId );

					if ( m_sessionType == "claimsession" )
					{
						m_result.push_back( pNameResult );
						m_claimedId.push_back( pNameClaimedId );
					}
					else
					{
						if ( m_sessionType == "enrolsession" )
						{
							m_result.push_back( "" );
							m_claimedId.push_back( pNameActualId );
						}
					}

					//printf( "faceid = %s\n", pText );

					numDataFiles++;
				}
			}

			//printf( "----------------------------\n" );
			//if ( strcmp( pNameActualId, pNameClaimedId ) )
			//{
			//	printf( "----------------------------\n" );
			//}
		}

    m_mskImg = cvLoadImage( pMaskFileName, 0);

		if ( !m_mskImg )
		{
			printf( "Can not read mask image file %s.\n", pMaskFileName );
			cvReleaseImage( &m_mskImg );
			return false;
		}

		int dimension = GetInputDimension( m_mskImg );

		m_dataMatrix = cvCreateMat( numDataFiles, dimension, CV_64FC1);

		CvMat rowVec;
		CvSize imgSize;
		CvSize maskSize = cvGetSize( m_mskImg );

		for(int i=0; i < numDataFiles; i++)
		{
			sprintf( fileLocation, "%s/%s.%s", pImagesDirName, m_fileName[i].c_str(), pImagesFileType );

			IplImage* normalizedImg = cvLoadImage( fileLocation, 0 );

			if ( !normalizedImg )
			{
				printf( "Can not read image file %s.\n", fileLocation );
				cvReleaseImage( &normalizedImg );
				return false;
			}

			imgSize = cvGetSize( normalizedImg );
			if ( (imgSize.width != maskSize.width) || (imgSize.height != maskSize.height) )
			{
				printf( "Image size does not match with the mask image size\n" );
				return false;
			}

			cvGetRow( m_dataMatrix, &rowVec, i);
	    GetImageVector( normalizedImg, &rowVec, m_mskImg );

			cvReleaseImage( &normalizedImg );
		}
	}

  return true;
}
