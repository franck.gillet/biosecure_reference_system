/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "IV_Common.h"
#include "FaceRecognizerProbPCA.h"

#include <string>
#include <iostream>

CvMat* averageShape = NULL;
CvMat* referenceMarkers = NULL;
CvMat* markerData = NULL;
CvMat* transformArray = NULL;
CvMat* refInvTrnsfrm = NULL;

/* Convert matrix to vector */
#define MAT2VEC(mat, vdata, vstep, num)					 \
    assert((mat).rows == 1 || (mat).cols == 1);  \
    (vdata) = ((mat).data.ptr);                    \
    if((mat).rows == 1)                          \
    {                                              \
        (vstep) = CV_ELEM_SIZE((mat).type);      \
        (num) = (mat).cols;                        \
    }                                              \
    else                                           \
    {                                              \
        (vstep) = (mat).step;                      \
        (num) = (mat).rows;                        \
    }


// Shuffling
#define RAND_SHUFFLE(suffix, type)																										 \
void RandShuffle_##suffix(uchar* data, size_t step, int num)													 \
{                                                                                        \
    CvRandState state;                                                                   \
    time_t seed;                                                                         \
    type tmp;                                                                            \
    int i;                                                                               \
    float rn;                                                                            \
                                                                                         \
    time(&seed);                                                                       \
                                                                                         \
    cvRandInit(&state, (double) 0, (double) 0, (int)seed);                             \
    for(i = 0; i < (num-1); i++)                                                       \
    {                                                                                    \
        rn = ((float) cvRandNext(&state)) / (1.0F + UINT_MAX);                         \
        CV_SWAP(*((type*)(data + i * step)),                                            \
                 *((type*)(data + (i + (int)(rn * (num - i)))* step)),              \
                 tmp);                                                                  \
    }                                                                                    \
}

RAND_SHUFFLE(8U, uchar)

RAND_SHUFFLE(16S, short)

RAND_SHUFFLE(32S, int)

RAND_SHUFFLE(32F, float)

void RandShuffle(CvMat* mat)
{
  CV_FUNCNAME("RandShuffle");

  __BEGIN__;

  uchar* data;
  size_t step;
  int num;

  if((mat == NULL) || !CV_IS_MAT(mat) || MIN(mat->rows, mat->cols) != 1)
    {
      CV_ERROR(CV_StsUnsupportedFormat, "");
    }

  MAT2VEC(*mat, data, step, num);
  switch(CV_MAT_TYPE(mat->type))
    {
    case CV_8UC1:
      RandShuffle_8U(data, step, num);
      break;
    case CV_16SC1:
      RandShuffle_16S(data, step, num);
      break;
    case CV_32SC1:
      RandShuffle_32S(data, step, num);
      break;
    case CV_32FC1:
      RandShuffle_32F(data, step, num);
      break;
    default:
      CV_ERROR(CV_StsUnsupportedFormat, "");
    }

  __END__;
}

CvMat* ReadMarkerData( CropParams* crpPrms )
{
	FILE* inStream = fopen( crpPrms->markersFileName, "rb" );
	if(!inStream)
	{
		printf( "Cannot open marker file %s for reading.\n", crpPrms->markersFileName );
		return NULL;
	}

	fprintf( stderr, "LOADING MARKER DATA...                            \r" );
  fflush( stderr );

	int i;
	double x, y;
	int numAvailableFiles = 0;
	int numFileNames = 0;
	int fileLabel = 0;
	int numMarkerPoints = 0;
	char imgType[MAX_PATH];
	char fileName[MAX_PATH];
	char fileLocation[MAX_PATH];
	CvMat rowVec;
	FILE* inStreamTst = NULL;

	// Read number of train files in file names list
	fscanf( inStream, "%d %d\n", &numFileNames, &numMarkerPoints );
	fscanf( inStream, "%s\n", imgType );

	CvMat* dataMatrix = cvCreateMat( numFileNames, 2*numMarkerPoints, CV_64FC1);

	numAvailableFiles = 0;
	while ( numAvailableFiles < dataMatrix->rows )
	{
		fscanf( inStream, "%s ", fileName );

		cvGetRow( dataMatrix, &rowVec, numAvailableFiles );

		// Take feature points for alignment
		for (i=0; i < numMarkerPoints-1; i++)
		{
      fscanf( inStream, "%lf %lf ", &x, &y );
			rowVec.data.db[i] = x;
			rowVec.data.db[i+numMarkerPoints] = y;
		}
    fscanf( inStream, "%lf %lf\n", &x, &y );
		rowVec.data.db[i] = x;
		rowVec.data.db[i+numMarkerPoints] = y;

		numAvailableFiles++;
	}

 	fclose( inStream );

	return dataMatrix;
}

CroppedImagesData* CreatePositiveCroppedImages( CropParams* crpPrms, CvMat* markerData, CvMat* transformArray )
{
	FILE* inStream = fopen( crpPrms->markersFileName, "rt" );
	if(!inStream)
	{
		printf( "Cannot open marker file %s for reading.\n", crpPrms->markersFileName );
		exit(0);
	}

	int i, j;
	double x, y;
	int fileLabel = 0;
	int imgCount = 0;
	int numFileNames, numMarkerPoints, numAvailableImages;
	double xcur, ycur, xnew, ynew;
	char fileName[MAX_PATH];
	std::string fileNameString;
	char fileLocation[MAX_PATH];
	CvPoint pntUpLft, pntBtRgt;
	CvRect ROIRect;
	char imgType[MAX_PATH];


	CvPoint centerRefPoint, centerTrnsfrmdPoint, centerAvgPnt;
	int radius = 1;

	float m[6];
	CvMat M = cvMat( 2, 3, CV_32F, m );

	// Transformation matrices
	float t[9], tu[9];
	CvMat T = cvMat( 3, 3, CV_32F, t );
	CvMat Tu = cvMat( 3, 3, CV_32F, tu );

	t[6] = 0; t[7] = 0; t[8] = 1;
	tu[6] = 0; tu[7] = 0; tu[8] = 1;

	numAvailableImages = transformArray->rows;

  CvMat img = cvMat( crpPrms->cropHeight, crpPrms->cropWidth, CV_8UC1, NULL );
  CvMat rowTrnsfrmMat = cvMat( 3, 3, CV_64FC1, NULL );

	CvMat* invTrnsfrm = cvCreateMat( 3, 3, CV_64FC1 );

	// Read number of train files in file names list
	fscanf( inStream, "%d %d\n", &numFileNames, &numMarkerPoints );
	fscanf( inStream, "%s\n", imgType );

	CroppedImagesData* crpData = new CroppedImagesData;

	crpData->croppedImgs = cvCreateMat( numAvailableImages, crpPrms->cropWidth*crpPrms->cropHeight, CV_8UC1 );
	crpData->fileName.clear();

	CvMat* croppedImgs = crpData->croppedImgs;

	// Initialize random generator
	srand( time(NULL) );

	while ( !feof( inStream ) )
	{
		fscanf( inStream, "%s ", fileName );
		sprintf( fileLocation, "%s/%s.%s", crpPrms->imageStorageFolder, fileName, imgType );

    fileNameString = fileName;
		crpData->fileName.push_back( fileNameString );

		// Take feature points for alignment
		for (i=0; i < numMarkerPoints-1; i++)
		{
      fscanf( inStream, "%lf %lf ", &x, &y );
		}
    fscanf( inStream, "%lf %lf\n", &x, &y );

 		IplImage* progressImageSrc = cvLoadImage( fileLocation, 1 );
		if ( progressImageSrc == NULL )
		{
			printf("Can not find the image %s\n", fileName );
			cvReleaseImage( &progressImageSrc );
			exit(0);
		}

		IplImage* progressImageDst = cvCloneImage( progressImageSrc );

		// Extract transform matrix
		uchar* trnsfrmData = (uchar*) (transformArray->data.ptr + imgCount * transformArray->step);
		rowTrnsfrmMat.data.ptr = (uchar*) trnsfrmData;

		// Find inverse transformation to bring the marker set back to its original position
		cvMatMul( refInvTrnsfrm, &rowTrnsfrmMat, invTrnsfrm );

		m[0] = CV_MAT_ELEM( *invTrnsfrm, double, 0, 0 );
		m[1] = CV_MAT_ELEM( *invTrnsfrm, double, 0, 1 );
		m[2] = CV_MAT_ELEM( *invTrnsfrm, double, 0, 2 );
		m[3] = CV_MAT_ELEM( *invTrnsfrm, double, 1, 0 );
		m[4] = CV_MAT_ELEM( *invTrnsfrm, double, 1, 1 );
		m[5] = CV_MAT_ELEM( *invTrnsfrm, double, 1, 2 );

		// Transfer ROI to the the center of image
		m[2] += progressImageSrc->width/2 - crpPrms->cropWidth/2;
		m[5] += progressImageSrc->height/2 - crpPrms->cropHeight/2;

		cvWarpAffine( progressImageSrc, progressImageDst, &M, CV_INTER_LINEAR+CV_WARP_FILL_OUTLIERS, cvScalarAll(0) );

		t[0] = CV_MAT_ELEM( *invTrnsfrm, double, 0, 0 );
		t[1] = CV_MAT_ELEM( *invTrnsfrm, double, 0, 1 );
		t[2] = CV_MAT_ELEM( *invTrnsfrm, double, 0, 2 );
		t[3] = CV_MAT_ELEM( *invTrnsfrm, double, 1, 0 );
		t[4] = CV_MAT_ELEM( *invTrnsfrm, double, 1, 1 );
		t[5] = CV_MAT_ELEM( *invTrnsfrm, double, 1, 2 );

		// Transfer ROI to the the center of image
		t[2] += progressImageSrc->width/2 - crpPrms->cropWidth/2;
		t[5] += progressImageSrc->height/2 - crpPrms->cropHeight/2;

		pntUpLft.x = progressImageSrc->width/2 - crpPrms->cropWidth/2;
		pntUpLft.y = progressImageSrc->height/2 - crpPrms->cropHeight/2;

		pntBtRgt.x = progressImageSrc->width/2 + crpPrms->cropWidth/2;
		pntBtRgt.y = progressImageSrc->height/2 + crpPrms->cropHeight/2;

		// Copy original
		ROIRect.x = pntUpLft.x;
		ROIRect.y = pntUpLft.y;
		ROIRect.width = crpPrms->cropWidth;
		ROIRect.height = crpPrms->cropHeight;
		cvSetImageROI( progressImageDst, ROIRect );

		// Copy cropped image to matrix
		uchar* imgData = (uchar*) (croppedImgs->data.ptr + imgCount * croppedImgs->step);
		img.data.ptr = (uchar*) imgData;

		cvCvtColor( progressImageDst, &img, CV_BGR2GRAY );

		cvResetImageROI( progressImageDst );
		imgCount++;

		if( (imgCount % 100) == 0 )
    {
			fprintf( stderr, "CROPPING IMAGES  : %3d%%                        \r",
				(int) ((100.0 * imgCount) / numAvailableImages) );
      fflush( stderr );
    }

		cvReleaseImage( &progressImageSrc );
		cvReleaseImage( &progressImageDst );
	}

	cvReleaseMat( &invTrnsfrm );

	fclose( inStream );

	return crpData;
}

void FindAlignmentTransform( CvMat* referenceSet, CvMat* featureSet, CvMat* transform )
{
	int j;

	if( referenceSet->cols != featureSet->cols )
	{
		printf( "Number of reference points are not equal to the one of the sample set.\n" );
		exit(0);
	}

	int numMarkers = featureSet->cols/2;

	// Find best translation and scale parameters
	CvMat* A = cvCreateMat(2*numMarkers,4,CV_64FC1);
	CvMat* u = cvCreateMat(2*numMarkers,1,CV_64FC1);
	CvMat* x = cvCreateMat(4,1,CV_64FC1);

	for (j=0; j < numMarkers; j++)
	{
		CV_MAT_ELEM( *A, double, 2*j, 0 ) = featureSet->data.db[j];
		CV_MAT_ELEM( *A, double, 2*j, 1 ) = -featureSet->data.db[j+numMarkers];
		CV_MAT_ELEM( *A, double, 2*j, 2 ) = 1;
		CV_MAT_ELEM( *A, double, 2*j, 3 ) = 0;

		CV_MAT_ELEM( *A, double, 2*j+1, 0 ) = featureSet->data.db[j+numMarkers];
		CV_MAT_ELEM( *A, double, 2*j+1, 1 ) = featureSet->data.db[j];
		CV_MAT_ELEM( *A, double, 2*j+1, 2 ) = 0;
		CV_MAT_ELEM( *A, double, 2*j+1, 3 ) = 1;

		CV_MAT_ELEM( *u, double, 2*j, 0 ) = referenceSet->data.db[j];
		CV_MAT_ELEM( *u, double, 2*j+1, 0 ) = referenceSet->data.db[j+numMarkers];
	}

	cvSolve( A, u, x, CV_SVD );

	double a = CV_MAT_ELEM( *x, double, 0, 0 );
	double b = CV_MAT_ELEM( *x, double, 1, 0 );
	double tx = CV_MAT_ELEM( *x, double, 2, 0 );
	double ty = CV_MAT_ELEM( *x, double, 3, 0 );

	CV_MAT_ELEM( *transform, double, 0, 0 ) = a;
	CV_MAT_ELEM( *transform, double, 0, 1 ) = -b;
	CV_MAT_ELEM( *transform, double, 0, 2 ) = tx;
	CV_MAT_ELEM( *transform, double, 1, 0 ) = b;
	CV_MAT_ELEM( *transform, double, 1, 1 ) = a;
	CV_MAT_ELEM( *transform, double, 1, 2 ) = ty;
	CV_MAT_ELEM( *transform, double, 2, 0 ) = 0;
	CV_MAT_ELEM( *transform, double, 2, 1 ) = 0;
	CV_MAT_ELEM( *transform, double, 2, 2 ) = 1;

	cvReleaseMat(&A);
	cvReleaseMat(&u);
	cvReleaseMat(&x);
}

void AlignShapes( CvMat* averageShape, CvMat* transformedPoints, CvMat* transformArray )
{
	int i, j;
	int numMarkers = transformedPoints->cols/2;
	int numShapes = transformedPoints->rows;
	double xnew, ynew, scale;
	CvMat rowVec;

  CvMat rowTrnsfrmMat = cvMat( 3, 3, CV_64FC1, NULL );

	CvMat* transform = cvCreateMat(3,3,CV_64FC1);
	CvMat* temp			 = cvCreateMat(3,3,CV_64FC1);

	CvMat* newAverageShape = cvCreateMat( 1, averageShape->cols, CV_64FC1 );
	cvSetZero( newAverageShape );

	for (i = 0; i < numShapes; i++)
	{
		cvGetRow( transformedPoints, &rowVec, i );

		FindAlignmentTransform( averageShape, &rowVec, transform );

		// Save new coordinates
		double a = CV_MAT_ELEM( *transform, double, 0, 0 );
		double b = CV_MAT_ELEM( *transform, double, 1, 0 );
		double tx = CV_MAT_ELEM( *transform, double, 0, 2 );
		double ty = CV_MAT_ELEM( *transform, double, 1, 2 );
		for (j=0; j < numMarkers; j++)
		{
			xnew = a*CV_MAT_ELEM( *transformedPoints, double, i, j )
					 - b*CV_MAT_ELEM( *transformedPoints, double, i, j+numMarkers ) + tx;

			ynew = b*CV_MAT_ELEM( *transformedPoints, double, i, j )
					 + a*CV_MAT_ELEM( *transformedPoints, double, i, j+numMarkers ) + ty;

			CV_MAT_ELEM( *transformedPoints, double, i, j ) = xnew;
			CV_MAT_ELEM( *transformedPoints, double, i, j+numMarkers ) = ynew;
		}

		// Project transformed shape into the tangent space 
		cvGetRow( transformedPoints, &rowVec, i );
		scale = 1./cvDotProduct( &rowVec, averageShape );
		cvScale( &rowVec, &rowVec, scale );
		cvAdd( &rowVec, newAverageShape, newAverageShape );

		// Extract transform matrix
		uchar* trnsfrmData = (uchar*) (transformArray->data.ptr + i * transformArray->step);
		rowTrnsfrmMat.data.ptr = (uchar*) trnsfrmData;

		// Update transform array
		cvMatMul( transform, &rowTrnsfrmMat, temp );
		cvCopy( temp, &rowTrnsfrmMat );
	}

	for (i = 0; i < numMarkers; i++)
	{
		newAverageShape->data.db[i] /= numShapes;
		newAverageShape->data.db[i+numMarkers] /= numShapes;
	}

	cvCopy( newAverageShape, averageShape );

	cvReleaseMat( &transform );
	cvReleaseMat( &temp );
}

void FindSimilarityTransformation( CvMat* referenceSet, CvMat* featureSet, CvMat* transform )
{
	int j;
	double Sx, Sy;
	double Sxy;
	double Sxp, Syp;
	double Sxx, Syy;
	double Sxxp, Syyp;
	double Sxyp, Syxp;
	double a, b, tx, ty;

	if( referenceSet->cols != featureSet->cols )
	{
		printf( "Number of reference points are not equal to the one of the sample set." );
		exit(0);
	}

	int numPoints = referenceSet->cols/2;

	Sx = 0; Sy = 0;
	Sxp = 0.; Syp = 0.;

	for (j=0; j < numPoints; j++)
	{
		Sx += featureSet->data.db[j];
		Sy += featureSet->data.db[j+numPoints];

		Sxp += referenceSet->data.db[j];
		Syp += referenceSet->data.db[j+numPoints];
	}

	Sx /= numPoints;
	Sy /= numPoints;

	// Make center of gravity of the feature set at the origin
	for (j=0; j < numPoints; j++)
	{
		featureSet->data.db[j] -= Sx;
		featureSet->data.db[j+numPoints] -= Sy;
	}

	Sxp /= numPoints;
	Syp /= numPoints;

	Sxx = 0.; Syy = 0.;
	Sxy = 0.;
	Sxxp = 0.; Syyp = 0.;
	Sxyp = 0.; Syxp = 0.;
	for (j=0; j < numPoints; j++)
	{
		Sxx += featureSet->data.db[j] * featureSet->data.db[j];
		Syy += featureSet->data.db[j+numPoints] * featureSet->data.db[j+numPoints];

		Sxy += featureSet->data.db[j] * featureSet->data.db[j+numPoints];

		Sxxp += featureSet->data.db[j] * referenceSet->data.db[j];
		Syyp += featureSet->data.db[j+numPoints] * referenceSet->data.db[j+numPoints];

		Sxyp += featureSet->data.db[j] * referenceSet->data.db[j+numPoints];
		Syxp += featureSet->data.db[j+numPoints] * referenceSet->data.db[j];
	}

	Sxx /= numPoints;
	Syy /= numPoints;

	Sxy /= numPoints;

	Sxxp /= numPoints;
	Syyp /= numPoints;

	Sxyp /= numPoints;
	Syxp /= numPoints;

	a = (Sxxp + Syyp)/(Sxx + Syy);
	b = (Sxyp - Syxp)/(Sxx + Syy);
	tx = Sxp - (a*Sx - b*Sy);
	ty = Syp - (b*Sx + a*Sy);

	CV_MAT_ELEM( *transform, float, 0, 0 ) = a;
	CV_MAT_ELEM( *transform, float, 0, 1 ) = -b;
	CV_MAT_ELEM( *transform, float, 0, 2 ) = tx;
	CV_MAT_ELEM( *transform, float, 1, 0 ) = b;
	CV_MAT_ELEM( *transform, float, 1, 1 ) = a;
	CV_MAT_ELEM( *transform, float, 1, 2 ) = ty;

	// Set values of the feature set to the original values
	for (j=0; j < numPoints; j++)
	{
		featureSet->data.db[j] += Sx;
		featureSet->data.db[j+numPoints] += Sy;
	}
}

void FindAffineTransformation( CvMat* referenceSet, CvMat* featureSet, CvMat* transform )
{
	int j;
	double Sx, Sy;
	double Sxy;
	double Sxp, Syp;
	double Sxx, Syy;
	double Sxxp, Syyp;
	double Sxyp, Syxp;
	double delta;
	double a, b, c, d, tx, ty;

	if( referenceSet->cols != featureSet->cols )
	{
		printf( "Number of reference points are not equal to the one of the sample set." );
		exit(0);
	}

	int numPoints = referenceSet->cols/2;

	Sx = 0; Sy = 0;
	Sxp = 0.; Syp = 0.;

	for (j=0; j < numPoints; j++)
	{
		Sx += featureSet->data.db[j];
		Sy += featureSet->data.db[j+numPoints];

		Sxp += referenceSet->data.db[j];
		Syp += referenceSet->data.db[j+numPoints];
	}

	Sx /= numPoints;
	Sy /= numPoints;

	// Make center of gravity of the feature set at the origin
	for (j=0; j < numPoints; j++)
	{
		featureSet->data.db[j] -= Sx;
		featureSet->data.db[j+numPoints] -= Sy;
	}

	Sxp /= numPoints;
	Syp /= numPoints;

	Sxx = 0.; Syy = 0.;
	Sxy = 0.;
	Sxxp = 0.; Syyp = 0.;
	Sxyp = 0.; Syxp = 0.;
	for (j=0; j < numPoints; j++)
	{
		Sxx += featureSet->data.db[j] * featureSet->data.db[j];
		Syy += featureSet->data.db[j+numPoints] * featureSet->data.db[j+numPoints];

		Sxy += featureSet->data.db[j] * featureSet->data.db[j+numPoints];

		Sxxp += featureSet->data.db[j] * referenceSet->data.db[j];
		Syyp += featureSet->data.db[j+numPoints] * referenceSet->data.db[j+numPoints];

		Sxyp += featureSet->data.db[j] * referenceSet->data.db[j+numPoints];
		Syxp += featureSet->data.db[j+numPoints] * referenceSet->data.db[j];
	}

	Sxx /= numPoints;
	Syy /= numPoints;

	Sxy /= numPoints;

	Sxxp /= numPoints;
	Syyp /= numPoints;

	Sxyp /= numPoints;
	Syxp /= numPoints;

	delta = Sxx*Syy - Sxy*Sxy;

	if ( delta != 0 )
	{
		a = (Sxxp*Syy - Syxp*Sxy)/delta;
		b = (Sxx*Syxp - Sxxp*Sxy)/delta;
		c = (Sxyp*Syy - Syyp*Sxy)/delta;
		d = (Sxx*Syyp - Sxyp*Sxy)/delta;
		tx = Sxp -(a*Sx + b*Sy);
		ty = Syp -(c*Sx + d*Sy);

		CV_MAT_ELEM( *transform, float, 0, 0 ) = a;
		CV_MAT_ELEM( *transform, float, 0, 1 ) = b;
		CV_MAT_ELEM( *transform, float, 0, 2 ) = tx;
		CV_MAT_ELEM( *transform, float, 1, 0 ) = c;
		CV_MAT_ELEM( *transform, float, 1, 1 ) = d;
		CV_MAT_ELEM( *transform, float, 1, 2 ) = ty;
	}
	else
	{
		a = (Sxxp + Syyp)/(Sxx + Syy);
		b = (Sxyp - Syxp)/(Sxx + Syy);
		tx = Sxp - (a*Sx - b*Sy);
		ty = Syp - (b*Sx + a*Sy);

		CV_MAT_ELEM( *transform, float, 0, 0 ) = a;
		CV_MAT_ELEM( *transform, float, 0, 1 ) = -b;
		CV_MAT_ELEM( *transform, float, 0, 2 ) = tx;
		CV_MAT_ELEM( *transform, float, 1, 0 ) = b;
		CV_MAT_ELEM( *transform, float, 1, 1 ) = a;
		CV_MAT_ELEM( *transform, float, 1, 2 ) = ty;
	}

	// Set values of the feature set to the original values
	for (j=0; j < numPoints; j++)
	{
		featureSet->data.db[j] += Sx;
		featureSet->data.db[j+numPoints] += Sy;
	}

}

bool FindReferenceFaceTemplate4Procrustes( CropParams* crpPrms, CvMat* markerData, CvMat* referenceMarkers, CvMat* refInvTrnsfrm )
{
	if ( markerData == NULL )
	{
		return false;
	}

	if ( referenceMarkers->cols != markerData->cols )
	{
		return false;
	}

	int i, j;
	int numMarkers = markerData->cols/2;
	int numAlignmentPoints = 2;
	CvMat rowVec;

	CvMat* featureSet = cvCreateMat(1, 2*numMarkers, CV_64FC1 );
	CvMat* poinSetRef = cvCreateMat(1, 2*numAlignmentPoints, CV_64FC1 );
	CvMat* poinSetSample = cvCreateMat(1, 2*numAlignmentPoints, CV_64FC1 );
	CvMat* transformation = cvCreateMat(2, 3, CV_32FC1);

	cvSetZero( referenceMarkers );

	poinSetRef->data.db[0] = crpPrms->tmpMrkrsInfo.points[0].x; poinSetRef->data.db[0+numAlignmentPoints] = crpPrms->tmpMrkrsInfo.points[0].y;
	poinSetRef->data.db[1] = crpPrms->tmpMrkrsInfo.points[1].x; poinSetRef->data.db[1+numAlignmentPoints] = crpPrms->tmpMrkrsInfo.points[1].y;

	for (i=0; i < markerData->rows; i++)
	{
		cvGetRow( markerData, &rowVec, i );
		for (j=0; j < numMarkers; j++)
		{
			featureSet->data.db[j] = (double) rowVec.data.db[j];
			featureSet->data.db[j+numMarkers] = (float) rowVec.data.db[j+numMarkers];
		}

		// Take eye centers as point set coordinates to be transformed
		poinSetSample->data.db[0] = featureSet->data.db[crpPrms->tmpMrkrsInfo.markerIdx[0]];
		poinSetSample->data.db[0+numAlignmentPoints] = featureSet->data.db[crpPrms->tmpMrkrsInfo.markerIdx[0]+numMarkers];
		poinSetSample->data.db[1] = featureSet->data.db[crpPrms->tmpMrkrsInfo.markerIdx[1]];
		poinSetSample->data.db[1+numAlignmentPoints] = featureSet->data.db[crpPrms->tmpMrkrsInfo.markerIdx[1]+numMarkers];

		FindSimilarityTransformation( poinSetRef, poinSetSample, transformation );

		// Save new coordinates
		double a = CV_MAT_ELEM( *transformation, float, 0, 0 );
		double b = CV_MAT_ELEM( *transformation, float, 1, 0 );

		double tx = CV_MAT_ELEM( *transformation, float, 0, 2 );
		double ty = CV_MAT_ELEM( *transformation, float, 1, 2 );

		for (j=0; j < numMarkers; j++)
		{
			double xnew = a*featureSet->data.db[j] - b*featureSet->data.db[j+numMarkers] + tx;
			double ynew = b*featureSet->data.db[j] + a*featureSet->data.db[j+numMarkers] + ty;

			referenceMarkers->data.db[j] += xnew;
			referenceMarkers->data.db[j+numMarkers] += ynew;
		}

	}

	double meanX = 0;
	double meanY = 0;
	double scale = 1;
	for (j=0; j < numMarkers; j++)
	{
		referenceMarkers->data.db[j] /= markerData->rows;
		referenceMarkers->data.db[j+numMarkers] /= markerData->rows;

		meanX += referenceMarkers->data.db[j];
		meanY += referenceMarkers->data.db[j+numMarkers];
	}
	meanX /= numMarkers;
	meanY /= numMarkers;

	// Store original reference marker positions
	CvMat* refMarkersOrg = cvCloneMat( referenceMarkers );

	// Make COG of reference template points origin
	for (j=0; j < numMarkers; j++)
	{
		referenceMarkers->data.db[j] -= meanX;
		referenceMarkers->data.db[j+numMarkers] -= meanY;
	}
	scale = sqrt( cvDotProduct( referenceMarkers, referenceMarkers ) );

	// Scale it so that |Xref| = 1;
	for (j=0; j < numMarkers; j++)
	{
		referenceMarkers->data.db[j] /= scale;
		referenceMarkers->data.db[j+numMarkers] /= scale;
	}

	//FindAlignmentTransform( refMarkersOrg, referenceMarkers, refInvTrnsfrm );
	CV_MAT_ELEM( *refInvTrnsfrm, double, 0, 0 ) = scale;
	CV_MAT_ELEM( *refInvTrnsfrm, double, 0, 1 ) = 0;
	CV_MAT_ELEM( *refInvTrnsfrm, double, 0, 2 ) = meanX;
	CV_MAT_ELEM( *refInvTrnsfrm, double, 1, 0 ) = 0;
	CV_MAT_ELEM( *refInvTrnsfrm, double, 1, 1 ) = scale;
	CV_MAT_ELEM( *refInvTrnsfrm, double, 1, 2 ) = meanY;
	CV_MAT_ELEM( *refInvTrnsfrm, double, 2, 0 ) = 0;
	CV_MAT_ELEM( *refInvTrnsfrm, double, 2, 1 ) = 0;
	CV_MAT_ELEM( *refInvTrnsfrm, double, 2, 2 ) = 1;

	cvReleaseMat( &featureSet );
	cvReleaseMat( &poinSetRef );
	cvReleaseMat( &poinSetSample );
	cvReleaseMat( &transformation );
	cvReleaseMat( &refMarkersOrg );

	return true;
}

bool SaveCroppedImages( CroppedImagesData* croppedData, CropParams* crpPrms )
{
	int i;
	int numSavedImgs = 0;
	char fileLocation[MAX_PATH];
	FILE* inStream = NULL;

	fprintf( stderr, "SAVING CROPPED IMAGES...                          \r" );
	fflush(stderr);

	int dimension = GetInputDimension( crpPrms->maskImg );

	CvSize imgSize = cvSize( crpPrms->cropWidth, crpPrms->cropHeight );

	CvMat imgMat = cvMat( crpPrms->cropHeight, crpPrms->cropWidth, CV_8UC1, NULL );
	CvMat* imgVector = cvCreateMat( 1, dimension, CV_64FC1 );

	IplImage* img = cvCreateImage( imgSize, IPL_DEPTH_8U, 1 );

	CvMat* croppedImgs = croppedData->croppedImgs;

	for ( i=0; i < croppedImgs->rows; i++)
	{
		// Save original image pixel data
		uchar* imgData = (uchar*) (croppedImgs->data.ptr + i * croppedImgs->step);
		imgMat.data.ptr = (uchar*) imgData;


		sprintf( fileLocation, "%s/%s.%s",crpPrms->outputLocation, croppedData->fileName[i].c_str(), crpPrms->imgType );
		cvCopy( &imgMat, img );

		// Normalize images
		GetNormalizedImageVector( img, imgVector, crpPrms->maskImg );

		// Convert vector to image
		GetNormalizedImage( imgVector, img, crpPrms->maskImg );

 		cvSaveImage( fileLocation, img );

		numSavedImgs++;
	}

	cvReleaseImage( &img );
	return true;
}

CvMat* LoadCroppedImages( const char* fileName )
{
	FILE* inStream = fopen( fileName, "rb" );
	if(!inStream)
	{
		printf( "Cannot open data file %s for writing.\n", fileName );
		return false;
	}

	int i;
	int numSavedImgs = 0;
	int imgTypeLabel = 0;
	int cropWidth = 0;
	int cropHeight = 0;

	fread( &numSavedImgs, sizeof(int), 1, inStream );
	fread( &cropWidth, sizeof(int), 1, inStream );
	fread( &cropHeight, sizeof(int), 1, inStream );

	int numPixels = cropWidth*cropHeight;

	CvMat* croppedImgs = cvCreateMat( numSavedImgs, numPixels, CV_8UC1 );

	for ( i=0; i < croppedImgs->rows; i++)
	{
		// Save original image pixel data
		uchar* rowPtr = (uchar*) (croppedImgs->data.ptr + i * croppedImgs->step);
    fread( rowPtr, sizeof(uchar), croppedImgs->step, inStream );

		numSavedImgs++;
	}

  fclose( inStream );

	return croppedImgs;
}

CvMat* Procrustes( CvMat* markerData, CropParams* crpPrms )
{
	int i, j;
	int numMrkrs = markerData->cols/2;
	int numImgs = markerData->rows;
	CvMat rowVec;
	double meanX, meanY;

	if ( markerData == NULL )
		return NULL;

	fprintf( stderr, "COMPUTING ALIGNMENT TRANSFORMATIONS...            \r" );
  fflush( stderr );

  CvMat rowTrnsfrmMat = cvMat( 3, 3, CV_64FC1, NULL );

	CvMat* transformedPoints = cvCloneMat( markerData );
	CvMat* transform = cvCreateMat(3,3,CV_64FC1);
	refInvTrnsfrm = cvCreateMat(3,3,CV_64FC1);
	referenceMarkers = cvCreateMat( 1, markerData->cols, CV_64FC1 );

	// Marker points transform array for each image
	CvMat* transformArray = cvCreateMat( numImgs, 3*3, CV_64FC1 );
  rowTrnsfrmMat = cvMat( 3, 3, CV_64FC1, NULL );
	for (i=0; i < numImgs; i++)
	{
		uchar* trnsfrmData = (uchar*) (transformArray->data.ptr + i * transformArray->step);
		rowTrnsfrmMat.data.ptr = (uchar*) trnsfrmData;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 0, 0 ) = 1;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 0, 1 ) = 0;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 0, 2 ) = 0;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 1, 0 ) = 0;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 1, 1 ) = 1;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 1, 2 ) = 0;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 2, 0 ) = 0;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 2, 1 ) = 0;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 2, 2 ) = 1;
	}

	FindReferenceFaceTemplate4Procrustes( crpPrms, transformedPoints, referenceMarkers, refInvTrnsfrm );

	// Translate each example so that its center of gravity at the origin
	for (i=0; i < numImgs; i++)
	{
		cvGetRow( transformedPoints, &rowVec, i );
		meanX = 0;
		meanY = 0;
		for ( j=0; j < numMrkrs; j++)
		{
			meanX += rowVec.data.db[j];
			meanY += rowVec.data.db[j+numMrkrs];
		}
		meanX /= numMrkrs;
		meanY /= numMrkrs;

		for ( j=0; j < numMrkrs; j++)
		{
			rowVec.data.db[j] -= meanX;
			rowVec.data.db[j+numMrkrs] -= meanY;
		}

		// Extract transform matrix
		uchar* trnsfrmData = (uchar*) (transformArray->data.ptr + i * transformArray->step);
		rowTrnsfrmMat.data.ptr = (uchar*) trnsfrmData;

		// Save transformation
		CV_MAT_ELEM( rowTrnsfrmMat, double, 0, 0 ) = 1;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 0, 1 ) = 0;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 0, 2 ) = -meanX;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 1, 0 ) = 0;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 1, 1 ) = 1;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 1, 2 ) = -meanY;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 2, 0 ) = 0;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 2, 1 ) = 0;
		CV_MAT_ELEM( rowTrnsfrmMat, double, 2, 2 ) = 1;
	}

	CvMat* prevAverageShape = cvCloneMat( referenceMarkers );
	averageShape = cvCloneMat( referenceMarkers );

	double delta = 0;
	double deltaSum = 0;
	double threshold = 1e-6;

	int loopCount = 0;
	do
	{
		// Align each shape to the average shape and update the average shape and the transformation array
		AlignShapes( averageShape, transformedPoints, transformArray );

		// Align new average shape with the reference frame
		FindAlignmentTransform( referenceMarkers, averageShape, transform );

		// Transform average shape
		for (j=0; j < numMrkrs; j++)
		{
			double a = CV_MAT_ELEM( *transform, double, 0, 0 );
			double b = CV_MAT_ELEM( *transform, double, 1, 0 );
			double tx = CV_MAT_ELEM( *transform, double, 0, 2 );
			double ty = CV_MAT_ELEM( *transform, double, 1, 2 );

			double xnew = a*averageShape->data.db[j] - b*averageShape->data.db[j+numMrkrs] + tx;
			double ynew = b*averageShape->data.db[j] + a*averageShape->data.db[j+numMrkrs] + ty;

			averageShape->data.db[j] = xnew;
			averageShape->data.db[j+numMrkrs] = ynew;
		}

		// Scale aveage shape so that it is unit norm.
		MakeUnitNorm( averageShape );

		// Compute error between current and previous average shape
		deltaSum = 0.;
		for (j=0; j < numMrkrs; j++)
		{
			delta = averageShape->data.db[j] - prevAverageShape->data.db[j];
			deltaSum += delta*delta;

			delta = averageShape->data.db[j+numMrkrs] - prevAverageShape->data.db[j+numMrkrs];
			deltaSum += delta*delta;
		}

		// Save previous average shape
    cvCopy( averageShape, prevAverageShape );

		loopCount++;
	}
	while ( deltaSum > threshold );

	cvReleaseMat( &transform );
	cvReleaseMat( &transformedPoints );
	cvReleaseMat( &prevAverageShape );

	return transformArray;
}

#define MAX_GRAY_LEVEL	256

void HistEq(IplImage* src, IplImage* dst, IplImage* mask)
{
  int i, x, y;
  float histVals[MAX_GRAY_LEVEL];
  float mappedGrayVals[MAX_GRAY_LEVEL];
  double numPixels;
  uchar pixelVal, maskVal;
  float val;

  numPixels = src->width*src->height;

  // Intialize gray values
  for (i=0; i < MAX_GRAY_LEVEL; i++)
    histVals[i] = 0.F;

  // Find number of the pixel levels in the image
  for (x=0; x < src->width; x++)
    {
      for (y=0; y < src->height; y++)
	{
	  maskVal = ((uchar*)(mask->imageData + mask->widthStep*y))[x];
	  if (maskVal != 0)
	    {
	      pixelVal = ((uchar*)(src->imageData + src->widthStep*y))[x];
	      histVals[pixelVal]++;
	    }
	}
    }

  // Loop through original gray levels
  val = 0.F;
  for (i=0; i < MAX_GRAY_LEVEL; i++)
    {
      val += histVals[i]/numPixels;
      mappedGrayVals[i] = (MAX_GRAY_LEVEL-1)*val + 0.5F;
    }

  // Write the mapped gray values to the destination image pixels
  for (x=0; x < dst->width; x++)
    {
      for (y=0; y < dst->height; y++)
	{
	  maskVal = ((uchar*)(mask->imageData + mask->widthStep*y))[x];
	  if (maskVal != 0)
	    {
	      pixelVal = ((uchar*)(src->imageData + src->widthStep*y))[x];
	      ((uchar*)(dst->imageData + dst->widthStep*y))[x] =
		mappedGrayVals[pixelVal];
	    }
	  else
	    {
	      ((uchar*)(dst->imageData + dst->widthStep*y))[x] = 0;
	    }
	}
    }
}

void  GetNormalizedImageVector(IplImage* srcImg, CvMat* imgVector, IplImage* mskImg)
{
	int i, x, y;
	int pixelCount;
	uchar pixelVal, maskVal;
	double pixelSum, pixelSumSq, pixelSumSqRoot;

	// Show source image
	IplImage* histEqImg = cvCreateImage(cvSize(srcImg->width,srcImg->height), IPL_DEPTH_8U, 1);

	// Histogram equalize source image
	HistEq( srcImg, histEqImg, mskImg );		

	// Write the gray values to the destination image vector
	pixelCount = 0;
	pixelSum = 0;
	for (y=0; y < histEqImg->height; y++)
	{
			for (x=0; x < histEqImg->width; x++)
			{
				maskVal = ((uchar*)(mskImg->imageData + mskImg->widthStep*y))[x];
				if (maskVal != 0)
				{
					pixelVal = ((uchar*)(histEqImg->imageData + histEqImg->widthStep*y))[x];
					imgVector->data.db[pixelCount++] = pixelVal;
					pixelSum += pixelVal;
				}
			}
	}
	pixelSum /= (double) imgVector->cols;

#if DEBUG_RECOGNIZER_CODE
	// Show image vector
	IplImage* tmpImg = cvCreateImage(cvSize(histEqImg->width,histEqImg->height), IPL_DEPTH_8U, 1);
	pixelCount = 0;
	for (y=0; y < tmpImg->height; y++)
	{
		for (x=0; x < tmpImg->width; x++)
		{
			maskVal = ((uchar*)(mskImg->imageData + mskImg->widthStep*y))[x];
			if (maskVal != 0)
			{
					((uchar*)(tmpImg->imageData + tmpImg->widthStep*y))[x] = imgVector->data.db[pixelCount++];
			}
			else
			{
					((uchar*)(tmpImg->imageData + tmpImg->widthStep*y))[x] = (uchar) 0;
			}
		}
	}

	cvNamedWindow("window1",CV_WINDOW_AUTOSIZE);
	cvShowImage("window1",tmpImg);
	cvWaitKey();
	cvDestroyWindow( "window1" );

	cvReleaseImage( &tmpImg );
#endif

	// Make zero mean imgVector
	pixelSumSq = 0;
	for (i=0; i < imgVector->cols; i++)
	{
			imgVector->data.db[i] -= pixelSum;
			pixelSumSq += imgVector->data.db[i]*imgVector->data.db[i];
	}
	pixelSumSq /= (double) imgVector->cols;
	pixelSumSqRoot = sqrt(pixelSumSq);

	// Make unit variance imgVector
	for (i=0; i < imgVector->cols; i++)
	{
		imgVector->data.db[i] /= pixelSumSqRoot;
	}

	cvReleaseImage( &histEqImg );
}

void  GetImageVector(IplImage* srcImg, CvMat* imgVector, IplImage* mskImg)
{
	int i, x, y;
	int pixelCount;
	uchar pixelVal, maskVal;
	double pixelSum, pixelSumSq, pixelSumSqRoot;

	// Write the gray values to the destination image vector
	pixelCount = 0;
	pixelSum = 0;
	for (y=0; y < srcImg->height; y++)
	{
			for (x=0; x < srcImg->width; x++)
			{
				maskVal = ((uchar*)(mskImg->imageData + mskImg->widthStep*y))[x];
				if (maskVal != 0)
				{
					pixelVal = ((uchar*)(srcImg->imageData + srcImg->widthStep*y))[x];
					imgVector->data.db[pixelCount++] = pixelVal;
					pixelSum += pixelVal;
				}
			}
	}
	pixelSum /= (double) imgVector->cols;

#if DEBUG_RECOGNIZER_CODE
	// Show image vector
	IplImage* tmpImg = cvCreateImage(cvSize(srcImg->width,srcImg->height), IPL_DEPTH_8U, 1);
	pixelCount = 0;
	for (y=0; y < tmpImg->height; y++)
	{
		for (x=0; x < tmpImg->width; x++)
		{
			maskVal = ((uchar*)(mskImg->imageData + mskImg->widthStep*y))[x];
			if (maskVal != 0)
			{
					((uchar*)(tmpImg->imageData + tmpImg->widthStep*y))[x] = imgVector->data.db[pixelCount++];
			}
			else
			{
					((uchar*)(tmpImg->imageData + tmpImg->widthStep*y))[x] = (uchar) 0;
			}
		}
	}

	cvNamedWindow("window1",CV_WINDOW_AUTOSIZE);
	cvShowImage("window1",tmpImg);
	cvWaitKey();
	cvDestroyWindow( "window1" );

	cvReleaseImage( &tmpImg );
#endif

	// Make zero mean imgVector
	pixelSumSq = 0;
	for (i=0; i < imgVector->cols; i++)
	{
			imgVector->data.db[i] -= pixelSum;
			pixelSumSq += imgVector->data.db[i]*imgVector->data.db[i];
	}
	pixelSumSq /= (double) imgVector->cols;
	pixelSumSqRoot = sqrt(pixelSumSq);

	// Make unit variance imgVector
	for (i=0; i < imgVector->cols; i++)
	{
		imgVector->data.db[i] /= pixelSumSqRoot;
	}
}

void  GetNormalizedImage( CvMat* imgVector, IplImage* normalizedImg, IplImage* mskImg )
{
	int i, x, y;
	double maxVal = 0;
	double minVal = 0;

	// Find maximum and minimum values of image vector
	for (i=0; i < imgVector->cols; i++)
	{
		if ( (imgVector->data.db[i] < minVal) || (i==0) )
			minVal = imgVector->data.db[i];

		if ( (imgVector->data.db[i] > maxVal) || (i==0) )
			maxVal = imgVector->data.db[i];
	}

	// Compute Linear scaling factors
	double scale = (255-0)/(maxVal-minVal);
	double shift = -scale*minVal;

	// Linearly scale the vector values and put them to valid pixels
	int pixelCount = 0;
	for (y=0; y < normalizedImg->height; y++)
	{
		for (x=0; x < normalizedImg->width; x++)
		{
			uchar pixelVal = 0;
			uchar maskVal = ((uchar*)(mskImg->imageData + mskImg->widthStep*y))[x];
			if (maskVal != 0)
			{
				pixelVal = (uchar) (scale*imgVector->data.db[pixelCount++]+shift);
			}
      ((uchar*)(normalizedImg->imageData + normalizedImg->widthStep*y))[x] = pixelVal;
		}
	}

	//cvNamedWindow("window1",CV_WINDOW_AUTOSIZE);
	//cvShowImage("window1",normalizedImg);
	//cvWaitKey();
	//cvDestroyWindow( "window1" );
}


int GetInputDimension(IplImage* maskImage)
{
  int x, y;
  int numValidPixels = 0;
  uchar maskVal;

  for (y = 0; y < maskImage->height; y++)
    for (x = 0; x < maskImage->width; x++)
      {
	maskVal = ((uchar*)(maskImage->imageData +
			    maskImage->widthStep * y))[x];
	if (maskVal != 0)
	  numValidPixels++;
      }

  return numValidPixels;
}

void MakeUnitNorm(CvMat *x)
{
  double euc_norm2, scale_fact;

  euc_norm2 = cvDotProduct(x,x);
  scale_fact = 1.0/(sqrt(euc_norm2)+DBL_MIN);
  cvScale(x,x,scale_fact);
}

static int CmpFunc(const void*	_a, const void*	_b, void*)
{
  int a = *((int*)_a);
  int b = *((int*)_b);
  return ((a < b) ? -1 : ((a > b) ? 1:0));
}

void ShowVectorImage( CvMat* imgVector, CvSize imgSize, std::string maskImageFile )
{
  // Show image test vector
  int i, x, y, pixelCount;
  double maxVal, minVal, vecVal;
  IplImage* tmpImg = cvCreateImage( imgSize, IPL_DEPTH_8U, 1);
	IplImage* mskImg = cvLoadImage( maskImageFile.c_str(), 0);

  maxVal = minVal = imgVector->data.db[0];
  for (i=0; i < imgVector->cols; i++)
    {
      vecVal = imgVector->data.db[i];
      if (vecVal < minVal)
	minVal = vecVal;
      if (vecVal > maxVal)
	maxVal = vecVal;
    }

  pixelCount = 0;
  for (y=0; y < tmpImg->height; y++)
  {
    for (x=0; x < tmpImg->width; x++)
		{
			uchar maskVal = ((uchar*)(mskImg->imageData + mskImg->widthStep*y))[x];
			if (maskVal != 0)
			{
				((uchar*)(tmpImg->imageData + tmpImg->widthStep*y))[x] = (uchar)((255./(maxVal-minVal))*(imgVector->data.db[pixelCount++]-minVal));
			}
			else
			{
				((uchar*)(tmpImg->imageData + tmpImg->widthStep*y))[x] = (uchar) 0;
			}
		}
  }

  cvNamedWindow("Show_Window",CV_WINDOW_AUTOSIZE);
  cvShowImage("Show_Window",tmpImg);
  cvWaitKey();
  cvDestroyWindow("Show_Window");

  cvReleaseImage(&tmpImg);
  cvReleaseImage(&mskImg);
}

// comparison function to be used to sort by word length
bool IsLessThan(const double &v1, const double &v2)
{
  return v1 < v2;
}
