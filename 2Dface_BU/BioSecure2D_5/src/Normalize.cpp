/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <time.h>
#include <float.h>
#include <math.h>
#include <limits.h>

#include <cv.h>
#include <highgui.h>

#include "IV_Common.h"

// Global variables
extern CvMat* averageShape;
extern CvMat* referenceMarkers;
extern CvMat* markerData;
extern CvMat* transformArray;
extern CvMat* refInvTrnsfrm;

char* showWindow = "ShowWindow";

int main( int argc, char* argv[] )
{
	int i, len;
	int outType = 0;
	int numClassLabels = 1;
	CropParams crpPrms;
	CvMat* croppedImgs = NULL;
	char imageType[MAX_PATH];
	char imageStorageFolder[MAX_PATH];
	char outputLocation[MAX_PATH];
	char markersFileName[MAX_PATH];
	char maskImgFileName[MAX_PATH];
	char stringBuffer[MAX_PATH];

  if( argc == 1 )
  {
    printf( "Usage: %s\n  -in <Crop_Parameters_File>", argv[0] );

    return 0;
  }

  for( i = 1; i < argc; i++ )
  {
    if( !strcmp( argv[i], "-in" ) )
    {
			crpPrms.prmsFileName = argv[++i];
    }
	}

	// Read cascade information file
	FILE* crpPrmsFile = fopen( crpPrms.prmsFileName, "rb" );
	if ( crpPrmsFile == NULL )
	{
    printf( "FAILED TO READ CROP PARAMETERS INFORMATION FILE %s\n", crpPrms.prmsFileName );
		return 0;
	}

	int paramsCount = 0;
	while ( !feof(crpPrmsFile) )
	{
		fgets( stringBuffer, MAX_PATH, crpPrmsFile );
		if ( (stringBuffer[0] == 13) && (stringBuffer[1] == 10) )
			continue;

		len = strlen( stringBuffer ); 
		if ( (strncmp( stringBuffer, "//", 2 ) == 0) || (len == 0) )
			continue;

		switch ( paramsCount )
		{
		case 0:
			sscanf( stringBuffer, "%d %d\n", &crpPrms.cropWidth, &crpPrms.cropHeight );
			break;
		case 1:
			sscanf( stringBuffer, "%f %f %d %f %f %d\n", &crpPrms.tmpMrkrsInfo.points[0].x, &crpPrms.tmpMrkrsInfo.points[0].y, &crpPrms.tmpMrkrsInfo.markerIdx[0],
																									 &crpPrms.tmpMrkrsInfo.points[1].x, &crpPrms.tmpMrkrsInfo.points[1].y, &crpPrms.tmpMrkrsInfo.markerIdx[1] );
			break;
		case 2:
			len = strlen( stringBuffer );
			if ( (stringBuffer[len-2] == 13) && (stringBuffer[len-1] == 10) )
				stringBuffer[len-2] = '\0';
			strcpy( markersFileName, stringBuffer );
			crpPrms.markersFileName = markersFileName;
			break;
		case 3:
			len = strlen( stringBuffer );
			if ( (stringBuffer[len-2] == 13) && (stringBuffer[len-1] == 10) )
				stringBuffer[len-2] = '\0';
			strcpy( imageStorageFolder, stringBuffer );
			crpPrms.imageStorageFolder = imageStorageFolder;
			break;
		case 4:
			len = strlen( stringBuffer );
			if ( (stringBuffer[len-2] == 13) && (stringBuffer[len-1] == 10) )
				stringBuffer[len-2] = '\0';
			strcpy( imageType, stringBuffer );
			crpPrms.imgType = imageType;
			break;
		case 5:
			len = strlen( stringBuffer );
			if ( (stringBuffer[len-2] == 13) && (stringBuffer[len-1] == 10) )
				stringBuffer[len-2] = '\0';
			strcpy( outputLocation, stringBuffer );
			crpPrms.outputLocation = outputLocation;
			break;
		case 6:
			len = strlen( stringBuffer );
			if ( (stringBuffer[len-2] == 13) && (stringBuffer[len-1] == 10) )
				stringBuffer[len-2] = '\0';
			strcpy( maskImgFileName, stringBuffer );
			crpPrms.maskImgFileName = maskImgFileName;
			crpPrms.maskImg = cvLoadImage( crpPrms.maskImgFileName, 0);
			break;
		default:
			break;
		}

		paramsCount++;
	}

	markerData = ReadMarkerData( &crpPrms );
	transformArray = Procrustes( markerData, &crpPrms );
	CroppedImagesData* croppedImgsData = CreatePositiveCroppedImages( &crpPrms, markerData, transformArray );
	SaveCroppedImages( croppedImgsData, &crpPrms );
	printf( "FINISHED CROPPING...                              \r" );

	// Release memory
	cvReleaseMat( &markerData );
	cvReleaseMat( &transformArray );
	cvReleaseMat( &refInvTrnsfrm );
	cvReleaseMat( &referenceMarkers );
	cvReleaseMat( &averageShape );
	cvReleaseMat( &croppedImgsData->croppedImgs );
	cvReleaseImage( &crpPrms.maskImg );
	croppedImgsData->fileName.clear();
	delete croppedImgsData;

	return 0;
}

