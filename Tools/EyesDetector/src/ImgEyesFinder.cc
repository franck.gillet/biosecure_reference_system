#include <stdio.h>
#include "cv.h"
#include "highgui.h"
#include "eyes.h"

#define MAX_LENGTH 512

int main (int argc, char **argv) { 
 
  char* list = (char*)malloc(MAX_LENGTH*sizeof(char));
  FILE* flist;
  char* filename = (char*)malloc(MAX_LENGTH*sizeof(char));
  char* eyes = (char*)malloc(MAX_LENGTH*sizeof(char));
  FILE* feyes;
  
  IplImage* frame;
  CvSize size;
  float ratio = 2.0;
  IplImage* cp_frame;
  
  int n, i;
  int xL, yL, xR, yR;
  
  if (argc < 4) {
	  fprintf(stderr, "USAGE: %s list nImg eyes\n", argv[0]);
	  fprintf(stderr, "list   : List of images\n");
	  fprintf(stderr, "nImg   : Number of images in list\n");
	  fprintf(stderr, "eyes   : List of eyes coordinates\n");	  
	  return -1;	  
  }
  
  strcpy(list, argv[1]);    // List of image
  n = atoi(argv[2]);        // Number of images in list
  strcpy(eyes, argv[3]);    // List of eyes coordinates
  
  // OPEN list FILE
  if (!( flist = fopen(list, "r") )) {
	  fprintf(stderr, "Error: Cannot read list %s.\n", list);
	  return -1;  
  }
  
    // OPEN eyes FILE
  if (!( feyes = fopen(eyes, "w") )) {
	  fprintf(stderr, "Error: Cannot write eyes coordinates in %s.\n", eyes);
	  return -1;	  
  }
 
  
  for(i=0; i<n; i++) {
	  fscanf(flist, "%s\n", filename);
	  frame = cvLoadImage(filename, -1);
	  size = cvGetSize(frame);
	  /*sub-sampling of the image only if its size is too big */
	  if (size.width*size.height>350000){
	  cp_frame = cvCreateImage(cvSize(floor(size.width/ratio),
				          floor(size.height/ratio)),
	                           IPL_DEPTH_8U, 3);
	  cvResize(frame, cp_frame, CV_INTER_LINEAR)	  ;
	  locateeyes(cp_frame, &xL, &yL, &xR, &yR);
	  
	  cvReleaseImage(&frame); cvReleaseImage(&cp_frame);
	  fprintf(feyes, "%s %d %d %d %d\n",
		  filename,
		  (int)floor(xR*ratio), (int)floor(yR*ratio),
		  (int)floor(xL*ratio), (int)floor(yL*ratio));
	  }
	  else {
          locateeyes(frame, &xL, &yL, &xR, &yR);
	  
	  cvReleaseImage(&frame);
	  fprintf(feyes, "%s %d %d %d %d\n",
		  filename,
		  (int)floor(xR), (int)floor(yR),
		  (int)floor(xL), (int)floor(yL));
	  }
	  
	  fflush(feyes);
  }
  fclose(flist); fclose(feyes);
  
  return 1;
}
