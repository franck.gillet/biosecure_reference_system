#include "cv.h"
#include "eyefinderBinary.h"
#include <Magick++.h>
using namespace Magick;

int locateeyes(IplImage* frame, int* xL, int* yL, int* xR, int* yR) {
	CvSize size;
	char* raw;
	char* raw_init;
	int i,j;
	CvScalar s;
	int max_face_size;
	
	MPEyeFinder *eyefinder;
	eyefinder = new MPEyeFinderBinary();
	
	size = cvGetSize(frame);
	raw = (char*)malloc(3*size.height*size.width*sizeof(char));
	raw_init = raw;
	for (i=0;i<size.height;i++)
	{
		for (j=0;j<size.width;j++)
		{
			s = cvGet2D(frame, i, j);
			raw[0] = (char)(s.val[2]);
			raw[1] = (char)(s.val[1]);
			raw[2] = (char)(s.val[0]);
			raw += 3;
		}
	}
	raw = raw_init;
	Image image(size.width, size.height, "RGB", CharPixel, raw );
	free(raw);

	// allocate our buffer and write the pixels to it. 
	RImage<float> pixels(image.columns(), image.rows());
	eyefinder->initStream(pixels.width, pixels.height);
      
	image.quantizeColors ( 256 ); // convert to 256 colors
	image.quantize( ); // convert to grayscale
	image.write(0,0, image.columns(), image.rows(), "I", FloatPixel, pixels.array);
	VisualObject faces;
      
	double numwindows;
	// Search nframes times, using ROI
	numwindows = eyefinder->findEyes(pixels, faces, 0.3, wt_avg);
      
	max_face_size = 0; *xL = 0; *yL = 0; *xR = 0; *yR = 0;
	if(faces.size() != 0)
	{
		list<VisualObject *>::iterator face = faces.begin();
		int facenumber=0;
		for( ; face != faces.end(); ++face) 
		{
			FaceObject *fo = static_cast<FaceObject*>(*face);
			if (fo->xSize > max_face_size)
			{
				max_face_size = (int)floor(fo->xSize);
				*xL = (int)floor(fo->eyes.xLeft);
				*yL = (int)floor(fo->eyes.yLeft);
				*xR = (int)floor(fo->eyes.xRight);
				*yR = (int)floor(fo->eyes.yRight);
			}
		}
	}	
	
	delete(eyefinder);
	return 1;
}
