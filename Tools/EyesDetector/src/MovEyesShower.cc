#include <stdio.h>
#include "cv.h"
#include "highgui.h"
#define MAX_LENGTH 512


int main (int argc, char **argv)
{  
	char* filename = (char*)malloc(MAX_LENGTH*sizeof(char));
	char* eyes     = (char*)malloc(MAX_LENGTH*sizeof(char));
	FILE* feyes;
  
	CvCapture* capture;
  
	IplImage* frame;
	IplImage* cp_frame;
	CvSize size;
  
	float ratio = 2.0;
	CvSize cp_size;
    
	int n = 0;  
	int xR, yR, xL, yL;

	if (argc < 3) {
		fprintf(stderr, "USAGE: %s movie eyes\n", argv[0]);
		fprintf(stderr, "movie: Video file\n");
		fprintf(stderr, "eyes : Eyes coordinates\n");	  
		return -1;	  
	}
  
	strcpy(filename, argv[1]); // Video file
	strcpy(eyes, argv[2]);     // Eyes coordinates  
  
	if (!(capture = cvCaptureFromFile( filename )))
	{
		fprintf(stderr, "Error: Cannot read video %s.\n", filename);
		return -1;
	}
  
	if (!( feyes = fopen(eyes, "r") ))
	{
		fprintf(stderr, "Error: Cannot read in %s.\n", eyes);
		return -1;
	}
	  
	cvNamedWindow(filename, 1);
  	cvMoveWindow(filename, 0, 0);
  
  	while(frame=cvQueryFrame(capture)) {
		size = cvGetSize(frame);
		cp_frame = cvCreateImage(size, IPL_DEPTH_8U, 3);
		cvCopy(frame, cp_frame, NULL);
		
		fscanf(feyes, "%d %d %d %d %d",
		       &n, &xL, &yL, &xR, &yR);
		cvLine(cp_frame, cvPoint(xR, yR), cvPoint(xL, yL),
		       CV_RGB(0,250,0), 1, 8);
		cvRectangle(cp_frame, cvPoint(xR-3,yR-3), cvPoint(xR+3, yR+3),
			    CV_RGB(255,0,0), 2, 8, 0);
		cvRectangle(cp_frame, cvPoint(xL-3,yL-3), cvPoint(xL+3, yL+3),
			    CV_RGB(0,0,255), 2, 8, 0);
		cvWaitKey(35);
		cvShowImage(filename, cp_frame);
		cvReleaseImage(&cp_frame);
	}
	fclose(feyes);
	cvReleaseCapture(&capture);
	return 1;
}
