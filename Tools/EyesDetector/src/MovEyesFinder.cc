#include <stdio.h>
#include "cv.h"
#include "highgui.h"
#include "eyes.h"

#define MAX_LENGTH 512

int main (int argc, char **argv)
{  
  char* filename = (char*)malloc(MAX_LENGTH*sizeof(char));
  char* eyes     = (char*)malloc(MAX_LENGTH*sizeof(char));
  FILE* feyes;
  
  CvCapture* capture;
  
  IplImage* frame;
  IplImage* cp_frame;
  CvSize size;
  
  float ratio = 2.0;
  CvSize cp_size;
    
  int n = 0;  
  int xR, yR, xL, yL;

  if (argc < 3) {
	  fprintf(stderr, "USAGE: %s movie eyes\n", argv[0]);
	  fprintf(stderr, "movie: Video file\n");
	  fprintf(stderr, "eyes : Eyes coordinates\n");	  
	  return -1;	  
  }
  
  strcpy(filename, argv[1]); // Video file
  strcpy(eyes, argv[2]);     // Eyes coordinates  
  
  if (!(capture = cvCaptureFromFile( filename )))
    {
      fprintf(stderr, "Error: Cannot read video %s.\n", filename);
      return -1;
    }
  
  if (!( feyes = fopen(eyes, "w") ))
    {
      fprintf(stderr, "Error: Cannot write in %s.\n", eyes);
      return -1;
    }

  while(frame=cvQueryFrame(capture))
    {
	size = cvGetSize(frame);	
	cp_size = cvSize(size.width/ratio, size.height/ratio);
	cp_frame = cvCreateImage(cp_size, IPL_DEPTH_8U, 3);
	cvResize(frame, cp_frame, CV_INTER_LINEAR);
	locateeyes(cp_frame, &xL, &yL, &xR, &yR);
	cvReleaseImage(&cp_frame);
	fprintf(feyes, "%04d %d %d %d %d\n", n, 
		(int)floor(ratio*xL), (int)floor(ratio*yL),
		(int)floor(ratio*xR), (int)floor(ratio*yR));
	fflush(feyes);
	n++;
    }
  fclose(feyes);
  cvReleaseCapture(&capture);
  return 1;
}
