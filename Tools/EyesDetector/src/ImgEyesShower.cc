#include <stdio.h>
#include "cv.h"
#include "highgui.h"

#define MAX_LENGTH 512

int main (int argc, char **argv) { 
  char* filename = (char*)malloc(MAX_LENGTH*sizeof(char));
  char* eyes = (char*)malloc(MAX_LENGTH*sizeof(char));
  FILE* feyes;
  float ratio = 2.0;
  IplImage* frame;
  CvSize size;
  IplImage* cp_frame;
  
  int n, i;
  int xL, yL, xR, yR;

  if (argc < 3) {
	  fprintf(stderr, "USAGE: %s eyes nImg\n", argv[0]);
	  fprintf(stderr, "eyes   : List of eyes coordinates\n");	  
	  fprintf(stderr, "nImg   : Number of images in list\n");
	  return -1;	  
  }
  
  n = atoi(argv[2]);        // Number of images in list
  strcpy(eyes, argv[1]);    // List of eyes coordinates

  // OPEN eyes FILE
  if (!( feyes = fopen(eyes, "r") )) {
	  fprintf(stderr, "Error: Cannot read eyes coordinates in %s.\n", eyes);
	  return -1;	  
  }
  
  cvNamedWindow(eyes, 1);
  cvMoveWindow(eyes, 0, 0);
  
  for(i=0; i<n; i++) {
	  fscanf(feyes, "%s %d %d %d %d\n",
		 filename, &xR, &yR, &xL, &yL); 
	  frame = cvLoadImage(filename, -1);
	  size = cvGetSize(frame);

	  if (size.width*size.height<350000){
	  cp_frame = cvCreateImage(size, IPL_DEPTH_8U, 3);
	  cvCopy(frame, cp_frame, NULL);
	  }
	  else{
	  cp_frame = cvCreateImage(cvSize(floor(size.width/ratio),
				          floor(size.height/ratio)),
	                           IPL_DEPTH_8U, 3);
	  cvResize(frame, cp_frame, CV_INTER_LINEAR);
	  xL=floor(xL/ratio);yL=floor(yL/ratio);xR=floor(xR/ratio);yR=floor(yR/ratio);
	  }
	  cvLine(cp_frame, cvPoint(xR, yR), cvPoint(xL, yL),
		 CV_RGB(0,250,0), 1, 8);
	  cvRectangle(cp_frame, cvPoint(xR-3,yR-3), cvPoint(xR+3, yR+3),
		      CV_RGB(255,0,0), 2, 8, 0);
	  cvRectangle(cp_frame, cvPoint(xL-3,yL-3), cvPoint(xL+3, yL+3),
		      CV_RGB(0,0,255), 2, 8, 0);
	  cvShowImage(eyes, cp_frame);
	  cvWaitKey(0);
	  cvReleaseImage(&frame); cvReleaseImage(&cp_frame);
  }
  fclose(feyes);

  return 1;
}
