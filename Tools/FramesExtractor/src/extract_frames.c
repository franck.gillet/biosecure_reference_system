/*
 *  save_frames.c
 *
 *  
 *  This source code aims at extracting frames from video
 *
 */

#include "play_video.h"

#define MAX_LENGTH 512

int main (int argc, const char * argv[]) {
	
	char* buffer = (char*)malloc(MAX_LENGTH*sizeof(char));
	// video file name
	char* filename = (char*)malloc(MAX_LENGTH*sizeof(char));
	// output folders
	char* foldernameTMP = (char*)malloc(MAX_LENGTH*sizeof(char));
	char* foldername = (char*)malloc(MAX_LENGTH*sizeof(char));
	
	
	// video capturing structure
	CvCapture* capture;
	int first_time = 1;
	
	// captured frame
	IplImage* frame;
	// its copy
	IplImage* copy_frame;
	// its size
	CvSize size;
	
	// Usage
	if(argc < 3)
	{
		fprintf(stderr, "USAGE            %s save_frames \n", argv[0]);
		fprintf(stderr, "video_file       path to the video file\n");
		fprintf(stderr, "folder_name       path to the output folder\n");
		return -1;
	}
	
	// Read path to the video
	strcpy(filename, argv[1]);

	// Read path to the output video
	strcpy(foldernameTMP, argv[2]);
	
	
	// Initializes video capture
	if (!(capture = cvCaptureFromFile( filename )))
	{
		fprintf(stderr, "Error: Cannot read video %s.\n", filename);
		return -1;
	}
	free(filename);
	
	int ci=1;

	// Grabs frames from the video
	while( frame = cvQueryFrame( capture ) )
	{	
		// Read size of current frame
		size = cvSize(frame->width,frame->height);
		// Copy the current frame
		copy_frame = cvCreateImage(size, frame->depth, frame->nChannels);
		cvCopy(frame, copy_frame, NULL);
		
		
		// Save current frame
		strcpy(foldername, foldernameTMP);
		sprintf(buffer, "/%06d",ci);
		strcat(foldername, buffer);
		strcat(foldername, ".JPEG");puts(foldername);
		cvSaveImage( foldername , copy_frame);
		ci=ci+1;

		// Trick to allow image refreshing at the beginning
		if (first_time)
		{
			first_time = 0;
			cvWaitKey(1000);
		}
		
		// Release copy
		cvReleaseImage(&copy_frame);
	}
	
	// Destroy display window
	cvDestroyAllWindows();
	// Releases the capture
	cvReleaseCapture( &capture );
	
	
	return 1;
}
