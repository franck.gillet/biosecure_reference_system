function [score_fusion]=fusion_banca(train_client_face,train_client_speech,train_imp_face, train_imp_speech, test_face, test_speech)

% function: fusion_banca
%
% DESCRIPTION:
% The function normalizes score s as follows:
% (s-m)/(M-m)
% where M is considered as maximum and m as minimum of score s.
% M and m are calculated thaks to client and impostor distribution in the
% training set.
%
% INPUTS:
% train_client_face: file containing the training genuine scores for face 
% modality
% train_client_speech: file containing the training genuine scores for
% speech modality
% train_imp_face: file containing the training impostor scores for face 
% modality
% train_imp_speech: file containing the training impostor scores for
% speech modality
% test_face: file containing the face scores used for fusion
% test_speech: file containing the speech scores used for fusion
% imposteurs: vector of impostor scores
%
% OUTPUT:
% score_fusion: score obtained by the fusion of test_face and test_speech
%
% 20/04/2008

% input files loading
% training files
[train_cf]=textread(train_client_face,'%f');
[train_cs]=textread(train_client_speech,'%f');
[train_if]=textread(train_imp_face,'%f');
[train_is]=textread(train_imp_speech,'%f');
% test files
[test_f]=textread(test_face,'%f');
[test_s]=textread(test_speech,'%f');

% calculation of the minimum of face distribution scores
min_face = mean(-train_if)-2*std(-train_if);
% calculation of the maximum of face distribution scores
max_face = mean(-train_cf)+2*std(-train_cf);
% calculation of the minimum of speech distribution scores
min_speech = mean(train_is)-2*std(train_is);
% calculation of the maximum of speech distribution scores
max_speech = mean(train_cs)+2*std(train_cs);

% normalization of test face scores
s_face=length(test_f);
for i=1:s_face
    if (test_f(i)>=max_face)
        score_face(i)=1;
    elseif (test_f(i)<=min_face)
        score_face(i)=0;
    else
        score_face(i)=(test_f(i)-min_face)/(max_face-min_face);
    end
end

% normalization of test speech scores
s_speech=length(test_s);
for i=1:s_speech
    if (test_s(i)>=max_speech)
        score_speech(i)=1;
    elseif (test_s(i)<=min_speech)
        score_speech(i)=0;
    else
        score_speech(i)=(test_s(i)-min_speech)/(max_speech-min_speech);
    end
end

% calculation of the fusion score
for i=1:s_face
    score_fusion(i)=(score_face(i)+score_speech(i))/2;
end

score_fusion=score_fusion';