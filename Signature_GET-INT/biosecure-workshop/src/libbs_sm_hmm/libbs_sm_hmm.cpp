/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <libbs_framework.h>


// TODO: module specific modifications start here
#include <chmm.h>
#include <cmemalc.h>
#include <bs_cclientmodel.h>

#define LAST_STATE_EXIT	true

char LastError[BS_MAX_ERROR];   // last error message of this module

int ParamSet=0;                 // becomes 1 if all parameters are valid


// return pointer to error string, nothing to do here
EXPORT char *smLastError(void)
{
    return LastError;
}


// validate and set parameters for later execution
EXPORT int smSetParameters(const char *smParamString)
{
    // everything fine
    ParamSet=1;
    return BS_OK;
}

EXPORT void smHmmTest(CHmm * hmm, CFeatureVector* ob,CResult *smResult)
{
	double similarityViterbiHam = 0;
	double similarityHMM = 0;

	if (ob->features != 0) // not inited yet
	{	

		hmm->normalizeObsParameters(ob);	// If the HMM did not use this functionality
											// there will have no problem.
		int * stSeq = NULL;
		int N = hmm->getStateNumber();
		int T = ob->features[0].num_feature_results; 
		
		
		// Verify that the test's signature is long enough
		// If not, algorithm cannot be applied
		if (T>2*N)
		{ 

			double LL = hmm->viterbi(ob, &stSeq, LAST_STATE_EXIT);

			double hamDis = 0;
			double dtwDis = 0;

			double meanLenth = 0;
			int k;
			for(k = 0; k < hmm->getObsNumber(); k++)
			{
				meanLenth += hmm->tbStSequenceLen[k];
				hamDis += hmm->computeViterbiHamDistance(stSeq, T, 
					hmm->tbStSequence[k], hmm->tbStSequenceLen[k]);	
			}
			CMemalc memalc;
			memalc.free_ivector(stSeq);
			meanLenth /= hmm->getObsNumber();
			hamDis /= hmm->getObsNumber();

			//similarityViterbiHam = exp(- hamDis * N / meanLenth);
			//similarityHMM = exp(-fabs(LL - hmm->getLLba()));
			smResult->scores[0] = exp(-fabs(LL - hmm->getLLba()));
			smResult->scores[1] = exp(- hamDis * N / meanLenth);
		}

	}
	else
	{
		//cout << "\nTest signature does not exist";
	}

	//return similarityHMM;
}

// execution of the matching functions based on the parameters
// and source data passed to smSetParameters
// smResult will be created by this function and returned on success
EXPORT int smExecute(CClientModel *smModel,const CFeatureSet *smTestData,CResult **smResult)
{
    if (ParamSet==0)
    {
        sprintf(LastError,"error in sm_hmm: parameters have not been set yet.\n");
        return BS_ERR_PARAM;
    }

    *smResult=new CResult;
    if (*smResult==0)
    {
        sprintf(LastError,"error in sm_hmm: memory allocation error.\n");
        return BS_ERR_MEM;
    }
	(*smResult)->num_scores = 2;
	(*smResult)->scores = (double*)bscalloc(sizeof(double),2);
	CHmm *hmm = (CHmm*)(smModel->getModel());

	if(hmm->trained)
	{
		// test model
		smHmmTest(hmm, smTestData->feat_segments->feat_vectors,*smResult);	
		(*smResult)->result_comments = 0;//"1: HMM's score, 2: Viterbi's score";
	}
	else
	{
		delete *smResult;
		*smResult = 0;
		return BS_ERR_INVALID;
	}

    return BS_OK;
}


// report information about module usage and capabilities depending on Type
EXPORT void smInfo(int Type)
{
    // TODO: module specific modifications start here
    // possible Types are defined in libbs_framework.h
    // not all Types have to be supported
    // multiple Types can be combined

    if ((Type&BS_INFO_NAME)!=0)
    {
        fprintf(stdout,"Module Name: HMM Training-Based Scoring\n");
    }

    if ((Type&BS_INFO_VERSION)!=0)
    {
        fprintf(stdout,"Module Version: 0.1\n");
    }

    if ((Type&BS_INFO_ABOUT)!=0)
    {
        fprintf(stdout,"Authors: GET-INT\n");
    }

    if ((Type&BS_INFO_PARAMETERS)!=0)
    {
        fprintf(stdout,"Module Parameters: none\n");
    }

    if ((Type&BS_INFO_CAPABILITIES)!=0)
    {
        fprintf(stdout,"This module calculates score based on HMM.\n");
    }
	
    // module specific modifications end here
}
