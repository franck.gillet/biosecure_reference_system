/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>

#include <libbs_framework.h>


// TODO: module specific modifications start here

// every module defines its own PARAM structure unless the module doesnt have any parameters
// this is an example structure:
typedef struct
{
    int    Param1;
    double Param2;
}
PARAM;

// module specific modifications end here

PARAM Param;                    // stores the parameters that were passed to feSetParameters

char LastError[BS_MAX_ERROR];   // last error message of this module

int ParamSet=0;                 // becomes 1 if all parameters are valid


// return pointer to error string, nothing to do here
EXPORT char *feLastError(void)
{
    return LastError;
}


// validate and set parameters for later execution
EXPORT int feSetParameters(const char *feParamString)
{
    ParamSet=0;

    // TODO: module specific modifications start here

    // check if parameter string has been passed
    // passing 0 could be optional for setting standard parameters
    // but in this example passing parameters is mandatory
    if (feParamString==0)
    {
        sprintf(LastError,"error in fe_<modulename>: parameters have to be set.\n");
        return BS_ERR_PARAM;
    }

    // parse parameters from string as defined in PARAM structure before
    if (sscanf(feParamString,"%i %lf",&Param.Param1,&Param.Param2)!=2)
    {
        sprintf(LastError,"error in fe_<modulename>: wrong parameter format.\n");
        return BS_ERR_PARAM;
    }

    // check validity of parameters here:
    // in this example parameters cannot be 0
    if (Param.Param1==0)
    {
        sprintf(LastError,"error in fe_<modulename>: parameter 1 cannot be 0.\n");
        return BS_ERR_PARAM;
    }

    if (Param.Param2==0)
    {
        sprintf(LastError,"error in fe_<modulename>: parameter 2 cannot be 0.\n");
        return BS_ERR_PARAM;
    }

    // module specific modifications end here

    // everything fine
    ParamSet=1;
    return BS_OK;
}


// execution of the feature extraction functions based on the parameters
// and source data passed to feSetParameters
// feFeatureSet will be created by this function and returned on success
EXPORT int feExecute(const CDataSet *feDataSet,CFeatureSet **feFeatureSet)
{
    if (ParamSet==0)
    {
        sprintf(LastError,"error in fe_<modulename>: parameters have not been set yet.\n");
        return BS_ERR_PARAM;
    }
    // TODO: module specific modifications start here

    // check if needed source data is available:
    // example: we check for existence of normalised data
    /*    if (feDataSet->data==0)
        {
            sprintf(LastError,"error in fe_<modulename>: normalised data not available.\n");
            return BS_ERR_SOURCE;
        }
    */
    // insert something useful here using data in DataSet and parameters in Param
    // store a valid pointer to the created featureset in feFeatureSet
    *feFeatureSet=new CFeatureSet;
    if (*feFeatureSet==0)
    {
        sprintf(LastError,"error in fe_<modulename>: memory allocation error.\n");
        return BS_ERR_MEM;
    }
    // add segments, vectors and featuredata the same way and fill in data
    // dont forget to clean up reserved memory on error condition
    // you should use: delete *feFeatureSet; *feFeatureSet=0;

    // module specific modifications end here
    return BS_OK;
}


// report information about module usage and capabilities depending on Type
EXPORT void feInfo(int Type)
{
    // TODO: module specific modifications start here
    // possible Types are defined in libbs_framework.h
    // not all Types have to be supported
    // multiple Types can be combined

    if ((Type&BS_INFO_NAME)!=0)
    {
        fprintf(stdout,"Module Name: Feature Extraction Template\n");
    }

    if ((Type&BS_INFO_VERSION)!=0)
    {
        fprintf(stdout,"Module Version: 0.8.15\n");
    }

    if ((Type&BS_INFO_ABOUT)!=0)
    {
        fprintf(stdout,"Authors: AMSL Magdeburg\n");
    }

    if ((Type&BS_INFO_PARAMETERS)!=0)
    {
        fprintf(stdout,"Module Parameters:\n");
        fprintf(stdout,"int Param1: example parameter 1, may not be 0\n");
        fprintf(stdout,"double Param2: example parameter 2, may not be 0\n");
    }

    if ((Type&BS_INFO_CAPABILITIES)!=0)
    {
        fprintf(stdout,"This module does nothing yet.\n");
    }

    // module specific modifications end here
}

