/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <string.h>

#include <libbs_framework.h>


// TODO: module specific modifications start here

// every module defines its own PARAM structure unless the module doesnt have any parameters
// for Parser modules one parameter always identifies the source data
// this is an example structure:
typedef struct
{
    char Source[1024];
}
PARAM;

// module specific modifications end here

PARAM Param;                    // stores the parameters that were passed to paSetParameters
                         
char LastError[BS_MAX_ERROR];   // last error message of this module

int ParamSet=0;                 // becomes 1 if all parameters are valid


// return pointer to error string, nothing to do here
EXPORT char *paLastError(void)
{
    return LastError;
}


// validate and set parameters for later execution
EXPORT int paSetParameters(const char *paParamString)
{
    ParamSet=0;

    // TODO: module specific modifications start here

    // check if parameter string has been passed
    // 0 pointer is not allowed in this example, ParamString must identify source dataset
    if (paParamString==0)
    {
        sprintf(LastError,"error in pa_<modulename>: parameters have to be set.\n");
        return BS_ERR_PARAM;
    }

    // parse parameters from string as defined in PARAM structure before
    strcpy(Param.Source,paParamString);
    /* check validity here
    {
        sprintf(LastError,"error in pa_<modulename>: wrong parameter format.\n");
        return BS_ERR_PARAM;
    }
    */

    // module specific modifications end here

    // everything fine
    ParamSet=1;
    return BS_OK;
}


// execution of the parser functions based on the parameters
// and source data passed to paSetParameters
EXPORT int paExecute(CDataSet **paDataSet,const char *paID)
{
    if (ParamSet==0)
    {
        sprintf(LastError,"error in pa_<modulename>: parameters have not been set yet.\n");
        return BS_ERR_PARAM;
    }
    if (paID==0)
    {
        sprintf(LastError,"error in pa_<modulename>: ID has to be set.\n");
        return BS_ERR_PARAM;    
    }
    // TODO: module specific modifications start here
    // insert something useful here using parameters in Param
    // store a valid pointer to the created dataset in paDataSet
    *paDataSet=new CDataSet;
    if (*paDataSet==0)
    {
        sprintf(LastError,"error in pa_<modulename>: memory allocation error.\n");
        return BS_ERR_MEM;
    }

    (*paDataSet)->device=new CDevice;
    if ((*paDataSet)->device==0)
    {
        delete *paDataSet; *paDataSet=0;
        sprintf(LastError,"error in pa_<modulename>: memory allocation error.\n");
        return BS_ERR_MEM;
    }

    (*paDataSet)->info=new CPData;
    if ((*paDataSet)->info==0)
    {
        delete *paDataSet; *paDataSet=0;
        sprintf(LastError,"error in pa_<modulename>: memory allocation error.\n");
        return BS_ERR_MEM;
    }

    // add sourcedata and textual content if needed and fill in data
    // dont forget to clean up reserved memory on error condition
    // you should use: delete *paDataSet; *paDataSet=0;;

    // module specific modifications end here
    return BS_OK;
}


// report information about module usage and capabilities depending on Type
EXPORT void paInfo(int Type)
{
    // TODO: module specific modifications start here
    // possible Types are defined in libbs_framework.h
    // not all Types have to be supported
    // multiple Types can be combined

    if ((Type&BS_INFO_NAME)!=0)
    {
        fprintf(stdout,"Module Name: Parser Template\n");
    }

    if ((Type&BS_INFO_VERSION)!=0)
    {
        fprintf(stdout,"Module Version: 0.8.15\n");
    }

    if ((Type&BS_INFO_ABOUT)!=0)
    {
        fprintf(stdout,"Authors: AMSL Magdeburg\n");
    }

    if ((Type&BS_INFO_PARAMETERS)!=0)
    {
        fprintf(stdout,"Module Parameters:\n");
        fprintf(stdout,"string Source: string identifying the source data\n");
    }

    if ((Type&BS_INFO_CAPABILITIES)!=0)
    {
        fprintf(stdout,"This module does nothing yet.\n");
    }

    // module specific modifications end here
}

