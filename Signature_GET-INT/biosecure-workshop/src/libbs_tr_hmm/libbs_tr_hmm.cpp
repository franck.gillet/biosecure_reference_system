/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <string.h>

#include <libbs_framework.h>


// TODO: module specific modifications start here
#include <chmm.h>
#include <cmemalc.h>
#include <bs_cclientmodel.h>

#define LAST_STATE_EXIT	true
#define N_VECTORS_GAUSSIAN		30	// nombre de gaussiennes par �tat, utiliser pour
									// chercher le nombre d'�tat optimal (d'apr�s BAO)
#define N_STATE_MIN				2

char LastError[BS_MAX_ERROR];   // last error message of this module

int ParamSet=0;                 // becomes 1 if all parameters are valid


// return pointer to error string, nothing to do here
EXPORT char *trLastError(void)
{
    return LastError;
}


// validate and set parameters for later execution
EXPORT int trSetParameters(const char *trParamString)
{    
    // everything fine
    ParamSet=1;
    return BS_OK;
}


// execution of the matching functions based on the parameters
// and source data passed to srSetParameters
// srResult will be created by this function and returned on success
EXPORT int trExecute(const CFeatureSet *trFeatureSet, CClientModel **trModel)
{
    if (ParamSet==0)
    {
        sprintf(LastError,"error in sr_<modulename>: parameters have not been set yet.\n");
        return BS_ERR_PARAM;
    }
    // TODO: module specific modifications start here
	(*trModel) = new CClientModel;
	// get number of training signatures
	int K = trFeatureSet->feat_segments->num_feature_vectors;
	// get the training observations
	CFeatureVector* obs = (CFeatureVector*) bscalloc(sizeof(CFeatureVector),K);

	int i, k;
	for (k = 0; k < K; k++)
	{
		int num_features = *(trFeatureSet->feat_segments->feat_vectors[k].num_features);
		obs[k].num_features = (int*) bsmalloc(sizeof(int));
		*(obs[k].num_features) = num_features;
		obs[k].features = (CFeatureData*) bscalloc(sizeof(CFeatureData), num_features);
		for (i = 0; i < num_features; i++)
		{
			int num_feature_results = trFeatureSet->feat_segments->feat_vectors[k].features[i].num_feature_results;
			obs[k].features[i].num_feature_results = num_feature_results;
			obs[k].features[i].feature_result = (double*) bscalloc(sizeof(double), num_feature_results);
			memcpy(obs[k].features[i].feature_result, trFeatureSet->feat_segments->feat_vectors[k].features[i].feature_result,num_feature_results*sizeof(double));
		}
	}
	
	double len = 0;
	int statesNumber, gaussiansNumber; // number of gaussiens and states
	// get the total number of observations
	for(k = 0; k < K; k++)
	{
		len += obs[k].features[0].num_feature_results;
	}
	// Compute the optimal number of states 
	gaussiansNumber = 4;
	statesNumber = int (len / N_VECTORS_GAUSSIAN / gaussiansNumber );

	if (statesNumber < N_STATE_MIN)
	{
		statesNumber = N_STATE_MIN;
	}

	// priorities of the HMM
	int transitionType = TRANS_LR;
	int covMatrixType = COV_DIAG;
	bool bHmmRandomInit = true;
	bool bParaNorm = PARAMETER_NORMALIZATION_YES;
	double paraStandardDiviation = 2;
	double endAccuracy = 0.01;		//we fix the number of iteration
	int endIterationNumber = 0;

	//create the HMM
	CHmm *hmm = new CHmm(statesNumber, gaussiansNumber, transitionType, covMatrixType,
		bHmmRandomInit, bParaNorm, paraStandardDiviation, endAccuracy, endIterationNumber);

	//assign training database
	hmm->assignObs(&obs, K); 

	// train model
	bool hmmOK = hmm->train();

	while(!hmmOK && (hmm->getStateNumber() > N_STATE_MIN))
	{
		hmm->setNStates(hmm->getStateNumber() - 1);
		hmmOK = hmm->train();
	}

	// free memories reserved by obs
	for (k = 0; k < K; k++)
	{
		int num_features = *(trFeatureSet->feat_segments->feat_vectors[k].num_features);
		bsfree(obs[k].num_features);
	
		for (i = 0; i < num_features; i++)
		{
			bsfree(obs[k].features[i].feature_result);
		}
		bsfree(obs[k].features);
	}
	bsfree(obs);

	(*trModel)->setModel(hmm);

    // module specific modifications end here
    return BS_OK;
}


// report information about module usage and capabilities depending on Type
EXPORT void trInfo(int Type)
{
    // TODO: module specific modifications start here
    // possible Types are defined in libbs_framework.h
    // not all Types have to be supported
    // multiple Types can be combined

    if ((Type&BS_INFO_NAME)!=0)
    {
        fprintf(stdout,"Module Name: Training module for HMM\n");
    }

    if ((Type&BS_INFO_VERSION)!=0)
    {
        fprintf(stdout,"Module Version: 0.1\n");
    }

    if ((Type&BS_INFO_ABOUT)!=0)
    {
        fprintf(stdout,"Authors: GET-INT\n");
    }

    if ((Type&BS_INFO_CAPABILITIES)!=0)
    {
        fprintf(stdout,"This module train a HMM model.\n");
    }

    // module specific modifications end here
}

