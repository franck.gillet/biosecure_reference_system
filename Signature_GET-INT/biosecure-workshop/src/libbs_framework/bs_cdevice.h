/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CDEVICE_H
#define BS_CDEVICE_H

#include <bs_dynlink.h>

#define TILT_UNKNOWN            -1      // non-standard tilt format
#define TILT_NAV                0       // no tilt available
#define TILT_VALUES             1       // tilt values available
                                        // tilt_x/ymin & tilt_x/ymax must be defined
                                        
#define TILT_ANGLE              2       // tilt as elevation/azimuth
                                        // azimuth_0/90 & elevation_0/90 must be defined

#define ROTATION_UNKNOWN        -1      // non-standard rotation format
#define ROTATION_NAV            0       // rotation not available
#define ROTATION_AV             1       // rotation available
                                        // rotation_0 and rotation_360 must be defined
                                        
#define FORCE_UNKNOWN           -1      // non-standard force format
#define FORCE_NAV               0       // force not available
#define FORCE_AV                1       // force available
                                        // force_min & force_max must be defined
                                        
#define RESOLUTION_UNKNOWN      -1      // resolution not known
#define DRIVERID_UNKNOWN        -1      // driver id unknown
#define FREQUENCY_UNKNOWN       -1      // sampling frequency unknown
                                     
class CDevice
{
public:
    char        *capture_device;        // capture device ID string, always static
    int         freq;                   // sampling frequency or FREQUENCY_UNKNOWN
    int         Xorigin;                // x axis origin, should be minimum data value if unknown
    int         Yorigin;                // y axis origin, should be minimum data value if unknown
    int         Xresolution;            // resolution of device in lpmm or RESOLUTION_UNKNOWN
    int         Yresolution;            // resolution of device in lpmm or RESOLUTION_UNKNOWN
    int         Xmax;                   // x axis maximum value, should be maximum data value if unknown
    int         Ymax;                   // y axis maximum value, should be maximum data value if unknown
    int         TiltFormat;             // format of tilt, allowed values:
                                        // TILT_UNKNOWN
                                        // TILT_NAV
                                        // TILT_VALUES
                                        // TILT_ANGLE
    int         RotationFormat;         // format of rotation, allowed values:
                                        // ROTATION_UNKNOWN
                                        // ROTATION_NAV
                                        // ROTATION_AV
    int         ForceFormat;            // format of force (pressure), allowed values:
                                        // FORCE_UNKNOWN
                                        // FORCE_NAV
                                        // FORCE_AV
    int         azimuth_0;              // azimuth value that represents 0° (north)
    int         azimuth_90;             // azimuth value that represents 90° (cw)
    int         elevation_0;            // elevation value that represents 0° (flat)
    int         elevation_90;           // elevation value that represents 90° (upright)
    int         tilt_xmin;              // Minimum x tilt value      
    int         tilt_xmax;              // Maximum x tilt value
    int         tilt_ymin;              // Minimum y tilt value        
    int         tilt_ymax;              // Maximum y tilt value
    int         rotation_0;             // rotation value that represents 0°
    int         rotation_360;           // rotation value that represents 360°
    int         force_min;              // force minimum value
    int         force_max;              // force maximum value
    int         Driver_ID;              // ID of OS driver, 0=WinTab,... or DRIVERID_UNKNOWN
    
    CDevice(){}
    ~CDevice(){}
    IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};

#endif
