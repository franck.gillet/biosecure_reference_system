/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CCHAIN_H
#define BS_CCHAIN_H

#include <bs_cmod.h>

class CChain
{
public:
    CModPA pa;
    CModPP *pp;
    CModFE fe;
    CModFS *fs;
	CModTR tr;
	CModFU fu;
    int ppcount,fscount;

    CChain(){}
    IMPORT_METHOD ~CChain();
    IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);

    IMPORT_METHOD int AddModulePA(const char *name,const char *paParamString);
    IMPORT_METHOD int AddModulePP(const char *name,const char *ppParamString);
    IMPORT_METHOD int AddModuleFE(const char *name,const char *feParamString);
    IMPORT_METHOD int AddModuleFS(const char *name,const char *fsParamString);
	IMPORT_METHOD int AddModuleTR(const char *name,const char *trParamString);
	IMPORT_METHOD int AddModuleFU(const char *name,const char *fuParamString);	
};

#endif
