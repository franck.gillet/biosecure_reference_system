/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_PARSER_H
#define BS_PARSER_H

#include <bs_types.h>
#include <bs_dynlink.h>
#include <bs_cdataset.h>
#include <bs_cfeatureset.h>
#include <bs_cresult.h>
#include <bs_error.h>
#include <bs_defines.h>
/*
 * Common parser for all data parser class. 
 * Cannot use instance of this class, use instances of its derived classes instead.
 */
class CParser
{
protected:
	CDataSet* pDataSet;
	CPData* pPData;
	CDevice* pDevice;
	CSourceData* pSource;
public:
//	non-virtual in virtual classes
//	CParser() {};
	virtual ~CParser() {};
	/*
	 * Pre-setting some variables for device
	 * Xorigin, Yorigin, Xmax, Ymax will be set by method parse
	 */
	IMPORT_METHOD int settingDevice(char* _capture_device = "WACOM INTUOS", 
									int _freq = FREQUENCY_UNKNOWN,
									int _Xresolution = 100,
									int _Yresolution = 100,
									int _TiltFormat = TILT_NAV,
									int _RotationFormat = ROTATION_NAV,
									int _ForceFormat = FORCE_AV,
									int _azimuth_0 = 0,
									int _azimuth_90 = 90,
									int _elevation_0 = 0,
									int _elevation_90 = 90,
									int _tilt_xmin = TILT_NAV,
									int _tilt_xmax = TILT_NAV,
									int _tilt_ymin = TILT_NAV,
									int _tilt_ymax = TILT_NAV,
									int _rotation_0 = ROTATION_NAV,
									int _rotation_360 = ROTATION_NAV,
									int _force_min = 0,
									int _force_max = 1023,
									int _Driver_ID = DRIVERID_UNKNOWN);
	/*
	 * Pre-setting some variables for writer's information
	 */
	IMPORT_METHOD int settingWriterInfor(int _test_id = 123456,
										 int _writing_hand = HAND_UNKNOWN,
										 int _age = AGE_UNKNOWN,
										 int _gender = GENDER_UNKNOWN);
	/*
	 * Pre-setting some variables for dataset
	 */
	IMPORT_METHOD int settingDataSet(int _tcapture = TIME_UNKNOWN,
									 int _genuine = GENUINE_YES,
									 int _semantic_class = SEMANTIC_SIGNATURE,
									 char* _textual_content = 0);
	/*
	 * Common interface to parse a database. Normally, user will just have to call this method
	 * @param paID : Database to be parsed
	 * @param presetting: false if user want to call settingDevice, settingWriterInfor, settingDataSet
	 * and set their parameters himself.
	 * @return : dataset parsed
	 */
	IMPORT_METHOD CDataSet* parse(const char *paID, bool presetting = true);
protected:
	/*
	 * This method will be called in the method parse. All deriving classes will have to reimplement this method.
	 * There are variables that cannot be set in settingDevice, settingWriterInfor and settingDataSet without parsing,
	 * so set them in this method in derived class.
	 */
	IMPORT_METHOD virtual int do_parse(const char* paID) = 0;
};

/*
 * SVC parser. Use instance of this class to parse database in SVC format
 */
class CParserSVC : public CParser
{
private:
	IMPORT_METHOD int do_parse(const char* paID);
};

/*
 * BIOMET parser. Use instance of this class to parse database in BIOMET format
 */
class CParserBiomet : public CParser
{
private:
	IMPORT_METHOD int do_parse(const char* paID);
};

/*
 * MCYT parser. Use instance of this class to parse database in BIOMET format
 */
class CParserMCYT : public CParser
{
private:
	IMPORT_METHOD int do_parse(const char* paID);
};

/*
 * BIOSECURE parser. Use instance of this class to parse database in BIOMET format
 */
class CParserBIOSECURE : public CParser
{
private:
	IMPORT_METHOD int do_parse(const char* paID);
};
#endif
