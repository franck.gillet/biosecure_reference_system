/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CDATASET_H
#define BS_CDATASET_H

#include <bs_dynlink.h>

#include <time.h>

#include <bs_cdata.h>
#include <bs_cdevice.h>
#include <bs_cpdata.h>
#include <bs_csourcedata.h>

#define NO_PROCESS              0       // data has not been pre-processed
#define PROCESSED               1       // data has been pre-processed

#define GENUINE_UNKNOWN		-1	// authencity is unknown
#define GENUINE_YES             0       // sample of the original authentic subject
#define GENUINE_BLIND           1       // forgery, generated with only textual knownledge
                                        // of the original's semantic
#define GENUINE_LOW             2       // forgery, generated with only textual and
                                        // structural knownledge of the original's semantic (blueprint)
#define GENUINE_HIGH            3       // forgery, generated with textual, structural
                                        // and dynamical knownledge of the original's semantic

#define SEMANTIC_UNKNOWN	-1	// semantic class unknown
#define SEMANTIC_SIGNATURE      0       // semantic classes for dataset
#define SEMANTIC_PSEUDONYM      1
#define SEMANTIC_PASSWORD       2
#define SEMANTIC_PIN            3

#define TIME_UNKNOWN            -1      // unknown time

class CDataSet
{
public:
    CPData          *info;              // Subject info class
    CDevice         *device;            // Capture device info class
    int             processed;          // NO_PROCESS
                                        // PROCESS
    int             num_packets;        // Number of packets captured
    time_t          tcapture;           // date & time of capture in ms since 1.1.1970 00:00:00 UTC
                                        // or TIME_UNKNOWN
    int             genuine;            // GENUINE_YES
                                        // GENUINE_BLIND
                                        // GENUINE_LOW
                                        // GENUINE_HIGH
                                        // GENUINE_UNKNOWN
    int             semantic_class;     // SEMANTIC_SIGNATURE
                                        // SEMANTIC_PSEUDONYM
                                        // SEMANTIC_PASSWORD
                                        // SEMANTIC_PIN
                                        // SEMANTIC_UNKNOWN
    char            *textual_content;   // textual content of data, always dynamic
    CSourceData     *sourcedata;        // Capture data class
    CData           *data;              // Normalized data class

    CDataSet(){}
    IMPORT_METHOD ~CDataSet();
    IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};

#endif
