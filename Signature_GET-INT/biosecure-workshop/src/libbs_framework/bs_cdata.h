/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CDATA_H
#define BS_CDATA_H

#include <bs_dynlink.h>

#ifndef TIP_NO_CONTACT
#define TIP_NO_CONTACT  0       // no tip contact
#define TIP_CONTACT     1       // tip contact
#endif

class CData
{
public:
    long        time;           // time offset of point in ms
    double      x;              // x position in cm
    double      y;              // y position in cm
    double      force;          // pen force (pressure), values from 0 to 1
    int         tip;            // NO_CONTACT=0, CONTACT=1
    int         button;         // barrel button status
    double      tiltx;          // x axis tilt from -1 to 1
    double      tilty;          // y axis tilt from -1 to 1
    double      azimuth;        // pen azimuth in degrees clockwise, 0=north
    double      elevation;      // pen elevation in degrees, 0=flat, 90=upright
    double      rotation;       // pen rotation in degrees clockwise

    CData(){}
    ~CData(){}
    IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};

#endif
