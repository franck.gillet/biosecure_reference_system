/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <bs_mem.h>
#include <bs_cchain.h>
#include <bs_error.h>
#include <bs_dynlink.h>


EXPORT_METHOD void* CChain::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD CChain::~CChain(void)
{
    int i;
    if (pa.handle!=0)
        pa.Unload();
    if (fe.handle!=0)
        fe.Unload();

    for (i=0;i<ppcount;i++)
        pp[i].Unload();
    for (i=0;i<fscount;i++)
        fs[i].Unload();

    if (pp!=0)
        bsfree(pp);
    if (fs!=0)
        bsfree(fs);
}

EXPORT_METHOD void CChain::operator delete(void *p)
{
    bsfree(p);
}

EXPORT_METHOD int CChain::AddModulePA(const char *name,const char *paParamString)
{
    int result;
    if (pa.Load(name)<0)
        return BS_ERR_LIB;
    result=pa.paSetParameters(paParamString);
    if (result<0)
    {
        fprintf(stderr,"%s\n",pa.paLastError());
        pa.Unload();
        return result;
    }
    return BS_OK;
}

EXPORT_METHOD int CChain::AddModulePP(const char *name,const char *ppParamString)
{
    CModPP *tmp;
    int result;
    
    tmp=(CModPP*)bsrealloc(pp,(ppcount+1)*sizeof(CModPP));
    if (tmp==0)
    {
        fprintf(stderr,"Not enough memory to load module.\n");
        return BS_ERR_MEM;
    }
    pp=tmp;
    pp[ppcount].handle=0;
    if (pp[ppcount].Load(name)<0)
    {
        pp=(CModPP*)bsrealloc(pp,ppcount*sizeof(CModPP));
        return BS_ERR_LIB;
    }

    result=pp[ppcount].ppSetParameters(ppParamString);
    if (result<0)
    {
        fprintf(stderr,"%s\n",pp[ppcount].ppLastError());
        pp[ppcount].Unload();
        pp=(CModPP*)bsrealloc(pp,ppcount*sizeof(CModPP));
        return result;
    }
    ppcount++;
    return BS_OK;
}

EXPORT_METHOD int CChain::AddModuleFE(const char *name,const char *feParamString)
{
    int result;
    if (fe.Load(name)<0)
        return BS_ERR_LIB;
    result=fe.feSetParameters(feParamString);
    if (result<0)
    {
        fprintf(stderr,"%s\n",fe.feLastError());
        fe.Unload();
        return result;
    }
    return BS_OK;
}

EXPORT_METHOD int CChain::AddModuleFS(const char *name,const char *fsParamString)
{
    CModFS *tmp;
    int result;

    tmp=(CModFS*)bsrealloc(fs,(fscount+1)*sizeof(CModFS));
    if (tmp==0)
    {
        fprintf(stderr,"Not enough memory to load module.\n");
        return BS_ERR_MEM;
    }
    fs=tmp;
    fs[fscount].handle=0;
    if (fs[fscount].Load(name)<0)
    {
        fs=(CModFS*)bsrealloc(fs,fscount*sizeof(CModFS));
        return BS_ERR_LIB;
    }

    result=fs[fscount].fsSetParameters(fsParamString);
    if (result<0)
    {
        fprintf(stderr,"%s\n",fs[fscount].fsLastError());
        fs[fscount].Unload();
        fs=(CModFS*)bsrealloc(fs,fscount*sizeof(CModFS));
        return result;
    }
    fscount++;
    return BS_OK;
}

EXPORT_METHOD int CChain::AddModuleTR(const char *name,const char *trParamString)
{
    int result;
    if (tr.Load(name)<0)
        return BS_ERR_LIB;
    result=tr.trSetParameters(trParamString);
    if (result<0)
    {
        fprintf(stderr,"%s\n",tr.trLastError());
        tr.Unload();
        return result;
    }
    return BS_OK;
}

EXPORT_METHOD int CChain::AddModuleFU(const char *name,const char *fuParamString)
{
    int result;
    if (fu.Load(name)<0)
        return BS_ERR_LIB;
    result=fu.fuSetParameters(fuParamString);
    if (result<0)
    {
        fprintf(stderr,"%s\n",fu.fuLastError());
        fu.Unload();
        return result;
    }
    return BS_OK;
}
