/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <bs_types.h>
#include <bs_dynlink.h>
#include <bs_cdataset.h>
#include <bs_cfeatureset.h>
#include <bs_cresult.h>
#include <bs_error.h>

EXPORT_METHOD void* CResult::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD CResult::~CResult(void)
{
    if (result_comments!=0)
        bsfree(result_comments);
	if (scores != 0)
		bsfree(scores);
	scores = 0;
	result_comments = 0;
}

EXPORT_METHOD void CResult::operator delete(void *p)
{
    bsfree(p);
}


EXPORT_METHOD void* CFeatureData::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD CFeatureData::~CFeatureData(void)
{
    if (num_feature_results!=RESULT_STATIC)
    {
        if (feature_result!=0)
        {
	    bsfree(feature_result);
            feature_result=0;
        }
    }
}

EXPORT_METHOD void CFeatureData::operator delete(void *p)
{
    bsfree(p);
}



EXPORT_METHOD void* CFeatureVector::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD CFeatureVector::~CFeatureVector(void)
{
    int i,n=0;

    if (num_features!=0)
    {
        for (i=0;i<num_dimensions;i++)
	    n+=num_features[i];
	bsfree(num_features);
	num_features=0;
    }
    if (features!=0)
    {
        for (i=0;i<n;i++)
            features[i].~CFeatureData();
        bsfree(features);
        features=0;
    }

}

EXPORT_METHOD void CFeatureVector::operator delete(void *p)
{
    bsfree(p);
}



EXPORT_METHOD void* CFeatureSegment::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD CFeatureSegment::~CFeatureSegment(void)
{
    int i;

    if (feat_vectors!=0)
    {

        for (i=0;i<num_feature_vectors;i++)
            feat_vectors[i].~CFeatureVector();
        bsfree(feat_vectors);
        feat_vectors=0;
    }

}

EXPORT_METHOD void CFeatureSegment::operator delete(void *p)
{
    bsfree(p);
}


EXPORT_METHOD void* CFeatureSet::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD CFeatureSet::~CFeatureSet(void)
{
    int i;

    if (feat_segments!=0)
    {

        for (i=0;i<num_feature_segments;i++)
            feat_segments[i].~CFeatureSegment();
        bsfree(feat_segments);
        feat_segments=0;
    }

}

EXPORT_METHOD void CFeatureSet::operator delete(void *p)
{
    bsfree(p);
}


EXPORT_METHOD void* CDataSet::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD CDataSet::~CDataSet(void)
{
    int i;

    if (info!=0)
    {
        delete info;
        info=0;
    }

    if (device!=0)
    {
        delete device;
        device=0;
    }

    if (textual_content!=0)
    {
        bsfree(textual_content);
        textual_content=0;
    }

    if (sourcedata!=0)
    {
        for (i=0;i<num_packets;i++) sourcedata[i].~CSourceData();
        bsfree(sourcedata);
        sourcedata=0;
    }

    if (data!=0)
    {
        for (i=0;i<num_packets;i++) data[i].~CData();
        bsfree(data);
        data=0;
    }
}

EXPORT_METHOD void CDataSet::operator delete(void *p)
{
    bsfree(p);
}



EXPORT_METHOD void* CDevice::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD void CDevice::operator delete(void *p)
{
    bsfree(p);
}


EXPORT_METHOD void* CData::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD void CData::operator delete(void *p)
{
    bsfree(p);
}


EXPORT_METHOD void* CPData::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD void CPData::operator delete(void *p)
{
    bsfree(p);
}


EXPORT_METHOD void* CSourceData::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD void CSourceData::operator delete(void *p)
{
    bsfree(p);
}

