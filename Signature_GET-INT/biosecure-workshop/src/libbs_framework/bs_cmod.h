/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CMOD_H
#define BS_CMOD_H

#include <bs_cdataset.h>
#include <bs_cfeatureset.h>
#include <bs_cresult.h>
#include <bs_dynlink.h>
#include <bs_defines.h>
#include <bs_cclientmodel.h>

class CModPA
{
public:
    void *handle;
    char *(*paLastError)(void);
    int (*paSetParameters)(const char *paParamString);
    int (*paExecute)(CDataSet **paDataSet,const char *paID);
    void (*paInfo)(int Type);
    char name[MAX_PATH];

    EXPORT_METHOD int Load(const char *name);
    EXPORT_METHOD void Unload(void);
};


class CModPP
{
public:
    void *handle;
    char *(*ppLastError)(void);
    int (*ppSetParameters)(const char *ppParamString);
    int (*ppExecute)(CDataSet *ppDataSet);
    void (*ppInfo)(int Type);
    char name[MAX_PATH];

    EXPORT_METHOD int Load(const char *name);
    EXPORT_METHOD void Unload(void);
};

class CModFE
{
public:
    void *handle;
    char *(*feLastError)(void);
    int (*feSetParameters)(const char *feParamString);
    int (*feExecute)(const CDataSet *feDataSet,CFeatureSet **feFeatureSet);
    void (*feInfo)(int Type);
    char name[MAX_PATH];

    EXPORT_METHOD int Load(const char *name);
    EXPORT_METHOD void Unload(void);
};

class CModFS
{
public:
    void *handle;
    char *(*fsLastError)(void);
    int (*fsSetParameters)(const char *fsParamString);
    int (*fsExecute)(CFeatureSet *fsDataSet);
    void (*fsInfo)(int Type);
    char name[MAX_PATH];

    EXPORT_METHOD int Load(const char *name);
    EXPORT_METHOD void Unload(void);
};

class CModSR
{
public:
    void *handle;
    char *(*srLastError)(void);
    int (*srSetParameters)(const char *srParamString);
    int (*srExecute)(const CFeatureSet *srReference,const CFeatureSet *srTestData,CResult **srResult);
    void (*srInfo)(int Type);
    char name[MAX_PATH];

    EXPORT_METHOD int Load(const char *name);
    EXPORT_METHOD void Unload(void);
};

class CModSM
{
public:
    void *handle;
    char *(*smLastError)(void);
    int (*smSetParameters)(const char *smParamString);
    int (*smExecute)(CClientModel *smModel,const CFeatureSet *smTestData,CResult **smResult);
    void (*smInfo)(int Type);
    char name[MAX_PATH];

    EXPORT_METHOD int Load(const char *name);
    EXPORT_METHOD void Unload(void);
};

class CModTR
{
public:
    void *handle;
    char *(*trLastError)(void);
    int (*trSetParameters)(const char *trParamString);
    int (*trExecute)(const CFeatureSet *trFeatureSet, CClientModel **trModel);
    void (*trInfo)(int Type);
    char name[MAX_PATH];

    EXPORT_METHOD int Load(const char *name);
    EXPORT_METHOD void Unload(void);
};

class CModFU
{
public:
    void *handle;
    char *(*fuLastError)(void);
    int (*fuSetParameters)(const char *fuParamString);
    int (*fuExecute)(CResult **fuFusionResult);
    void (*fuInfo)(int Type);
    char name[MAX_PATH];

    EXPORT_METHOD int Load(const char *name);
    EXPORT_METHOD void Unload(void);
};

#endif
