/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <bs_dynlink.h>
#include <bs_cmod.h>
#include <bs_error.h>

void *bsLoadLibSys(const char *prefix,const char *file)
{
    char tmp[4096];

#ifdef _MSC_VER

    return 0;

#else

    sprintf(tmp,"libbs_%s_%s/libbs_%s_%s.so",prefix,file,prefix,file);
#endif

    return dlopen(tmp,RTLD_LAZY);
}

void *bsLoadLib(const char *prefix,const char *file)
{
    char tmp[4096];

#ifdef _MSC_VER

    sprintf(tmp,"modules/libbs_%s_%s.dll",prefix,file);
#else

    sprintf(tmp,"libbs_%s_%s/.libs/libbs_%s_%s.so",prefix,file,prefix,file);
#endif

    return dlopen(tmp,RTLD_LAZY);
}

EXPORT_METHOD int CModPA::Load(const char *name)
{
    if (handle!=0)
    {
        fprintf(stderr,"Parser Module is already loaded.\n");
        return BS_ERR_LIB;
    }

    handle=bsLoadLib("pa",name);
    if (handle==0)
        handle=bsLoadLibSys("pa",name);

    if (handle==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&paLastError)=dlsym(handle,"paLastError");
    if (paLastError==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        dlclose(handle);
        handle=0;
        return BS_ERR_LIB;
    }

    *(void**)(&paSetParameters)=dlsym(handle,"paSetParameters");
    if (paSetParameters==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        dlclose(handle);
        handle=0;
        return BS_ERR_LIB;
    }

    *(void**)(&paExecute)=dlsym(handle,"paExecute");
    if (paExecute==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        dlclose(handle);
        handle=0;
        return BS_ERR_LIB;
    }

    *(void**)(&paInfo)=dlsym(handle,"paInfo");
    if (paInfo==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        dlclose(handle);
        handle=0;
        return BS_ERR_LIB;
    }

    strncpy(this->name,name,MAX_PATH-1);
    this->name[MAX_PATH-1]=0;
    return BS_OK;
}


EXPORT_METHOD int CModPP::Load(const char *name)
{
    if (handle!=0)
    {
        fprintf(stderr,"Preprocessing Module is already loaded.\n");
        return BS_ERR_LIB;
    }

    handle=bsLoadLib("pp",name);
    if (handle==0)
        handle=bsLoadLibSys("pp",name);

    if (handle==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&ppLastError)=dlsym(handle,"ppLastError");
    if (ppLastError==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&ppSetParameters)=dlsym(handle,"ppSetParameters");
    if (ppSetParameters==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&ppExecute)=dlsym(handle,"ppExecute");
    if (ppExecute==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&ppInfo)=dlsym(handle,"ppInfo");
    if (ppInfo==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    strncpy(this->name,name,MAX_PATH-1);
    this->name[MAX_PATH-1]=0;
    return BS_OK;
}

EXPORT_METHOD int CModFE::Load(const char *name)
{
    if (handle!=0)
    {
        fprintf(stderr,"Feature Extraction Module is already loaded.\n");
        return BS_ERR_LIB;
    }

    handle=bsLoadLib("fe",name);
    if (handle==0)
        handle=bsLoadLibSys("fe",name);

    if (handle==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&feLastError)=dlsym(handle,"feLastError");
    if (feLastError==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&feSetParameters)=dlsym(handle,"feSetParameters");
    if (feSetParameters==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&feExecute)=dlsym(handle,"feExecute");
    if (feExecute==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&feInfo)=dlsym(handle,"feInfo");
    if (feInfo==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    strncpy(this->name,name,MAX_PATH-1);
    this->name[MAX_PATH-1]=0;
    return BS_OK;
}

EXPORT_METHOD int CModFS::Load(const char *name)
{
    if (handle!=0)
    {
        fprintf(stderr,"Feature Selection Module is already loaded.\n");
        return BS_ERR_LIB;
    }

    handle=bsLoadLib("fs",name);
    if (handle==0)
        handle=bsLoadLibSys("fs",name);

    if (handle==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&fsLastError)=dlsym(handle,"fsLastError");
    if (fsLastError==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&fsSetParameters)=dlsym(handle,"fsSetParameters");
    if (fsSetParameters==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&fsExecute)=dlsym(handle,"fsExecute");
    if (fsExecute==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&fsInfo)=dlsym(handle,"fsInfo");
    if (fsInfo==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    strncpy(this->name,name,MAX_PATH-1);
    this->name[MAX_PATH-1]=0;
    return BS_OK;
}

EXPORT_METHOD int CModSR::Load(const char *name)
{
    if (handle!=0)
    {
        fprintf(stderr,"Reference-Based Scoring Module is already loaded.\n");
        return BS_ERR_LIB;
    }

    handle=bsLoadLib("sr",name);
    if (handle==0)
        handle=bsLoadLibSys("sr",name);

    if (handle==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&srLastError)=dlsym(handle,"srLastError");
    if (srLastError==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&srSetParameters)=dlsym(handle,"srSetParameters");
    if (srSetParameters==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&srExecute)=dlsym(handle,"srExecute");
    if (srExecute==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&srInfo)=dlsym(handle,"srInfo");
    if (srInfo==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }


    strncpy(this->name,name,MAX_PATH-1);
    this->name[MAX_PATH-1]=0;
    return BS_OK;
}

EXPORT_METHOD int CModSM::Load(const char *name)
{
    if (handle!=0)
    {
        fprintf(stderr,"Training-Based Scoring Module is already loaded.\n");
        return BS_ERR_LIB;
    }

    handle=bsLoadLib("sm",name);
    if (handle==0)
        handle=bsLoadLibSys("sm",name);

    if (handle==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&smLastError)=dlsym(handle,"smLastError");
    if (smLastError==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&smSetParameters)=dlsym(handle,"smSetParameters");
    if (smSetParameters==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&smExecute)=dlsym(handle,"smExecute");
    if (smExecute==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&smInfo)=dlsym(handle,"smInfo");
    if (smInfo==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    strncpy(this->name,name,MAX_PATH-1);
    this->name[MAX_PATH-1]=0;
    return BS_OK;
}

EXPORT_METHOD int CModTR::Load(const char *name)
{
    if (handle!=0)
    {
        fprintf(stderr,"Training Module is already loaded.\n");
        return BS_ERR_LIB;
    }

    handle=bsLoadLib("tr",name);
    if (handle==0)
        handle=bsLoadLibSys("tr",name);

    if (handle==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&trLastError)=dlsym(handle,"trLastError");
    if (trLastError==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&trSetParameters)=dlsym(handle,"trSetParameters");
    if (trSetParameters==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&trExecute)=dlsym(handle,"trExecute");
    if (trExecute==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&trInfo)=dlsym(handle,"trInfo");
    if (trInfo==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    strncpy(this->name,name,MAX_PATH-1);
    this->name[MAX_PATH-1]=0;
    return BS_OK;
}

EXPORT_METHOD int CModFU::Load(const char *name)
{
    if (handle!=0)
    {
        fprintf(stderr,"Fusion Module is already loaded.\n");
        return BS_ERR_LIB;
    }

    handle=bsLoadLib("fu",name);
    if (handle==0)
        handle=bsLoadLibSys("fu",name);

    if (handle==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&fuLastError)=dlsym(handle,"fuLastError");
    if (fuLastError==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&fuSetParameters)=dlsym(handle,"fuSetParameters");
    if (fuSetParameters==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&fuExecute)=dlsym(handle,"fuExecute");
    if (fuExecute==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    *(void**)(&fuInfo)=dlsym(handle,"fuInfo");
    if (fuInfo==0)
    {
        fprintf(stderr,"%s\n",dlerror());
        return BS_ERR_LIB;
    }

    strncpy(this->name,name,MAX_PATH-1);
    this->name[MAX_PATH-1]=0;
    return BS_OK;
}



EXPORT_METHOD void CModPA::Unload(void)
{
    if (handle!=0)
    {
        dlclose(handle);
        handle=0;
    }
}

EXPORT_METHOD void CModPP::Unload(void)
{
    if (handle!=0)
    {
        dlclose(handle);
        handle=0;
    }
}

EXPORT_METHOD void CModFE::Unload(void)
{
    if (handle!=0)
    {
        dlclose(handle);
        handle=0;
    }
}

EXPORT_METHOD void CModFS::Unload(void)
{
    if (handle!=0)
    {
        dlclose(handle);
        handle=0;
    }
}

EXPORT_METHOD void CModSR::Unload(void)
{
    if (handle!=0)
    {
        dlclose(handle);
        handle=0;
    }
}

EXPORT_METHOD void CModSM::Unload(void)
{
    if (handle!=0)
    {
        dlclose(handle);
        handle=0;
    }
}

EXPORT_METHOD void CModTR::Unload(void)
{
    if (handle!=0)
    {
        dlclose(handle);
        handle=0;
    }
}

EXPORT_METHOD void CModFU::Unload(void)
{
    if (handle!=0)
    {
        dlclose(handle);
        handle=0;
    }
}

