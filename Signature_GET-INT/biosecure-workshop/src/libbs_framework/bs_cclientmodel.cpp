/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <bs_cclientmodel.h>


EXPORT_METHOD CClientModel::CClientModel()
{
	m_model = 0;
}
EXPORT_METHOD CClientModel::~CClientModel()
{
	if (m_model)
		delete m_model;
	m_model = 0;
}

EXPORT_METHOD void CClientModel::setModel(CModel* _model)
{
	m_model = _model;
}
EXPORT_METHOD const CModel* CClientModel::getModel()
{
	return m_model;
}

EXPORT_METHOD void CClientModel::operator delete(void *p)
{
    bsfree(p);
}


EXPORT_METHOD void* CClientModel::operator new(size_t size)
{
    return bscalloc(size,1);
}

EXPORT_METHOD void CModel::operator delete(void *p)
{
    bsfree(p);
}


EXPORT_METHOD void* CModel::operator new(size_t size)
{
    return bscalloc(size,1);
}

