/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "cgmm.h"
#include "cmemalc.h"
#include "ckmeans.h"
#include "memory.h"
#include "math.h"
#include "statistics.h"
#include "stdio.h"

EXPORT_METHOD CGmm::CGmm(int M_, int L_, int covMatrixType_,
					bool bRandomInit_,
					bool bParameterNormalization_, 
					double parameterStandardDeviation_,
					double endAccuracy_,
					int endIterationNumber_)
{
	M = M_;		// number of gaussiens
	L = L_;		// size of un observation's vector
	covMatrixType = covMatrixType_;
	bRandomInit = bRandomInit_;
	bParameterNormalization = bParameterNormalization_;
	parameterStandardDeviation = parameterStandardDeviation_;
	endAccuracy = endAccuracy_;
	endIterationNumber = endIterationNumber_;
	normalizationFactors = NULL;

	CMemalc memalc;
	// allocate mixture
	c = memalc.dvector(M);	
	// allocate vector of means
	mu = memalc.dmatrix(L, M);	
	// allocate covariance matrix
	u = memalc.dmatrix3d(M, L, L);	
	
	bInit = false;
	logLikelihood = 0;

	gama = NULL;
	bi_gaus = NULL;
}

void CGmm::setGaussianNumber(int M_)
{
	CMemalc memalc;
	if(c!=NULL)		memalc.free_dvector(c);
	if(mu != NULL)	memalc.free_dmatrix(mu, L);
	if(u != NULL)	memalc.free_dmatrix3d(u, M, L);

	M = M_;

	c = memalc.dvector(M);
	mu = memalc.dmatrix(L, M);
	u = memalc.dmatrix3d(M, L, L);	
}

int CGmm::getGaussianNumber(){
	return M;
}


EXPORT_METHOD bool CGmm::initGMM(CFeatureVector *obs_gmm)
{	
	// Use kmeans to distribute the vectors to M different groups.
	// Then calcule the mean and the covariance of each group
	CKmeans kms(obs_gmm, M, bRandomInit);
		
	if(kms.isInit()) // verify if kmeans work well?
	{
		CMemalc memalc;
		if(c!=NULL)		memalc.free_dvector(c);
		if(mu != NULL)	memalc.free_dmatrix(mu, L);
		if(u != NULL)	memalc.free_dmatrix3d(u, M, L);

		// allocate mixture
		c = memalc.dvector(M);
		// allocate the means' vector
		mu = memalc.dmatrix(L, M);
		// allocate the covariance matrix
		u = memalc.dmatrix3d(M, L, L);	
		
		// assign means of M groups to M gaussiens
		int l, m;
		for(l = 0; l < L; l++)
		{
			for(m = 0; m < M; m++)
			{
				mu[l][m] = kms.means[l][m];
			}
		}
		// and the covariances
		int ln, cl;
		for(m = 0; m < M; m++)
		{
			for(ln = 0; ln < L; ln++)
			{
				for(cl = 0; cl < L; cl++)
				{
					u[m][ln][cl] = kms.covariances[m][ln][cl];
				}
			}
		}
		
		for(m = 0; m < M; m++)
		{
			for(l = 0; l < L; l++)
			{
				if(u[m][l][l] < COV_MIN_VAL)
				{
					u[m][l][l] = COV_MIN_VAL;
				}
			}
		}
		
		int denominator = 0;
		int i;
		for(i = 0; i < M; i++)
		{
			denominator = denominator + kms.gains[i];
		}
		for(i = 0; i < M; i++)
		{
			c[i] = double(kms.gains[i]) / denominator;
		}
		bInit = true;
		return true;
	}
	else // kmeans doesn't not works fine
	{	
		bInit = false;
		return false;
	}
}

//====================================================

EXPORT_METHOD bool CGmm::train()
{
	// bRandomInit
	// compute bi_gaus et gama in function of ob
	int loop = 0;
	double oldLL; 
	if(!initGMM(obs)){
		printf("Cannot initialize the GMM for training\n");
		trained = false;
		return false;	// Number of gaussians is soo great
	}
	printf("Training the GMM with %d gaussians\n", M);
	do{
		loop++;
		//dump();
		oldLL = logLikelihood;
		initProObs(obs);	// recompute the gama matrix
		logLikelihood = restimateModel();

		printf("Iteration %d, LL = %f\n", loop, logLikelihood);

		if(endAccuracy != 0){
			// stop criteria
			if(fabs((exp(oldLL) - exp(logLikelihood))/exp(oldLL)) <= endAccuracy)
				break;
		}else{
			if(loop == endIterationNumber)
				break;
		}
	}while(true);
	trained = true;
	return true;
}

EXPORT_METHOD bool CGmm::initProObs(CFeatureVector *ob)
{
	CMemalc	memalc;
	// soit ob est incorrecte, soit bInit est fausse, ne fais rien
	if((ob->num_features[0] != L)){
		return false;
	}

	int T = ob->features[0].num_feature_results;

	if(bi_gaus != NULL){
		memalc.free_dmatrix(bi_gaus, M);
		bi_gaus = NULL;
	}
	bi_gaus = memalc.dmatrix(M, T);//M x T: compute the probabilities of O(t) given by each gaussian
	
	int m, t;
	//Calculer les probabilités ici, loi gaussienne
	for(m = 0; m < M; m++){		//pour chaque gaussienne
		// det_u contient le déterminant de la matrice covariance
		double det_u = 1;
		int l;
		for(l = 0; l < L; l++){
			det_u *= u[m][l][l];	// diagonal only
		}

		if(det_u == 0){ 
			printf("matrice de covariance singulière\n"); return false;
		}

		////////// Calculer la matrice inverse ////////// 
		double ** inverse_u = memalc.dmatrix(L, L); // contient des zeros

		int i, j;
		for(i = 0; i < L; i++){
			double det_for_inv = 1;
			// pour une matrice diagonale, j'utilise quand même matrice 2D, pour l'avenir
			for(j = 0; j < L; j ++){
				if (j != i) det_for_inv *= u[m][j][j];
			}
			inverse_u[i][i] = det_for_inv / det_u;
		}
		////////// Terminer les calculs de la matrice inverse////////// 

		double con = pow(2 * PI, double(-L/2)) / sqrt(det_u);
		for(t = 0; t < T; t++){	// pour chaque vecteur d'observation
			double exp_com = 0;				// entre les parenthèses de exp
			int l;
			for(l = 0; l < L; l++){
				//exp_com += (ob->o[l][t] - mu[l][m]) * inverse_u[l][l] * 
				//		   (ob->o[l][t] - mu[l][m]); // (o-mu)' * u^-1 * (o-mu)
				exp_com += (ob->features[l].feature_result[t] - mu[l][m]) * inverse_u[l][l] * 
						   (ob->features[l].feature_result[t] - mu[l][m]); // (o-mu)' * u^-1 * (o-mu)
			}				
			bi_gaus[m][t] = c[m] * con * exp(-exp_com/2);

			if(bi_gaus[m][t] < PRO_MIN_VAL){
				bi_gaus[m][t] = PRO_MIN_VAL;
			}
		}
		memalc.free_dmatrix(inverse_u, L);
	}//pour chaque gaussienne

	return true;
}

EXPORT_METHOD void CGmm::computeGama()
{
	int T = obs->features[0].num_feature_results;
	int t, m;
	CMemalc	memalc;
	double *denominator;
	denominator = memalc.dvector(T); //contains only zero

	for(t = 0; t < T; t++){
		for(m = 0; m < M; m++){
			denominator[t] += bi_gaus[m][t];
		}
	}

	for(t = 0; t < T; t++){
		for(m = 0; m < M; m++){
			gama[m][t] = bi_gaus[m][t] / denominator[t];
		}
	}
	memalc.free_dvector(denominator);
}


EXPORT_METHOD double CGmm::restimateModel()
{
	int t, m, l;
	CMemalc memalc;
	int T = obs->features[0].num_feature_results;

	// Allocate memory for gama and compute gama
	if(gama != NULL){
		memalc.free_dmatrix(gama, M);
		gama = NULL;
	}
	gama = memalc.dmatrix(M, T);
	computeGama();

	// Reestimation GMM parameters
	for(m = 0; m < M; m++){
		double sum_gama = 0;
		for(t = 0; t < T; t++){ 
			sum_gama += gama[m][t];
		}

		// restimate
		c[m] = sum_gama / T;	//OK

		double *nominator_mu = memalc.dvector(L);
		for(l = 0; l < L; l++){
			for(t = 0; t < T; t++){ 
				nominator_mu[l] += gama[m][t] * obs->features[l].feature_result[t];
			}
		}

		double *nominator_u = memalc.dvector(L); //diagonal matrix only
		for(t = 0; t < T; t++){ 
			for(l = 0; l < L; l++){
				nominator_u[l] += gama[m][t] * (obs->features[l].feature_result[t] - mu[l][m]) * (obs->features[l].feature_result[t] - mu[l][m]);
			}
		}

		for(l = 0; l < L; l++){
			mu[l][m] = nominator_mu[l] / sum_gama;
		}
		
		for(l = 0; l < L; l++){
			u[m][l][l] = nominator_u[l] / sum_gama;
			if(u[m][l][l] < COV_MIN_VAL){
				u[m][l][l] = COV_MIN_VAL;
			}
		}
		
		memalc.free_dvector(nominator_mu);
		memalc.free_dvector(nominator_u);
	}
	return test(obs);

}

EXPORT_METHOD double CGmm::test(CFeatureVector *ob)	//normalize observation if needed
{
	initProObs(ob);
	double LL = 0;
	int T = ob->features[0].num_feature_results;
	int t, m;
	for(t = 0; t < T; t++){
		double tmp = 0;
		for(m = 0; m < M; m++){
			tmp += bi_gaus[m][t];
		}
		LL += log(tmp);
	}
	return (LL / T / L);
}

EXPORT_METHOD void CGmm::computeNormalizationFactor() // of traning data
// calculer les paramètres de normalisation personalisée
{
	CMemalc memalc;
	CVariance variance;
	// calculer les paramètres de normalisation des paras, l'écart-type moyen
	memalc.free_dvector(normalizationFactors);
	normalizationFactors = memalc.dvector(L);	// full fill zero

	double mean, var;
	int l;
	for(l = 0; l < L; l++){
		variance(obs->features[l].feature_result, obs->features[l].num_feature_results, &mean, &var);
		normalizationFactors[l] = sqrt(var) / parameterStandardDeviation;
	}
}

EXPORT_METHOD bool CGmm::assignObs(CFeatureVector *ob_)
{
	obs = ob_;
	// Normalize the data if asked (specified in the caractéristics of GMM)
	if(bParameterNormalization == PARAMETER_NORMALIZATION_YES){
		computeNormalizationFactor();
		normalizeParameters(obs);
	}

  return true;
}

EXPORT_METHOD void CGmm::normalizeParameters(CFeatureVector* ob)
{
	int l, i;
	for(l = 0; l < L; l++)
	{
		for(i = 0; i < ob->features[0].num_feature_results; i++)
		{
			ob->features[l].feature_result[i] /= (normalizationFactors[l]);
		}
	}
}

EXPORT_METHOD double CGmm::getTrainingLogLikelihood(){
	return logLikelihood;
}

EXPORT_METHOD CGmm::~CGmm()
{
	CMemalc memalc;
	memalc.free_dvector(c);
	memalc.free_dmatrix(mu, L);
	memalc.free_dmatrix3d(u, M, L);
	memalc.free_dmatrix(gama, M);
	memalc.free_dmatrix(bi_gaus, M);
	if(bParameterNormalization == PARAMETER_NORMALIZATION_YES)
	{
		memalc.free_dvector(normalizationFactors);
	}

}
