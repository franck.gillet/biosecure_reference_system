/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if !defined(_HMM_MEM_ALC_)
#define _HMM_MEM_ALC_
#include <bs_dynlink.h>


class CMemalc  
{
public:
	IMPORT_METHOD void free_dmatrix4d(double **** dmat4d, int nm3d, int nm, int nr);
	IMPORT_METHOD double **** dmatrix4d(int nm3d, int nm, int nr, int nc);
	IMPORT_METHOD void free_imatrix(int ** imat, int nr);
	IMPORT_METHOD double *dvector(int nel);
	IMPORT_METHOD double **dmatrix(int nr, int nc);
	IMPORT_METHOD double ***dmatrix3d(int nm, int nr, int nc);

	IMPORT_METHOD int *ivector(int nel);
	IMPORT_METHOD int **imatrix(int nr, int nc);

	IMPORT_METHOD void free_dvector(double * dvec);
	IMPORT_METHOD void free_dmatrix(double ** dmat, int nr);
	IMPORT_METHOD void free_dmatrix3d(double *** dmat3d, int nm, int nr);
	IMPORT_METHOD void free_ivector(int * ivec);
	
};

#endif //!defined(_HMM_MEM_ALC_)
