/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if !defined(KMEANS_)
#define KMEANS_

#include <libbs_framework.h>
#include <bs_dynlink.h>

#define MAX_ITER_KMEANS			20	

class CKmeans
{
// private member variables
private:	
	CFeatureVector *obs; // vectors for quantify
	int ncluster;		 // number of clusters 
	int L;				 // size of an observation's vector
	int T;				 // number of observations
	int *membership;	 // membership[t] = k where 1 <= k <= ncluster: vector t of the observation is appeared in group k
	bool bInit;
// public member variables
public:  
	double **means;			// means of K groups
	double ***covariances;	// covariances
	int * gains;			// probabilities of being appeared in each group
// public member functions
public:	
	IMPORT_METHOD bool isInit();
	
	IMPORT_METHOD CKmeans(CFeatureVector *obs, int ncluster, bool bRandomInit);
	IMPORT_METHOD virtual ~CKmeans();
// private member functions
private:
	IMPORT_METHOD void updateCovariances();
	IMPORT_METHOD double distance(int t, int cluster);
	IMPORT_METHOD bool updateCluster();	
	IMPORT_METHOD bool cluster(bool randomInit);
};

#endif //#if !defined
