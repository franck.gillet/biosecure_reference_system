/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "statistics.h"

EXPORT_METHOD void CVariance::operator() (double *elements, int number, double *mean_, double *var_)
{
	double mean = 0;
	double var = 0;
	int i;
	for(i = 0; i < number; i ++)
	{
		mean += elements[i];
	}
	mean /= number;

	for(i = 0; i < number; i ++)
	{
		var += (elements[i] - mean) * (elements[i] - mean);
	}
	
	if(number == 1)
	{
		var = 0;
	}
	else
	{
		var /= (number - 1);
	}

	if(var == 0) var = COV_MIN_VAL;

	*mean_ = mean;
	*var_ = var;
}

EXPORT_METHOD double CMedian::operator() (double *windows, int size)
{
	double tmp;
	int i, j;
	for(i = 0; i < size; i ++)
	{
		for(j = size - 2; j >= i; j--)
		{
			if(windows[j+1] < windows[j])
			{
				tmp = windows[j+1];
				windows[j+1] = windows[j];
				windows[j] = tmp;
			}
		}
	}
	int m = (int)(size/2); 
						   						   
	if( size == 2 * m)
	{
		return (windows[m-1] + windows[m])/2;
	}
	else
	{
		return windows[m];
	}
}


EXPORT_METHOD int CCombination::operator() (int k, int N)
{	
	if((k > N) || (k <= 0) || (N <= 0)) return 0;
	int ncombi = 1;
	int i, j, l;
	for(i = 1; i <= N; i++)	ncombi *= i;
	for(j = 1; j <= k; j++) ncombi /= j;
	for(l = 1;  l<= (N-k); l++) ncombi /= l;
	return ncombi;	
}
