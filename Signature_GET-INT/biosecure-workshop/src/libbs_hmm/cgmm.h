/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if !defined(_CGMM_)
#define _CGMM_

#include <libbs_framework.h>

#define COV_MIN_VAL				1e-4	// threshold for the variance
#define LOG_ZERO				-1e5	// 
#define PRO_MIN_VAL				1e-40	// minimum probability

#define COV_DIAG				1	
#define COV_FULL				2

#define PARAMETER_STANDARD_VARIATION  2	// pour la normalisation personalisée des paramètres

#define PARAMETER_NORMALIZATION_YES		true	
#define PARAMETER_NORMALIZATION_NO		false

#define PI						3.14159265358979323846

class CGmm : public CModel
{
private:
	bool bInit;		// Has GMM been inited yet?
	int M;			// M: number of gaussiens
	int L;			// L: size of observation vector
	int covMatrixType;		//covariance matrix type
	bool bRandomInit;		//init the HMM ramdomly or not each time run the prog: debug purpose
	bool bParameterNormalization;	// normalize observation parameters or not
	double parameterStandardDeviation;	// the parameter standard deviation after normalized
	double * normalizationFactors;
	double endAccuracy;			// Criterion to stop: normally 1e-2
	int endIterationNumber;		// If 0, use endAccuracy 
	double logLikelihood;

public:	
	double ** mu;		// L x M		: means of gaussiens
	double *** u;		// M x L x L	: covariance's matrix
	double *c;			// 1 x M		: gain of gaussien
	CFeatureVector * obs;			// Point to training base

	double ** bi_gaus;//M x T: compute the probabilities of O(t) given by each gaussian
	double ** gama;		//M x T: proportion of each gaussian by using also mixture weight
						//computed from bi_gaus. 
						//See "Assignment 3: GMM Speaker Identification 2E1400 Speech Signal Processing"
	bool trained;
	

	IMPORT_METHOD CGmm(int M_, int L_, int covMatrixType_ = COV_DIAG,
					bool bRandomInit_ = false,
					bool bParameterNormalization_ = PARAMETER_NORMALIZATION_YES, 
					double parameterStandardDeviation_ = PARAMETER_STANDARD_VARIATION,
					double endAccuracy_ = 0,
					int endIterationNumber_ = 10);
	IMPORT_METHOD ~CGmm();
	IMPORT_METHOD bool initGMM(CFeatureVector *obs_gmm);	

	IMPORT_METHOD bool train();


	IMPORT_METHOD double restimateModel();
	IMPORT_METHOD double test(CFeatureVector *ob);
	IMPORT_METHOD bool initProObs(CFeatureVector *ob);
	IMPORT_METHOD void computeGama();
	IMPORT_METHOD bool assignObs(CFeatureVector *ob);
	IMPORT_METHOD double getTrainingLogLikelihood();

	IMPORT_METHOD void setGaussianNumber(int M_);
	IMPORT_METHOD int getGaussianNumber();
	IMPORT_METHOD void computeNormalizationFactor();
	IMPORT_METHOD void normalizeParameters(CFeatureVector* obs);

};

#endif // #if !defined

