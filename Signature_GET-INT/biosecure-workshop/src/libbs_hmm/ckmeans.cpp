/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ckmeans.h"
#include "time.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "cmemalc.h"
EXPORT_METHOD CKmeans::CKmeans(CFeatureVector *obs_, int ncluster_, bool bRandomInit)
{
	obs = obs_;
	ncluster = ncluster_;
	// get number of observations
	L = obs->num_features[0]; //here 25
	// get size of an observation
	T = obs->features[0].num_feature_results; 


	CMemalc memalc;
	if (T >= ncluster)
	{	
		membership = memalc.ivector(T); 
		
		means = memalc.dmatrix(L, ncluster);
		
		covariances = memalc.dmatrix3d(ncluster, L, L);
		
		gains = memalc.ivector(ncluster);

		bInit = true;

		// regroup the vectors of observations obs in ncluster groups
		bool b = cluster(bRandomInit);
		int counter = 1;
		while (!b)
		{
			counter ++;
			b = cluster(bRandomInit);
			if(counter == MAX_ITER_KMEANS)
			{
				bInit = false;
				printf("Error in kmeans -> cluster!\n");
				break;
			}
		}
	}
	else // T < ncluster
	{
		membership = NULL;
		means = NULL;
		covariances = NULL;
		gains = NULL;
		bInit = false;	
	}

	if(bInit)
	{
		updateCovariances();
	}
}

EXPORT_METHOD bool CKmeans::cluster(bool randomInit)
{
	// calculate "membership" and " means"
	int ti;

	if(randomInit)
	{
		// the time function does not exist in the POKET PC 2003
		//ti = time(0);	// initialize randomly
		ti=rand();
		ti=ti+160;//158 is a critical value?
	}
	else
	{
		ti = 0;			//fix initialization
	}
	
	//printf("kmeansInit:%d\n",ti);
	
	/* initialize random generator */
	srand(ti);

	// initialize K moyennes:
	// for the first time, membership is all 0
	int t, nc, l;
	for(t = 0; t < T; t++)
	{
		membership[t] = 0;
	}
	int ind;
	
	for(nc = 0; nc < ncluster; nc ++)
	{
		ind = rand()%T;

		while (membership[ind] != 0)
		{
			ind = rand()%T;
		}
		
		//printf("randomInd:%d\n",ind);

		membership[ind] = nc;
		// init means
		for(l = 0; l < L; l++)
		{
			means[l][nc] = obs->features[l].feature_result[ind];
		}
	}

	CMemalc memalc;

	int * oldMembership = memalc.ivector(T);
	bool stop = false;

	while(!stop)
	{
		memcpy(oldMembership, membership, T * sizeof(int));
		int t;
		for(t = 0; t < T; t++)
		{
			double dis = 1e40;
			int clusterNearest = 1;

			for(nc = 0; nc < ncluster; nc ++)
			{
				double dis1 = distance(t, nc);
				if(dis1 < dis)
				{
					dis = dis1;
					clusterNearest = nc;
				}
			}

			membership[t] = clusterNearest;
		}

		if (updateCluster())
		{
			for(t = 0; t < T; t++)
			{
				stop = true;
				if(oldMembership[t] != membership[t])
				{
					stop = false;
					break;
				}
			}
		}
		else // error in update cluster
		{
			memalc.free_ivector(oldMembership);
			return false;
		}
	}
	memalc.free_ivector(oldMembership);
	return true;
}

EXPORT_METHOD bool CKmeans::updateCluster()
{
	int nc, l, t;
	for(nc = 0; nc < ncluster; nc ++)
	{
		gains[nc] = 0;
	}

	for(nc = 0; nc < ncluster; nc ++)
	{
		for(l = 0; l < L; l++)
		{
			means[l][nc] = 0;
		}
	}

	for(t = 0; t < T; t++)
	{
		for(l = 0; l < L; l++)
		{
			means[l][membership[t]] = means[l][membership[t]] + obs->features[l].feature_result[t];
		}
		gains[membership[t]] ++;
	}
	
	for(nc = 0; nc < ncluster; nc++)
	{
		for(l = 0; l < L; l++)
		{
			if (gains[nc] != 0)
			{
				means[l][nc] /= gains[nc];
			}
			else
			{				
				// recluster with another initialisation
				return false;
			}
		}
	}
	return true;
}

EXPORT_METHOD void CKmeans::updateCovariances()
{
	int nc, r, c, t;
	for(nc = 0; nc < ncluster; nc ++)
	{
		for(r = 0; r < L; r++)
		{
			for(c = 0; c < L; c++)
			{
				covariances[nc][r][c] = 0;
			}
		}
	}

	int cluster;
	double tmp;

	for(t = 0; t < T; t++)
	{
		cluster = membership[t];
		for(r = 0; r < L; r++)
		{			
			tmp = obs->features[r].feature_result[t] - means[r][cluster];
			tmp = tmp * tmp;
			covariances[cluster][r][r] += tmp;
		}
	}

	for(nc = 0; nc < ncluster; nc++)
	{
		for(r = 0; r < L; r++)
		{
			if(gains[nc] > 1)
			{
				covariances[nc][r][r] /= (gains[nc] - 1);
			}
			else
			{
				covariances[nc][r][r] = 0;
			}
		}
	}
}

EXPORT_METHOD double CKmeans::distance(int t, int cluster)
{
	double dis = 0;
	double tmp;
	int l;
	for(l = 0; l < L; l++)
	{
		tmp = (means[l][cluster] - obs->features[l].feature_result[t]);
		dis = dis + tmp * tmp;
	}
	dis /= L;
	return dis;
}
EXPORT_METHOD CKmeans::~CKmeans()
{
	CMemalc memalc;
	memalc.free_ivector(gains);
	memalc.free_ivector(membership);
	memalc.free_dmatrix(means, L);
	memalc.free_dmatrix3d(covariances, ncluster, L);
}

EXPORT_METHOD bool CKmeans::isInit()
{
	return bInit;
}
