/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if !defined(_HMM_)
#define _HMM_
#include "cgmm.h"
#include <bs_cclientmodel.h>

#define MAX_ITER_HMM			30	

#define END_ACCURACY			1e-2	// condition to stop
#define END_ITERATION_NUMBER		0	//Si 0, utiliser END_ACCURACY comme crit�re d'arret
									//Si non, c'est le nombre d'it�ration d'EM pour BW

#define PARAMETER_STANDARD_VARIATION  2	//pour la normalisation personalis�e des param�tres

#define TRANS_LR				1	//left-right
#define TRANS_BAKIS				2
#define TRANS_ERGODIC			3	//full connexion

#define COV_DIAG				1	
#define COV_FULL				2

#define PARAMETER_NORMALIZATION_YES		true	
#define PARAMETER_NORMALIZATION_NO		false

#define LOG_ZERO				-1e5	// = MIN_DOUBLE
#define PRO_MIN_VAL				1e-40	// minimum probability

class CHmm : public CModel
{
/*
 * Private member variables
 */
private:
	bool bInit;			// Has HMM been inited yet?

	int N;				// number of states
	int M;				// number of gaussienns
	int L;				// size of un vector of observation
	int K;				// number of observations
	double LLba;		// log_vraisemblance 
	double VARba;
	double * normalizationFactors; //1xL: used in normalizing parameters of observation

	double **a;			// N x N:	matrix of transition;
	double *pi;			// 1 x N:	initial probabilities of states

	int biLen;			// length of bi, see below
	double ** bi;		// N x biLen:	probabilities of seeing an vector at each state
	double *** bi_gaus; //M x N x biLen

	double ** alpha;	// N x betaLen
	double ** beta;		// N x alphaLen
	int alphaLen, betaLen;	
	double * scale;			// scale is used in algorithms: Backward, Fordward and baumwelch
	CFeatureVector **obs;	// 1 x K:	K observations used to train the model
	CGmm ** gmm;			// 1 x N:	distribution, at each state we have one GMM *

	int transType;			//transition matrix type 
	int covMatrixType;		//covariance matrix type
	bool bRandomInit;		//init the HMM ramdomly or not each time run the prog: debug purpose
	bool bParameterNormalization;	// normalize observation parameters or not
	double parameterStandardDeviation;	// the parameter standard deviation after normalized
	double endAccuracy;			// condition to stop: normally 1e-2
	int endIterationNumber;		// If 0, use endAccuracy 
								// if not, it's the number of iteration of EM for BW
/*
 * Public member variables
 */ 
public:
	int ** tbStSequence;	// viterbi's paths of K "obs": train base state sequence
	int * tbStSequenceLen;	// train base state sequence length
	double meanSegVecDis;
	double varSegVecDis;
	double meanLLDis;
	double varLLDis;
	bool trained;
/*
 * Private member functions
 */
private:

	IMPORT_METHOD CFeatureVector ** distributeObs();

	IMPORT_METHOD void initTrans(int transType);
	IMPORT_METHOD bool initProObs(CFeatureVector * ob);
	IMPORT_METHOD bool backward(CFeatureVector *ob);
	
	IMPORT_METHOD bool normalizeTrainBaseParameters();
	IMPORT_METHOD void computeNormalizationFactor();
/*
 * Public member functions
 */
public:
	IMPORT_METHOD void destroyHmmVariable();
	IMPORT_METHOD double computeViterbiHamDistance(int * firstSeq, int firstSeqLen, int * secondSeq, int secondSeqLen);
	IMPORT_METHOD bool normalizeObsParameters(CFeatureVector *ob);
	IMPORT_METHOD void setNStates(int N_);
	IMPORT_METHOD void setNGaussians(int M_);
	IMPORT_METHOD double getLLba();
	IMPORT_METHOD double getVARba();
	IMPORT_METHOD bool assignObs(CFeatureVector **obs_, int K_);
	IMPORT_METHOD int getStateNumber();
	IMPORT_METHOD int getObsNumber();
	IMPORT_METHOD bool isInit();
	IMPORT_METHOD CHmm(int N_, int M_, int transType_ = TRANS_LR, 
		int covMatrixType_ = COV_DIAG,
		bool bRandomInit_ = true,
		bool bParameterNormalization_ = PARAMETER_NORMALIZATION_YES, 
		double parameterStandardDeviation_ = PARAMETER_STANDARD_VARIATION,
		double endAccuracy_ = END_ACCURACY,
		int endIterationNumber_ = END_ITERATION_NUMBER);	
	
	IMPORT_METHOD bool initHMM();
	IMPORT_METHOD double viterbi(CFeatureVector *ob, int ** statSeq, bool last_state_exit_oblige);
	IMPORT_METHOD double forward(CFeatureVector *ob);
	IMPORT_METHOD double baumwelch();		
	IMPORT_METHOD bool train();	
	IMPORT_METHOD ~CHmm();
	IMPORT_METHOD CFeatureVector* newObs(int _L, int _T);
	IMPORT_METHOD bool addObs(CFeatureVector* obs, CFeatureVector* obs_supl,int index, int size);
	IMPORT_METHOD void normalizeParameters(CFeatureVector* obs, double * normFactor);

	IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};

#endif // #if !defined

