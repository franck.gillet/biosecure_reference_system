/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if !defined(statistics_)
#define statistics_

#include <bs_dynlink.h>

#define COV_MIN_VAL				1e-4	//threshold for variance

class CCombination
{
public:
	IMPORT_METHOD int operator() (int k, int N);
};

class CMedian
{
public:
	IMPORT_METHOD double operator() (double *windows, int size);
};

class CVariance
{
public:
	IMPORT_METHOD void operator() (double *elements, int number, double *mean_, double * var_);
};


#endif 
