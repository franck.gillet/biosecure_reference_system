/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "cmemalc.h"
#include <bs_mem.h>

EXPORT_METHOD double * CMemalc::dvector(int size)
{
	double * dvec = (double*)bscalloc(sizeof(double), size);
	// init all elements to 0.0
	int i;
	for(i = 0; i < size; i ++)
	{
		dvec[i] = 0.0;
	}
	return dvec;	
}

EXPORT_METHOD int * CMemalc::ivector(int size)
{
	int * ivec = (int*)bscalloc(sizeof(int),size);
	int i;
	for(i = 0; i < size; i ++)
	{
		ivec[i] = 0;
	}
	return ivec;	
}

EXPORT_METHOD int ** CMemalc::imatrix(int nr, int nc)
{
	int **imat = (int**)bscalloc(sizeof(int*),nr);
	for(int r = 0; r < nr; r++)
	{
		imat[r]= ivector(nc);
	}
	return imat;
}

EXPORT_METHOD double ** CMemalc::dmatrix(int nr, int nc)
{
	double ** dmat = (double**)bscalloc(sizeof(double*),nr);
	for(int r = 0; r < nr; r++)
	{
		dmat[r]= dvector(nc);
	}
	return dmat;
}

EXPORT_METHOD double *** CMemalc::dmatrix3d(int nm, int nr, int nc)
{
	double *** dmat3d = (double***)bscalloc(sizeof(double**), nm);
	int i;
	for (i = 0; i < nm; i++)
	{
		dmat3d[i]=  dmatrix(nr, nc);
	}
	return dmat3d;
}

EXPORT_METHOD double **** CMemalc::dmatrix4d(int nm3d, int nm, int nr, int nc)
{
	double **** dmat4d;
	dmat4d = (double****)bscalloc(sizeof(double***), nm3d);
	int i;
	for(i = 0; i < nm3d; i++)
	{
		dmat4d[i] = dmatrix3d(nm, nr, nc);
	}
	return dmat4d;
}

EXPORT_METHOD void CMemalc::free_dvector(double * dvec)
{
	if (dvec != 0)
	{
		bsfree(dvec);
	}
}

EXPORT_METHOD void CMemalc::free_ivector(int * ivec){
	if (ivec != 0) 
	{
		bsfree(ivec);
	}
}

EXPORT_METHOD void CMemalc::free_imatrix(int **imat, int nr)
{
	if (imat != 0)
	{
		int r;
		for (r = 0; r < nr; r++)
		{
			free_ivector(imat[r]);
			imat[r] = 0;
		}
		bsfree(imat);
	}
}

EXPORT_METHOD void CMemalc::free_dmatrix(double ** dmat, int nr)
{
	if (dmat != 0){
		int r;
		for (r = 0; r < nr; r++){
			free_dvector(dmat[r]);
			dmat[r] = 0;
		}
		bsfree(dmat);
	}
}

EXPORT_METHOD void CMemalc::free_dmatrix3d(double *** dmat3d, int nm, int nr)
{
	if (dmat3d != 0)
	{
		int m;
		for(m = 0; m < nm; m++)
		{
			free_dmatrix(dmat3d[m], nr);
		}
		bsfree(dmat3d);
	}
}

EXPORT_METHOD void CMemalc::free_dmatrix4d(double ****dmat4d, int nm3d, int nm, int nr)
{
	if(dmat4d !=0)
	{
		int i;
		for(i = 0; i< nm3d; i ++)
		{
			free_dmatrix3d(dmat4d[i], nm, nr);
		}
		bsfree(dmat4d);
	}
}

