#!/bin/bash

LD_LIBRARY_PATH=./libbs_framework:./libbs_hmm:./libbs_pa_mcyt:./libbs_pa_biomet:./libbs_pa_svc2004:./libbs_pa_biosecure:./libbs_fe_hmm:./libbs_sm_hmm:./libbs_tr_hmm:./libraries:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

./biosecure $1
