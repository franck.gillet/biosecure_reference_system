/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream.h>

#include <libbs_framework.h>

#define MAX_CMD 4096

int debug_main(void);

//To Akash: I suppress all these variables since it do not make sense in the program.
// When you change the database, you have to change it, if not, the program show sth non sense
/*int count=0;
int user=1;
double total=0;
int trial=1;
int number=1;

// Even these variable. I think we have to add new command in the framework for the 
// normalisation factors of singature features. Here, we have the same problem when testing 
// another model, the system continue to ask us the number of writer used to normalize signature feature,
// even the HMM which doesn't use this kind of normalization.
int once=0;
int _random,value,trials;
*/


// I propose you that, each time you make significant modification in the framework, you have to retest the HMM,
// at least, to see if it work. In fact, when you modified this file for the DTW, the HMM didn't work. 
// I had to recover it this morning (17-10-2006).

CChain *bsenrollment=0;
CChain *bstestdata=0;
CChain *bstrain=0;
CResult *bstestresult=0;
char bsdatapath[MAX_CMD]="";
char bsenfile[MAX_CMD]="";
char bstdfile[MAX_CMD]="";
char bsoutputfile[MAX_CMD]="scores.txt";

int bscp=0,bsmod=0,bsen=0,bstd=0;

void cleanup(void)
{
    if (bstestresult!=0)
        delete bstestresult;
    bstestresult=NULL;
    if (bsen!=0)
        UnloadEnrollment();
    bsen=0;
    if (bstd!=0)
        UnloadTestData();
    bstd=0;
    if (bsmod!=0)
        UnloadModules();
    bsmod=0;
    if (bstestdata!=0)
        delete bstestdata;
    bstestdata=NULL;
    if (bsenrollment!=0)
        delete bsenrollment;
	bsenrollment=NULL;
	if (bstrain != 0)
		delete bstrain;
	bstrain = NULL;
    //bsenrollment=0;
    if (bscp!=0)
        UnloadCP();
    bscp=0;
}

int getword(const char *buf,char *w,int &p,int l)
{
    int s;
    int quote=0;
    while (p<l)
    {
        if (buf[p]>32)
            break;
        p++;
    }
    if (p==l)
        return -1;
    s=0;
    while (p<l)
    {
        if (buf[p]=='\"')
        {
            quote^=1;
            continue;
        }
        if ((buf[p]<33) & (quote==0))
            break;
        w[s++]=buf[p++];
    }
    w[s++]=0;
    return 0;
}


int cmdloadcp(const char *buf,int pos,int l)
{
    int p=pos;
    char data[MAX_CMD];
    int result;

    if (bscp!=0)
    {
        fprintf(stderr,"Error: loadcp: Compatibility profile already loaded.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,data,p,l)<0)
    {
        fprintf(stderr,"Error: loadcp: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    result=LoadCP(data);
    if (result==BS_OK)
        bscp=1;
    return result;
}

int cmdsetdatapath(const char *buf,int pos,int l)
{
    int p=pos;

    if (getword(buf,bsdatapath,p,l)<0)
    {
        fprintf(stderr,"Error: setdatapath: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }
    return BS_OK;
}

int cmdlistmodules(const char *buf,int pos,int l)
{
    if (bscp==0)
    {
        fprintf(stderr,"Error: listmodules: Compatibility profile not loaded.\n");
        return BS_ERR_INVALID;
    }
    ListModules(BS_TYPE_ALL,BS_INFO_ALL);
    return BS_OK;
}

int cmdreset(const char *buf,int pos,int l)
{
    if (bscp==0)
    {
        fprintf(stderr,"Error: createenrollment: Compatibility profile not loaded.\n");
        return BS_ERR_INVALID;
    }
    if (bsmod!=0)
        UnloadModules();
    bsmod=0;
    if (bstestdata!=0)
        delete bstestdata;
    bstestdata=NULL;
    if (bsenrollment!=0)
        delete bsenrollment;
    bsenrollment=NULL;

    bsenrollment=new CChain;
    if (bsenrollment==0)
    {
        fprintf(stderr,"Not enough memory to create enrollment chain.");
        return BS_ERR_MEM;
    }
    bstestdata=new CChain;
    if (bstestdata==0)
    {
        fprintf(stderr,"Not enough memory to create enrollment chain.");
        return BS_ERR_MEM;
    }
	bstrain = new CChain;
	if (bstrain == 0)
	{
		fprintf(stderr,"Not enough memory to create train chain.");
		return BS_ERR_MEM;
	}


    return BS_OK;
}

int cmdenrollmentmodule(const char *buf,int pos,int l)
{
    int t,p=pos;
    char type[MAX_CMD];
    char name[MAX_CMD];
    const char *args=0;
    int result;

    if (bscp==0)
    {
        fprintf(stderr,"Error: enrollmentmodule: Compatibility profile not loaded.\n");
        return BS_ERR_INVALID;
    }
    if (bsenrollment==0)
    {
        fprintf(stderr,"Error: enrollmentmodule: Enrollment chain was not created.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,type,p,l)<0)
    {
        fprintf(stderr,"Error: enrollmentmodule: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,name,p,l)<0)
    {
        fprintf(stderr,"Error: enrollmentmodule: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    while (p<l)
    {
        if (buf[p]>32)
            break;
        p++;
    }

    if (buf[p]!=0)
        args=&buf[p];

    t=bsGetModuleType(type);
    switch (t)
    {
    case BS_TYPE_PA:
        result=bsenrollment->AddModulePA(name,args);
        break;
    case BS_TYPE_PP:
        result=bsenrollment->AddModulePP(name,args);
        break;
    case BS_TYPE_FE:
        result=bsenrollment->AddModuleFE(name,args);
        break;
    case BS_TYPE_FS:
        result=bsenrollment->AddModuleFS(name,args);
        break;
    default:
        fprintf(stderr,"Error: enrollmentmodule: Illegal module type: %s\n",type);
        return BS_ERR_INVALID;
    }
    return result;
}
int cmdtrainmodule(const char *buf,int pos,int l)
{
    int t,p=pos;
    char type[MAX_CMD];
    char name[MAX_CMD];
    const char *args=0;
    int result;

    if (bscp==0)
    {
        fprintf(stderr,"Error: trainmodule: Compatibility profile not loaded.\n");
        return BS_ERR_INVALID;
    }
    if (bstrain==0)
    {
        fprintf(stderr,"Error: trainmodule: Enrollment chain was not created.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,type,p,l)<0)
    {
        fprintf(stderr,"Error: trainmodule: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,name,p,l)<0)
    {
        fprintf(stderr,"Error: trainmodule: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    while (p<l)
    {
        if (buf[p]>32)
            break;
        p++;
    }

    if (buf[p]!=0)
        args=&buf[p];

    t=bsGetModuleType(type);
    switch (t)
    {
    case BS_TYPE_TR:
        result=bstrain->AddModuleTR(name,args);
        break;
    default:
        fprintf(stderr,"Error: enrollmentmodule: Illegal module type: %s\n",type);
        return BS_ERR_INVALID;
    }
    return result;
}
int cmdtestdatamodule(const char *buf,int pos,int l)
{
    int t,p=pos;
    char type[MAX_CMD];
    char name[MAX_CMD];
    const char *args=0;
    int result;

    if (bscp==0)
    {
        fprintf(stderr,"Error: testdatamodule: Compatibility profile not loaded.\n");
        return BS_ERR_INVALID;
    }
    if (bstestdata==0)
    {
        fprintf(stderr,"Error: testdatamodule: Test data chain was not created.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,type,p,l)<0)
    {
        fprintf(stderr,"Error: testdatamodule: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,name,p,l)<0)
    {
        fprintf(stderr,"Error: testdatamodule: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    while (p<l)
    {
        if (buf[p]>32)
            break;
        p++;
    }

    if (buf[p]!=0)
        args=&buf[p];

    t=bsGetModuleType(type);
    switch (t)
    {
    case BS_TYPE_PA:
        result=bstestdata->AddModulePA(name,args);
        break;
    case BS_TYPE_PP:
        result=bstestdata->AddModulePP(name,args);
        break;
    case BS_TYPE_FE:
        result=bstestdata->AddModuleFE(name,args);
        break;
    case BS_TYPE_FS:
        result=bstestdata->AddModuleFS(name,args);
        break;
    default:
        fprintf(stderr,"Error: testdatamodule: Illegal module type: %s\n",type);
        return BS_ERR_INVALID;
    }
    return result;
}

int cmdmatchermodule(const char *buf,int pos,int l)
{
    int t,p=pos;
    char type[MAX_CMD];
    char name[MAX_CMD];
    const char *args=0;
    int result;

    if (bscp==0)
    {
        fprintf(stderr,"Error: matchermodule: Compatibility profile not loaded.\n");
        return BS_ERR_INVALID;
    }
    if (bsmod!=0)
    {
        UnloadModules();
        bsmod=0;
    }

    if (bsenrollment==0)
    {
        fprintf(stderr,"Error: matchermodule: Enrollment chain was not created.\n");
        return BS_ERR_INVALID;
    }
    if (bstestdata==0)
    {
        fprintf(stderr,"Error: matchermodule: Test data chain was not created.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,type,p,l)<0)
    {
        fprintf(stderr,"Error: matchermodule: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,name,p,l)<0)
    {
        fprintf(stderr,"Error: matchermodule: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    while (p<l)
    {
        if (buf[p]>32)
            break;
        p++;
    }

    if (buf[p]!=0)
        args=&buf[p];

    UnloadEnrollment();
    t=bsGetModuleType(type);
    switch (t)
    {
    case BS_TYPE_SR:
        result=ConfigureModules(bsenrollment,bstestdata,name,args,BS_MATCHER_REFERENCE);
        break;
    case BS_TYPE_SM:
        result=ConfigureModules(bsenrollment,bstestdata,name,args,BS_MATCHER_TRAINING);
        break;
    default:
        fprintf(stderr,"Error: matchermodule: Illegal module type: %s\n",type);
        return BS_ERR_INVALID;
    }

    if (result<0)
        return result;
    bsmod=t;
    result=CheckValidity();
    if (result!=BS_OK)
        fprintf(stderr,"Error: matchermodule: Module configuration failed validation check.\n");
    return result;
}

int cmdenroll(const char *buf,int pos,int l)
{
    int p=pos;
    char name[MAX_CMD];
    int result;

    if (bscp==0)
    {
        fprintf(stderr,"Error: enroll: Compatibility profile not loaded.\n");
        return BS_ERR_INVALID;
    }
    if (bsmod==0)
    {
        fprintf(stderr,"Error: enroll: Module configuration incomplete.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,name,p,l)<0)
    {
        fprintf(stderr,"Error: enroll: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    //if ((bsen!=0) & (bsmod!=BS_TYPE_SM))
    //    UnloadEnrollment();
    bsen=0;
    sprintf(bsenfile,"%s/%s",bsdatapath,name);
	fprintf(stdout, "Enroll file: %s\n", name);
	result=LoadEnrollment(bsenfile);
    strcpy(bsenfile,name);
    
	FILE* fscore = fopen(bsoutputfile,"a");
	fprintf(fscore,"%s\t",name);
	fprintf(fscore,"\n");
	fclose(fscore);
	
	
	if (result==BS_OK)
        bsen=1;
    return result;
}

int cmdunloadenrollment(const char *buf,int pos,int l)
{
    if (bsen!=0)
        UnloadEnrollment();
    bsen=0;
    return BS_OK;
}

int cmdtest(const char *buf,int pos,int l)
{

// To Akash: this is not a good way to show the message.
// I've added some code to show directly the file being enrolled and tested.

/*    cout<<"testing user "<<user<<"trial "<<trial<<"\n";
	cout.flush();
    if(number==45)
	{
		number=0;
		trial++;
	}

	if(trial==50)
		trial=1;
	number++;
*/

	int p=pos;
    char name[MAX_CMD];
    char genuinestr[MAX_CMD];
//    char idstr[MAX_CMD];
    int genuine;
    int result;

    if (bscp==0)
    {
        fprintf(stderr,"Error: test: Compatibility profile not loaded.\n");
        return BS_ERR_INVALID;
    }
    if (bsmod==0)
    {
        fprintf(stderr,"Error: test: Module configuration incomplete.\n");
        return BS_ERR_INVALID;
    }

    if (getword(buf,genuinestr,p,l)<0)
    {
        fprintf(stderr,"Error: test: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    if (sscanf(genuinestr,"%i",&genuine)!=1)
    {
        fprintf(stderr,"Error: test: Wrong parameter format.\n");
        return BS_ERR_INVALID;    
    }
/*        
    if (getword(buf,idstr,p,l)<0)
    {
        fprintf(stderr,"Error: test: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }
*/
    if (getword(buf,name,p,l)<0)
    {
        fprintf(stderr,"Error: test: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    if (bsen==0)
    {
        fprintf(stderr,"Error: test: enrollment not loaded.\n");
        return BS_ERR_INVALID;
    }

    if (bstd!=0)
        UnloadTestData();
    bstd=0;
    sprintf(bstdfile,"%s/%s",bsdatapath,name);
	fprintf(stdout, "Testing file: %s\n", name);
    result=LoadTestData(bstdfile,genuine);
    strcpy(bstdfile,name);

    if (result==BS_OK)
    {
        bstd=1;
        if (bstestresult!=0)
            delete bstestresult;
        bstestresult=NULL;

        result=CalcScore(&bstestresult);
        
		if (result!=BS_OK)
            return result;
		/*
        switch (genuine)
		{
            case GENUINE_UNKNOWN: sprintf(genuinestr,"Unknown"); break;
            case GENUINE_YES: sprintf(genuinestr,"Yes"); break;
            case GENUINE_BLIND: sprintf(genuinestr,"Blind"); break;
            case GENUINE_LOW: sprintf(genuinestr,"Low"); break;
            case GENUINE_HIGH: sprintf(genuinestr,"High"); break;
			default: sprintf(genuinestr,"%i",genuine); break;
		} 
		*/
        
		FILE* fscore = fopen(bsoutputfile,"a");
		int nscore = 0;
        
/*		if(once==0)
		{
			cout<<"Estimation of normalization parameters complete";
			cout.flush();
			cout<<"\nEnter the number of trials for each signer to be tested";
			cout.flush();
			cin>>trials;
			cout<<"\nEnter the number of random forgeries being used per signer to be tested";
			cout.flush();
			cin>>_random;
			value=trials*(45+_random);
			once++;
		}
				
		if(count==value)
		{
			user++;
			count=0;
		}
		
			count++;
*/		
//		fprintf(fscore,"%d\t",user);
		fprintf(fscore,"%s\t",genuinestr);
		fprintf(fscore,"%s\t",name);
		
//	    cout<<bstestresult->num_scores<<"\n";
//		cout.flush();

		for (; nscore < bstestresult->num_scores; nscore++)
		{
			fprintf(fscore,"%6.6f\t",bstestresult->scores[nscore]);
		}
		
/*		for (; nscore < bstestresult->num_scores; nscore++)
		{
			total=total + bstestresult->scores[nscore];
		}
		fprintf(fscore,"%6.9f\n",(5/total));
		
         total=0;
*/		
		fprintf(fscore,"\n");
		fclose(fscore); 
    }
     delete bstestresult;// changes made here
	 bstestresult=NULL;     //changes made here
    return result;
}
int cmdtrain(const char *buf,int pos,int l)
{
//	fprintf(stdout,"Train here\n");
///	return BS_OK;
	int result;
	result = TrainModel();
	return result;
}

int cmdsetoutputfile(const char *buf,int pos,int l)
{
    int p=pos;
    if (getword(buf,bsoutputfile,p,l)<0)
    {
        fprintf(stderr,"Error: setoutputfile: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }
    
	// delete the old score file
	FILE *fscore = fopen(bsoutputfile,"r");
	if (fscore)
	{
		fclose(fscore);
		fscore = fopen(bsoutputfile,"w");
		fclose(fscore);
	}
	         
    return BS_OK;
}

int cmdfusion(const char *buf,int pos,int l)
{    
    int p=pos;
    char name[MAX_CMD];
    const char *args=0;
    int result;
    
    if (getword(buf,name,p,l)<0)
    {
        fprintf(stderr,"Error: enrollmentmodule: Wrong parameter format.\n");
        return BS_ERR_INVALID;
    }

    while (p<l)
    {
        if (buf[p]>32)
            break;
        p++;
    }

    if (buf[p]!=0)
        args=&buf[p];    

    result = Fusion(name,args);
    return BS_OK;   
}

typedef struct
{
    int (*cmdFunc)(const char *buf,int pos,int l);
    char *cmd;
}
BS_CMD;

BS_CMD bs_cmd[]=
    {
        {&cmdloadcp,"loadcp"},
        {&cmdsetdatapath,"setdatapath"},
        {&cmdlistmodules,"listmodules"},
        {&cmdreset,"reset"},
        {&cmdenrollmentmodule,"enrollmentmodule"},
        {&cmdtestdatamodule,"testdatamodule"},
        {&cmdmatchermodule,"matchermodule"},
        {&cmdenroll,"enroll"},
        {&cmdtest,"test"},
		{&cmdtrain,"train"},
		{&cmdtrainmodule,"trainmodule"},
        {&cmdunloadenrollment,"unloadenrollment"},
        {&cmdsetoutputfile,"setoutputfile"},
        {&cmdfusion,"fusion"}
    };

#define BS_CMDS ((signed)(sizeof(bs_cmd)/sizeof(BS_CMD)))

int execbss(FILE *f)
{
    int result;
    char buf[MAX_CMD];
    char cmd[MAX_CMD];
    int p,l,i,line=0;

    while (fgets(buf,sizeof(buf),f)!=0)
    {
        line++;
        l=strlen(buf);
        if (l>=(signed)(sizeof(buf)-1))
        {
            fprintf(stderr,"Error: Line too long, aborting.\n");
            cleanup();
            return BS_ERR_INVALID;
        }

        p=0;
        result=getword(buf,cmd,p,l);

        if ((result==0) & (cmd[0]!='#'))
        {
            for (i=0;i<BS_CMDS;i++)
            {
                if (strcmp(bs_cmd[i].cmd,cmd)==0)
                {
                    
					result=bs_cmd[i].cmdFunc(buf,p,l);
                    if (result<0)
                    {
                        cleanup();
                        fprintf(stderr,"Line %i: %s\n",line,buf);
                        return result;
                    }
                    break;
                }
            }
            if (i==BS_CMDS)
            {
                fprintf(stderr,"Error: Unknown command, aborting: %s.\n",cmd);
                fprintf(stderr,"Line %i: %s\n",line,buf);
                cleanup();
                return BS_ERR_INVALID;
            }
        }

    }
    cleanup();
    return BS_OK;
}

int main(int argc, char *argv[])
{
	// delete the old score file
	FILE *fscore = fopen(bsoutputfile,"r");
	if (fscore)
	{
		fclose(fscore);
		fscore = fopen(bsoutputfile,"w");
		fclose(fscore);
	}
    FILE *f;
    if (argc==2)
        if (strcmp("-d",argv[1])==0)
            return debug_main();

    if (argc==1)
        f=stdin;
    else
    {
        f=fopen(argv[1],"r");
        if (f==0)
        {
            fprintf(stderr,"Error: could not open script file: %s\n",argv[1]);
            return BS_ERR_LOAD;
        }
    }
    execbss(f);
    if (argc!=1)
        fclose(f);
    fprintf(stdout,"Done\n");

#ifdef _MSC_VER

    getchar();
#endif

    return BS_OK;
}
