/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <libbs_framework.h>

#define LEVENSHTEIN_XMIN 0             // local minimum for x values
#define LEVENSHTEIN_XMAX 1             // local maximum for x values
#define LEVENSHTEIN_XMINGLOBAL 2       // global minimum for x values
#define LEVENSHTEIN_XMAXGLOBAL 3       // global maximum for x values

#define LEVENSHTEIN_YMIN 10            // local minimum for y values
#define LEVENSHTEIN_YMAX 11            // local maximum for y values
#define LEVENSHTEIN_YMINGLOBAL 12      // global minimum for y values
#define LEVENSHTEIN_YMAXGLOBAL 13      // global maximum for y values

#define LEVENSHTEIN_PMIN 20            // local minimum for pressure values
#define LEVENSHTEIN_PMAX 21            // local maximum for pressure values
#define LEVENSHTEIN_PMINGLOBAL 22      // global minimum for pressure values
#define LEVENSHTEIN_PMAXGLOBAL 23      // global maximum for pressure values

#define LEVENSHTEIN_GAP 30             // pen-up event (gap)

#define LEVENSHTEIN_XMINDIST 4         // minimal distance between two x events
#define LEVENSHTEIN_YMINDIST 2         // minimal distance between two y events
#define LEVENSHTEIN_PMINDIST 5         // minimal distance between two pressure events

#define LEVENSHTEIN_USEP               // use pressure events
#define LEVENSHTEIN_USEGAP             // use pen-up events
//#define LEVENSHTEIN_USEGLOBAL          // use global events

// TODO: module specific modifications start here

// every module defines its own PARAM structure unless the module doesnt have any parameters
// this is an example structure:

char LastError[BS_MAX_ERROR];   // last error message of this module

int ParamSet=0;                 // becomes 1 if all parameters are valid


// return pointer to error string, nothing to do here
EXPORT char *feLastError(void)
{
    return LastError;
}


// validate and set parameters for later execution
EXPORT int feSetParameters(const char *feParamString)
{
    // everything fine
    ParamSet=1;
    return BS_OK;
}

int AddElement(CFeatureVector *fv,double d)
{
    int n=fv->num_features[0];
    CFeatureData *dst;

    dst=(CFeatureData*)bsrealloc(fv->features,sizeof(CFeatureData)*(n+1));
    if (dst==0)
    {
        sprintf(LastError,"error in fe_levenshtein: memory allocation error.\n");
        return BS_ERR_MEM;
    }

    dst[n].num_feature_results=RESULT_STATIC;
    dst[n].value=d;
    dst[n].feat_id="Levenshtein";
    dst[n].normalised=NOT_NORMALISED;
    dst[n].discrete=DISCRETE;
    dst[n].lower_range=NOT_KNOWN;
    dst[n].upper_range=NOT_KNOWN;
    dst[n].time=TIME_UNKNOWN;
    fv->features=dst;
    fv->num_features[0]++;
    return 0;
}

inline int GetDir(CFeatureSet **feFeatureSet,CFeatureVector *fv,int v0,int v1,int &v,
                  int &vminv,int &vmaxv,int &vminf,int &vmaxf,int &vlast,int c,int mindist)
{
    int res;

    if (v0>v1)
    {
        if (v>0)
        {
            if (abs(v0-vlast)>=mindist)
            {
                if (v0>vmaxv)
                {
                    vmaxv=v0;
                    vmaxf=fv->num_features[0];
                }
                res=AddElement(fv,c+1);
                if (res!=0)
                {
                    delete *feFeatureSet;
                    return res;
                }
                vlast=v0;
            }
        }
        v=-1;
    }
    else
    {
        if (v<0)
        {
            if (abs(v0-vlast)>=mindist)
            {
                if (v0<vminv)
                {
                    vminv=v0;
                    vminf=fv->num_features[0];
                }
                res=AddElement(fv,c);
                if (res!=0)
                {
                    delete *feFeatureSet;
                    return res;
                }
                vlast=v0;
            }
        }
        v=1;
    }
    return 0;
}
// execution of the feature extraction functions based on the parameters
// and source data passed to feSetParameters
// feFeatureSet will be created by this function and returned on success
EXPORT int feExecute(const CDataSet *feDataSet,CFeatureSet **feFeatureSet)
{
    CFeatureVector *fv;
    CSourceData *src;
    int x=0,y=0,p=0,i,l,res;
    int xminv=2147483647,xmaxv=-2147483647,xminf=-1,xmaxf=-1,xlast=xminv;
    int yminv=2147483647,ymaxv=-2147483647,yminf=-1,ymaxf=-1,ylast=yminv;
#ifdef LEVENSHTEIN_USEP

    int pminv=2147483647,pmaxv=-2147483647,pminf=-1,pmaxf=-1,plast=pminv;
#endif

    if (ParamSet==0)
    {
        sprintf(LastError,"error in fe_levenshtein: parameters have not been set yet.\n");
        return BS_ERR_PARAM;
    }
    // TODO: module specific modifications start here

    // check if needed source data is available:
    // example: we check for existence of normalised data
    if (feDataSet->sourcedata==0)
    {
        sprintf(LastError,"error in fe_levenshtein: source data not available.\n");
        return BS_ERR_SOURCE;
    }

    // insert something useful here using data in DataSet and parameters in Param
    // store a valid pointer to the created featureset in feFeatureSet
    if ( (*feFeatureSet) == 0)
    {
        *feFeatureSet=new CFeatureSet;
        if (*feFeatureSet==0)
        {
            sprintf(LastError,"error in fe_levenshtein: memory allocation error.\n");
            return BS_ERR_MEM;
        }
        (*feFeatureSet)->feat_set_id="Levenshtein";
        (*feFeatureSet)->feat_segments=(CFeatureSegment*)bscalloc(sizeof(CFeatureSegment),1);
        if ((*feFeatureSet)->feat_segments==0)
        {
            delete *feFeatureSet;
            *feFeatureSet=0;
            sprintf(LastError,"error in fe_levenshtein: memory allocation error.\n");
            return BS_ERR_MEM;
        }
        (*feFeatureSet)->num_feature_segments=1;
        (*feFeatureSet)->feat_segments->feat_seg_id="Levenshtein";
        (*feFeatureSet)->feat_segments->feat_vectors = 0;
    }
    if ((*feFeatureSet)->feat_segments->feat_vectors == 0)
    {
        fv=(CFeatureVector*)bscalloc(sizeof(CFeatureVector),1);
        (*feFeatureSet)->feat_segments->feat_vectors=fv;
        if (fv==0)
        {
            delete *feFeatureSet;
            *feFeatureSet=0;
            sprintf(LastError,"error in fe_levenshtein: memory allocation error.\n");
            return BS_ERR_MEM;
        }
        (*feFeatureSet)->feat_segments->num_feature_vectors=1;
    }
    else
    {
        int size = (*feFeatureSet)->feat_segments->num_feature_vectors + 1;
        (*feFeatureSet)->feat_segments->feat_vectors = (CFeatureVector*)bsrealloc((*feFeatureSet)->feat_segments->feat_vectors,sizeof(CFeatureVector)*size);
        (*feFeatureSet)->feat_segments->num_feature_vectors = size;
        if ((*feFeatureSet)->feat_segments->feat_vectors == 0)
        {
            delete *feFeatureSet;
            *feFeatureSet=0;
            sprintf(LastError,"error in fe_levenshtein: memory allocation error.\n");
            return BS_ERR_MEM;
        }
        fv = &((*feFeatureSet)->feat_segments->feat_vectors[size-1]);
    }

    fv->feat_vec_id="Levenshtein";


    fv->num_features=(int*)bsmalloc(sizeof(int));
    if (fv->num_features==0)
    {
        delete *feFeatureSet;
        *feFeatureSet=0;
        sprintf(LastError,"error in fe_levenshtein: memory allocation error.\n");
        return BS_ERR_MEM;
    }
    fv->num_features[0]=0;
    fv->num_dimensions=1;
    fv->features = 0;

    src=feDataSet->sourcedata;
    l=feDataSet->num_packets;

    for (i=1;i<l;i++)
    {
#ifdef LEVENSHTEIN_USEGAP
        if ((src[i].force==0) & (src[i-1].force!=0))
        {
            res=AddElement(fv,LEVENSHTEIN_GAP);
            if (res!=0)
            {
                delete *feFeatureSet;
                return res;
            }
        }
        else
#endif

        {
            res=GetDir(feFeatureSet,fv,src[i-1].x,src[i].x,x,xminv,xmaxv,xminf,xmaxf,xlast,LEVENSHTEIN_XMIN,LEVENSHTEIN_XMINDIST);
            if (res<0)
                return res;
            res=GetDir(feFeatureSet,fv,src[i-1].y,src[i].y,y,yminv,ymaxv,yminf,ymaxf,ylast,LEVENSHTEIN_YMIN,LEVENSHTEIN_YMINDIST);
            if (res<0)
                return res;
#ifdef LEVENSHTEIN_USEP

            res=GetDir(feFeatureSet,fv,src[i-1].force,src[i].force,p,pminv,pmaxv,pminf,pmaxf,plast,LEVENSHTEIN_PMIN,LEVENSHTEIN_PMINDIST);
            if (res<0)
                return res;
#endif

        }
    }

#ifdef LEVENSHTEIN_USEGLOBAL
    if (xminf>=0)
        fv->features[xminf].value=LEVENSHTEIN_XMINGLOBAL;
    if (xmaxf>=0)
        fv->features[xmaxf].value=LEVENSHTEIN_XMAXGLOBAL;
    if (yminf>=0)
        fv->features[yminf].value=LEVENSHTEIN_YMINGLOBAL;
    if (ymaxf>=0)
        fv->features[ymaxf].value=LEVENSHTEIN_YMAXGLOBAL;
#ifdef LEVENSHTEIN_USEP

    if (pminf>=0)
        fv->features[pminf].value=LEVENSHTEIN_PMINGLOBAL;
    if (pmaxf>=0)
        fv->features[pmaxf].value=LEVENSHTEIN_PMAXGLOBAL;
#endif
#endif
    // module specific modifications end here
    return BS_OK;
}


// report information about module usage and capabilities depending on Type
EXPORT void feInfo(int Type)
{
    // TODO: module specific modifications start here
    // possible Types are defined in libbs_framework.h
    // not all Types have to be supported
    // multiple Types can be combined

    if ((Type&BS_INFO_NAME)!=0)
    {
        fprintf(stdout,"Module Name: Levenshtein Feature Extraction\n");
    }

    if ((Type&BS_INFO_VERSION)!=0)
    {
        fprintf(stdout,"Module Version: 0.2\n");
    }

    if ((Type&BS_INFO_ABOUT)!=0)
    {
        fprintf(stdout,"Authors: AMSL Magdeburg\n");
    }

    if ((Type&BS_INFO_PARAMETERS)!=0)
    {
        fprintf(stdout,"Module Parameters: none\n");
    }

    if ((Type&BS_INFO_CAPABILITIES)!=0)
    {
        fprintf(stdout,"This module extracts features in the Levenshtein format.\n");
    }

    // module specific modifications end here
}
