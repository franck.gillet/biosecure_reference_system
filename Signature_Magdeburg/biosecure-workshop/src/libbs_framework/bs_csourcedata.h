/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CSOURCEDATA_H
#define BS_CSOURCEDATA_H

#include <bs_dynlink.h>

#ifndef TIP_NO_CONTACT
#define TIP_NO_CONTACT  0       // no tip contact
#define TIP_CONTACT     1       // tip contact
#endif

class CSourceData
{
public:
    long        time;           // time offset of point in ms
    int         x;              // x position
    int         y;              // y position
    int         force;          // pen force (pressure) non negative
    int         tip;            // TIP_NO_CONTACT, TIP_CONTACT
    int         button;         // barrel button status
    int         tiltx;          // x axis tilt
    int         tilty;          // y axis tilt
    int         azimuth;        // pen azimuth
    int         elevation;      // pen elevation
    int         rotation;       // pen rotation

    CSourceData(){}
    ~CSourceData(){}
    IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};

#endif
