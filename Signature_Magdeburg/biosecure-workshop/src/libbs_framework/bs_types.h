/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_TYPES_H
#define BS_TYPES_H

#ifndef _MSC_VER

typedef long long int __int64;
#define SNAN64 0x7ff0000000000001LL;    // signaling NaN
#define QNAN64 0x7ff8000000000001LL;    // quiet NaN

#else

#define SNAN64 0x7ff0000000000001L;    // signaling NaN
#define QNAN64 0x7ff8000000000001L;    // quiet NaN

#endif


#endif
