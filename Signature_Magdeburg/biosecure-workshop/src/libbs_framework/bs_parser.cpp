/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <bs_parser.h>
#include <iostream>
#include <fstream>
using namespace std;

EXPORT_METHOD int CParser::settingDevice(char* _capture_device, 
									int _freq,
									int _Xresolution,
									int _Yresolution,
									int _TiltFormat,
									int _RotationFormat,
									int _ForceFormat,
									int _azimuth_0,
									int _azimuth_90,
									int _elevation_0,
									int _elevation_90,
									int _tilt_xmin,
									int _tilt_xmax,
									int _tilt_ymin,
									int _tilt_ymax,
									int _rotation_0,
									int _rotation_360,
									int _force_min,
									int _force_max,
									int _Driver_ID)
{
	pDevice = new CDevice;
	if (pDevice == 0)
	{
		return BS_ERR_MEM;
	}
	pDevice->azimuth_0 = _azimuth_0;
	pDevice->azimuth_90 = _azimuth_90;
	pDevice->capture_device = _capture_device;
	pDevice->Driver_ID = _Driver_ID;
	pDevice->elevation_0 = _elevation_0;
	pDevice->elevation_90 = _elevation_90;
	pDevice->force_max = _force_max;
	pDevice->force_min = _force_min;
	pDevice->ForceFormat = _ForceFormat;
	pDevice->freq = _freq;
	pDevice->rotation_0 = _rotation_0;
	pDevice->rotation_360 = _rotation_360;
	pDevice->RotationFormat = _RotationFormat;
	pDevice->tilt_xmax = _tilt_xmax;
	pDevice->tilt_xmin = _tilt_xmin;
	pDevice->tilt_ymax = _tilt_ymax;
	pDevice->tilt_ymin = _tilt_ymin;
	pDevice->TiltFormat = _TiltFormat;
	pDevice->Xresolution = _Xresolution;
	pDevice->Yresolution = _Yresolution;

	return BS_OK;
}

EXPORT_METHOD int CParser::settingWriterInfor(int _test_id,
											 int _writing_hand,
											 int _age,
											 int _gender)
{
	pPData = new CPData;
	if (pPData == 0)
	{		
        return BS_ERR_MEM;
	}
	pPData->test_id = _test_id;
	pPData->age = _age;
	pPData->gender = _gender;
	pPData->writing_hand = _writing_hand;

	return BS_OK;
}
EXPORT_METHOD int CParser::settingDataSet(int _tcapture,
									 int _genuine,
									 int _semantic_class,
									 char* _textual_content)
{
	pDataSet = new CDataSet;
	if (pDataSet == 0)
	{
		return BS_ERR_MEM;
	}

	pDataSet->tcapture = _tcapture;
	pDataSet->genuine = _genuine;
	pDataSet->textual_content = _textual_content;

	return BS_OK;
	
}

EXPORT_METHOD CDataSet* CParser::parse(const char *paID, bool presetting)
{
	int result;
	if (presetting)
	{		
		result = settingDevice();
		if (result == BS_ERR_MEM)
		{
			return 0;
		}

		result = settingWriterInfor();
		if (result == BS_ERR_MEM)
		{
			return 0;
		}

		result = settingDataSet();
		if (result == BS_ERR_MEM)
		{
			return 0;
		}
	}
	else
	{
		// verify if user has set parameters himself?
		if ( (pDevice == 0) || (pPData == 0) || (pDataSet == 0))
		{
			return 0;
		}
	}

	
	pDataSet->device = pDevice;
	pDataSet->info = pPData;

	result = do_parse(paID);
	if ( (result == BS_ERR_MEM) || (result == BS_ERR_LOAD))
	{
		delete pDataSet;
		return 0;
	}

	pDataSet->sourcedata = pSource;

	return pDataSet;
}

EXPORT_METHOD int CParserSVC::do_parse(const char* paID)
{
	int xmin,ymin,xmax,ymax;
    int i = 0;
    ifstream inFile;
    int totalPoints;

    inFile.open(paID);

    if (!inFile)
    {
        fprintf(stdout,"error in pa_svc2004: Unable to open file %s .\n",paID);
        return BS_ERR_LOAD;
    }

    inFile >> totalPoints;


	pSource = (CSourceData*)bscalloc(sizeof(CSourceData), totalPoints);

    if (pSource == 0)
    {
        inFile.close();
        fprintf(stdout,"error in pa_svc2004: memory allocation error.\n");
        return BS_ERR_MEM;
    }

    while (i < totalPoints)
    {

        //this is what is available from the sample SVC2004 raw data file

        inFile >> (pSource[i].x);
        inFile >> (pSource[i].y);
        inFile >> (pSource[i].time);
        inFile >> (pSource[i].tip);
        inFile >> (pSource[i].azimuth);
        inFile >> (pSource[i].elevation);
        inFile >> (pSource[i].force);


        //these values are not present in the raw data format being used for testing

        pSource[i].button =                        0;

        i ++;
    }

    inFile.close();

    xmin=pSource[0].x;
    xmax=pSource[0].x;
    ymin=pSource[0].y;
    ymax=pSource[0].y;


	//Since the max and min x,y coordinates are not available from the SVC2004 data, these values are calculated 
	//from the data itself

    for (i=1;i<totalPoints;i++)
    {
        xmin=min(xmin,pSource[i].x);
        xmax=max(xmax,pSource[i].x);
        ymin=min(ymin,pSource[i].y);
        ymax=max(ymax,pSource[i].y);
    }

	// setting extra parameters for device
	pDevice->Xorigin = xmin;
	pDevice->Yorigin = ymin;
	pDevice->Xmax = xmax;
	pDevice->Ymax = ymax;

	// setting extra parameters for dataset
	pDataSet->num_packets = totalPoints;

	return BS_OK;
}

EXPORT_METHOD int CParserBiomet::do_parse(const char* paID)
{
	int xmin,ymin,xmax,ymax;
    int i = 0;
    ifstream inFile;
    int totalPoints = 0;

    inFile.open(paID);

    if (!inFile)
    {
        fprintf(stdout,"error in pa_biomet: Unable to open file %s .\n",paID);
        return BS_ERR_LOAD;
    }

    // Because file in BIOMET format has no line indicate the number of lines
	// we have to count the total points first
	char buffer[256];
	while (! inFile.eof() )
	{
		inFile.getline(buffer, 256);
		totalPoints++;
	}

	totalPoints--; // pass the last empty lines
	inFile.close();
	
	ifstream inFile1;
	
	inFile1.open(paID);

	pSource = (CSourceData*)bscalloc(sizeof(CSourceData), totalPoints);

    if (pSource == 0)
    {
        inFile1.close();
        fprintf(stdout,"error in pa_biomet: memory allocation error.\n");
        return BS_ERR_MEM;
    }

    while (i < totalPoints)
    {

        //this is what is available from the sample BIOMET raw data file

        inFile1 >> (pSource[i].x);
        inFile1 >> (pSource[i].y);        
		inFile1 >> (pSource[i].force);
		inFile1 >> (pSource[i].elevation);
        inFile1 >> (pSource[i].azimuth);

        //these values are not present in the raw data format being used for testing

        pSource[i].button = 0;
		pSource[i].time = TIME_UNKNOWN;
        pSource[i].tip = 0;

        i ++;
    }

    inFile1.close();

    xmin=pSource[0].x;
    xmax=pSource[0].x;
    ymin=pSource[0].y;
    ymax=pSource[0].y;


	//Since the max and min x,y coordinates are not available from the BIOMET data, these values are calculated 
	//from the data itself

    for (i=1;i<totalPoints;i++)
    {
        xmin=min(xmin,pSource[i].x);
        xmax=max(xmax,pSource[i].x);
        ymin=min(ymin,pSource[i].y);
        ymax=max(ymax,pSource[i].y);
    }

	// setting extra parameters for device
	pDevice->Xorigin = xmin;
	pDevice->Yorigin = ymin;
	pDevice->Xmax = xmax;
	pDevice->Ymax = ymax;

	// setting extra parameters for dataset
	pDataSet->num_packets = totalPoints;

	return BS_OK;
}

EXPORT_METHOD int CParserMCYT::do_parse(const char* paID)
{
	int xmin,ymin,xmax,ymax;
    int i = 0;
    ifstream inFile;
    int totalPoints = 0;

    inFile.open(paID);

    if (!inFile)
    {
        fprintf(stdout,"error in pa_mcyt: Unable to open file %s .\n",paID);
        return BS_ERR_LOAD;
    }

    // Because file in MCYT format has no line indicate the number of lines
	// we have to count the total points first
	char buffer[256];
	while (! inFile.eof() )
	{
		inFile.getline(buffer, 256);
		totalPoints++;
	}

	totalPoints--; // pass the last empty lines
	inFile.close();
	ifstream inFile1;
	inFile1.open(paID);

	pSource = (CSourceData*)bscalloc(sizeof(CSourceData), totalPoints);

    if (pSource == 0)
    {
        inFile1.close();
        fprintf(stdout,"error in pa_mcyt: memory allocation error.\n");
        return BS_ERR_MEM;
    }

    while (i < totalPoints)
    {

        //this is what is available from the sample MCYT raw data file

        inFile1 >> (pSource[i].x);
        inFile1 >> (pSource[i].y);        
		inFile1 >> (pSource[i].force);		
        inFile1 >> (pSource[i].azimuth);
		inFile1 >> (pSource[i].elevation);

        //these values are not present in the raw data format being used for testing

        pSource[i].button = 0;
		pSource[i].time = TIME_UNKNOWN;
        pSource[i].tip = 0;

        i ++;
    }

    inFile1.close();

    xmin=pSource[0].x;
    xmax=pSource[0].x;
    ymin=pSource[0].y;
    ymax=pSource[0].y;


	//Since the max and min x,y coordinates are not available from the MCYT data, these values are calculated 
	//from the data itself

    for (i=1;i<totalPoints;i++)
    {
        xmin=min(xmin,pSource[i].x);
        xmax=max(xmax,pSource[i].x);
        ymin=min(ymin,pSource[i].y);
        ymax=max(ymax,pSource[i].y);
    }

	// setting extra parameters for device
	pDevice->Xorigin = xmin;
	pDevice->Yorigin = ymin;
	pDevice->Xmax = xmax;
	pDevice->Ymax = ymax;

	// setting extra parameters for dataset
	pDataSet->num_packets = totalPoints;

	return BS_OK;
}
