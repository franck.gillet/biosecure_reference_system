/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_DYNLINK_H
#define BS_DYNLINK_H

#ifdef _MSC_VER

#ifdef __cplusplus
#define IMPORT extern "C" __declspec (dllexport)
#define EXPORT extern "C" __declspec (dllexport)
#define IMPORT_METHOD __declspec (dllexport)
#define EXPORT_METHOD __declspec (dllexport)
#else
#define IMPORT __declspec (dllimport)
#define EXPORT __declspec (dllexport)
#endif

#define RTLD_LAZY 0x1
#define RTLD_NOW 0x2
#define RTLD_GLOBAL 0x4
#define RTLD_LOCAL 0x8

IMPORT void *dlopen(const char *file,int mode);
IMPORT void *dlsym(void *handle, const char *name);
IMPORT int dlclose(void *handle);
IMPORT char *dlerror(void);

#else

#include <dlfcn.h>

#ifdef __cplusplus
#define IMPORT extern "C"
#define EXPORT extern "C"
#define IMPORT_METHOD
#define EXPORT_METHOD
#else
#define IMPORT extern
#define EXPORT extern
#endif

#endif

#endif

