/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include<iostream.h>
#include <bs_types.h>
#include <bs_dynlink.h>
#include <bs_cdataset.h>
#include <bs_cfeatureset.h>
#include <bs_cresult.h>
#include <bs_mem.h>
#include <bs_cchain.h>
#include <bs_error.h>
#include <bs_defines.h>
#include <bs_cp.h>
#include <bs_cclientmodel.h>

const CChain *fwEnroll,*fwTest,*fwTrain;         // local enrollment & testdata chain
CModSM fwsm={0};                        // local training based matcher
CModSR fwsr={0};                        // local reference based matcher
CModTR fwtr={0};
CModFU fwfu={0};
int fwmtype=-1;                         // local matcher type
int fwenrollid=-1;                      // local enrollment id
int fwtestid=-1;                        // local testdata id

CDataSet *fwenrollds,*fwtestds;         // local dataset
CFeatureSet *fwenrollfs,*fwtestfs;      // local featureset
CClientModel *fwmodel;					// local client model
EXPORT int ConfigureModules(const CChain *enrollment,const CChain *testdata,const char *mname,
                            const char *ParamString,int mtype)
{
    int result;

    if (fwmtype!=-1)
    {
        fprintf(stderr,"Error: Modules already configured.\n");
        return BS_ERR_INVALID;
    }
    switch (mtype)
    {
    case BS_MATCHER_REFERENCE:
        result=fwsr.Load(mname);
        if (result<0)
            return result;
        result=fwsr.srSetParameters(ParamString);
        if (result<0)
        {
            fwsr.Unload();
            fprintf(stderr,fwsr.srLastError());
            return result;
        }
        fwmtype=mtype;
        fwEnroll=enrollment;
        fwTest=testdata;
        return BS_OK;
    case BS_MATCHER_TRAINING:
        result=fwsm.Load(mname);
        if (result<0)
            return result;
        result=fwsm.smSetParameters(ParamString);
        if (result<0)
        {
            fwsm.Unload();
            fprintf(stderr,fwsm.smLastError());
            return result;
        }
		result = fwtr.Load(mname);
        fwmtype=mtype;
        fwEnroll=enrollment;
        fwTest=testdata;
        return BS_OK;
    }
    fprintf(stderr,"Error: Invalid matcher type.\n");
    return BS_ERR_INVALID;
}

EXPORT int LoadEnrollment(const char *id)
{
    int result;
    int i;
    
	
    if (fwmtype==-1)
    {
        fprintf(stderr,"Error: Modules are not configured.\n");
        return BS_ERR_INVALID;
    }
/*
    if ((fwenrollid!=-1) & (fwmtype!=BS_MATCHER_TRAINING))
    {
        fprintf(stderr,"Error: Enrollment already loaded.\n");
        return BS_ERR_INVALID;
    }
*/
    result=fwEnroll->pa.paExecute(&fwenrollds,id);
    if (result<0)
    {
        fprintf(stderr,fwEnroll->pa.paLastError());
        return result;
    }
    fwenrollds->processed=NO_PROCESS;
    if (fwenrollds->genuine==GENUINE_UNKNOWN) fwenrollds->genuine=GENUINE_YES;

    for (i=0;i<fwEnroll->ppcount;i++)
    {
        result=fwEnroll->pp[i].ppExecute(fwenrollds);
        if (result<0)
        {
            fprintf(stderr,fwEnroll->pp[i].ppLastError());
            delete fwenrollds;
			fwenrollds=NULL;
            return result;
        }
        fwenrollds->processed=PROCESSED;
    }

    result=fwEnroll->fe.feExecute(fwenrollds,&fwenrollfs);
    
	if (result<0)
    {
        fprintf(stderr,fwEnroll->fe.feLastError());
        delete fwenrollds;
		fwenrollds=NULL;
        return result;
    }

    for (i=0;i<fwEnroll->fscount;i++)
    {
        result=fwEnroll->fs[i].fsExecute(fwenrollfs);
        if (result<0)
        {
            fprintf(stderr,fwEnroll->fs[i].fsLastError());
            delete fwenrollfs;
            fwenrollfs=NULL;
            delete fwenrollds;
			fwenrollds=NULL;
            return result;
        }
    }

    delete fwenrollds;
	fwenrollds=NULL;
    fwenrollid++;
    return BS_OK;
}

EXPORT void UnloadEnrollment(void)
{
    if (fwenrollid==-1)
        return;
    delete fwenrollfs;
    fwenrollfs=NULL;
    fwenrollid=-1;
	delete fwmodel;
	fwmodel = NULL;
}

EXPORT int LoadTestData(const char *id,int genuine)
{
    int result;
    int i;

    if (fwmtype==-1)
    {
        fprintf(stderr,"Error: Modules are not configured.\n");
        return BS_ERR_INVALID;
    }

    if (fwtestid!=-1)
    {
        fprintf(stderr,"Error: Test-Data already loaded.\n");
        return BS_ERR_INVALID;
    }

    result=fwTest->pa.paExecute(&fwtestds,id);
    if (result<0)
    {
        fprintf(stderr,fwTest->pa.paLastError());
        return result;
    }
    fwtestds->processed=NO_PROCESS;
    if (fwtestds->genuine==GENUINE_UNKNOWN) fwtestds->genuine=genuine;

    for (i=0;i<fwTest->ppcount;i++)
    {
        result=fwTest->pp[i].ppExecute(fwtestds);
        if (result<0)
        {
            fprintf(stderr,fwTest->pp[i].ppLastError());
            delete fwtestds;
			fwtestds=NULL;
            return result;
        }
        fwtestds->processed=PROCESSED;
    }

    result=fwTest->fe.feExecute(fwtestds,&fwtestfs);
    if (result<0)
    {
        fprintf(stderr,fwTest->fe.feLastError());
        delete fwtestds;
		fwtestds=NULL;
        return result;
    }

    for (i=0;i<fwTest->fscount;i++)
    {
        result=fwEnroll->fs[i].fsExecute(fwtestfs);
        if (result<0)
        {
            fprintf(stderr,fwEnroll->fs[i].fsLastError());
            delete fwtestfs;
	    fwtestfs=NULL;
            delete fwtestds;
			fwtestds=NULL;
            return result;
        }
    }

    fwtestid++;
    return BS_OK;
}

EXPORT void UnloadTestData(void)
{
    if (fwtestid==-1)
        return;
    delete fwtestfs;
    fwtestfs=NULL;
    delete fwtestds;
    fwtestds=NULL;
    fwtestid=-1;
}

EXPORT int CalcScore(CResult **fwResult)
{
    int result;

    if (fwmtype==-1)
    {
        fprintf(stderr,"Error: Modules are not configured.\n");
        return BS_ERR_INVALID;
    }

    if (fwenrollid==-1)
    {
        fprintf(stderr,"Error: Enrollment not loaded.\n");
        return BS_ERR_INVALID;
    }

    if (fwtestid==-1)
    {
        fprintf(stderr,"Error: Test-Data not loaded.\n");
        return BS_ERR_INVALID;
    }
    
    switch (fwmtype)
    {
    case BS_MATCHER_REFERENCE:
        result=fwsr.srExecute(fwenrollfs,fwtestfs,fwResult);
		UnloadTestData();              // changes made here
        
		if (result<0)
            fprintf(stderr,fwsr.srLastError());
        return result;
    case BS_MATCHER_TRAINING:
        result=fwsm.smExecute(fwmodel,fwtestfs,fwResult);
        if (result<0)
            fprintf(stderr,fwsm.smLastError());

        return result;
    }

    fprintf(stderr,"Error: Illegal Matcher Type.\n");
    return BS_ERR_INVALID;
}

EXPORT void UnloadModules(void)
{
    UnloadEnrollment();
    UnloadTestData();
    switch (fwmtype)
    {
    case BS_MATCHER_REFERENCE:
        fwsr.Unload();
        break;
    case BS_MATCHER_TRAINING:
        fwsm.Unload();
        break;
    }
    fwmtype=-1;
}

EXPORT int bsGetModuleType(const char *type)
{
    if ((type[0]=='p') & (type[1]=='a'))
        return BS_TYPE_PA;
    if ((type[0]=='p') & (type[1]=='p'))
        return BS_TYPE_PP;
    if ((type[0]=='f') & (type[1]=='e'))
        return BS_TYPE_FE;
    if ((type[0]=='f') & (type[1]=='s'))
        return BS_TYPE_FS;
    if ((type[0]=='s') & (type[1]=='r'))
        return BS_TYPE_SR;
    if ((type[0]=='s') & (type[1]=='m'))
        return BS_TYPE_SM;
	if ((type[0]=='t') & (type[1]=='r'))
        return BS_TYPE_TR;
    if ((type[0]=='f') & (type[1]=='u'))
        return BS_TYPE_FU;        
    return -1;
}

EXPORT void ListModules(int ModuleType,int InfoType)
{
    int i,c;
    CModPA pa={0};
    CModPP pp={0};
    CModFE fe={0};
    CModFS fs={0};
    CModSR sr={0};
    CModSM sm={0};
	CModTR tr={0};
	CModFU fu={0};

    fprintf(stdout,"Listing Modules:\n\n");
    if (cprofiles==0)
    {
        fprintf(stderr,"Error: No modules found.\n");
        return;
    }

    if ((ModuleType&BS_TYPE_PA)!=0)
    {
        fprintf(stdout,"Listing available parser modules:\n\n");
        c=0;
        for (i=0;i<cprofiles;i++)
            if (bsGetModuleType(cprofile[i].type)==BS_TYPE_PA)
            {
                fprintf(stdout,"Module ID: %s\n",cprofile[i].name);
                if (pa.Load(cprofile[i].name)==BS_OK)
                {
                    pa.paInfo(InfoType);
                    pa.Unload();
                    c++;
                }
                fprintf(stdout,"\n");
            }
        if (c==0)
            fprintf(stdout,"No parser modules found.\n");
        fprintf(stdout,"\n");
    }

    if ((ModuleType&BS_TYPE_PP)!=0)
    {
        fprintf(stdout,"Listing available preprocessing modules:\n\n");
        c=0;
        for (i=0;i<cprofiles;i++)
            if (bsGetModuleType(cprofile[i].type)==BS_TYPE_PP)
            {
                fprintf(stdout,"Module ID: %s\n",cprofile[i].name);
                if (pp.Load(cprofile[i].name)==BS_OK)
                {
                    pp.ppInfo(InfoType);
                    pp.Unload();
                    c++;
                }
                fprintf(stdout,"\n");
            }
        if (c==0)
            fprintf(stdout,"No preprocessing modules found.\n");
        fprintf(stdout,"\n");
    }

    if ((ModuleType&BS_TYPE_FE)!=0)
    {
        fprintf(stdout,"Listing available feature extraction modules:\n\n");
        c=0;
        for (i=0;i<cprofiles;i++)
            if (bsGetModuleType(cprofile[i].type)==BS_TYPE_FE)
            {
                fprintf(stdout,"Module ID: %s\n",cprofile[i].name);
                if (fe.Load(cprofile[i].name)==BS_OK)
                {
                    fe.feInfo(InfoType);
                    fe.Unload();
                    c++;
                }
                fprintf(stdout,"\n");
            }
        if (c==0)
            fprintf(stdout,"No feature extraction modules found.\n");
        fprintf(stdout,"\n");
    }

    if ((ModuleType&BS_TYPE_FS)!=0)
    {
        fprintf(stdout,"Listing available feature selection modules:\n\n");
        c=0;
        for (i=0;i<cprofiles;i++)
            if (bsGetModuleType(cprofile[i].type)==BS_TYPE_FS)
            {
                fprintf(stdout,"Module ID: %s\n",cprofile[i].name);
                if (fs.Load(cprofile[i].name)==BS_OK)
                {
                    fs.fsInfo(InfoType);
                    fs.Unload();
                    c++;
                }
                fprintf(stdout,"\n");
            }
        if (c==0)
            fprintf(stdout,"No feature selection modules found.\n");
        fprintf(stdout,"\n");
    }

    if ((ModuleType&BS_TYPE_SR)!=0)
    {
        fprintf(stdout,"Listing available reference based matching modules:\n\n");
        c=0;
        for (i=0;i<cprofiles;i++)
            if (bsGetModuleType(cprofile[i].type)==BS_TYPE_SR)
            {
                fprintf(stdout,"Module ID: %s\n",cprofile[i].name);
                if (sr.Load(cprofile[i].name)==BS_OK)
                {
                    sr.srInfo(InfoType);
                    sr.Unload();
                    c++;
                }
                fprintf(stdout,"\n");
            }
        if (c==0)
            fprintf(stdout,"No reference based matching modules found.\n");
        fprintf(stdout,"\n");
    }

    if ((ModuleType&BS_TYPE_SM)!=0)
    {
        fprintf(stdout,"Listing available training based matching modules:\n\n");
        c=0;
        for (i=0;i<cprofiles;i++)
            if (bsGetModuleType(cprofile[i].type)==BS_TYPE_SM)
            {
                fprintf(stdout,"Module ID: %s\n",cprofile[i].name);
                if (sm.Load(cprofile[i].name)==BS_OK)
                {
                    sm.smInfo(InfoType);
                    sm.Unload();
                    c++;
                }
                fprintf(stdout,"\n");
            }
        if (c==0)
            fprintf(stdout,"No training based matching modules found.\n");
        fprintf(stdout,"\n");
    }

	if ((ModuleType&BS_TYPE_TR)!=0)
    {
        fprintf(stdout,"Listing available training based modules:\n\n");
        c=0;
        for (i=0;i<cprofiles;i++)
            if (bsGetModuleType(cprofile[i].type)==BS_TYPE_TR)
            {
                fprintf(stdout,"Module ID: %s\n",cprofile[i].name);
                if (tr.Load(cprofile[i].name)==BS_OK)
                {
                    tr.trInfo(InfoType);
                    tr.Unload();
                    c++;
                }
                fprintf(stdout,"\n");
            }
        if (c==0)
            fprintf(stdout,"No training based modules found.\n");
        fprintf(stdout,"\n");
    }
    
	 if ((ModuleType&BS_TYPE_FU)!=0)
    {
        fprintf(stdout,"Listing available fusion modules:\n\n");
        c=0;
        for (i=0;i<cprofiles;i++)
            if (bsGetModuleType(cprofile[i].type)==BS_TYPE_FU)
            {
                fprintf(stdout,"Module ID: %s\n",cprofile[i].name);
                if (fu.Load(cprofile[i].name)==BS_OK)
                {
                    fu.fuInfo(InfoType);
                    fu.Unload();
                    c++;
                }
                fprintf(stdout,"\n");
            }
        if (c==0)
            fprintf(stdout,"No fusion modules found.\n");
        fprintf(stdout,"\n");
    }
}

int bsCheckChain(const CChain *ch,int &inpm)
{
    int i;
    int input,module;

    if (ch->pa.handle==0)
    {
        fprintf(stderr,"Parser module not specified.\n");
        return -1;
    }
    if (ch->fe.handle==0)
    {
        fprintf(stderr,"Feature extraction module not specified.\n");
        return -1;
    }

    input=FindCProfile("pa",ch->pa.name);

    for (i=0;i<ch->ppcount;i++)
    {
        module=FindCProfile("pp",ch->pp[i].name);
        if (TestCProfile(input,module)<0)
            return -1;
        input=module;
    }

    module=FindCProfile("fe",ch->fe.name);
    inpm=module;
    if (TestCProfile(input,module)<0)
        return -1;
    input=module;

    for (i=0;i<ch->fscount;i++)
    {
        module=FindCProfile("fs",ch->fs[i].name);
        if (TestCProfile(input,module)<0)
            return -1;
        input=module;
    }

    return module;
}

EXPORT int CheckValidity(void)
{
    int inpenroll1,inpenroll2,inptest1,inptest2,module;
    int i,j;

    switch (fwmtype)
    {
    case BS_MATCHER_REFERENCE:
        module=FindCProfile("sr",fwsr.name);
        break;
    case BS_MATCHER_TRAINING:
        module=FindCProfile("sm",fwsm.name);
        break;
    default:
        fprintf(stderr,"Error: Modules are not configured.\n");
        return BS_ERR_INVALID;
    }

    inpenroll1=bsCheckChain(fwEnroll,inpenroll2);
    if (inpenroll1<0)
        return BS_ERR_INVALID;
    inptest1=bsCheckChain(fwTest,inptest2);
    if (inptest1<0)
        return BS_ERR_INVALID;

    if (TestCProfile(inpenroll1,module)<0)
        return BS_ERR_INVALID;
    if (TestCProfile(inptest1,module)<0)
        return BS_ERR_INVALID;
    if (TestCProfile(inpenroll2,module)<0)
        return BS_ERR_INVALID;
    if (TestCProfile(inptest2,module)<0)
        return BS_ERR_INVALID;

    // check for required feature selectors
    for (i=0;i<cprofile[module].inputs;i++)
    {
        inptest1=cprofile[module].input[i];
        if (inptest1<0)
        {
            inptest1=~inptest1;
            if (bsGetModuleType(cprofile[inptest1].type)==BS_TYPE_FS)
            {
                for (j=0;j<fwEnroll->fscount;j++)
                    if (FindCProfile("fs",fwEnroll->fs[j].name)==inptest1)
                        break;
                if (j==fwEnroll->fscount)
                    return BS_ERR_INVALID;
                for (j=0;j<fwTest->fscount;j++)
                    if (FindCProfile("fs",fwTest->fs[j].name)==inptest1)
                        break;
                if (j==fwEnroll->fscount)
                    return BS_ERR_INVALID;
            }
        }
    }

    // check for required preprocessors
    module=FindCProfile("fe",fwEnroll->fe.name);
    for (i=0;i<cprofile[module].inputs;i++)
    {
        inptest1=cprofile[module].input[i];
        if (inptest1<0)
        {
            inptest1=~inptest1;
            if (bsGetModuleType(cprofile[inptest1].type)==BS_TYPE_PP)
            {
                for (j=0;j<fwEnroll->ppcount;j++)
                    if (FindCProfile("pp",fwEnroll->pp[j].name)==inptest1)
                        break;
                if (j==fwEnroll->ppcount)
                    return BS_ERR_INVALID;
            }
        }
    }

    module=FindCProfile("fe",fwTest->fe.name);
    for (i=0;i<cprofile[module].inputs;i++)
    {
        inptest1=cprofile[module].input[i];
        if (inptest1<0)
        {
            inptest1=~inptest1;
            if (bsGetModuleType(cprofile[inptest1].type)==BS_TYPE_PP)
            {
                for (j=0;j<fwTest->ppcount;j++)
                    if (FindCProfile("pp",fwTest->pp[j].name)==inptest1)
                        break;
                if (j==fwTest->ppcount)
                    return BS_ERR_INVALID;
            }
        }
    }

    return BS_OK;
}

EXPORT int TrainModel()
{
	int result = BS_ERR_INVALID;

    if (fwmtype==-1)
    {
        fprintf(stderr,"Error: Modules are not configured.\n");
        return BS_ERR_INVALID;
    }

    if (fwenrollid==-1)
    {
        fprintf(stderr,"Error: Enrollment not loaded.\n");
        return BS_ERR_INVALID;
    }

    switch (fwmtype)
    {
		case BS_MATCHER_REFERENCE:
			return BS_OK;
		case BS_MATCHER_TRAINING:
			result = fwtr.trExecute(fwenrollfs,&fwmodel);
			if (result<0)
				fprintf(stderr,fwsm.smLastError());
			return result;
    }

    fprintf(stderr,"Error: Illegal Matcher Type.\n");
    return BS_ERR_INVALID;
}

EXPORT int Fusion(const char *name, const char *ParamString)
{
    int result = BS_ERR_INVALID;
    CResult *fwResult; 
    
    // change module name here
    if (fwfu.Load(name)<0)
        return BS_ERR_LIB;

    result=fwfu.fuSetParameters(ParamString);
    if (result<0)
    {
        fprintf(stderr,"%s\n",fwfu.fuLastError());
        fwfu.Unload();
        return result;
    }

    result=fwfu.fuExecute(&fwResult);
    if (result<0)
    {
        fprintf(stderr,"%s\n",fwfu.fuLastError());
        fwfu.Unload();
        return result;
    }

    if (fwResult)
      delete fwResult;
    
    fwfu.Unload();
    return BS_OK;
}
