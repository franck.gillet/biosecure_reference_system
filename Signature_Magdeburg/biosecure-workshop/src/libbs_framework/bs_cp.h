/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CP_H
#define BS_CP_H

#include <bs_defines.h>
#include <bs_dynlink.h>

class CProfile
{
public:
    char name[MAX_PATH];
    char type[2];
    int *input;
    int inputs;
};

extern CProfile *cprofile;
extern int cprofiles;

IMPORT int LoadCP(const char *fname);
IMPORT void UnloadCP(void);

long FindCProfile(const char *mtype,const char *mname);
int TestCProfile(int input,int module);

#endif
