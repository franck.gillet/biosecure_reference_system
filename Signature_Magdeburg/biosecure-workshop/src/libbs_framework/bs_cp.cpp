/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <bs_cp.h>
#include <bs_error.h>
#include <bs_mem.h>

#define TAG_MODULE_START "<module "
#define TAG_MODULE_END "</module>"
#define TAG_INPUT_START "<input "
#define TAG_INPUT_END "</input>"
#define TAG_CP_START "<cprofile"
#define TAG_CP_END "</cprofile>"
#define PROP_TYPE "type"
#define PROP_NAME "name"
#define PROP_REQUIRED "required"

CProfile *cprofile=0;
int cprofiles=0;

char lc(char c)
{
    if ((c>='A') & (c<='Z'))
        return c-'A'+'a';
    return c;
}

int cmpstr(const char *s1,const char *s2,long size)
{
    int i;

    for (i=0;i<size;i++)
        if (lc(s1[i])!=lc(s2[i]))
            return -1;
    return 0;
}

long FindTag(const char *buf,long size,long start,const char *tag)
{
    long i;
    long l=strlen(tag);

    for (i=start;i<(size-l);i++)
    {
        if (cmpstr(&(buf[i]),tag,l)==0)
            return i;
    }
    return -1;
}

long FindProp(const char *buf,long size,long cur,const char *prop)
{
    int eq=0,c;
    long l=strlen(prop);

    for (c=cur;c<(size-l);c++)
    {
        if (buf[c]=='>')
            return -1;
        if (cmpstr(&(buf[c]),prop,l)==0)
            break;
    }
    if (c==(size-l))
        return -1;

    c+=strlen(prop);
    while (c<size)
    {
        if (buf[c]=='=')
            eq=1;
        else if (buf[c]=='\"')
        {
            if (eq==0)
                return -1;
            else
                return c+1;
        }
        else if ((buf[c]!=' ') & (buf[c]!=0xa) & (buf[c]!=0xd))
            return -1;
        c++;
    }
    return -1;
}

long FindCPModule(const char *buf,long size,long cur,char *mtype,char *mname,long &c)
{
    long ptype,pname,e;

    c=FindTag(buf,size,cur,TAG_MODULE_START);
    if (c==-1)
    {
        c=cur;
        return -2;
    }
    ptype=FindProp(buf,size,c,PROP_TYPE);
    pname=FindProp(buf,size,c,PROP_NAME);

    if ((ptype==-1) | (pname==-1))
        return -1;

    e=FindTag(buf,size,ptype,"\"");
    if (e!=(ptype+2))
        return -1;
    mtype[0]=buf[ptype];
    mtype[1]=buf[ptype+1];
    e=FindTag(buf,size,pname,"\"");
    if (e==-1)
        return -1;
    memcpy(mname,&(buf[pname]),e-pname);
    mname[e-pname]=0;
    return c;
}

long FindCPInput(const char *buf,long size,long cur,char *mtype,long &c,long &req)
{
    long ptype,e,preq;

    c=FindTag(buf,size,cur,TAG_INPUT_START);
    if (c==-1)
    {
        c=cur;
        return -2;
    }
    ptype=FindProp(buf,size,c,PROP_TYPE);
    preq=FindProp(buf,size,c,PROP_REQUIRED);

    if (ptype==-1)
        return -1;

    e=FindTag(buf,size,ptype,"\"");
    if (e!=(ptype+2))
        return -1;
    c=ptype;
    while (c<size)
        if (buf[c++]=='>')
            break;
    mtype[0]=buf[ptype];
    mtype[1]=buf[ptype+1];

    req=0;

    if (preq==-1)
        return c;

    e=FindTag(buf,size,preq,"\"");
    if (e!=(preq+1))
        return c;

    req=buf[preq]-'0';
    return c;
}

long FindCPInputName(const char *buf,long size,long cur,char *mname,long &c)
{
    long i;
    const char endtag[]=TAG_INPUT_END;

    c=cur;
    while ((buf[c]==' ') | (buf[c]==0xa) | (buf[c]==0xd))
    {
        c++;
        if (c==size)
            return -1;
    }

    if (cmpstr(&(buf[c]),endtag,strlen(endtag))==0)
        return -2;

    i=0;
    while ((c<size) & (i<MAX_PATH))
    {
        if ((buf[c]==' ') | (buf[c]==0xa) | (buf[c]==0xd) | (buf[c]=='<'))
            break;
        mname[i++]=buf[c++];
    }
    if ((c==size) | (i==MAX_PATH))
        return -1;
    mname[i]=0;
    return c;
}

long FindCProfile(const char *mtype,const char *mname)
{
    long i;

    for (i=0;i<cprofiles;i++)
    {
        if ( (strncmp(mtype,cprofile[i].type,2)==0) &
                (strcmp(mname,cprofile[i].name)==0) )
            return i;
    }
    return -1;
}

int TestCProfile(int input,int module)
{
    int i,j;

    for (i=0;i<cprofile[module].inputs;i++)
    {
        j=cprofile[module].input[i];
        if (j<0)
            j=~j;
        if (j==input)
            return 0;
    }
    return -1;
}

EXPORT int LoadCP(const char *fname)
{
    char *buf;
    FILE *f;
    long size;
    long first,last,cur,res,c,req;
    long mods,mode,modi,i;
    char mname[MAX_PATH];
    char mtype[2];
    void *tmp;

    f=fopen(fname,"rb");
    if (f==0)
    {
        fprintf(stderr,"Error: Could not open Compatibility Profile.\n");
        return BS_ERR_LOAD;
    }

    if (fseek(f,0,SEEK_END)!=0)
    {
        fclose(f);
        fprintf(stderr,"Error: Could not execute fseek() in Compatibility Profile.\n");
        return BS_ERR_LOAD;
    }

    size=ftell(f);
    if (size<=0)
    {
        fclose(f);
        fprintf(stderr,"Error: Could not execute ftell() on Compatibility Profile3.\n");
        return BS_ERR_LOAD;
    }

    if (fseek(f,0,SEEK_SET)!=0)
    {
        fclose(f);
        fprintf(stderr,"Error: Could not execute fseek() Compatibility Profile4.\n");
        return BS_ERR_LOAD;
    }

    buf=(char*)bsmalloc(size);
    if (buf==0)
    {
        fclose(f);
        fprintf(stderr,"Error: Not enough memory to load Compatibility Profile.\n");
        return BS_ERR_MEM;
    }

    if (fread(buf,1,size,f)==0)
    {
        bsfree(buf);
        fclose(f);
        fprintf(stderr,"Error: Could not load Compatibility Profile5.\n");
        return BS_ERR_LOAD;
    }

    fclose(f);

    c=0;
    for (i=0;i<(size-4);i++)
    {
        if (cmpstr(&(buf[i]),"<!--",4)==0)
            c++;
        if (cmpstr(&(buf[i]),"-->",3)==0)
            c--;
        if (c>0)
            buf[i]=' ';
    }

    first=FindTag(buf,size,0,TAG_CP_START);
    last=FindTag(buf,size,0,TAG_CP_END);

    if ((first==-1) | (last==-1))
    {
        bsfree(buf);
        fprintf(stderr,"Error: Parse error in Compatibility Profile.\n");
        return BS_ERR_INVALID;
    }

    cur=first;

    do
    {
        res=FindCPModule(buf,size,cur,mtype,mname,c);
        if (res>=0)
        {
            tmp=bsrealloc(cprofile,(cprofiles+1)*sizeof(CProfile));
            if (tmp==0)
            {
                bsfree(buf);
                UnloadCP();
                fprintf(stderr,"Error: Not enough memory to load Compatibility Profile.\n");
                return BS_ERR_MEM;
            }
            cprofile=(CProfile*)tmp;
            memcpy(cprofile[cprofiles].type,mtype,2);
            strcpy(cprofile[cprofiles].name,mname);
            cprofile[cprofiles].input=0;
            cprofile[cprofiles].inputs=0;
            cprofiles++;
        }
        cur=c+1;
    }
    while (res!=-2);

    cur=first;
    for (i=0;i<cprofiles;i++)
    {
        mods=FindCPModule(buf,size,cur,mtype,mname,c);
        cur=c+1;
        if (mods!=-1)
        {
            mode=FindTag(buf,size,mods,TAG_MODULE_END);
            if (mode==-1)
            {
                bsfree(buf);
                UnloadCP();
                fprintf(stderr,"Error: Parse error in Compatibility Profile.\n");
                return BS_ERR_INVALID;
            }
            do
            {
                res=FindCPInput(buf,mode,cur,mtype,c,req);
                cur=c+1;
                if (res>=0)
                {
                    while (FindCPInputName(buf,mode,cur,mname,c)>=0)
                    {
                        cur=c+1;
                        modi=FindCProfile(mtype,mname);
                        if (modi>=0)
                        {
                            tmp=bsrealloc(cprofile[i].input,(cprofile[i].inputs+1)*sizeof(int));
                            if (tmp==0)
                            {
                                bsfree(buf);
                                UnloadCP();
                                fprintf(stderr,"Error: Not enough memory to load Compatibility Profile.\n");
                                return BS_ERR_INVALID;
                            }
                            cprofile[i].input=(int*)tmp;
                            if (req!=0)
                                modi=~modi; //~n identifies required modules
                            cprofile[i].input[cprofile[i].inputs++]=modi;

                        }

                    }
                }
            }
            while (res>=0);
        }
    }

    bsfree(buf);
    fprintf(stderr,"Found %i modules in compatibility profile.\n",cprofiles);
    return BS_OK;
}

EXPORT void UnloadCP(void)
{
    int i;

    if (cprofile!=0)
    {
        for (i=0;i<cprofiles;i++)
            if (cprofile[i].input!=0)
                bsfree(cprofile[i].input);
        bsfree(cprofile);
    }
    cprofile=0;
    cprofiles=0;
}
