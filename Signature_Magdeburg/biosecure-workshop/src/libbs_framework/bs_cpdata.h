/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CPDATA_H
#define BS_CPDATA_H

#include <bs_dynlink.h>

#define HAND_UNKNOWN    -1      // writing hand in unknown
#define HAND_RIGHT      0       // writing hand is right
#define HAND_LEFT       1       // writing hand is left

#define GENDER_UNKNOWN  -1      // gender is unknown
#define GENDER_MALE     0       // gender is male
#define GENDER_FEMALE   1       // gender is female

#define AGE_UNKNOWN     -1      // age is unknown

class CPData
{
public:
    int         test_id;        // numerical subject identifier
    int         writing_hand;   // HAND_UNKNOWN, HAND_RIGHT, HAND_LEFT
    int         age;            // Subject age or AGE_UNKNOWN
    int         gender;         // GENDER_UNKNOWN, GENDER_MALE, GENDER_FEMALE

    CPData(){}
    ~CPData(){}
    IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};


#endif
