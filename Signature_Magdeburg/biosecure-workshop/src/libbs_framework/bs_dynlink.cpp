/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef WIN32

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int uErrorOccurred=0;
char uErrorBuffer[0x100]="";


#ifdef __cplusplus
#define EXPORT extern "C" __declspec (dllexport)
#else
#define EXPORT __declspec (dllexport)
#endif

EXPORT void* dlopen(const char* file,int mode)
{
    void* handle=0;
    char* win_path=0;
    int i,file_length=strlen(file);

    if (file==0)
        return (void*)GetModuleHandle(0);

    win_path=(char*)malloc(strlen(file)+1);
    if (win_path==0)
    {
        uErrorOccurred=1;
        sprintf(uErrorBuffer,"dlopen: unable to allocate memory");
        return 0;
    }

    strncpy(win_path,file,file_length+1);
    for (i=0;i<file_length;i++)
        if (win_path[i] == '/')
            win_path[i] = '\\';

    handle=(void*)LoadLibrary(win_path);
    free(win_path);

    if (handle==0)
    {
        uErrorOccurred=1;
        _snprintf(uErrorBuffer,sizeof(uErrorBuffer),"dlopen: LoadLibrary() failed, error = %d",(int)GetLastError());
        return 0;
    }

    return handle;
}

EXPORT void* dlsym(void *handle,const char *name)
{
    void* proc_address=(void*)GetProcAddress((HMODULE)handle,name);

    if (proc_address==0)
    {
        uErrorOccurred=1;
        _snprintf(uErrorBuffer,sizeof(uErrorBuffer),"dlsym: GetProcAddress(%s) failed, error = %d",name,(int)GetLastError());
    }

    return proc_address;
}

EXPORT int dlclose(void* handle)
{
    if (handle==0)
        return 0;

    if (FreeLibrary((HMODULE)handle)==0)
    {
        uErrorOccurred=1;
        _snprintf(uErrorBuffer,sizeof(uErrorBuffer),"dlclose: FreeLibrary() failed, error = %d",(int)GetLastError());
        return 1;
    }

    return 0;
}

EXPORT char* dlerror(void)
{
    if (uErrorOccurred==0)
        return 0;

    uErrorOccurred=0;
    return uErrorBuffer;
}

#endif
