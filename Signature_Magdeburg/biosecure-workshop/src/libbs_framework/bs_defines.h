/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_DEFINES_H
#define BS_DEFINES_H

#include <bs_types.h>

#define BS_INFO_ALL             -1      // report everything
#define BS_INFO_NAME            1       // report module name
#define BS_INFO_VERSION         2       // report module version
#define BS_INFO_ABOUT           4       // report author information
#define BS_INFO_PARAMETERS      8       // report module parameters
#define BS_INFO_CAPABILITIES    16      // report the modules functionality

#define BS_MATCHER_REFERENCE    0
#define BS_MATCHER_TRAINING     1

#define BS_TYPE_ALL             -1      // All module types
#define BS_TYPE_PA              1       // Parser module
#define BS_TYPE_PP              2       // Preprocessing module
#define BS_TYPE_FE              4       // Feature extraction module
#define BS_TYPE_FS              8       // Feature selection module
#define BS_TYPE_SR              16      // Reference based matcher module
#define BS_TYPE_SM              32      // Training based matcher module
#define BS_TYPE_TR				64		// Training module
#define BS_TYPE_FU				128	// Fusion module

inline double not_known(void)
{
    __int64 n=SNAN64;
    return *((double*)&n);
}
#define NOT_KNOWN not_known()
#define IS_KNOWN(x) (*((__int64*)&(x))==SNAN64)

#ifndef MAX_PATH
#define MAX_PATH 4096
#endif

#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

#endif
