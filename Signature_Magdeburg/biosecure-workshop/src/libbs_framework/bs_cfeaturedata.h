/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CFEATUREDATA_H
#define BS_CFEATUREDATA_H

#include <bs_dynlink.h>

#define NOT_NORMALISED 0                // data not normalised
#define NORMALISED 1                    // data normalised

#define NOT_DISCRETE 0                  // values not discrete
#define DISCRETE 1                      // values discrete

#define TIME_UNKNOWN -1                 // time not specified

#define RESULT_STATIC -1		// use static value instead of dynamic feature_result

class CFeatureData
{
public:
    int		num_feature_results;	// size of feature_result or RESULT_STATIC to use static value
    union
    {
        double      *feature_result;    // feature result array
        double      value;		// feature result value	    
    };
    char        *feat_id;               // feature identifier string, always static
    int         normalised;             // NOT_NORMALISED, NORMALISED
    int         discrete;               // NOT_DISCRETE, DISCRETE
    double      lower_range;            // Lower feature range
    double      upper_range;            // Upper feature range
    int         time;                   // time stamp or TIME_UNKNOWN;

    CFeatureData(){}
    IMPORT_METHOD ~CFeatureData();
    IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};

#endif
