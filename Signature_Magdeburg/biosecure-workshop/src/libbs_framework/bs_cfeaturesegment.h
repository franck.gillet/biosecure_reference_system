/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CFEATURSEGMENT_H
#define BS_CFEATURESEGMENT_H

#include <bs_dynlink.h>
#include <bs_cfeaturevector.h>

class CFeatureSegment
{
public:
    int                 num_feature_vectors;    // number of feature vectors in segment
    char                *feat_seg_id;           // feature segment identifier string, always static
    CFeatureVector      *feat_vectors;          // segment of features

    CFeatureSegment(){}
    IMPORT_METHOD ~CFeatureSegment();
    IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};

#endif
