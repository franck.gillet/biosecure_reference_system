/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef LIBBS_FRAMEWORK_H
#define LIBBS_FRAMEWORK_H

#include <bs_types.h>
#include <bs_dynlink.h>
#include <bs_cdataset.h>
#include <bs_cfeatureset.h>
#include <bs_cresult.h>
#include <bs_error.h>
#include <bs_mem.h>
#include <bs_cchain.h>
#include <bs_defines.h>
#include <bs_cp.h>
#include <bs_parser.h>

IMPORT int ConfigureModules(const CChain *enrollment,const CChain *testdata,const char *mname,
                            const char *ParamString,int mtype);
IMPORT void UnloadModules(void);
IMPORT int LoadEnrollment(const char *id);
IMPORT void UnloadEnrollment(void);
IMPORT int LoadTestData(const char *id,int genuine);
IMPORT void UnloadTestData(void);
IMPORT void ListModules(int ModuleType,int InfoType);
IMPORT int CheckValidity(void);
IMPORT int CalcScore(CResult **fwResult);
IMPORT int bsGetModuleType(const char *type);
IMPORT int TrainModel(void);
IMPORT int Fusion(const char *name, const char *ParamString);

#endif
