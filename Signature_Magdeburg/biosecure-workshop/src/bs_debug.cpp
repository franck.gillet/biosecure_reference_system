/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <libbs_framework.h>
#include <bs_cmod.h>

CDataSet *testdataset;
CFeatureSet *testfeatureset;

// testing environment for shared modules
// later this will be done for the framework controller

// testing environment for parser module

int paTest(void)
{
    CModPA pa={0};
    int result;
    CDataSet *dataset;

    // change module name here
    if (pa.Load("svc2004")<0)
        return BS_ERR_LIB;

    pa.paInfo(BS_INFO_ALL);

    result=pa.paSetParameters(0);
    if (result<0)
    {
        fprintf(stderr,"%s\n",pa.paLastError());
        pa.Unload();
        return result;
    }

    result=pa.paExecute(&dataset,"SVC2004_Testdata/USER1_1.txt");
    if (result<0)
    {
        fprintf(stderr,"%s\n",pa.paLastError());
        pa.Unload();
        return result;
    }

    delete dataset;
    pa.Unload();
    return BS_OK;
}



// testing environment for pre-processing module

int ppTest(void)
{
    CModPP pp={0};
    int result;

    // change module name here
    if (pp.Load("normalise")<0)
        return BS_ERR_LIB;

    pp.ppInfo(BS_INFO_ALL);

    result=pp.ppSetParameters(0);
    if (result<0)
    {
        fprintf(stderr,"%s\n",pp.ppLastError());
        pp.Unload();
        return result;
    }

    result=pp.ppExecute(testdataset);
    if (result<0)
    {
        fprintf(stderr,"%s\n",pp.ppLastError());
        pp.Unload();
        return result;
    }

    pp.Unload();
    return BS_OK;
}





// testing environment for feature extraction module

int feTest(void)
{
    CModFE fe={0};
    CFeatureSet *featureset;

    int result;

    // change module name here
    if (fe.Load("template")<0)
        return BS_ERR_LIB;

    fe.feInfo(BS_INFO_ALL);

    result=fe.feSetParameters("1 1");
    if (result<0)
    {
        fprintf(stderr,"%s\n",fe.feLastError());
        fe.Unload();
        return result;
    }

    result=fe.feExecute(testdataset,&featureset);
    if (result<0)
    {
        fprintf(stderr,"%s\n",fe.feLastError());
        fe.Unload();
        return result;
    }

    delete featureset;
    fe.Unload();
    return BS_OK;
}



// testing environment for feature selection module

int fsTest(void)
{
    CModFS fs={0};
    int result;

    // change module name here
    if (fs.Load("template")<0)
        return BS_ERR_LIB;

    fs.fsInfo(BS_INFO_ALL);

    result=fs.fsSetParameters("1 1");
    if (result<0)
    {
        fprintf(stderr,"%s\n",fs.fsLastError());
        fs.Unload();
        return result;
    }

    result=fs.fsExecute(testfeatureset);
    if (result<0)
    {
        fprintf(stderr,"%s\n",fs.fsLastError());
        fs.Unload();
        return result;
    }

    fs.Unload();
    return BS_OK;
}

// testing environment for reference based scoring module

int srTest(void)
{
    CModSR sr={0};
    int result;
    CResult *testresult;

    // change module name here
    if (sr.Load("template")<0)
        return BS_ERR_LIB;

    sr.srInfo(BS_INFO_ALL);

    result=sr.srSetParameters("1 1");
    if (result<0)
    {
        fprintf(stderr,"%s\n",sr.srLastError());
        sr.Unload();
        return result;
    }

    result=sr.srExecute(testfeatureset,testfeatureset,&testresult);
    if (result<0)
    {
        fprintf(stderr,"%s\n",sr.srLastError());
        sr.Unload();
        return result;
    }

    delete testresult;
    testresult=0;
    sr.Unload();
    return BS_OK;
}




// testing environment for training based scoring module

int smTest(void)
{
    CModSM sm={0};
    int result;
    CResult *testresult;

    // change module name here
    if (sm.Load("template")<0)
        return BS_ERR_LIB;

    sm.smInfo(BS_INFO_ALL);

    result=sm.smSetParameters("1 1");
    if (result<0)
    {
        fprintf(stderr,"%s\n",sm.smLastError());
        sm.Unload();
        return result;
    }

/*    result=sm.smExecute(testfeatureset,testfeatureset,&testresult);
    if (result<0)
    {
        fprintf(stderr,"%s\n",sm.smLastError());
        sm.Unload();
        return result;
    }

    delete testresult;
 */   
	testresult=0;
    sm.Unload();
    return BS_OK;
}

void chainTest(void)
{
    CChain *enrollment;
    CChain *testdata;
    CResult *testresult;
    int result;

    if (LoadCP("../bs_cp.xml")<0)
        return;

    ListModules(BS_TYPE_ALL,BS_INFO_NAME|BS_INFO_CAPABILITIES);

    enrollment=new CChain;
    if (enrollment==0)
    {
        fprintf(stderr,"Not enough memory to create enrollment chain.");
        UnloadCP();
        return;
    }
    testdata=new CChain;
    if (testdata==0)
    {
        fprintf(stderr,"Not enough memory to create testdata chain.");
        UnloadCP();
        delete enrollment;
        return;
    }

    enrollment->AddModulePA("svc2004",0);
    enrollment->AddModuleFE("levenshtein",0);

    testdata->AddModulePA("svc2004",0);
    testdata->AddModuleFE("levenshtein",0);

    fprintf(stdout,"Created enrollment and test data chain.\n");

    result=ConfigureModules(enrollment,testdata,"levenshtein",0,BS_MATCHER_REFERENCE);
    if (result<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }

    fprintf(stdout,"Configure Modules complete.\n");

    if (CheckValidity()!=BS_OK)
    {
        fprintf(stderr,"Error: Module configuration failed validation check.\n");
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }

    fprintf(stdout,"Validity check passed.\n");

    result=LoadEnrollment("../SVC2004_Testdata/USER2_1.txt");

    if (result<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }

    fprintf(stdout,"Enrollment loaded USER2_1.\n");

    result=LoadTestData("../SVC2004_Testdata/USER1_1.txt",GENUINE_BLIND);

    if (result<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }

    fprintf(stdout,"Test Data loaded USER1_1.\n");

    if (CalcScore(&testresult)<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }

    fprintf(stdout,"Calculated Score USER1_1 vs USER2_1: %g\n",testresult->scores[0]);

    delete testresult;

    UnloadTestData();

    result=LoadTestData("../SVC2004_Testdata/USER3_1.txt",GENUINE_BLIND);

    if (result<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }

    fprintf(stdout,"Test Data loaded USER3_1.\n");

    if (CalcScore(&testresult)<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }

    fprintf(stdout,"Calculated Score USER3_1 vs USER2_1: %g\n",testresult->scores[0]);

    delete testresult;

    UnloadTestData();

    UnloadEnrollment();
    UnloadModules();
    UnloadCP();

    delete testdata;
    delete enrollment;
}


void print_features_in_file(CFeatureSet *fs_in, char* filename)
{
  int num = fs_in->feat_segments->feat_vectors->num_features[0];
 
  FILE *f = fopen(filename,"w");
  
  for(int i=0; i < num; i++)
    fprintf(f,"%d. %s: %f\n", i+1, fs_in->feat_segments->feat_vectors->features[i].feat_id, fs_in->feat_segments->feat_vectors->features[i].value);
  
  fclose(f);
}

void print_dataset_in_file(CDataSet *dataSet, char* filename)
{
  FILE *f;
  f = fopen(filename,"w");
  fprintf(f,"#index;time;x;y;pressure;tip;button;tiltx;tilty;azimuth;elevation;rotation;\n");
  for (int i=0; i < dataSet->num_packets; i++)
  {
    fprintf(f,"%d %d %d %d %d %d %d %d %d %d %d %d\n", i, dataSet->sourcedata[i].time, dataSet->sourcedata[i].x, dataSet->sourcedata[i].y, dataSet->sourcedata[i].force, dataSet->sourcedata[i].tip, dataSet->sourcedata[i].button, dataSet->sourcedata[i].tiltx, dataSet->sourcedata[i].tilty, dataSet->sourcedata[i].azimuth, dataSet->sourcedata[i].elevation, dataSet->sourcedata[i].rotation);   
  }
  fclose(f);  
}

void print_sourcedata(CDataSet *dataSet)
{
  fprintf(stdout,"\n\n");
  for (int i=0; i < dataSet->num_packets; i++)
  {
    fprintf(stdout,"[%d] time: %d; x: %d; y: %d; pressure: %d; tip: %d; button: %d; tiltx: %d; tilty: %d; azimuth: %d; elevation: %d; rotation: %d; \n", i, dataSet->sourcedata[i].time, dataSet->sourcedata[i].x, dataSet->sourcedata[i].y, dataSet->sourcedata[i].force, dataSet->sourcedata[i].tip, dataSet->sourcedata[i].button, dataSet->sourcedata[i].tiltx, dataSet->sourcedata[i].tilty, dataSet->sourcedata[i].azimuth, dataSet->sourcedata[i].elevation, dataSet->sourcedata[i].rotation);   
  }
  fprintf(stdout,"\n\n");  
}

void print_xy(CDataSet *dataSet)
{
  fprintf(stdout,"\n\n");
  for (int i=0; i < dataSet->num_packets; i++)
  {
    fprintf(stdout,"x[%d]=%d; y[%d]=%d\n", i, dataSet->sourcedata[i].x, i, dataSet->sourcedata[i].y);   
  }
  fprintf(stdout,"\n\n");  
}

int pp_fs_Test(char *sample)
{
    CModPA pa={0};
    CModPP pp={0};
    CModFE fe={0};
    CModFS fs={0};
   
    int result;
    CDataSet *dataset;
    CFeatureSet *featset;

    // change module name here
    if (pa.Load("svc2004")<0) 
      return BS_ERR_LIB;
    
    pa.paInfo(BS_INFO_ALL);
    
    result=pa.paSetParameters(0);
    if (result<0) {
        fprintf(stderr,"%s\n",pa.paLastError());
        pa.Unload();
        return result;
    }
        
    result=pa.paExecute(&dataset,sample);
    if (result<0) {
        fprintf(stderr,"%s\n",pa.paLastError());
        pa.Unload();
        return result;	
    }

    fprintf(stdout,"\n\nEnrollment loaded.\n\n");
    
/*
    print_dataset_in_file(dataset,"before_bandpass.dat");
   
    // change module name here
    if (pp.Load("bandpass")<0)
        return BS_ERR_LIB;

    pp.ppInfo(BS_INFO_ALL);

    result=pp.ppSetParameters("0.0 0.3");
    if (result<0) {
        fprintf(stderr,"%s\n",pp.ppLastError());
        pp.Unload();
        return result;
    }

    result=pp.ppExecute(dataset);
    if (result<0) {
        fprintf(stderr,"%s\n",pp.ppLastError());
        pp.Unload();
        return result;
    }
    
    fprintf(stdout,"\n\nBandpass Filter [0.0, 0.3] ready.\n\n");
    print_dataset_in_file(dataset,"after_bandpass.dat");        
*/
    

    print_dataset_in_file(dataset,"before_spline.dat");
        
    // change module name here
    if (pp.Load("interpolation")<0)
       return BS_ERR_LIB;

    pp.ppInfo(BS_INFO_ALL);

    result=pp.ppSetParameters("0 5");
    if (result<0) {
        fprintf(stderr,"%s\n",pp.ppLastError());
        pp.Unload();
        return result;
    }

    result=pp.ppExecute(dataset);
    if (result<0) {
        fprintf(stderr,"%s\n",pp.ppLastError());
        pp.Unload();
        return result;
    }           
    
    fprintf(stdout,"\nInterpolation [bspline, split=5] ready.\n\n");
    print_dataset_in_file(dataset,"after_spline.dat");

    
    
/*
    if (fe.Load("statistical")<0)
        return BS_ERR_LIB;

    fe.feInfo(BS_INFO_ALL);

    result=fe.feSetParameters("0 100");
    if (result<0) {
        fprintf(stderr,"%s\n",fe.feLastError());
        fe.Unload();
        return result;
    }        

    result=fe.feExecute(dataset,&featset);
    if (result<0) {
        fprintf(stderr,"%s\n",fe.feLastError());
        fe.Unload();
        return result;
    }           
    
    fprintf(stdout,"\n\nStatistical Features Extraction: Complit.\n\n");
    
 
    print_features_in_file(featset,"before_stat_fs.dat");    

    if (fs.Load("statistical")<0)
        return BS_ERR_LIB;

    fs.fsInfo(BS_INFO_ALL);

    result=fs.fsSetParameters("cov.dat 7");
    if (result<0) {
        fprintf(stderr,"%s\n",fs.fsLastError());
        fs.Unload();
        return result;
    }        

    result=fs.fsExecute(featset);
    if (result<0) {
        fprintf(stderr,"%s\n",fs.fsLastError());
        fs.Unload();
        return result;
    }           
    
    fprintf(stdout,"\n\nStatistical Features Selection: Complit.\n\n");
    print_features_in_file(featset,"after_stat_fs.dat");    
*/


 
    if (fe.Load("levenshtein")<0)
        return BS_ERR_LIB;

    fe.feInfo(BS_INFO_ALL);

    result=fe.feSetParameters("");
    if (result<0) {
        fprintf(stderr,"%s\n",fe.feLastError());
        fe.Unload();
        return result;
    }        

    result=fe.feExecute(dataset,&featset);
    if (result<0) {
        fprintf(stderr,"%s\n",fe.feLastError());
        fe.Unload();
        return result;
    }           
    
    fprintf(stdout,"\n\nLevenshtein Features Extraction: Complit.\n\n");

        
    print_features_in_file(featset,"before_lev_fs.dat");    

    if (fs.Load("levenshtein")<0)
        return BS_ERR_LIB;

    fs.fsInfo(BS_INFO_ALL);

    result=fs.fsSetParameters("XYP");
    if (result<0) {
        fprintf(stderr,"%s\n",fs.fsLastError());
        fs.Unload();
        return result;
    }
    
    result=fs.fsExecute(featset);
    if (result<0) {
        fprintf(stderr,"%s\n",fs.fsLastError());
        fs.Unload();
        return result;
    }

    fprintf(stdout,"\n\nLevenshtein Features Selection: Complit.\n\n");     
    print_features_in_file(featset,"after_lev_fs.dat");    
      
    pa.Unload();
    pp.Unload();
    fe.Unload();
    fs.Unload();    
    
    return BS_OK;
}


void pp_fs_Test2()
{
    CChain *enrollment;
    CChain *testdata;
    CResult *testresult;
    int result;

    if (LoadCP("bs_cp.xml")<0)
        return;
    
    ListModules(BS_TYPE_ALL,BS_INFO_NAME|BS_INFO_CAPABILITIES);

    enrollment=new CChain;
    if (enrollment==0)
    {
        fprintf(stderr,"Not enough memory to create enrollment chain.");
        UnloadCP();
        return;
    }
    testdata=new CChain;
    if (testdata==0)
    {
        fprintf(stderr,"Not enough memory to create testdata chain.");
        UnloadCP();
        delete enrollment;
        return;
    }

    enrollment->AddModulePA("svc2004",0);
    enrollment->AddModulePP("interpolation","0 10");    
    enrollment->AddModulePP("bandpass","0.0 0.4");
//    enrollment->AddModuleFE("levenshtein",0);
//    enrollment->AddModuleFS("levenshtein","XYP");
    enrollment->AddModuleFE("statistical",0);
    enrollment->AddModuleFS("statistical","011110000100111");    
    
    testdata->AddModulePA("svc2004",0);
    testdata->AddModulePP("interpolation","0 10");    
    testdata->AddModulePP("bandpass","0.0 0.4");
//    testdata->AddModuleFE("levenshtein",0);
//    testdata->AddModuleFS("levenshtein","XYP");
    testdata->AddModuleFE("statistical",0);
    testdata->AddModuleFS("statistical","011110000100111");
    
    fprintf(stdout,"Created enrollment and test data chain.\n");

    result=ConfigureModules(enrollment,testdata,"statistical",0,BS_MATCHER_REFERENCE);
    if (result<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }
    fprintf(stdout,"Configure Modules complete.\n");

    if (CheckValidity()!=BS_OK)
    {
        fprintf(stderr,"Error: Module configuration failed validation check.\n");
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }
    fprintf(stdout,"Validity check passed.\n");

    result=LoadEnrollment("SVC2004_Testdata/USER2_1.txt");
    if (result<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }
    fprintf(stdout,"Enrollment loaded USER2_1.\n");

    result=LoadTestData("SVC2004_Testdata/USER1_1.txt",GENUINE_BLIND);
    if (result<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }
    fprintf(stdout,"Test Data loaded USER1_1.\n");

    if (CalcScore(&testresult)<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }
    fprintf(stdout,"Calculated Score USER1_1 vs USER2_1: %g\n",testresult->scores[0]);

    delete testresult;

    UnloadTestData();

    result=LoadTestData("SVC2004_Testdata/USER3_1.txt",GENUINE_BLIND);
    if (result<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }
    fprintf(stdout,"Test Data loaded USER3_1.\n");

    if (CalcScore(&testresult)<0)
    {
        UnloadCP();
        delete testdata;
        delete enrollment;
        return;
    }
    fprintf(stdout,"Calculated Score USER3_1 vs USER2_1: %g\n",testresult->scores[0]);

    delete testresult;

    UnloadTestData();
    UnloadEnrollment();
    UnloadModules();
    UnloadCP();

    delete testdata;
    delete enrollment;
}

int fuTest()
{
    CModFU fu={0};
    int result;
    CResult *testresult;

    // change module name here
    if (fu.Load("eer")<0)
        return BS_ERR_LIB;

    fu.fuInfo(BS_INFO_ALL);

    result=fu.fuSetParameters("5 scores_stat.txt scores_lev.txt scores_hmm.txt");
    if (result<0)
    {
        fprintf(stderr,"%s\n",fu.fuLastError());
        fu.Unload();
        return result;
    }

    result=fu.fuExecute(&testresult);
    if (result<0)
    {
        fprintf(stderr,"%s\n",fu.fuLastError());
        fu.Unload();
        return result;
    }

    delete testresult;
    testresult=0;
    fu.Unload();
    return BS_OK;
}

int debug_main(void)
{
    testdataset=new CDataSet;

    testfeatureset=new CFeatureSet;
    testfeatureset->feat_set_id="featureset example type";

    // you might wanna create some testing data at this point

    // select the call for your module, remove the others
    /*
    paTest();
    fprintf(stdout,"\n");
    ppTest();
    fprintf(stdout,"\n");
    feTest();
    fprintf(stdout,"\n");
    fsTest();
    fprintf(stdout,"\n");
    srTest();
    fprintf(stdout,"\n");
    smTest();
    fprintf(stdout,"\n");
    */
    //chainTest();
    //pp_fs_Test("SVC2004_Testdata/USER1_1.txt");
    //pp_fs_Test2();
    fuTest();
    fprintf(stdout,"\n");

    delete testfeatureset;
    testfeatureset=0;
    delete testdataset;
    testdataset=0;
#ifdef _MSC_VER

    getchar();
#endif

    return BS_OK;
}
