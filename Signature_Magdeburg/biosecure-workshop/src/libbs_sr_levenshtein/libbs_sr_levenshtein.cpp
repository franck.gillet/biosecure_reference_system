/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <string.h>

#include <libbs_framework.h>


// TODO: module specific modifications start here

// every module defines its own PARAM structure unless the module doesnt have any parameters
// this is an example structure:

char LastError[BS_MAX_ERROR];   // last error message of this module

int ParamSet=0;                 // becomes 1 if all parameters are valid


// return pointer to error string, nothing to do here
EXPORT char *srLastError(void)
{
    return LastError;
}


// validate and set parameters for later execution
EXPORT int srSetParameters(const char *srParamString)
{
    // everything fine
    ParamSet=1;
    return BS_OK;
}

inline int lmin(int a,int b,int c)
{
    if ((a<b) & (a<c))
        return a;
    return b<c ? b : c;
}

inline int dist(CFeatureVector *v1,CFeatureVector *v2,int i1,int i2)
{
    double a,b;
    a=v1->features[i1].value;
    b=v2->features[i2].value;
    return (int)(a!=b);
}

// execution of the matching functions based on the parameters
// and source data passed to srSetParameters
// srResult will be created by this function and returned on success
EXPORT int srExecute(const CFeatureSet *srReference,const CFeatureSet *srTestData,CResult **srResult)
{
    int l1,l2,i,j,s;
    int *m;
    CFeatureVector *v1,*v2;
    double score;

    if (ParamSet==0)
    {
        sprintf(LastError,"error in sr_levenshtein: parameters have not been set yet.\n");
        return BS_ERR_PARAM;
    }
    // TODO: module specific modifications start here

    // check if needed source data is available:
    // example: we check for the correct featureset data type
    if (strcmp(srReference->feat_set_id,"Levenshtein")!=0)
    {
        sprintf(LastError,"error in sr_levenshtein: invalid reference data format.\n");
        return BS_ERR_SOURCE;
    }

    if (strcmp(srTestData->feat_set_id,"Levenshtein")!=0)
    {
        sprintf(LastError,"error in sr_levenshtein: invalid test data format.\n");
        return BS_ERR_SOURCE;
    }

    v1=srReference->feat_segments->feat_vectors;

    *srResult=new CResult;
    if (*srResult==0)
    {
        sprintf(LastError,"error in sr_levenshtein: memory allocation error.\n");
        return BS_ERR_MEM;
    }
	
    
    (*srResult)->result_comments=0;       //no comment needed


	int n = 0, nvector = srReference->feat_segments->num_feature_vectors;
	(*srResult)->num_scores = nvector;
	(*srResult)->scores = (double*)bscalloc(sizeof(double),nvector);
	for (; n < nvector ;n++)
	{

		v2=srTestData->feat_segments->feat_vectors;
		l1=(v1+n)->num_features[0];
		l2=v2->num_features[0];
		s=l2+1;

		m=(int*)bscalloc(sizeof(int)*(l1+1)*(l2+1),1);
		if (m==0)
		{
			sprintf(LastError,"error in sr_levenshtein: memory allocation error.\n");
			return BS_ERR_MEM;
		}

		for (i=0;i<=l1;i++)
			m[i*s]=i;
		for (i=1;i<=l2;i++)
			m[i]=i;
		for (i=0;i<l1;i++)
			for (j=0;j<l2;j++)
				m[(i+1)*s+j+1]=lmin(m[i*s+j+1]+1,m[(i+1)*s+j]+1,m[i*s+j]+dist((v1+n),v2,i,j));

		if ((l1==0)|(l2==0))
			score=0;
		else
			score=1.0-(m[l1*s+l2]/(double)(max(l1,l2)));

		bsfree(m);

		(*srResult)->scores[n]=score;
	}

    // insert something useful here using feature data in Reference, TestData and parameters in Param
    // create and store a CResult object in srResult:
    // module specific modifications end here
    return BS_OK;
}


// report information about module usage and capabilities depending on Type
EXPORT void srInfo(int Type)
{
    // TODO: module specific modifications start here
    // possible Types are defined in libbs_framework.h
    // not all Types have to be supported
    // multiple Types can be combined

    if ((Type&BS_INFO_NAME)!=0)
    {
        fprintf(stdout,"Module Name: Levenshtein-Distance Scoring\n");
    }

    if ((Type&BS_INFO_VERSION)!=0)
    {
        fprintf(stdout,"Module Version: 0.1\n");
    }

    if ((Type&BS_INFO_ABOUT)!=0)
    {
        fprintf(stdout,"Authors: AMSL Magdeburg\n");
    }

    if ((Type&BS_INFO_PARAMETERS)!=0)
    {
        fprintf(stdout,"Module Parameters: none\n");
    }

    if ((Type&BS_INFO_CAPABILITIES)!=0)
    {
        fprintf(stdout,"This module calculates score based on the Levenshtein-Distance.\n");
    }

    // module specific modifications end here
}

