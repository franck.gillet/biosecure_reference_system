/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;

#include <libbs_framework.h>


// TODO: module specific modifications start here

// every module defines its own PARAM structure unless the module doesnt have any parameters
// for Parser modules one parameter always identifies the source data
// this is an example structure:

char LastError[BS_MAX_ERROR];   // last error message of this module

int ParamSet=0;                 // becomes 1 if all parameters are valid


// return pointer to error string, nothing to do here
EXPORT char *paLastError(void)
{
    return LastError;
}


// validate and set parameters for later execution
EXPORT int paSetParameters(const char *paParamString)
{
    // everything fine
    ParamSet=1;
    return BS_OK;
}


// execution of the parser functions based on the parameters
// and source data passed to paSetParameters
EXPORT int paExecute(CDataSet **paDataSet,const char *paID)
{

    if (ParamSet==0)
    {
        sprintf(LastError,"error in pa_svc2004: parameters have not been set yet.\n");
        return BS_ERR_PARAM;
    }
    if (paID==0)
    {
        sprintf(LastError,"error in pa_svc2004: ID has to be set.\n");
        return BS_ERR_PARAM;
    }

    // TODO: module specific modifications start here
    // insert something useful here using parameters in Param
    // store a valid pointer to the created dataset in paDataSet

	// GETINT: Now use class CParserSVC to parse
	CParserSVC parser;
	*paDataSet = parser.parse(paID);

    // module specific modifications end here
    return BS_OK;
}


// report information about module usage and capabilities depending on Type
EXPORT void paInfo(int Type)
{
    // TODO: module specific modifications start here
    // possible Types are defined in libbs_framework.h
    // not all Types have to be supported
    // multiple Types can be combined

    if ((Type&BS_INFO_NAME)!=0)
    {
        fprintf(stdout,"Module Name: SVC2004 Parser\n");
    }

    if ((Type&BS_INFO_VERSION)!=0)
    {
        fprintf(stdout,"Module Version: 0.1\n");
    }

    if ((Type&BS_INFO_ABOUT)!=0)
    {
        fprintf(stdout,"Authors: University of Kent\n"
                       "         AMSL Magdeburg\n");
    }

    if ((Type&BS_INFO_PARAMETERS)!=0)
    {
        fprintf(stdout,"Module Parameters: none\n");
    }

    if ((Type&BS_INFO_CAPABILITIES)!=0)
    {
        fprintf(stdout,"+++This module reads raw data in the SVC2004 format and\n"
                       "   converts it into the predefined class structure");
    }

    // module specific modifications end here
}

