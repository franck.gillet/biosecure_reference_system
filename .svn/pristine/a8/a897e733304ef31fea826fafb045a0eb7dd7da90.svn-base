/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <libbs_framework.h>

#include "statistics.h"

// TODO: module specific modifications start here
#define FE_HMM_MAXRADIUS 10000
#define FE_HMM_MAX_L2W_RATIO 5


// module specific modifications end here


char LastError[BS_MAX_ERROR];   // last error message of this module

int ParamSet=0;                 // becomes 1 if all parameters are valid

// declare some functions needed in this module
int FillVector(const CDataSet *feDataSet,CFeatureVector *feFeatureVector);
int centralizeCoordinates(double *coordinates, int size);
int filterCoordinates(double *coordinates, int size);
int filterAngle(double *coordinates, int size);

// return pointer to error string, nothing to do here
EXPORT char *feLastError(void)
{
    return LastError;
}


// validate and set parameters for later execution
EXPORT int feSetParameters(const char *feParamString)
{
     // everything fine
    ParamSet=1;
    return BS_OK;
}


// execution of the feature extraction functions based on the parameters
// and source data passed to feSetParameters
// feFeatureSet will be created by this function and returned on success
EXPORT int feExecute(const CDataSet *feDataSet,CFeatureSet **feFeatureSet)
{
    if (ParamSet==0)
    {
        sprintf(LastError,"error in fe_<modulename>: parameters have not been set yet.\n");
        return BS_ERR_PARAM;
    }
    // TODO: module specific modifications start here

    // check if needed source data is available:
    // example: we check for existence of normalised data
    if (feDataSet->sourcedata==0)
    {
        sprintf(LastError,"error in fe_<modulename>: normalised data not available.\n");
        return BS_ERR_SOURCE;
    }
    
    // insert something useful here using data in DataSet and parameters in Param
    // store a valid pointer to the created featureset in feFeatureSet
	if ( (*feFeatureSet) == 0)
	{
		//*feFeatureSet = (CFeatureSet*)bsmalloc(sizeof(CFeatureSet));
		*feFeatureSet = new CFeatureSet;
	
		if (*feFeatureSet==0)
		{
			sprintf(LastError,"error in fe_hmm: memory allocation error.\n");
			return BS_ERR_MEM;
		}
	
		(*feFeatureSet)->feat_set_id = "hmm";
	
		// add segments, vectors and featuredata the same way and fill in data
		// dont forget to clean up reserved memory on error condition
		// you should use: delete *feFeatureSet; *feFeatureSet=0;
		(*feFeatureSet)->feat_segments = (CFeatureSegment*)bsmalloc(sizeof(CFeatureSegment));
		//(*feFeatureSet)->feat_segments = new CFeatureSegment;
		if ((*feFeatureSet)->feat_segments == 0)
		{
			delete *feFeatureSet;
			*feFeatureSet = 0;
			sprintf(LastError,"error in fe_hmm: memory allocation error.\n");
			return BS_ERR_MEM;
		}
		
		(*feFeatureSet)->num_feature_segments = 1;
		(*feFeatureSet)->feat_segments->feat_seg_id = "hmm";
		(*feFeatureSet)->feat_segments->feat_vectors = 0;
	}

	if ((*feFeatureSet)->feat_segments->feat_vectors == 0)
	{
		(*feFeatureSet)->feat_segments->feat_vectors = (CFeatureVector*)bscalloc(sizeof(CFeatureVector),1);
		(*feFeatureSet)->feat_segments->num_feature_vectors = 1;
		// check error in allocation
		if ((*feFeatureSet)->feat_segments->feat_vectors == 0)
		{
			delete *feFeatureSet;
			*feFeatureSet = 0;
			sprintf(LastError,"error in fe_hmm: memory allocation error.\n");
			return BS_ERR_MEM;
		}
	}
	else
	{
		int size = (*feFeatureSet)->feat_segments->num_feature_vectors + 1;
		(*feFeatureSet)->feat_segments->feat_vectors = (CFeatureVector*)bsrealloc((*feFeatureSet)->feat_segments->feat_vectors,sizeof(CFeatureVector)*size);
		(*feFeatureSet)->feat_segments->num_feature_vectors = size;
		// check error in reallocation
		if ((*feFeatureSet)->feat_segments->feat_vectors == 0)
		{
			delete *feFeatureSet;
			*feFeatureSet = 0;
			sprintf(LastError,"error in fe_hmm: memory allocation error.\n");
			return BS_ERR_MEM;
		}
	}

	int size = (*feFeatureSet)->feat_segments->num_feature_vectors;

	int result = FillVector(feDataSet,(*feFeatureSet)->feat_segments->feat_vectors + size - 1);
	if (result == BS_ERR_MEM)
	{
		delete *feFeatureSet;
		*feFeatureSet = 0;
		sprintf(LastError,"error in fe_hmm: memory allocation error.\n");
		return BS_ERR_MEM;
	}

    // module specific modifications end here
    return BS_OK;
}


// report information about module usage and capabilities depending on Type
EXPORT void feInfo(int Type)
{
    // TODO: module specific modifications start here
    // possible Types are defined in libbs_framework.h
    // not all Types have to be supported
    // multiple Types can be combined

    if ((Type&BS_INFO_NAME)!=0)
    {
        fprintf(stdout,"Module Name: HMM Feature Extraction\n");
    }

    if ((Type&BS_INFO_VERSION)!=0)
    {
        fprintf(stdout,"Module Version: 0.1\n");
    }

    if ((Type&BS_INFO_ABOUT)!=0)
    {
        fprintf(stdout,"Authors: GET-INT\n");
    }

    if ((Type&BS_INFO_PARAMETERS)!=0)
    {
        fprintf(stdout,"Module Parameters: none\n");        
    }

    if ((Type&BS_INFO_CAPABILITIES)!=0)
    {
        fprintf(stdout,"This module extract features in HMM format. .\n");
    }	

    // module specific modifications end here
}

int FillVector(const CDataSet *feDataSet,CFeatureVector *feFeatureVector)
{
	const int FVXINDEX = 0;
	const int FVYINDEX = 1;
	const int FVAZIMUTHINDEX = 19;
	const int FVELEVATIONINDEX = 20;  // also called altitude
	const int FVFORCEINDEX = 21;      // also called pressure
	const int FVSPEEDXINDEX = 2; 
	const int FVSPEEDYINDEX = 3; 
	const int FVABSOLUTESPEEDINDEX = 4; 
	const int FVACCELERATIONXINDEX = 5; 
	const int FVACCELERATIONYINDEX = 6; 
	const int FVABSOLUTEACCELERATIONINDEX = 7; 
	const int FVTANGENTIALACCELERATIONINDEX = 8; 
	const int FVCOSALPHAINDEX = 9; 
	const int FVSINALPHAINDEX = 10; 
	const int FVALPHAINDEX = 11; 
	const int FVCOSPHIINDEX = 12; 
	const int FVSINPHIINDEX = 13; 
	const int FVPHIINDEX = 14; 
	const int FVRADIUSINDEX = 15; 
	const int FVPROPORTIONLHINDEX = 16; 
	const int FVPROPORTIONLH7INDEX = 17; 
	const int FVCONTEXTSPEEDINDEX = 18; 
	const int FVDELTAAZIMUTHINDEX = 22; 
	const int FVDELTAALTITUDEINDEX = 23; 
	const int FVDELTAPRESSUREINDEX = 24; 
	
        int i;
	int num_features = 25;
	int num_results = feDataSet->num_packets; 

	if (num_results < 17)
	{
		fprintf(stdout, "Number of packets is too small.");
		return BS_ERR_MEM;
	}
	feFeatureVector->num_dimensions = 1;
	feFeatureVector->features = (CFeatureData*)bscalloc(sizeof(CFeatureData),num_features);
	feFeatureVector->num_features = (int*)bsmalloc(sizeof(int));
	feFeatureVector->num_features[0] = num_features;
	if (feFeatureVector->features == 0)
	{
		return BS_ERR_MEM;
	}
	// allocate feature results
	for (i = 0; i < num_features; i++)
	{
		feFeatureVector->features[i].feature_result = (double*)bscalloc(sizeof(double),num_results);
		feFeatureVector->features[i].num_feature_results = num_results;
		if (feFeatureVector->features[i].feature_result == 0)
		{
			return BS_ERR_MEM;
		}
		feFeatureVector->features[i].discrete = DISCRETE;
		feFeatureVector->features[i].normalised = NOT_NORMALISED;
		feFeatureVector->features[i].time = TIME_UNKNOWN;			
	}
	
	// vector x
	feFeatureVector->features[FVXINDEX].feat_id = "x";
	for (i = 0; i < num_results; i++)
	{
		feFeatureVector->features[FVXINDEX].feature_result[i] = feDataSet->sourcedata[i].x;
	}
	
	filterCoordinates(feFeatureVector->features[FVXINDEX].feature_result,num_results);

	// after filterCoordinates, num_results should be reduced by 5
	// but we don't want to influent to other calculations following
	// so let transmit num_results - 5 instead
	centralizeCoordinates(feFeatureVector->features[FVXINDEX].feature_result,num_results - 5);

	// vector y
	feFeatureVector->features[FVYINDEX].feat_id = "y";
	for (i = 0; i < num_results; i++)
	{
		feFeatureVector->features[FVYINDEX].feature_result[i] = feDataSet->sourcedata[i].y;
	}

	filterCoordinates(feFeatureVector->features[FVYINDEX].feature_result,num_results);

	// after filterCoordinates, num_results should be reduced by 5
	// but we don't want to influent to other calculations following
	// so let transmit num_results - 5 instead
	centralizeCoordinates(feFeatureVector->features[FVYINDEX].feature_result,num_results - 5);

	// vector azimuth
	feFeatureVector->features[FVAZIMUTHINDEX].feat_id = "azimuth";
	for (i = 0; i < num_results; i++)
	{
		feFeatureVector->features[FVAZIMUTHINDEX].feature_result[i] = feDataSet->sourcedata[i].azimuth;
	}
	filterAngle(feFeatureVector->features[FVAZIMUTHINDEX].feature_result,num_results);

	// vector elevation
	feFeatureVector->features[FVELEVATIONINDEX].feat_id = "elevation"; // also called altitude
	for (i = 0; i < num_results; i++)
	{
		feFeatureVector->features[FVELEVATIONINDEX].feature_result[i] = feDataSet->sourcedata[i].elevation;
	}
	filterAngle(feFeatureVector->features[FVELEVATIONINDEX].feature_result, num_results);
	
	// vector force
	feFeatureVector->features[FVFORCEINDEX].feat_id = "force"; // also called pressure
	for (i = 0; i < num_results; i++)
	{
		feFeatureVector->features[FVFORCEINDEX].feature_result[i] = feDataSet->sourcedata[i].force;
	}

	// vector speed in direction x
	feFeatureVector->features[FVSPEEDXINDEX].feat_id = "speed_x";
	for (i = 0; i < num_results - 4; i++)
	{
		// v3 = ( 2(x5-x1) + (x4-x2) ) / 10
		feFeatureVector->features[FVSPEEDXINDEX].feature_result[i] = (2*feFeatureVector->features[FVXINDEX].feature_result[i+4] + 
															feFeatureVector->features[FVXINDEX].feature_result[i+3] -
															feFeatureVector->features[FVXINDEX].feature_result[i+1] - 
														  2*feFeatureVector->features[FVXINDEX].feature_result[i]) / 10;
	}

	// vector speed in direction y
	feFeatureVector->features[FVSPEEDYINDEX].feat_id = "speed_y";
	for (i = 0; i < num_results - 4; i++)
	{
		// v3 = ( 2(y5-y1) + (y4-y2) ) / 10
		feFeatureVector->features[FVSPEEDYINDEX].feature_result[i] = (2*feFeatureVector->features[FVYINDEX].feature_result[i+4] + 
															feFeatureVector->features[FVYINDEX].feature_result[i+3] -
															feFeatureVector->features[FVYINDEX].feature_result[i+1] - 
														  2*feFeatureVector->features[FVYINDEX].feature_result[i]) / 10;
	}

	// vector absolute speed
	feFeatureVector->features[FVABSOLUTESPEEDINDEX].feat_id = "absolute_speed";
	for (i = 0; i < num_results - 4; i++)
	{
		// v = sqrt(vx*vx + vy*vy)
		feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i] = sqrt(feFeatureVector->features[FVSPEEDXINDEX].feature_result[i]*feFeatureVector->features[FVSPEEDXINDEX].feature_result[i] +
															  feFeatureVector->features[FVSPEEDYINDEX].feature_result[i]*feFeatureVector->features[FVSPEEDYINDEX].feature_result[i]);
	}

	// vector acceleration in direction x, y and absolute acceleration
	feFeatureVector->features[FVACCELERATIONXINDEX].feat_id = "acceleration_x";
	feFeatureVector->features[FVACCELERATIONYINDEX].feat_id = "acceleration_Y";
	feFeatureVector->features[FVABSOLUTEACCELERATIONINDEX].feat_id = "absolute_acceleration";
	for (i = 0; i < num_results - 8; i++)
	{
		feFeatureVector->features[FVACCELERATIONXINDEX].feature_result[i] = (2*feFeatureVector->features[FVSPEEDXINDEX].feature_result[i+4] + 
															feFeatureVector->features[FVSPEEDXINDEX].feature_result[i+3] -
															feFeatureVector->features[FVSPEEDXINDEX].feature_result[i+1] - 
														  2*feFeatureVector->features[FVSPEEDXINDEX].feature_result[i]) / 10;
		feFeatureVector->features[FVACCELERATIONYINDEX].feature_result[i] = (2*feFeatureVector->features[FVSPEEDYINDEX].feature_result[i+4] + 
															feFeatureVector->features[FVSPEEDYINDEX].feature_result[i+3] -
															feFeatureVector->features[FVSPEEDYINDEX].feature_result[i+1] - 
														  2*feFeatureVector->features[FVSPEEDYINDEX].feature_result[i]) / 10;
		feFeatureVector->features[FVABSOLUTEACCELERATIONINDEX].feature_result[i] = sqrt(feFeatureVector->features[FVACCELERATIONXINDEX].feature_result[i]*feFeatureVector->features[FVACCELERATIONXINDEX].feature_result[i] +
															   feFeatureVector->features[FVACCELERATIONYINDEX].feature_result[i]*feFeatureVector->features[FVACCELERATIONYINDEX].feature_result[i]);
	}

	// vector tangential acceleration
	feFeatureVector->features[FVTANGENTIALACCELERATIONINDEX].feat_id = "tangential_acceleration";
	for (i = 0; i < num_results - 8; i++)
	{
		feFeatureVector->features[FVTANGENTIALACCELERATIONINDEX].feature_result[i] = (2*feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i+4] + 
															 feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i+3] -
															 feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i+1] - 
														   2*feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i]) / 10;
	}
	// vector cos alpha, sine alpha and alpha
	feFeatureVector->features[FVCOSALPHAINDEX].feat_id = "cos_alpha";
	feFeatureVector->features[FVSINALPHAINDEX].feat_id = "sin_alpha";
	feFeatureVector->features[FVALPHAINDEX].feat_id = "alpha";
	for (i = 0; i < num_results - 4; i++)
	{
		if (feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i] !=0)
		{
			feFeatureVector->features[FVCOSALPHAINDEX].feature_result[i] = feFeatureVector->features[FVSPEEDXINDEX].feature_result[i] / feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i];
			feFeatureVector->features[FVSINALPHAINDEX].feature_result[i] = - feFeatureVector->features[FVSPEEDYINDEX].feature_result[i] / feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i];
		}
		else
		{
			feFeatureVector->features[FVCOSALPHAINDEX].feature_result[i] = sqrt(0.5);
			feFeatureVector->features[FVSINALPHAINDEX].feature_result[i] = sqrt(0.5);
		}
		feFeatureVector->features[FVALPHAINDEX].feature_result[i] = asin(feFeatureVector->features[FVSINALPHAINDEX].feature_result[i]);
	}

	// vector cos phi, sin phi
	for (i = 0; i < num_results - 6; i++)
	{
		feFeatureVector->features[FVCOSPHIINDEX].feature_result[i] = feFeatureVector->features[FVCOSALPHAINDEX].feature_result[i] * feFeatureVector->features[FVCOSALPHAINDEX].feature_result[i+2] +
																	 feFeatureVector->features[FVSINALPHAINDEX].feature_result[i] * feFeatureVector->features[FVSINALPHAINDEX].feature_result[i+2];
		feFeatureVector->features[FVSINPHIINDEX].feature_result[i] = feFeatureVector->features[FVCOSALPHAINDEX].feature_result[i] * feFeatureVector->features[FVSINALPHAINDEX].feature_result[i+2] -
																	 feFeatureVector->features[FVSINALPHAINDEX].feature_result[i] * feFeatureVector->features[FVCOSALPHAINDEX].feature_result[i+2];

	}

	// vector phi
	for (i = 0; i < num_results - 8; i++)
	{
		feFeatureVector->features[FVPHIINDEX].feature_result[i] = (2*feFeatureVector->features[FVALPHAINDEX].feature_result[i+4] +
																	 feFeatureVector->features[FVALPHAINDEX].feature_result[i+3] -
																	 feFeatureVector->features[FVALPHAINDEX].feature_result[i+1] -
																   2*feFeatureVector->features[FVALPHAINDEX].feature_result[i]) / 10;
	}

	double * delta_phi = (double*)bscalloc(sizeof(double),num_results - 12);
	for (i = 0; i < num_results - 12; i++)
	{
		delta_phi[i] = (2*feFeatureVector->features[FVPHIINDEX].feature_result[i+4] +
						  feFeatureVector->features[FVPHIINDEX].feature_result[i+3] -
						  feFeatureVector->features[FVPHIINDEX].feature_result[i+1] -
						2*feFeatureVector->features[FVPHIINDEX].feature_result[i]) / 10;
	}

	// vector radius
	for (i = 0; i < num_results - 12; i++)
	{
		if (delta_phi[i] != 0)
		{
			feFeatureVector->features[FVRADIUSINDEX].feature_result[i] = feFeatureVector->features[FVTANGENTIALACCELERATIONINDEX].feature_result[i+2] / delta_phi[i];
		}
		else
		{
			feFeatureVector->features[FVRADIUSINDEX].feature_result[i] = FE_HMM_MAXRADIUS;
		}

	}

	// delete delta_phi
	bsfree(delta_phi);

	// vector proportion_lh
	for (i = 2; i < num_results - 2; i++)
	{
		double maxX = -1e40;
		double maxY = -1e40;
		double minX = 1e40;
		double minY = 1e40;
		int indexX, indexY, indexx, indexy;

		int j;
		for (j = -2; j < 3; j++)
		{
			if (maxX < feFeatureVector->features[FVXINDEX].feature_result[i+j]) 
			{
				maxX = feFeatureVector->features[FVXINDEX].feature_result[i+j];
				indexX = i+j;
			}
			if (maxY < feFeatureVector->features[FVYINDEX].feature_result[i+j]) 
			{
				maxY = feFeatureVector->features[FVYINDEX].feature_result[i+j];
				indexY = i+j;
			}
			if (minX > feFeatureVector->features[FVXINDEX].feature_result[i+j]) 
			{
				minX = feFeatureVector->features[FVXINDEX].feature_result[i+j];
				indexx = i+j;
			}
			if (minY > feFeatureVector->features[FVYINDEX].feature_result[i+j]) 
			{
				minY = feFeatureVector->features[FVYINDEX].feature_result[i+j];			
				indexy = i+j;
			}
		}

		if (maxY != minY)
		{
			feFeatureVector->features[FVPROPORTIONLHINDEX].feature_result[i-2] = (maxX - minX) / (maxY - minY);
		}
		else
		{
			feFeatureVector->features[FVPROPORTIONLHINDEX].feature_result[i-2] = FE_HMM_MAX_L2W_RATIO;
		}
	}

	// vector proportion_lh7
	for (i = 3; i < num_results - 3; i++)
	{
		double maxX = -1e40;
		double maxY = -1e40;
		double minX = 1e40;
		double minY = 1e40;

		int j;
		for (j = -3; j < 4; j++)
		{
			if (maxX < feFeatureVector->features[FVXINDEX].feature_result[i+j]) maxX = feFeatureVector->features[FVXINDEX].feature_result[i+j];
			if (maxY < feFeatureVector->features[FVYINDEX].feature_result[i+j]) maxY = feFeatureVector->features[FVYINDEX].feature_result[i+j];
			if (minX > feFeatureVector->features[FVXINDEX].feature_result[i+j]) minX = feFeatureVector->features[FVXINDEX].feature_result[i+j];
			if (minY > feFeatureVector->features[FVYINDEX].feature_result[i+j]) minY = feFeatureVector->features[FVYINDEX].feature_result[i+j];

			if (maxY != minY)
			{
				feFeatureVector->features[FVPROPORTIONLH7INDEX].feature_result[i-3] = (maxX - minX) / (maxY - minY);
			}
			else
			{
				feFeatureVector->features[FVPROPORTIONLH7INDEX].feature_result[i-3] = FE_HMM_MAX_L2W_RATIO;
			}

		}
	}

	// vector context speed
	for (i = 2; i < num_results - 6; i++)
	{
		double maxV = -1e40;
		double minV = 1e40;
		int j;
		for (j = -2; j < 3; j++)
		{
			if (maxV < feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i+j]) maxV = feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i+j];
			if (minV > feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i+j]) minV = feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result[i+j];
		}

		if (maxV != 0)
		{
			feFeatureVector->features[FVCONTEXTSPEEDINDEX].feature_result[i-2] = minV / maxV;
		}
		else
		{
			feFeatureVector->features[FVCONTEXTSPEEDINDEX].feature_result[i-2] = 0;
		}
	}

	// vector delta_azimuth, delta_altitude and delta_pressure
	for (i = 0; i < num_results - 4; i++)
	{
		feFeatureVector->features[FVDELTAAZIMUTHINDEX].feature_result[i] = (2*feFeatureVector->features[FVAZIMUTHINDEX].feature_result[i+4] + 
																			  feFeatureVector->features[FVAZIMUTHINDEX].feature_result[i+3] -
																			  feFeatureVector->features[FVAZIMUTHINDEX].feature_result[i+1] -
																			2*feFeatureVector->features[FVAZIMUTHINDEX].feature_result[i]) / 10;
		feFeatureVector->features[FVDELTAALTITUDEINDEX].feature_result[i] = (2*feFeatureVector->features[FVELEVATIONINDEX].feature_result[i+4] +
																			   feFeatureVector->features[FVELEVATIONINDEX].feature_result[i+3] -
																			   feFeatureVector->features[FVELEVATIONINDEX].feature_result[i+1] -
																			 2*feFeatureVector->features[FVELEVATIONINDEX].feature_result[i]) / 10;
		feFeatureVector->features[FVDELTAPRESSUREINDEX].feature_result[i] = (2*feFeatureVector->features[FVFORCEINDEX].feature_result[i+4] +
																			   feFeatureVector->features[FVFORCEINDEX].feature_result[i+3] -
																			   feFeatureVector->features[FVFORCEINDEX].feature_result[i+1] -
																			 2*feFeatureVector->features[FVFORCEINDEX].feature_result[i] ) / 10;
	}

	memcpy(feFeatureVector->features[FVXINDEX].feature_result,feFeatureVector->features[FVXINDEX].feature_result + 6, (num_results - 12) * sizeof(double));
	memcpy(feFeatureVector->features[FVYINDEX].feature_result,feFeatureVector->features[FVYINDEX].feature_result + 6, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVSPEEDXINDEX].feature_result,feFeatureVector->features[FVSPEEDXINDEX].feature_result + 4, (num_results - 12) * sizeof(double));
	memcpy(feFeatureVector->features[FVSPEEDYINDEX].feature_result,feFeatureVector->features[FVSPEEDYINDEX].feature_result + 4, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result,feFeatureVector->features[FVABSOLUTESPEEDINDEX].feature_result + 4, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVACCELERATIONXINDEX].feature_result,feFeatureVector->features[FVACCELERATIONXINDEX].feature_result + 2, (num_results - 12) * sizeof(double));
	memcpy(feFeatureVector->features[FVACCELERATIONYINDEX].feature_result,feFeatureVector->features[FVACCELERATIONYINDEX].feature_result + 2, (num_results - 12) * sizeof(double));
	memcpy(feFeatureVector->features[FVABSOLUTEACCELERATIONINDEX].feature_result,feFeatureVector->features[FVABSOLUTEACCELERATIONINDEX].feature_result + 2, (num_results - 12) * sizeof(double));
	memcpy(feFeatureVector->features[FVTANGENTIALACCELERATIONINDEX].feature_result,feFeatureVector->features[FVTANGENTIALACCELERATIONINDEX].feature_result + 2, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVCOSALPHAINDEX].feature_result,feFeatureVector->features[FVCOSALPHAINDEX].feature_result + 4, (num_results - 12) * sizeof(double));
	memcpy(feFeatureVector->features[FVSINALPHAINDEX].feature_result,feFeatureVector->features[FVSINALPHAINDEX].feature_result + 4, (num_results - 12) * sizeof(double));
	memcpy(feFeatureVector->features[FVALPHAINDEX].feature_result,feFeatureVector->features[FVALPHAINDEX].feature_result + 4, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVCOSPHIINDEX].feature_result,feFeatureVector->features[FVCOSPHIINDEX].feature_result + 3, (num_results - 12) * sizeof(double));
	memcpy(feFeatureVector->features[FVSINPHIINDEX].feature_result,feFeatureVector->features[FVSINPHIINDEX].feature_result + 3, (num_results - 12) * sizeof(double));
	memcpy(feFeatureVector->features[FVPHIINDEX].feature_result,feFeatureVector->features[FVPHIINDEX].feature_result + 2, (num_results - 12) * sizeof(double));

	// no needs memcpy for radius

	memcpy(feFeatureVector->features[FVPROPORTIONLHINDEX].feature_result,feFeatureVector->features[FVPROPORTIONLHINDEX].feature_result + 4, (num_results - 12) * sizeof(double));
	memcpy(feFeatureVector->features[FVPROPORTIONLH7INDEX].feature_result,feFeatureVector->features[FVPROPORTIONLH7INDEX].feature_result + 3, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVCONTEXTSPEEDINDEX].feature_result,feFeatureVector->features[FVCONTEXTSPEEDINDEX].feature_result + 2, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVAZIMUTHINDEX].feature_result,feFeatureVector->features[FVAZIMUTHINDEX].feature_result + 6, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVELEVATIONINDEX].feature_result,feFeatureVector->features[FVELEVATIONINDEX].feature_result + 6, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVFORCEINDEX].feature_result,feFeatureVector->features[FVFORCEINDEX].feature_result + 8, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVDELTAAZIMUTHINDEX].feature_result,feFeatureVector->features[FVDELTAAZIMUTHINDEX].feature_result + 4, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVDELTAALTITUDEINDEX].feature_result,feFeatureVector->features[FVDELTAALTITUDEINDEX].feature_result + 4, (num_results - 12) * sizeof(double));

	memcpy(feFeatureVector->features[FVDELTAPRESSUREINDEX].feature_result,feFeatureVector->features[FVDELTAPRESSUREINDEX].feature_result + 6, (num_results - 12) * sizeof(double));


	// reallocate feature results
	// after filter we lost 5 in number of results
	// then after calculate speed, acceleration, ... we lost 4x3 = 12 in number of results
	// so we have to reallocate feature results and reduce last 17 results
	for (i = 0; i < num_features; i++)
	{
		double* result = (double*)bsrealloc(feFeatureVector->features[i].feature_result,(num_results - 17)*sizeof(double));
		if (result == 0)
		{
			return BS_ERR_MEM;
		}
		feFeatureVector->features[i].num_feature_results = num_results - 17;
	}
	
	return BS_OK;
}

int centralizeCoordinates(double *coordinates, int size)
{
	double mean = 0;
	int i;
	for (i = 0; i < size; i++)
	{
		mean += coordinates[i];
	}

	mean /= size;

	for (i = 0; i < size; i++)
	{
		coordinates[i] -= mean;
	}

	return BS_OK;
}

int filterCoordinates(double *coordinates, int size)
{
	double fir[] = {0.0457, 0.4543, 0.4543, 0.0457};
	double *tmp = (double*)bscalloc(sizeof(double),size-5);

	memcpy(tmp,coordinates+2,(size-5)*sizeof(double));

	int i,j;
	for (i = 2; i < size - 3; i++)
	{
		tmp[i-2] = 0;
		for (j = 0; j < 4; j++)
		{
			tmp[i-2] += coordinates[i+j] * fir[j];
		}
	}

	memcpy(coordinates,tmp,(size-5)*sizeof(double));
	bsfree(tmp);

	return BS_OK;
}

int filterAngle(double *coordinates, int size)
{
	double *tmp = (double*)bscalloc(sizeof(double),size-4);
	double *tmp2 =   (double*)bscalloc(sizeof(double),5);
	
	CMedian median;
	int i, j;
	for (i = 2; i < size - 2; i++)
	{
		for (j = 0; j < 5; j++)
		{
			tmp2[j] = coordinates[i-2+j];
		}
		
		tmp[i-2] = median(tmp2,5);
	}

	memcpy(coordinates,tmp,(size-5)*sizeof(double));
	bsfree(tmp);
	bsfree(tmp2);

	return BS_OK;
}
