###########################################################
######### Reference System based on hand modality #########
###########################################################
###################### BioSecure ##########################
##################### GET - EPITA #########################
###########################################################
######### first official version 1.0 - 15/03/2007 #########
###########################################################

Notice
------

If you use this BioSecure reference system in order to 
produce scores and/or write an article, you have to mention 
the next paper:

Geoffroy Fouquier, Laurence Likforman, Jerome Darbon and Bulent Sankur
The Biosecure Geometry-based System for Hand Modality
In the proceedings of the 32nd IEEE International Conference on
Acoustics, Speech, and Signal Processing (ICASSP'2007), vol I, pages
801--804, ISBN: 1-4244-0728-1. Honolulu, Hawaii, USA, April 2007


What can you find here?
-----------------------

- the BioSecure reference system (source code and scripts) 
used to make a full recognition experiment based on hand 
modality.
- a documentation which explains how to use the system.


First step
----------

To begin, I advise you to read the document stored in 
/doc directory.


Short description of each directory
-----------------------------------

- doc: documentation which explains how to install and 
use the reference system.

- download: reference system in the form of compressed 
file (tar.gz) to download it.

- lib: qgar library.

- results: intermediate results and final scores (obtained 
on the reference database in accordance with the 
benchmarking protocol). These files can be used by the 
user who does not want to make a full experiment from 
A to Z.

- scripts: scripts to manage experiments.

- src: source code of the reference system.

- trials: lists of trials in accordance with the 
benchmarking protocol.


Questions
---------

If you have questions about the use of the reference system, 
you can contact the next person:
Aurelien Mayoue: aurelien.mayoue@int-evry.fr
