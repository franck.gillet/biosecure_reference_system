/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "chmm.h"
#include "math.h"
#include "stdio.h"
#include "statistics.h"
#include "cmemalc.h"

EXPORT_METHOD CHmm::CHmm(int N_, int M_, int transType_, int covMatrixType_,
					bool bRandomInit_,
					bool bParameterNormalization_, 
					double parameterStandardDeviation_,
					double endAccuracy_,
					int endIterationNumber_)
{
	N = N_;
	M = M_;
	transType = transType_;
	covMatrixType = covMatrixType_;
	bRandomInit = bRandomInit_;
	bParameterNormalization = bParameterNormalization_;
	parameterStandardDeviation = parameterStandardDeviation_;
	endAccuracy = endAccuracy_;
	endIterationNumber = endIterationNumber_;
	
	a = NULL;
	pi = NULL;
	obs = NULL;
	gmm = NULL;
	bi = NULL;
	bi_gaus = NULL;
	alpha = NULL;
	beta = NULL;
	scale = NULL;

	tbStSequenceLen = NULL;
	tbStSequence = NULL;
	normalizationFactors = NULL;

	biLen = 0;
	alphaLen = 0;
	betaLen = 0;
	bInit = false;
	K = 0;	
	L = 0;	
	// L & K concern the observations so they will be inited later
}

EXPORT_METHOD bool CHmm::assignObs(CFeatureVector **obs_, int K_)
{
	if(K_ < 0) return false;
	
	obs = obs_;
	K = K_;
	L = obs[0]->num_features[0];
	
	// Normalize
	if(bParameterNormalization == PARAMETER_NORMALIZATION_YES)
	{
		computeNormalizationFactor();
		normalizeTrainBaseParameters();
	}

	return true;
}

EXPORT_METHOD CHmm::~CHmm()
{
	destroyHmmVariable();

	if(normalizationFactors != NULL)
	{
		CMemalc memalc;
		memalc.free_dvector(normalizationFactors);
		normalizationFactors = NULL;
	}	
}

EXPORT_METHOD void CHmm::destroyHmmVariable()
// If we cannot train HMM then use this function
{
	int n;
	CMemalc memalc;

	if(gmm != NULL)
	{
		for (n = 0; n < N; n++)
		{
			if(gmm[n] != NULL) delete gmm[n];
		}
		delete gmm;
		gmm = NULL;
	}
	
	if(pi != NULL)
	{
		memalc.free_dvector(pi);
		pi = NULL;
	}

	if(a != NULL)
	{
		memalc.free_dmatrix(a, N);
		a = NULL;
	}

	if(alpha != NULL)
	{
		memalc.free_dmatrix(alpha, N);
		alpha = NULL;
	}

	if(beta != NULL)
	{
		memalc.free_dmatrix(beta, N);
		beta = NULL;
	}

	if(scale != NULL)
	{
		memalc.free_dvector(scale);
		scale = NULL;
	}

	if(bi != NULL)
	{
		memalc.free_dmatrix(bi, N);
		bi = NULL; 
	}

	if(bi_gaus != NULL)
	{
		memalc.free_dmatrix3d(bi_gaus,	M, N);
		bi_gaus = NULL;
	}
	if (tbStSequenceLen != NULL)
	{
		memalc.free_ivector(tbStSequenceLen);
		tbStSequenceLen = NULL;
	}
	if (tbStSequence != NULL)
	{
		memalc.free_imatrix(tbStSequence, K);	
		tbStSequence = NULL;
	}
}

EXPORT_METHOD bool CHmm::initHMM()
{
	CMemalc memalc;
	if (obs != NULL)
	{	
		destroyHmmVariable();

		CFeatureVector ** obs_etat = distributeObs();
		
		gmm = new CGmm * [N];
		bInit = true;

		int n;

		for (n = 0; n < N; n++)
		{
			
			gmm[n] = new CGmm(M, L, covMatrixType, bRandomInit);
			bInit = bInit & gmm[n]->initGMM(obs_etat[n]);
			int* test = obs_etat[n]->num_features;
			delete obs_etat[n];
		}
		delete obs_etat;
		
		if(bInit) // if GMMs are well inited 
		{
			pi = memalc.dvector(N);
			pi[0] = 1;	
			initTrans(transType); 
		}
		else	  // if not, delete GMMS
		{
			printf("GMMs are not well inited\n");
			destroyHmmVariable();
		}
		return bInit;
	}
	else
	{
		printf("Can not load the signature file!\n");		
		bInit = false;
		return bInit;
	}
}

EXPORT_METHOD double CHmm::viterbi(CFeatureVector *ob, int ** statSeq, bool last_state_exit_oblige)
{	
	double LL;
	double **phi;	// N x T
	int **psi;		// N x T
	double ** log_a;	// log of transition matrix N x N
	double ** log_bi;	// log of matrix bi N * T;
	int * q;
	CMemalc memalc;

	bool b = initProObs(ob);	//init bi, bi_gaus
	
	if(b) //	initProObs OK	
	{						
		int len = ob->features[0].num_feature_results; 
		phi = memalc.dmatrix(N, len);
		psi = memalc.imatrix(N, len);
		q = memalc.ivector(len);

		log_a = memalc.dmatrix(N, N);
		int i, j;
		for(i = 0; i < N; i++)
		{
			for(j = 0; j < N; j++)
			{
				if(a[i][j] > 0)
				{
					log_a[i][j] = log(a[i][j]);
				}
				else
				{
					log_a[i][j] = LOG_ZERO;
				}
			}
		}

		log_bi = memalc.dmatrix(N, len);
		int t, n;
		for(t = 0; t < len; t++)
		{
			for(n = 0; n < N; n ++)
			{
				if(bi[n][t] > 0)
				{
					log_bi[n][t] = log(bi[n][t]);
				}
				else
				{
					log_bi[n][t] = LOG_ZERO;
				}
			}
		}

		// equations 105a, 105b, 105c RABINER
		for(i = 0; i < N; i++)
		{
			if(pi[i] > 0)
			{
				phi[i][0] = log(pi[i]) + log_bi[i][0];
			}
			else
			{
				phi[i][0] = LOG_ZERO + log_bi[i][0];
			}
		}
		
		for(t = 1; t < len; t++)
		{
			for(j = 0; j < N; j++)
			{				
				double maxVal = -1e40;
				int indice;
				for(i = 0; i < N; i++)
				{
					if((phi[i][t-1] + log_a[i][j]) >= maxVal)
					{
						maxVal = phi[i][t-1] + log_a[i][j];
						indice = i;
					}
				}
				phi[j][t] = maxVal + log_bi[j][t];
				psi[j][t] = indice;
			}
		}

		if(last_state_exit_oblige && (transType == TRANS_LR))
		{ 
			LL = phi[N-1][len-1];		
			q[len-1] = N-1;
		}
		else
		{
			LL = -1e40; 
			for(i = 0; i < N; i++)
			{
				if(phi[i][len-1] >= LL)
				{
					LL = phi[i][len-1];
					q[len-1] = i;
				}
			}
		}
		// Back tracking
		for(t = len - 2; t >= 0; t--)
		{
			q[t] = psi[q[t+1]][t+1];
		}		
		memalc.free_dmatrix(phi, N);
		memalc.free_imatrix(psi, N);
		memalc.free_dmatrix(log_a, N);
		memalc.free_dmatrix(log_bi, N);
		* statSeq = q;
		LL = LL / (len * L);
		return LL;
	}
	else
	{
		return 0;
	}
}

EXPORT_METHOD bool CHmm::initProObs(CFeatureVector *ob)
{
	CMemalc memalc;
	
	if((ob->features[0].num_feature_results < 1) | (!bInit))
	{
		return false;
	}

	if(bi != NULL)
	{
		memalc.free_dmatrix(bi, N);
		bi = NULL; 
	}
	if(bi_gaus != NULL)
	{
		memalc.free_dmatrix3d(bi_gaus,	M, N);
		bi_gaus = NULL;
	}

	// delete before change biLen
	biLen = ob->features[0].num_feature_results; 
	bi = memalc.dmatrix(N, biLen);

	bi_gaus = memalc.dmatrix3d(M, N, biLen);
	//Calcule thes probabilities here, gaussienne rule
	int m, n, l, i, j, t;
	for(m = 0; m < M; m++) // each gaussien
	{		
		for(n = 0; n < N; n++) // each state
		{			
			double det_u = 1;
			for(l = 0; l < L; l++)
			{
				det_u *= gmm[n]->u[m][l][l];				
			}
			if(det_u == 0)
			{ 
				printf("matrice de covariance singuli�e\n"); return false;
			}

			////////// Calcule the inverse matrix ////////// 
			double ** inverse_u = memalc.dmatrix(L, L); 
			for(i = 0; i < L; i++)
			{
				double det_for_inv = 1;				
				for(j = 0; j < L; j ++)
				{
					if (j != i) det_for_inv *= gmm[n]->u[m][j][j];
				}
				inverse_u[i][i] = det_for_inv / det_u;
			}
			

			double con = pow(2 * PI, double(-L/2)) / sqrt(det_u);
			for(t = 0; t < biLen; t++) // each vector of observation
			{	
				double exp_com = 0;
				for(l = 0; l < L; l++)
				{
					exp_com += (ob->features[l].feature_result[t] - gmm[n]->mu[l][m]) * inverse_u[l][l] * 
							   (ob->features[l].feature_result[t] - gmm[n]->mu[l][m]);
				}				
				bi_gaus[m][n][t] = gmm[n]->c[m] * con * exp(-exp_com/2);

				if(bi_gaus[m][n][t] < PRO_MIN_VAL)
				{
					bi_gaus[m][n][t] = PRO_MIN_VAL;
				}
			}
			memalc.free_dmatrix(inverse_u, L);
		} 
	}

	for(n = 0; n < N; n++)
	{
		for(t = 0; t < biLen; t ++)
		{
			for(m = 0; m < M; m++)
			{
				bi[n][t] += bi_gaus[m][n][t];
			}
		}
	}

	return true;
}

EXPORT_METHOD double CHmm::forward(CFeatureVector *ob)
{
	CMemalc memalc;
	// alpha^ and beta^. equation 93b and 94 RABINER
	// scaling, page 272, RABINER
	double LL = 0;

	bool b = initProObs(ob); //init bi, bi_gaus
	if(b) //	initProObs OK
	{		
		if(alpha != NULL)
		{
			memalc.free_dmatrix(alpha, N);
			alpha = NULL;
		}
		if(scale != NULL)
		{
			memalc.free_dvector(scale);
			scale = NULL;
		}

		alphaLen = ob->features[0].num_feature_results;
		alpha = memalc.dmatrix(N, alphaLen);
		scale = memalc.dvector(alphaLen);

		// equations 19, 20, 21 RABINER
		int i;
		for(i = 0; i < N; i ++)
		{
			alpha[i][0] = pi[i] * bi[i][0];	// equation 19
			scale[0] += alpha[i][0];
		}

		scale[0] = 1/scale[0];
		// scaling...
		for(i = 0; i < N; i ++)
		{
			alpha[i][0] *= scale[0];
		}

		int t;
		for(t = 0; t < alphaLen - 1; t++)
		{
			int j;
			for(j = 0; j < N; j++)
			{
				double tmp = 0;
				int i;
				for(i = 0; i < N; i++)
				{
					tmp += alpha[i][t] * a[i][j];
				}
				alpha[j][t+1] = tmp * bi[j][t+1];	// equation 20

				scale[t+1] += alpha[j][t+1];
			}
			// scaling...			
			scale[t+1] = 1 / scale[t+1];
			for(j = 0; j < N; j++)
			{
				alpha[j][t+1] *= scale[t+1];
			}// end scaling...
		}

		for(t = 0; t < alphaLen; t++)
		{
			LL -= log(scale[t]);
		}
		LL = LL / (alphaLen * L);
		return LL;
	}
	else
	{
		return 0;
	}
}

EXPORT_METHOD bool CHmm::backward(CFeatureVector *ob)
// it must always call forward before backward beacause of scaling
{
	CMemalc memalc;

	if(beta != NULL)
	{
		memalc.free_dmatrix(beta, N);
		beta = NULL;
	}
	
	betaLen = ob->features[0].num_feature_results;
	beta = memalc.dmatrix(N, betaLen);
	
	// equations 24, 25  RABINER
	int i, t, j;
	for(i = 0; i < N; i ++)
	{
		beta[i][betaLen-1] = scale[betaLen-1];	// equation 24
	}

	for(t = betaLen - 2; t >= 0; t--)
	{
		for(i = 0; i < N; i++)
		{
			beta[i][t] = 0;
			for(j = 0; j < N; j++)
			{
				beta[i][t] += a[i][j] * bi[j][t+1] * beta[j][t+1];	// equation 25
			}
			beta[i][t] *= scale[t];

			if(beta[i][t] < 0)
			{ 
				return false;
			}
		}
	}

	return true;
}

EXPORT_METHOD CFeatureVector** CHmm::distributeObs()
{
	CFeatureVector ** obs_etat;
	obs_etat = new CFeatureVector * [N];
	int n, k;
	for(n = 0; n < N; n++)
	{
		obs_etat[n] = newObs(L, 0);
	}

	for(k = 0; k < K; k++)
	{
		int size = (*obs)[k].features[0].num_feature_results / N;
		// N-1 first states
		for(n = 0; n < N-1; n++)
		{			
			addObs(obs_etat[n],&((*obs)[k]), n*size, size);
		}
		// last state
		addObs(obs_etat[N-1],&((*obs)[k]), (N-1) * size, (*obs)[k].features[0].num_feature_results - (N-1) * size);
	}
	return obs_etat;
}


EXPORT_METHOD void CHmm::initTrans(int transType)
{
	CMemalc memalc;
	a = memalc.dmatrix(N, N);
	int i, j;
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < N; j++)
		{
			a[i][j] = 0;
		}
	}
	if(transType == TRANS_LR)
	{
		for(i = 0; i < N-1; i ++)
		{
			a[i][i] = 0.5;
			a[i][i+1] = 0.5;
		}
		a[N-1][N-1] = 1;
	}
}

EXPORT_METHOD double CHmm::baumwelch()
// re-estimate the parameters of HMM
{
	double LLacc = 0;	// Log likelihood accumulate on K observations 
	double ** nume_a;		// N x N	
	double ** denom_a;		// N x N
	double ** nume_c;		// N * M	
	double * denom_c;		// 1 x N
	double *** nume_mu;		// N x L x M 
	double **** nume_u;		// N x (M x L x L) 

	CMemalc memalc;
	// attention: allocations fill all matrix with 0
	nume_a = memalc.dmatrix(N, N);
	denom_a = memalc.dmatrix(N, N);
	nume_c = memalc.dmatrix(N, M);
	denom_c = memalc.dvector(N);
	nume_mu = memalc.dmatrix3d(N, L, M);
	nume_u = memalc.dmatrix4d(N, M, L, L);

	int k;
	for(k = 0; k < K; k++)
	{	
		int T;						// long of the observation k (obs[k])
		double *** gama;			// M x N x T, gama is recalculed for each observation
		double ll = forward(&((*obs)[k]));

		bool backwardOk = backward(&((*obs)[k]));
		if(!backwardOk)
		{
			memalc.free_dmatrix(nume_c, N);
			memalc.free_dvector(denom_c);
			memalc.free_dmatrix3d(nume_mu, N, L);
			memalc.free_dmatrix4d(nume_u, N, M, L);
			memalc.free_dmatrix(nume_a, N);
			memalc.free_dmatrix(denom_a, N);
			return 1;
		}

		T = (*obs)[k].features[0].num_feature_results;
		LLacc += ll;
		gama = memalc.dmatrix3d(M, N, T);
		// calcule gama: page 267 RABINER
		int t, i, j, m;
		for(t = 0; t < T; t++)
		{
			for(j = 0; j < N; j++)
			{
				double denom_tmp = 0;
				for(i = 0; i < N; i++)
				{
					denom_tmp += alpha[i][t] * beta[i][t];
				}
				// m is used instead of k to calcule gama, equation 54, RABINER
				for(m = 0; m < M; m++)
				{
					gama[m][j][t] = (alpha[j][t] * beta[j][t] / denom_tmp) * 
						(bi_gaus[m][j][t] / bi[j][t]);
				}
			}
		}

		// For c, mu and u: the equations (52), (53), (54) RABINER
		for(j = 0; j < N; j++)
		{
			for(m = 0; m < M; m++)
			{
				// nominator and denominator of c
				for(t = 0; t < T; t++)
				{
					nume_c[j][m] += gama[m][j][t];
					denom_c[j] += gama[m][j][t];	
				}			
			}
		}

		int l;
		for(j = 0; j < N; j++)
		{
			for(m = 0; m < M; m++)
			{
				for(l=0; l < L; l++)
				{
					for(t = 0; t < T; t++)
					{
						nume_mu[j][l][m] += gama[m][j][t] * (*obs)[k].features[l].feature_result[t];
						nume_u[j][m][l][l] += gama[m][j][t] * ((*obs)[k].features[l].feature_result[t] - gmm[j]->mu[l][m]) * ((*obs)[k].features[l].feature_result[t] - gmm[j]->mu[l][m]);
					}
				}
			}
		}

		memalc.free_dmatrix3d(gama, M, N);
		// re-estimate the matrix of transition
		for(i = 0; i < N; i++)
		{
			for(j = 0; j < N; j++)
			{
				for(t = 0; t < T-1; t++)
				{
					// equation 111 RABINER does not work
					// if must divise the denom_a by the factor of normalisation
					nume_a[i][j] += alpha[i][t] * a[i][j] * bi[j][t+1] * beta[j][t+1];
					denom_a[i][j] += alpha[i][t] * beta[i][t] / scale[t];
				}
			}
		}
	} 

	// re-estimate the parameters of HMM
	int j, m, l, i; 
	for(j = 0; j < N; j++)
	{
		for(m = 0; m < M; m++)
		{
			gmm[j]->c[m] = nume_c[j][m] / denom_c[j];
			for(l = 0; l < L; l++)
			{
				gmm[j]->mu[l][m] = nume_mu[j][l][m] / nume_c[j][m] ;
				gmm[j]->u[m][l][l] = nume_u[j][m][l][l] / nume_c[j][m] ;
				if(gmm[j]->u[m][l][l] < COV_MIN_VAL)
				{
					gmm[j]->u[m][l][l] = COV_MIN_VAL;
				}
			}
		}
	}

	for(i = 0; i < N; i++)
	{
		for(j = 0; j < N; j++)
		{
			if(denom_a[i][j] != 0)
			{
				a[i][j] = nume_a[i][j] / denom_a[i][j];
			} 			
			else
			{
				printf("%f\t", denom_a[i][j]);
				if ( j == N-1)	printf("\n");
			}
		}
	}

	memalc.free_dmatrix(nume_c, N);
	memalc.free_dvector(denom_c);
	memalc.free_dmatrix3d(nume_mu, N, L);
	memalc.free_dmatrix4d(nume_u, N, M, L);
	memalc.free_dmatrix(nume_a, N);
	memalc.free_dmatrix(denom_a, N);
	return (LLacc/K);
}

/********************/
EXPORT_METHOD bool CHmm::train()
{
	fprintf(stderr,"Training HMM\n");
	initHMM();
	if(!bInit)
	{
		trained = false;
		return false;
	}
	else
	{
		double LLbefore, LLbelow;
		CMemalc memalc;
		LLbefore = LOG_ZERO;
		int loop;
		for(loop = 1; loop <= MAX_ITER_HMM; loop++)
		{
			fprintf(stdout, "Iteration %d\n", loop);
			LLbelow = baumwelch();

			if(LLbelow > 0) // reason: there are so much states to train HMM
			{	
				destroyHmmVariable();
				trained = false;
				return false;
			}

			if(endIterationNumber == 0)
			{
				if(fabs((LLbelow - LLbefore)/LLbelow) <= endAccuracy)
				{
					break;
				}
				LLbefore = LLbelow;
			}
			else
			{
				if(loop == endIterationNumber)
				{
					break;
				}
			}
		}

		tbStSequenceLen = memalc.ivector(K);
		tbStSequence = (int**) bscalloc(sizeof(int*), K);
		double * llbas = memalc.dvector(K);
		int k;
		for(k = 0; k < K; k++)
		{
			tbStSequenceLen[k] = (*obs)[k].features[0].num_feature_results;
			llbas[k] = viterbi(&((*obs)[k]), tbStSequence + k, false);
		}
		CVariance variance;
		variance(llbas, K, &LLba, & VARba);

		CCombination combination;
		int ncombi = combination(2, K);
		double* disArray = memalc.dvector(ncombi);
		int i, j, counter;
		counter = 0;
		for(i = 0; i < K-1; i++)
		{
			for(j = i+1; j < K; j ++)
			{
				disArray[counter++] = computeViterbiHamDistance(tbStSequence[i], tbStSequenceLen[i], tbStSequence[j], tbStSequenceLen[j]);
			}
		}
		variance(disArray, ncombi,&meanSegVecDis, &varSegVecDis);

		counter = 0;
		for(i = 0; i < K-1; i++)
		{
			for(j = i+1; j < K; j ++)
			{
				disArray[counter++] = fabs(llbas[i] - llbas[j]);
			}
		}
		variance(disArray, ncombi,&meanLLDis, &varLLDis);
		memalc.free_dvector(disArray);
		memalc.free_dvector(llbas);

		trained = true;
		return true;
	}	
}

/********************/

EXPORT_METHOD double CHmm::getLLba()
{
	return LLba;
}

EXPORT_METHOD double CHmm::getVARba()
{
	return VARba;
}

EXPORT_METHOD bool CHmm::isInit()
{
	return bInit;
}

EXPORT_METHOD int CHmm::getObsNumber()
{	
	return K;
}

EXPORT_METHOD int CHmm::getStateNumber()
{
	return N;
}

EXPORT_METHOD void CHmm::setNStates(int N_)
{
	N = N_;
}


EXPORT_METHOD void CHmm::setNGaussians(int M_)
{
	M = M_;
}

EXPORT_METHOD void CHmm::computeNormalizationFactor()
{
	CMemalc memalc;
	memalc.free_dvector(normalizationFactors);
	normalizationFactors = memalc.dvector(L);	// full fill zero
	double mean, var;
	int k, l;
	CVariance variance;
	for(k = 0; k < K; k ++)
	{
		for(l = 0; l < L; l++)
		{
			variance((*obs)[k].features[l].feature_result, (*obs)[k].features[l].num_feature_results, &mean, &var);
			normalizationFactors[l] += sqrt(var);
		}
	}
	// moyenner
	for(l = 0; l < L; l++)
	{
		normalizationFactors[l] = normalizationFactors[l] / K / parameterStandardDeviation;
	}
}

EXPORT_METHOD bool CHmm::normalizeTrainBaseParameters()
{
	if(normalizationFactors == NULL)
	{
		return false;
	}
	else
	{
		int k;
		for(k = 0; k < K; k++)
		{
			normalizeParameters(&((*obs)[k]),normalizationFactors);
		}
		return true;
	}
}

EXPORT_METHOD bool CHmm::normalizeObsParameters(CFeatureVector * ob)
// use to normalize a 
{
	if(bParameterNormalization) //control the HMM property
	{ 
		if(ob->features == 0)
			return false;	// not init

		if(ob->num_features[0] != L)
			return false;	// not compatible

		normalizeParameters(ob,normalizationFactors);
	}
	return true;
}

EXPORT_METHOD double CHmm::computeViterbiHamDistance(int *firstSeq, int firstSeqLen, 
									  int *secondSeq, int secondSeqLen)
{
	double * hisx;
	double * hisy;
	double dis = 0;
	CMemalc memalc;

	hisx = memalc.dvector(N);
	hisy = memalc.dvector(N);
	int i, n;
	for(i = 0; i < firstSeqLen; i++)
	{
		hisx[firstSeq[i]]++;
	}
	for(i = 0; i < secondSeqLen; i++)
	{
		hisy[secondSeq[i]]++;
	}

	for(n = 0; n < N; n++)
	{
		dis += fabs(hisx[n] - hisy[n]);
	}
	memalc.free_dvector(hisx);
	memalc.free_dvector(hisy);
	return dis/N;
}

EXPORT_METHOD CFeatureVector* CHmm::newObs(int _L, int _T)
{
	CFeatureVector* obs = new CFeatureVector;
	if (obs->num_features == 0)
	{
		obs->num_features = (int*) bsmalloc(sizeof(int));
	}
	obs->num_dimensions = 1;//without this, we cannot delete all features
	obs->num_features[0] = _L;
	if (obs->features == 0) 
	{
		obs->features = (CFeatureData*) bscalloc(sizeof(CFeatureData),_L);
	}
	int i;
	for (i = 0; i < _L; i++)
	{
		obs->features[i].num_feature_results = _T;
		obs->features[i].feature_result = (double*) bscalloc(sizeof(double),_T);
	}
	return obs;
}

EXPORT_METHOD bool CHmm::addObs(CFeatureVector* obs, CFeatureVector* obs_supl,int index, int size)
{
	if( (index < 0) | (index >= obs_supl->features[0].num_feature_results) )
	{
		return false;
	}

	if( (size < 0) | (size >= (obs_supl->features[0].num_feature_results - index + 1)) )
	{
		return false;
	}

	int T;
	for(int l = 0; l < L; l++)
	{
		T = obs->features[l].num_feature_results;
		if (T == 0)
		{
			bsfree(obs->features[l].feature_result);
			obs->features[l].feature_result = (double*) bscalloc(sizeof(double),size);
		}
		else
		{
			obs->features[l].feature_result = (double*) bsrealloc(obs->features[l].feature_result,(T + size)*sizeof(double));
		}

		obs->features[l].num_feature_results += size;		
		for (int t = 0; t < size; t++)
		{
			obs->features[l].feature_result[T + t] = obs_supl->features[l].feature_result[index + t];
		}
	}	
	
	return true;
}

EXPORT_METHOD void CHmm::normalizeParameters(CFeatureVector* obs, double * normFactor)
{
	int l, i;
	for(l = 0; l < L; l++)
	{
		for(i = 0; i < obs->features[0].num_feature_results; i++)
		{
			obs->features[l].feature_result[i] /= (normFactor[l]);
		}
	}
}

EXPORT_METHOD void* CHmm::operator new(size_t size)
{
	return bscalloc(size,1);
}

EXPORT_METHOD void CHmm::operator delete(void *p)
{
	bsfree(p);
}
