/*
 * Copyright (c) 2007 Uni. Magdeburg
 * Authors Andreas Engel, Glen Masgai, Tobias Scheidat & Claus Vielhauer
 *
 * This file is part of the BioSecure - Magdeburg reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_ERROR_H
#define BS_ERROR_H

#define BS_MAX_ERROR            1024    // maximum length of error string

#define BS_OK                   0       // no error
#define BS_ERR_SOURCE           -1      // invalid source data
#define BS_ERR_PARAM            -2      // invalid parameter
#define BS_ERR_MEM              -3      // memory allocation error
#define BS_ERR_LIB              -4      // error using shared libraries
#define BS_ERR_INVALID          -5      // invalid function
#define BS_ERR_LOAD             -6      // error while loading file
#define BS_ERR_SAVE             -7      // error while saving file

#endif
