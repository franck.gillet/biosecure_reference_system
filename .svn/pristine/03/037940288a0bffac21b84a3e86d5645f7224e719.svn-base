/*
 * Copyright (c) 2007 Bogazici University
 * Author Cem Demirkir
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <time.h>
#include <float.h>
#include <math.h>
#include <limits.h>

#include <string>
#include <iostream>
#include <cstdlib>

#include <cv.h>
#include <highgui.h>

#include "IV_Common.h"
#include "FaceRecognizerPCA.h"
#include "FaceRecognizerProbPCA.h"

int main( int argc, char* argv[] )
{
	int i;
	int test=0;
	int target=0;
	std::string inputFile;
	std::string modelFile;
	std::string inTargetProtocolFileName;
	std::string inTestProtocolFileName;
	std::string outTargetVectorsFileName;
	std::string outTestVectorsFileName;

  if( argc == 1 )
  {
    printf( "Usage: %s\n  -params <IV_System_Parameters_File> -model <Model_File_Name> -inTarget <Target_Protocol> -inTest <Test_Protocol> -outTarget <Target_Vectors> -outTest <Test_Vectors>", argv[0] );
		printf( "Program produces xml test output using given input parameters by the -params and -model switches.\n" );
		printf( "-params : IV system parameters given by <IV_System_Parameters_File> in XML format.\n" );
		printf( "-model	 : Model file name produced by the SaveModel program.\n" );
		printf( "-inTarget : Target protocol file in XML format.\n" );
		printf( "-inTest : Test protocol file in XML format.\n" );
		printf( "-outTarget : Target vectors ouput file in XML format.\n" );
		printf( "-outTest : Test vectors ouput file in XML format.\n" );
		//printf( "-outLog : Test ouput log file in XML format.\n" );
		//printf( "-outROC : Text output fie for ROC.\n" );
    return 0;
  }

  for( i = 1; i < argc; i++ )
  {
    if( !strcmp( argv[i], "-params" ) )
    {
			inputFile = argv[++i];
    }
		else
		{
			if ( !strcmp( argv[i], "-model" ) )
			{
				modelFile = argv[++i];
			}
			else
			{
				if ( !strcmp( argv[i], "-inTarget" ) )
				{
					inTargetProtocolFileName = argv[++i];
					target=1;
				}
				else
				{
					if ( !strcmp( argv[i], "-inTest" ) )
					{
						inTestProtocolFileName = argv[++i];
						test=1;
					}
					else
					{
						if ( !strcmp( argv[i], "-outTarget" ) )
						{
							outTargetVectorsFileName = argv[++i];
						}
						else
						{
							if ( !strcmp( argv[i], "-outTest" ) )
							{
								outTestVectorsFileName = argv[++i];
							}
						}
					}
				}
			}
		}
	}

	IVPrmtrSettings settings;

	settings.load_test(inputFile.c_str(),inTargetProtocolFileName.c_str(),inTestProtocolFileName.c_str());

	FaceData trgtData, tstData;

	tstData.Load( settings.imageFilesLocation.c_str(), 
								settings.imageFilesType.c_str(),
								settings.testSessionProtocolFile.c_str(),
								settings.maskImageFile.c_str());

	trgtData.Load( settings.imageFilesLocation.c_str(), 
								settings.imageFilesType.c_str(),
								settings.targetSessionProtocolFile.c_str(),
								settings.maskImageFile.c_str() );

	if ( settings.method == "Maximum_Likelihood_PCA" )
	{
		if (target==1)
		{
		CRecognizerProbPCA probPCARec( modelFile, trgtData );
		probPCARec.WriteTargetVectors( settings, outTargetVectorsFileName );
		}
		if (test==1)
		{
		CRecognizerProbPCA probPCARec( modelFile, tstData );
		probPCARec.WriteTestVectors( settings, tstData, outTestVectorsFileName );
		}
	}
	else
	{
		if ( settings.method == "PCA" )
		{
			if (target==1)
			{
			CRecognizerPCA pcaRec( modelFile, trgtData );
			pcaRec.WriteTargetVectors( outTargetVectorsFileName );
			}
			if (test==1)
			{
			CRecognizerPCA pcaRec( modelFile, tstData );
			pcaRec.WriteTestVectors( tstData, outTestVectorsFileName );
			}
		}
	}	

	return 1;
}

