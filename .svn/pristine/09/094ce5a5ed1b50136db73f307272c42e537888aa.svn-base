/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _CClient_Model_
#define _CClient_Model_
#include <bs_dynlink.h>
#include <bs_mem.h>
class CModel
{
public:
	CModel(){};
	IMPORT_METHOD virtual ~CModel() {};
	IMPORT_METHOD virtual bool train() = 0;
	IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};

class CClientModel
{
private:
	CModel* m_model;
public:
	IMPORT_METHOD CClientModel();
	IMPORT_METHOD virtual ~CClientModel();

	IMPORT_METHOD void setModel(CModel* _model);
	IMPORT_METHOD const CModel* getModel();
	IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};
#endif

