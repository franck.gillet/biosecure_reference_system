/*
 * Copyright (c) 2007 GET-INT
 * Authors Bao Ly Van, Sonia Salicetti & Bernadette Dorizzi
 *
 * This file is part of the BioSecure - GET_INT reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BS_CFEATUREVECTOR_H
#define BS_CFEATUREVECTOR_H

#include <bs_dynlink.h>
#include <bs_cfeaturedata.h>

class CFeatureVector
{
public:
    int                 num_dimensions;	// number of dimensions of the features matrix
    int                 *num_features;  // number of features for each dimension
    CFeatureData        *features;      // Features
    char                *feat_vec_id;   // feature vector identifier string, always static

    CFeatureVector(){}
    IMPORT_METHOD ~CFeatureVector();
    IMPORT_METHOD void* operator new(size_t size);
    IMPORT_METHOD void operator delete(void *p);
};

#endif
