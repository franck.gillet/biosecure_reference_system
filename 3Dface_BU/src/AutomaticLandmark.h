/*
 * Copyright (c) 2007 BU
 * Authors Berk Gokberk, Okan Irfanoglu
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


// AutomaticLandmark.h: interface for the AutomaticLandmark class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AUTOMATICLANDMARK_H__9F91D488_923F_4EE3_9F41_A98A194700FF__INCLUDED_)
#define AFX_AUTOMATICLANDMARK_H__9F91D488_923F_4EE3_9F41_A98A194700FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "includes.h"

/*!
  \defgroup AutomaticLandmark Automatic landmark localization
*/

/*! 
  \ingroup AutomaticLandmark
	\brief Finds the positions of facial landmarks of all input faces
	
	A void constructor starts the automatic landmarking procedure		

  Example:
	\code
        AutomaticLandmark * mylandmarking= (AutomaticLandmark *) new AutomaticLandmark;
	    if(!mylandmarking)
		{
		    printf("Error allocating memory for mylandmarking! Exiting!\n");
		    exit(0);
		}
	    delete mylandmarking;
		\endcode 
*/

class AutomaticLandmark  
{
private:

	
	/*! \brief Finds the landmarks of a given face
			\param myface Input face data
	*/
	void LandmarkIt(vtkPolyData *myface);

	/*! \brief Reads mean face's plane information from a file : "mean face planes.txt"
			\param meanplanenormals Normals of mean face plane
			\param meanplanepositions  Positions of mean face plane
	*/
	void GetMeanPlaneData(float meanplanenormals[][3],float meanplanepositions[][3]);


	/*! \brief Triangulates a given face data
			\param myface Input face data
	*/
	vtkPolyData* GetTriangles(vtkPolyData *myface);

	/*! \brief Computes the ICP transformation between mean face and a given face
			\param triangulatedface Triangulated input face
	*/
	vtkIterativeClosestPointTransform *GetICPTransformation(vtkPolyData *triangulatedface);

	/*! \brief Computes the positions of mean face landmarks using the ICP transformation
			\param icp ICP transformation parameters
			\param Transformed mean face
	*/
	vtkPoints *GetMatchedMeanLandmarks(vtkIterativeClosestPointTransform *icp,vtkPolyData **resultingmeanface);

	/*! \brief Coarse localization of landmark points on the test face
			\param triangulatedface Triangulated input face
			\param closecells 
			\param resultingmeanfacelandmarkspoints
	*/
	vtkPoints *GetFirstGuessLandmarkPoints(vtkPolyData *triangulatedface,vtkCellLocator *closecells,vtkPoints *resultingmeanfacelandmarkspoints);


	/*! \brief Get all data
			\param symmetrypolydata
			\param eyespolydata
			\param mouthpolydata
			\param icp
			\param meanplanenormals[][3]
			\param meanplanepositions[][3]
			\param triangulatedface
			\param ren1
	*/
	void GetAllPolyData(vtkPolyData **symmetrypolydata,vtkPolyData **eyespolydata,vtkPolyData **mouthpolydata,vtkIterativeClosestPointTransform *icp,float meanplanenormals[][3], float meanplanepositions[][3],vtkPolyData *triangulatedface,vtkRenderer *ren1);

	/*! \brief Translate according to nose tip
			\param positions
			\param closecells
			\param newlandmarkpoints
			\param symmetrypolydata
	*/
	void BurunUcunaGoreTranslate(float *positions,vtkCellLocator *closecells,vtkPoints *newlandmarkpoints,vtkPolyData *symmetrypolydata);


	/*! \brief Get face normals
			\param triangulatedface
	*/
	vtkFloatArray *GetFaceNormals(vtkPolyData *triangulatedface);


	/*! \brief Optimize landmark locations using surface normals
			\param facenormals
			\param resultingmeanface
			\param triangulatedface
			\param newlandmarkpoints
			\param resultingmeanfacelandmarkspoints
	*/
	vtkFloatArray *NormallereGoreOptimize(vtkFloatArray *facenormals, vtkPolyData *resultingmeanface,vtkPolyData *triangulatedface,vtkPoints *newlandmarkpoints,vtkPoints *resultingmeanfacelandmarkspoints);

	/*! \brief Find nose tip landmark
			\param symmetrypolydata 
			\param eyespolydata 
			\param newlandmarkpoints 
			\param facenormals 
			\param triangulatedface 
			\param closecells
	*/
	void BurunTepeBul(vtkPolyData *symmetrypolydata,vtkPolyData *eyespolydata,vtkPoints *newlandmarkpoints,vtkFloatArray *facenormals,vtkPolyData *triangulatedface,vtkCellLocator *closecells);


	/*! \brief Find lower nose landmark
			\param symmetrypolydata 
			\param newlandmarkpoints 
			\param closecells
			\param triangulatedface 
			\param facenormals 
	*/
	void BurnunHemenAltindakiNokta(vtkPolyData *symmetrypolydata,vtkPoints *newlandmarkpoints,vtkCellLocator *closecells,vtkPolyData *triangulatedface,vtkFloatArray *facenormals);

	/*! \brief Find jaw landmark
			\param cutpolydata
			\param newlandmarkpoints 
			\param closecells
			\param triangulatedface 
			\param facenormals 
	*/
	void Cene(vtkPolyData *cutpolydata,vtkPoints *newlandmarkpoints,vtkCellLocator *closecells,vtkPolyData *triangulatedface,vtkFloatArray *facenormals);

	/*! \brief Sort mouth points
			\param newlandmarkpoints 
			\param cutpolydata
			\param ncutpoints
	*/
	float **SortCutPointsForMouth(vtkPoints *newlandmarkpoints,vtkPolyData *cutpolydata, int *ptsfound,int *ncutpoints);

	/*! \brief Find mouth center
			\param sortedcutpoints
			\param camera
			\param newlandmarkpoints 
			\param pointsfound
	*/
	int AgizOrta(float **sortedcutpoints,vtkCamera *camera,vtkPoints *newlandmarkpoints,int pointsfound);

	/*! \brief Find left and right corners of mouth
			\param newlandmarkpoints 
			\param mouthplanenormal 
			\param sortedcutpoints
			\param mindistdifid
			\param triangulatedface
			\param numbercutpoints
	*/
	void AgizSagSol(vtkPoints *newlandmarkpoints,float mouthplanenormal[], float **sortedcutpoints,int mindistdifid,vtkPolyData *triangulatedface,int numbercutpoints);

	/*! \brief Cut points for eyes
			\param symmetrynormal 
			\param symmetryposition
			\param eyeplanenormal
			\param eyeplaneposition
			\param triangulatedface
			\param newlandmarkpoints
			\param srightcutpoints
			\param sleftcutpoints
			\param rpointsfound
			\param lpointsfound
			\param ren1
	*/
	int CutPointsForEyes(float symmetrynormal[],float symmetryposition[], float eyeplanenormal[],float eyeplaneposition[],vtkPolyData *triangulatedface,vtkPoints *newlandmarkpoints,float ***srightcutpoints,float ***sleftcutpoints,int *rpointsfound,int *lpointsfound,vtkRenderer *ren1);

	/*! \brief Find left point of right eye
			\param rightpointsfound
			\param sortedrightcutpoints
			\param newlandmarkpoints
	*/
	void RightEyeLeftPoint(int rightpointsfound,float **sortedrightcutpoints,vtkPoints *newlandmarkpoints);

	/*! \brief Find right point of left eye
			\param leftpointsfound
			\param sortedleftcutpoints
			\param newlandmarkpoints
			\param ren1
	*/
	void LeftEyeRightPoint(int leftpointsfound,float **sortedleftcutpoints,vtkPoints *newlandmarkpoints,vtkRenderer *ren1);

	/*! \brief Find left point of left eye
			\param closecells
			\param leftpointsfound
			\param sortedleftcutpoints
			\param  newlandmarkpoints
	*/
	void LeftEyeLeftPoint(vtkCellLocator *closecells,int leftpointsfound,float **sortedleftcutpoints,vtkPoints *newlandmarkpoints);

	/*! \brief Find right point of right eye
			\param closecells
			\param rightpointsfound
			\param sortedrightcutpoints
			\param  newlandmarkpoints
	*/
	void RightEyeRightPoint(vtkCellLocator *closecells,int rightpointsfound,float **sortedrightcutpoints,vtkPoints *newlandmarkpoints);

	//! \brief Landmark positions
	m_point_3d testlandmarks[NUMBERLANDMARKS];

	//! \brief Mean face
	vtkPolyData *meanface;

	char *mean_face_planesDC;
	char *meanfacelandmarksDC;

public:
	int GetCorrectData(char *filename);
	
    //! \brief A void constructor for the class
	AutomaticLandmark(char*, char*, char*, char*, char*);

	//! \brief A void destructor for the class
	virtual ~AutomaticLandmark();

};

#endif // !defined(AFX_AUTOMATICLANDMARK_H__9F91D488_923F_4EE3_9F41_A98A194700FF__INCLUDED_)
