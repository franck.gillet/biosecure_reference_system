/*
 * Copyright (c) 2007 BU
 * Authors Berk Gokberk, Okan Irfanoglu
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// DenseCorrespondence.cpp: implementation of the DenseCorrespondence class.
//
//////////////////////////////////////////////////////////////////////

#include "DenseCorrespondence.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DenseCorrespondence::DenseCorrespondence(char *filenames, char *mean_face, char *meanfacelandmarks, char *alltestlandmarks, char *faces, char *output)
{
	printf("\nDENSE CORREPONDENCE STARTED....\n\n");
	int taille1, taille2;
	int i,j;
	char filename[10],path[20];
	invertedtps=0;
	totalimageread=0;

	FILE *fp= fopen(meanfacelandmarks,"rb");
	if(!fp)
	{
		printf("Could not open file meanfacelandmarks-rigid-11.dat! Exiting\n");
		exit(0);
	}
	for(i=0; i<NUMBERLANDMARKS;i++)
		fread(meanlandmarks+i,sizeof(m_point_3d),1,fp);
	fclose(fp);
	
	taille1 = strlen(filenames) + 1;
	taille2 = strlen(alltestlandmarks) + 1;
	filenamesDC = NULL;
	alltestlandmarksDC = NULL;
	filenamesDC = (char*)malloc(taille1);
	alltestlandmarksDC = (char*)malloc(taille2);

	 
	for(unsigned int i=0;i<taille1;i++) filenamesDC[i]=filenames[i];
	for(unsigned int i=0;i<taille2;i++) alltestlandmarksDC[i]=alltestlandmarks[i];

	
	vtkPolyDataReader *reader = vtkPolyDataReader::New();
	reader->SetFileName(mean_face);    
	vtkPolyData *allpointsface= vtkPolyData::New();
	reader->Update();
	allpointsface->DeepCopy(reader->GetOutput());
	allpointsface->Modified();
	allpointsface->Update();
	reader->Delete();
	
	
	vtkPoints *allpointsfacepoints= allpointsface->GetPoints();
	int nallpointsfacepoints=allpointsfacepoints->GetNumberOfPoints();
	vtkCellArray* newcells= allpointsface->GetPolys();
	
	int numberpeople;
	fp= fopen(filenames,"r");
	if(!fp)
	{
		printf("Could not open file filenames.txt! Exiting\n");
		exit(0);
	}
	fscanf(fp,"%d",&numberpeople);
	
	m_point_3d *pointmaps= (m_point_3d * ) new m_point_3d[nallpointsfacepoints];
    vtkGeneralTransform *inversetps;
	
	
	for(i=0; i< numberpeople;i++)
	{
		int imagesperperson;
		fscanf(fp,"%s",filename);
		fscanf(fp,"%d",&imagesperperson);
		
		for(j=0; j<imagesperperson;j++)
		{
			sprintf(path,"%s/%s%d.vp",faces,filename,j+1);
			printf(" person =%d image=%d\n\n",i,j);
			
			vtkPolyData *testface=GetWarpedFace(path,&inversetps);
			totalimageread++;
			
			vtkPoints *testfacepoints= testface->GetPoints();
			int ntestfacepoints= testfacepoints->GetNumberOfPoints();
			
			vtkCellLocator *closecells= vtkCellLocator::New();
			closecells->CacheCellBoundsOn();
			closecells->SetDataSet(testface);
			closecells->BuildLocator();
			
			float allpointscoords[3];
			int k;
			for(k=0;k<nallpointsfacepoints;k++)
			{
				allpointsfacepoints->GetPoint(k,allpointscoords);				
				
				float closestpoint[3];
				int dummy;
				float dummy3;
				int dummy2;
				//vtkGenericCell *mycell=vtkGenericCell::New();
				//closecells->FindClosestPointWithinRadius  (allpointscoords,0.04,closestpoint,mycell,dummy,dummy2,dummy3)  ;
				closecells->FindClosestPoint  ( allpointscoords, closestpoint,dummy,dummy2,dummy3);  
				
				pointmaps[k].x= closestpoint[0];
				pointmaps[k].y= closestpoint[1];
				pointmaps[k].z= closestpoint[2];
				
				//mycell->Delete();
			}
			BuildAndWriteNewFaces(filename,pointmaps,inversetps,nallpointsfacepoints,newcells,j+1,output);
			closecells->Delete();
			if(invertedtps)
				inversetps->Delete();
			testface->Delete();
		}
	}
	
	fclose(fp);
	delete[] pointmaps;
	printf("\n\nDENSE CORREPONDENCE ENDED....\n\n");
}

DenseCorrespondence::~DenseCorrespondence()
{
free(filenamesDC);free(alltestlandmarksDC);
}


int DenseCorrespondence::GetCorrectData(char *filename,m_point_3d *data)
{
	int i;
	
    FILE *fpface= fopen(filename,"rb");
	if(!fpface)
	{
		printf("Error opening file %s!\n",filename);
		exit(0);
	}
	
    short n;
	int numberpoints=0;
	double meanx=0,meany=0,meanz=0;
    while  ( fread(&n,sizeof(short),1,fpface))
	{
		short temp;
		
		for(i=0; i<n ; i++)
		{
			fread(&temp,sizeof(short),1,fpface);
			data[numberpoints].x=temp;
			meanx+=temp;
			data[numberpoints].x/=1;
			
			fread(&temp,sizeof(short),1,fpface);
			data[numberpoints].y=temp;
			meany+=temp;
			data[numberpoints].y/=1;
			
			fread(&temp,sizeof(short),1,fpface);
			data[numberpoints].z=temp;
			meanz+=temp;
			data[numberpoints].z/=1;
			
			numberpoints++;
		}
	}
    fclose(fpface);
	meanx/=numberpoints;
	meany/=numberpoints;
	meanz/=numberpoints;
	
	for(i=0; i< numberpoints;i++)
	{
		data[i].x-=meanx;
		data[i].y-=meany;
		data[i].z-=meanz;
	}
    double cos45= 0.707106781;
	
	for(i=0; i< numberpoints;i++)
	{
		double tempx,tempy;
		tempx= data[i].x*cos45 - data[i].y*cos45;
		tempy= data[i].x*cos45 + data[i].y*cos45;
		
		data[i].x=tempx;
		data[i].y=tempy;
    }
	
	return numberpoints;
}


void DenseCorrespondence::GetData(m_point_3d *sourcelandmarks)
{
	int offset= totalimageread*NUMBERLANDMARKS*sizeof(m_point_3d);
	
	FILE *fp= fopen(alltestlandmarksDC,"rb");
	if(!fp)
	{
		printf("Could not open file alltestlandmarks.dat! Exiting\n");
		exit(0);
	}
	fseek(fp,offset,SEEK_SET);
	int i;
	for(i=0; i<NUMBERLANDMARKS;i++)
		fread(sourcelandmarks+i,sizeof(m_point_3d),1,fp);
	fclose(fp);
}


void DenseCorrespondence::GetNumbers(int *imgenumber,int *v,char *filename)
{
	*v= filename[7]-'0';
	
	FILE *fp= fopen(filenamesDC,"r");
	if(!fp)
	{
		printf("Could not open file filenames.txt! Exiting\n");
		exit(0);
	}
	
	int n;
	fscanf(fp,"%d",&n);
	char temp[8];
	
	n=0;
	*imgenumber=0;
	while(1)
	{
		if(fscanf(fp,"%s",temp)==EOF)
		{
			fclose(fp);
			break;
		}
		if( strncmp(temp,filename,6)==0)
		{
			fclose(fp);
			break;
		}
		else
			n++;
		
		*imgenumber=n;
	}
}


void DenseCorrespondence::BuildAndWriteNewFaces(char *filename, m_point_3d *pointmaps,vtkGeneralTransform *inversetps,int nallpointsfacepoints,vtkCellArray * newcells,int surname, char *output)
{
	int k;
	
	char path[30];
	
	sprintf(path,"%s/%sv%d.txt",output,filename,surname);
	vtkPolyData *newface = vtkPolyData::New();
	vtkPoints *newpoints = vtkPoints::New();
	
	newpoints->Allocate(nallpointsfacepoints);
	newpoints->SetNumberOfPoints(nallpointsfacepoints);
	
	for(k=0; k<nallpointsfacepoints;k++)
		newpoints->InsertPoint(k,pointmaps[k].x,pointmaps[k].y,pointmaps[k].z);
	
	newface->SetPoints(newpoints);
	newface->SetPolys(newcells);  
	newface->Update();
	
	
	vtkPolyDataWriter *wrt= vtkPolyDataWriter::New();
	wrt->SetFileName(path);
	
	if(invertedtps)
	{
		vtkTransformPolyDataFilter* warp =  vtkTransformPolyDataFilter::New();
        warp->SetInput(newface);
		warp->SetTransform(inversetps);
		warp->Modified();
		warp->Update();
		wrt->SetInput(warp->GetOutput());
		wrt->Write();
		warp->Delete();
		
	}
	else
	{
		wrt->SetInput(newface);
		wrt->Write();
	}
	
	wrt->Delete();
	newface->Delete();
	newpoints->Delete();
}


vtkPolyData *DenseCorrespondence::GetWarpedFace(char *filename,vtkGeneralTransform **inversetps)
{
	int i;
	m_point_3d sourcelandmarks[NUMBERLANDMARKS];
	GetData(sourcelandmarks);
	
	vtkPolyDataReader *rd= vtkPolyDataReader::New();
	rd->SetFileName(filename);
	rd->Update();
	rd->Modified();
	vtkPolyData *myface= vtkPolyData::New();
	myface->DeepCopy(rd->GetOutput());
	myface->Modified();
	myface->Update();
	rd->Delete();
	
    vtkDelaunay2D *del = vtkDelaunay2D::New(); 
    del->SetInput(myface);
    del->SetTolerance(0.001);
    del->Update();
    vtkSmoothPolyDataFilter *filter= vtkSmoothPolyDataFilter::New();
    filter->SetInput(del->GetOutput());
    filter->SetNumberOfIterations(50);
    filter->Update();
	
    int numberl=0;
	bool landmarkmaps[NUMBERLANDMARKS]={1,0,1,0,0,1,1,0,1,1,0};   //hangi landmarklari tps de kullanacagi
	for(i=0;i<NUMBERLANDMARKS;i++)   //0-burun ucu 1-burun tepe 2-solg�z sol 3-sol g�z sa� 4-sa�g�z sol 5-sa� g�z sa�
		if(landmarkmaps[i])          //6 a��z sol 7-a��z sa� 8-cene 9-burun alti 10-agiz orta
			numberl++;
		
		vtkPoints* Sp1 = vtkPoints::New();
		Sp1-> SetNumberOfPoints(numberl);
		int cnt=0;
		for(i=0; i<NUMBERLANDMARKS;i++)
		{
			if(landmarkmaps[i])
			{
				Sp1-> SetPoint( cnt, sourcelandmarks[i].x,sourcelandmarks[i].y,sourcelandmarks[i].z);
				cnt++;
			}
		}  
		vtkPoints* Tp1 = vtkPoints::New();
		Tp1-> SetNumberOfPoints(numberl);
		cnt=0;
		for(i=0; i<NUMBERLANDMARKS;i++)
		{
			if(landmarkmaps[i])
			{
				Tp1-> SetPoint( cnt, meanlandmarks[i].x,meanlandmarks[i].y,meanlandmarks[i].z);
				cnt++;
			}
		}
		
		vtkThinPlateSplineTransform* tpst = vtkThinPlateSplineTransform::New();
		tpst->SetSourceLandmarks(Sp1);
		tpst->SetTargetLandmarks(Tp1);   
		tpst->SetBasisToR();
		
		if(invertedtps)
		{	
			vtkGeneralTransform* tpstconcat = vtkGeneralTransform::New();
			tpstconcat->SetInput(tpst->GetInverse());
			*inversetps = tpstconcat;
		}
        
		
		vtkTransformPolyDataFilter* warp =  vtkTransformPolyDataFilter::New();
		warp->SetInput(filter->GetOutput());
		warp->SetTransform(tpst);
		warp->Modified();
		warp->Update();
		
		vtkPolyData *toreturn = vtkPolyData::New();
		toreturn->DeepCopy(warp->GetOutput());
		
		
		warp->Delete();
		tpst->Delete();
		Tp1->Delete();
		Sp1->Delete();
		filter->Delete();
		del->Delete();
		myface->Delete();
		
		return toreturn;
}

