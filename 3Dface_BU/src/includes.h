/*
 * Copyright (c) 2007 BU
 * Authors Berk Gokberk, Okan Irfanoglu
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if !defined(AFX_INCLUDES_H__9F91D488_923F_4EE3_9F41_A98A194700FF__INCLUDED_)
#define AFX_INCLUDES_H__9F91D488_923F_4EE3_9F41_A98A194700FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vtkDelaunay2D.h> 
#include <vtkExtractEdges.h> 
#include <vtkSmoothPolyDataFilter.h>
#include <vtkSphereSource.h>
#include <vtkTubeFilter.h>
#include <vtkGlyph3D.h>
#include <vtkWindowToImageFilter.h>
#include <vtkBMPWriter.h>
#include <vtkCamera.h>
#include <vtkImageData.h> 
#include <vtkIterativeClosestPointTransform.h> 
#include <vtkTransformPolyDataFilter.h>
#include <vtkLandmarkTransform.h>
#include <vtkCellLocator.h>
#include <vtkGenericCell.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkPolyDataReader.h>
#include <vtkCurvatures.h>
#include <vtkPointLocator.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>
#include <vtkPolyDataNormals.h>
#include <vtkCutter.h>
#include <vtkPlane.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCellArray.h>
#include <vtkGeometryFilter.h>
#include <vtkMaskPoints.h>
#include <vtkGeneralTransform.h>
#include <vtkPolyDataWriter.h>
#include <vtkThinPlateSplineTransform.h>


#include <math.h>
#include <stdio.h>


//! \brief NUMBEROFLANDMARKS
#define NUMBERLANDMARKS 11

//! \brief MAXDATAPOINTS
#define MAXDATAPOINTS 7000

//! \brief 3D point structure
struct m_point_3d {
	double x;
	double y;
	double z;
	
public: double distance(m_point_3d a){	m_point_3d temp; temp.x=  x-a.x; 	temp.y=	 y-a.y; temp.z=	 z-a.z; return temp.dot(temp);};
		double dot(m_point_3d a){return x*a.x+y*a.y+z*a.z;};
		
};


#endif
