/*
 * Copyright (c) 2007 BU
 * Authors Berk Gokberk, Okan Irfanoglu
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "AutomaticLandmark.h"


int main(int argc, char* argv[])
{

	char *filenames, *mean_face, *meanfacelandmarks, *mean_face_planes, *faces;
	filenames = (char *) calloc (50, sizeof(*filenames));
	mean_face = (char *) calloc (50, sizeof(*mean_face));
	meanfacelandmarks = (char *) calloc (50, sizeof(*meanfacelandmarks));
	mean_face_planes = (char *) calloc (50, sizeof(*mean_face_planes));
	faces = (char *) calloc (50, sizeof(*faces));

	if( argc == 1 )
  	{
    	printf( "Usage: %s filenames.txt mean_face.vp meanfacelandmarks.dat mean_face_planes.txt faces\n", argv[0] );
    	return(0);
  	}

	filenames = argv[1];
	mean_face = argv[2];
	meanfacelandmarks = argv[3];
	mean_face_planes = argv[4];
	faces = argv[5];

		AutomaticLandmark * mylandmarking= (AutomaticLandmark *) new AutomaticLandmark(filenames, mean_face, meanfacelandmarks, mean_face_planes, faces);
		if(!mylandmarking)
		{
			printf("Error allocating memory for mylandmarking! Exiting!\n");
			exit(0);
		}
		delete mylandmarking;
	
	return(0);
}
