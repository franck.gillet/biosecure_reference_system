/*
 * Copyright (c) 2007 BU
 * Authors Berk Gokberk, Okan Irfanoglu
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// Recognition.cpp: implementation of the Recognition class.
//
//////////////////////////////////////////////////////////////////////

#include "Recognition.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Recognition::Recognition(char *filenames, char *mean_face, char *test_faces, char *output)
{
	int i,j,k;
	
	int knn=1;
	double ortalama=0;
	
	int *knnclasses= (int *) new int[knn];
	double *knnmins= (double *) new double[knn];

	/// creation of score files
	FILE *scoresVerif_g0, *scoresVerif_g1, *scoresVerif_g2, *scoresVerif_g3, *scoresVerif_g4;
	FILE *scoresVerif_i0, *scoresVerif_i1, *scoresVerif_i2, *scoresVerif_i3, *scoresVerif_i4;
	char *resg0, *resg1, *resg2, *resg3, *resg4;
	char *resi0, *resi1, *resi2, *resi3, *resi4;

	char *sg0="/scoresGenuine_set0.txt";
	char *sg1="/scoresGenuine_set1.txt";
	char *sg2="/scoresGenuine_set2.txt";
	char *sg3="/scoresGenuine_set3.txt";
	char *sg4="/scoresGenuine_set4.txt";
	int size_g = strlen(sg0)+strlen(output)+1;
	char *si0="/scoresImpostor_set0.txt";
	char *si1="/scoresImpostor_set1.txt";
	char *si2="/scoresImpostor_set2.txt";
	char *si3="/scoresImpostor_set3.txt";
	char *si4="/scoresImpostor_set4.txt";
	int size_i = strlen(si0)+strlen(output)+1;
	
	resg0 = (char *) calloc (size_g, sizeof(*resg0));
	strcpy (resg0, output); strcat (resg0, sg0);
	resg1 = (char *) calloc (size_g, sizeof(*resg1));
	strcpy (resg1, output); strcat (resg1, sg1);
	resg2 = (char *) calloc (size_g, sizeof(*resg2));
	strcpy (resg2, output); strcat (resg2, sg2);
	resg3 = (char *) calloc (size_g, sizeof(*resg3));
	strcpy (resg3, output); strcat (resg3, sg3);
	resg4 = (char *) calloc (size_g, sizeof(*resg4));
	strcpy (resg4, output); strcat (resg4, sg4);
	resi0 = (char *) calloc (size_g, sizeof(*resi0));
	strcpy (resi0, output); strcat (resi0, si0);
	resi1 = (char *) calloc (size_g, sizeof(*resi1));
	strcpy (resi1, output); strcat (resi1, si1);
	resi2 = (char *) calloc (size_g, sizeof(*resi2));
	strcpy (resi2, output); strcat (resi2, si2);
	resi3 = (char *) calloc (size_g, sizeof(*resi3));
	strcpy (resi3, output); strcat (resi3, si3);
	resi4 = (char *) calloc (size_g, sizeof(*resi4));
	strcpy (resi4, output); strcat (resi4, si4);

	// OPEN score FILES
  	if (!( scoresVerif_g0 = fopen(resg0, "w") )) {
	fprintf(stderr, "Error: Cannot write scores file \n");}
	if (!( scoresVerif_g1 = fopen(resg1, "w") )) {
	fprintf(stderr, "Error: Cannot write scores file \n");}
	if (!( scoresVerif_g2 = fopen(resg2, "w") )) {
	fprintf(stderr, "Error: Cannot write scores file \n");}
	if (!( scoresVerif_g3 = fopen(resg3, "w") )) {
	fprintf(stderr, "Error: Cannot write scores file \n");}
	if (!( scoresVerif_g4 = fopen(resg4, "w") )) {
	fprintf(stderr, "Error: Cannot write scores file \n");}

	if (!( scoresVerif_i0 = fopen(resi0, "w") )) {
	fprintf(stderr, "Error: Cannot write scores file \n");}
	if (!( scoresVerif_i1 = fopen(resi1, "w") )) {
	fprintf(stderr, "Error: Cannot write scores file \n");}
	if (!( scoresVerif_i2 = fopen(resi2, "w") )) {
	fprintf(stderr, "Error: Cannot write scores file \n");}
	if (!( scoresVerif_i3 = fopen(resi3, "w") )) {
	fprintf(stderr, "Error: Cannot write scores file \n");}
	if (!( scoresVerif_i4 = fopen(resi4, "w") )) {
	fprintf(stderr, "Error: Cannot write scores file \n");}
	///
	printf("\nRECOGNITION STARTED....\n\n");
	
	vtkPolyDataReader *rd = vtkPolyDataReader::New();
	rd->SetFileName(mean_face);
	vtkPolyData *meanface= rd->GetOutput();
	meanface->Modified();
	meanface->Update();
	
	int numberpoints= meanface->GetNumberOfPoints();
	
	vtkFloatArray *scalars =(vtkFloatArray *)meanface->GetPointData()->GetScalars();
	int *regionmaps= (int *) new int[numberpoints];
	
	for(i=0;i<numberpoints;i++)
	{
		regionmaps[i]= scalars->GetValue(i);
	}
	
	
	int trainperperson=1;
	//int trainperperson=4;
	//int testperperson=2;
	bool featuremaps[9]={1,1,1,1,1,1,1,0,1};
	
	
	
    FILE *fp=fopen(filenames,"r");
    if(!fp)
	{
		printf("Could not open file filenames.txt! Exiting\n");
		exit(0);
	}
	
	int numberpeople;
	fscanf(fp,"%d",&numberpeople);
	
	char ***trainFilename;
	char ***testFilename;
	m_point_3d   ***trainset, ***testset;

	trainFilename = (char ***) calloc (numberpeople, sizeof(*trainFilename));
	testFilename = (char ***) calloc (numberpeople, sizeof(*testFilename));
	trainset = (m_point_3d ***) new m_point_3d**[numberpeople];
	testset = (m_point_3d ***) new m_point_3d**[numberpeople];
	
	char dum[10];
	for(i=0;i<numberpeople;i++)
	{
		fscanf(fp,"%s",dum);
		int perperson;
		fscanf(fp,"%d",&perperson);
		
		trainFilename[i] = (char **) calloc (trainperperson, sizeof(**trainFilename));
		testFilename[i] = (char **) calloc (perperson-trainperperson, sizeof(**testFilename));
		trainset[i]=(m_point_3d **) new m_point_3d*[trainperperson];
		testset[i]=(m_point_3d **) new m_point_3d*[perperson -trainperperson];
		
		for(j=0; j< trainperperson;j++)
			{
			trainset[i][j] =(m_point_3d *) new m_point_3d[numberpoints];
			trainFilename[i][j] = (char *) calloc (50, sizeof(***trainFilename));
			}
		for(j=0; j< perperson -trainperperson;j++)
			{
			testset[i][j] =(m_point_3d *) new m_point_3d[numberpoints];
			testFilename[i][j] = (char *) calloc (50, sizeof(***testFilename));
			}
	}



	fclose(fp);
	
	
	char filename[10], path[30];
	//int trainmap[6][3] ={{0,1,2},{0,1,3},{0,1,4},{1,2,3},{1,2,4},{2,3,4}};
	//int testmap[6][2]= {{3,4}, {2,4}, {2,3},{0,4},{0,3},{0,1}};
	
	//int trainmap[5][4] ={{0,1,2,3},{0,1,2,4},{0,1,3,4},{0,2,3,4},{1,2,3,4}};
	//int testmap[5][1]= {4,3,2,1,0};
	
	int testmap[5][4]= {{1,2,3,4},{0,2,3,4},{0,1,3,4},{0,1,2,4},{0,1,2,3}};
	int trainmap[5][1] = {0,1,2,3,4};

	
    int *perpersons= (int *) new int[numberpeople];
	int tcomb;

	for(tcomb=0;tcomb<5;tcomb++) 
	{
		fp=fopen(filenames,"r");
		fscanf(fp,"%d",&numberpeople);
		
		if(tcomb==4)
			int ma=0;
		
		for(i=0;i<numberpeople;i++)
		{
			fscanf(fp,"%s",filename);
			fscanf(fp,"%d",perpersons+i);
			
			
			for(j=0; j<trainperperson;j++)
			{
				sprintf(path,"%s/%sv%d.txt",test_faces,filename,trainmap[tcomb][j]+1);
				vtkPolyDataReader *reader = vtkPolyDataReader::New();
				reader->SetFileName(path);
				vtkPolyData *face= reader->GetOutput();
				face->Modified();
				face->Update();
				
				vtkPoints *facepoints= face->GetPoints();
				
				strcpy(trainFilename[i][j],path);

				float temp[3];			
				for(k=0; k<numberpoints;k++)
				{
					facepoints->GetPoint(k,temp);
					trainset[i][j][k].x=temp[0];
					trainset[i][j][k].y=temp[1];
					trainset[i][j][k].z=temp[2];
				}
				reader->Delete();
			}
			
			for(j=0; j<perpersons[i]-trainperperson;j++)
			{
				int m;
				if(j==6-trainperperson-1)
					m=6;
				else
					m=testmap[tcomb][j]+1;
				
				sprintf(path,"%s/%sv%d.txt",test_faces,filename,m);
				vtkPolyDataReader *reader = vtkPolyDataReader::New();
				reader->SetFileName(path);
				vtkPolyData *face= reader->GetOutput();
				face->Modified();
				face->Update();
				
				vtkPoints *facepoints= face->GetPoints();
				
				strcpy(testFilename[i][j],path);

				float temp[3];
				for(k=0; k<numberpoints;k++)
				{
					facepoints->GetPoint(k,temp);
					testset[i][j][k].x=temp[0];
					testset[i][j][k].y=temp[1];
					testset[i][j][k].z=temp[2];
				}
				reader->Delete();
			}
		}
		fclose(fp);
		
		int correct=0;
		int tests=0;
        for(i=0; i<numberpeople;i++)
		{
			for(j=0; j<perpersons[i]-trainperperson;j++)
			{
				for(k=0;k<knn;k++)
					knnmins[k]=99999999;
				
				tests++;
				
				for(k=0; k<numberpeople;k++)
				{
					int l;
					for(l=0; l<trainperperson;l++)
					{
						double sum[9]={0};
						int m = 0;
						for(m=0; m<numberpoints;m++)
						{
							sum [regionmaps[m]] += sqrt (   (testset[i][j][m].x - trainset[k][l][m].x) *(testset[i][j][m].x - trainset[k][l][m].x) + (testset[i][j][m].y - trainset[k][l][m].y) *(testset[i][j][m].y - trainset[k][l][m].y) + (testset[i][j][m].z - trainset[k][l][m].z) *(testset[i][j][m].z - trainset[k][l][m].z));						
							//sum[regionmaps[m]]  += fabs((testset[i][j][m].x - trainset[k][l][m].x)) + fabs((testset[i][j][m].y - trainset[k][l][m].y)) + fabs((testset[i][j][m].z - trainset[k][l][m].z));
							//sum += sqrt (   (testset[i][j][m].x - trainset[k][l][m].x) *(testset[i][j][m].x - trainset[k][l][m].x) + (testset[i][j][m].y - trainset[k][l][m].y) *(testset[i][j][m].y - trainset[k][l][m].y) + (testset[i][j][m].z - trainset[k][l][m].z) *(testset[i][j][m].z - trainset[k][l][m].z));						
							//sum+= fabs((testset[i][j][m].x - trainset[k][l][m].x)) + fabs((testset[i][j][m].y - trainset[k][l][m].y)) + fabs((testset[i][j][m].z - trainset[k][l][m].z));
						}
						
						double totaldist=0;
						
						for(m=0;m<9;m++)
							totaldist+= sum[m]*featuremaps[m];
						
						if (tcomb==0)
						{
							if (k==i)
							{
							fprintf (scoresVerif_g0, "train: %s\t test: %s\t %lf\n", trainFilename[k][l], testFilename[i][j], totaldist);
							}
							else
							{
							fprintf (scoresVerif_i0, "train: %s\t test: %s\t %lf\n", trainFilename[k][l], testFilename[i][j], totaldist);
							}
						}
						if (tcomb==1)
						{
							if (k==i)
							{
							fprintf (scoresVerif_g1, "train: %s\t test: %s\t %lf\n", trainFilename[k][l], testFilename[i][j], totaldist);
							}
							else
							{
							fprintf (scoresVerif_i1, "train: %s\t test: %s\t %lf\n", trainFilename[k][l], testFilename[i][j], totaldist);
							}
						}
						if (tcomb==2)
						{
							if (k==i)
							{
							fprintf (scoresVerif_g2, "train: %s\t test: %s\t %lf\n", trainFilename[k][l], testFilename[i][j], totaldist);
							}
							else
							{
							fprintf (scoresVerif_i2, "train: %s\t test: %s\t %lf\n", trainFilename[k][l], testFilename[i][j], totaldist);
							}
						}
						if (tcomb==3)
						{
							if (k==i)
							{
							fprintf (scoresVerif_g3, "train: %s\t test: %s\t %lf\n", trainFilename[k][l], testFilename[i][j], totaldist);
							}
							else
							{
							fprintf (scoresVerif_i3, "train: %s\t test: %s\t %lf\n", trainFilename[k][l], testFilename[i][j], totaldist);
							}
						}
						if (tcomb==4)
						{
							if (k==i)
							{
							fprintf (scoresVerif_g4, "train: %s\t test: %s\t %lf\n", trainFilename[k][l], testFilename[i][j], totaldist);
							}
							else
							{
							fprintf (scoresVerif_i4, "train: %s\t test: %s\t %lf\n", trainFilename[k][l], testFilename[i][j], totaldist);
							}
						}


						int minplace=-1;
						for(m=0;m<knn;m++)
						{
							if(totaldist<knnmins[m])
							{
								minplace=m;
								break;
							}
						}
						
						if(minplace!=-1)
						{
							int towritedist=totaldist;
							int towriteclass=k;
							
							for(m=minplace;m<knn;m++)
							{
								int tempclass= knnclasses[m];
								double tempdist=knnmins[m];
								
								knnclasses[m]=towriteclass;
								knnmins[m]=towritedist;
								
								towriteclass= tempclass;
								towritedist=tempdist;
							}
						}
					}
				}
				
				int *mins=(int *) new int[knn];
				for(k=0;k<knn;k++)
					mins[k]=1;
				
				for(k=0;k<knn-1;k++)
				{
					int l;
					for(l=k+1;l<knn;l++)
						if(knnclasses[l]==knnclasses[k])
							mins[k]++;
				}
				
				int minindex=0;
				int max=-1;
				for(k=0;k<knn;k++)
				{
					if(mins[k]>max)
					{
						max=mins[k];
						minindex=k;
					}
				}
				
				
				if(minindex!=0)
				{
					if(((knnmins[1]-knnmins[0])/knnmins[0])>0.02)
						minindex=0;
				}
				
				
				if(knnclasses[minindex]==i)
					correct++;
				delete[] mins;
				
			}
		}
		
		double perc = 1.0* correct/tests;
		ortalama+=perc;
		printf("\nCorrect Identification for set %d:  %lf\n",tcomb,perc);
		
	}
	printf("\n\nAverage: %lf\n",ortalama/5);
	
	for(i=0;i<numberpeople;i++)
	{
		for(j=0; j< trainperperson;j++)
		{
			delete[] trainset[i][j] ;
			trainset[i][j]=NULL;
		}
        for(j=0; j< perpersons[i]-trainperperson;j++)
		{
			delete[] testset[i][j];
			testset[i][j]=NULL;
		}
		
		delete[] trainset[i];
		trainset[i]=NULL;
		delete[] testset[i];
		testset[i]=NULL;
	}
	delete[] trainset;
	trainset=NULL;
	delete[] testset;
	testset=NULL;
	
	rd->Delete();
	delete[] regionmaps;
	
}

Recognition::~Recognition()
{
	
}
