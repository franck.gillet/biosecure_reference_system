#!/bin/bash

LD_LIBRARY_PATH=/home/VTK-4.2.1/bin:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

./automaticLandmark filenames.txt mean_face_rigid-az_nokta.vp meanfacelandmarks-rigid-11.dat mean_face_planes.txt faces

./denseCorrespondence filenames.txt mean_face_rigid-az_nokta.vp meanfacelandmarks-rigid-11.dat alltestlandmarks.dat faces test_faces

./recognition filenames.txt mean_face_rigid-az_nokta.vp test_faces scores
