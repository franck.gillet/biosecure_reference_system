/*
 * Copyright (c) 2007 BU
 * Authors Berk Gokberk, Okan Irfanoglu
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#if !defined(AFX_DENSECORRESPONDENCE_H__B857875D_3634_4DD2_A17F_338C96CCCE39__INCLUDED_)
#define AFX_DENSECORRESPONDENCE_H__B857875D_3634_4DD2_A17F_338C96CCCE39__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "includes.h"

/*!
  \defgroup DenseCorrespondence Automatic dense correspondence establishment
*/

/*! 
  \ingroup DenseCorrespondence
	\brief Finds dense correspondence of each face to the mean face using Thin Plate Spline warpnig algorithm

  Example:
	\code
        DenseCorrespondence *mycorrespondence= (DenseCorrespondence*) new DenseCorrespondence;
	    if(!mycorrespondence)
		{
		    printf("Error allocating memory for mycorrespondence! Exiting!\n");
		    exit(0);
		}
	    delete mycorrespondence;
		\endcode 
*/


class DenseCorrespondence  
{
	m_point_3d meanlandmarks[NUMBERLANDMARKS];

    int GetCorrectData(char *filename,m_point_3d *data);
	
	vtkPolyData *GetWarpedFace(char *filename,vtkGeneralTransform **inversetps);
	
	void GetNumbers(int *imgenumber,int *v,char *filename);
	
	void GetData(m_point_3d *sourcelandmarks);
	
	void BuildAndWriteNewFaces(char *filename, m_point_3d *pointmaps,vtkGeneralTransform *inversetps,int nallpointsfacepoints,vtkCellArray * newcells,int surname, char *output);

	bool invertedtps;
	
	int totalimageread;
	
	char *filenamesDC;
	char *alltestlandmarksDC;
		

public:
	
	//! \brief A void constructor for the class
	DenseCorrespondence(char*, char*, char*, char*, char*, char*);
	//! \brief A void destructor for the class
	virtual ~DenseCorrespondence();

};

#endif // !defined(AFX_DENSECORRESPONDENCE_H__B857875D_3634_4DD2_A17F_338C96CCCE39__INCLUDED_)
