/*
 * Copyright (c) 2007 BU
 * Authors Berk Gokberk, Okan Irfanoglu
 *
 * This file is part of the BioSecure - BU reference system
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "Recognition.h"


int main( int argc, char* argv[] )
{
	
	char *filenames, *mean_face, *test_faces, *output_folder;
	filenames = (char *) calloc (50, sizeof(*filenames));
	mean_face = (char *) calloc (50, sizeof(*mean_face));
	test_faces = (char *) calloc (50, sizeof(*test_faces));
	output_folder = (char *) calloc (50, sizeof(*output_folder));

	if( argc == 1 )
  	{
    	printf( "Usage: %s filenames.txt mean_face.vp test_faces output\n", argv[0] );
    	return(0);
  	}

	filenames = argv[1];
	mean_face = argv[2];
	test_faces = argv[3];
	output_folder = argv[4];
	
		Recognition *myrecognizer= (Recognition *) new Recognition(filenames, mean_face, test_faces, output_folder);
		if(!myrecognizer)
		{
			printf("Error allocating memory for myrecognizer! Exiting!\n");
			exit(0);
		}
		delete myrecognizer;

	return(0);
}
