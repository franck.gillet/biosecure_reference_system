// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef __VERIFICATION_HH__
#define __VERIFICATION_HH__

#include	<iostream>
#include	<list>
#include	<math.h>

#include	"filedb.hh"
#include	"result.hh"

#include	"opts.hh"

#include	"permut.hh"
#include	"dist.hh"

extern int permcard[7];
extern int permsize[7];
extern int enrollset2[2][1];
extern int enrollset3[6][2];
extern int enrollset4[14][3];
extern int enrollset5[30][4];
extern int enrollset6[62][5];

struct s_cmp {
  float		dist;
  unsigned	idx;
  std::string	name;

  s_cmp(const float&		dist,
	const unsigned&		idx,
	const std::string&	name)
    : dist (dist), idx (idx), name (name)
  {
    ;
  };

  bool
  operator<(const struct s_cmp&	s)
  {
    if (dist <= s.dist)
	return true;
    return false;
  }

};

template < class MyExtractor >
static inline
void
OnetoOnedist(Filedb< MyExtractor >&	db,
	     distfunc			dist)
{
  for (unsigned user1 = 0 ; user1 < db.population; ++user1)
    if (db[user1][0].hasfeatures)
      for (unsigned user2 = user1 + 1; user2 < db.population; ++user2)
	if (db[user2][0].hasfeatures)
	  std::cout << shortname2(db[user1][0].name) << ':'
		    << shortname2(db[user2][0].name) << ':'
		    << dist(db[user1][0].feat, db[user2][0].feat) << std::endl;
}


template< class MyExtractor >
static inline
std::list< struct s_cmp >*
getdist(const unsigned&		user,
	const unsigned&		session,
	Filedb< MyExtractor >&	db,
	distfunc&		dist,
	int*			enrollset,
	unsigned		enrollsetsize,
	std::vector< float >&	intra,
	std::vector< float >&	inter)
{
  std::list< struct s_cmp >*	cmplist = new std::list< struct s_cmp >;

  for (unsigned user2 = 0 ; user2 < db.population; ++user2)
    for (unsigned i = 0; i < enrollsetsize; ++i)
      {
	if (enrollset[i] < 0)
	  continue;
	unsigned session2 = unsigned(enrollset[i]);
	if (session2 < db[user2].nbsession && db[user2][session2].hasfeatures)
	  {
// 	    std::cout << "\t\t" << db[user].idx + 1 << " " << session  + 1 << "/"
// 		      << db[user].nbsession << " vs " << db[user2].idx + 1 << " "
// 		      << session2 + 1 << "/" << db[user2].nbsession << std::endl;
	    float Dist = dist(db[user][session].feat, db[user2][session2].feat);
	    cmplist->push_back(s_cmp(Dist, db[user2].idx,
				     shortname2(db[user2][session2].name)));
	    if (user == db[user2].idx)
	      intra.push_back(Dist);
	    else
	      inter.push_back(Dist);
	  }
      }
  cmplist->sort();
  return cmplist;
}

template< class MyExtractor >
static inline
std::list< struct s_cmp >*
getdist2(const unsigned&		user,
	 const unsigned&		session,
	 Filedb< MyExtractor >&	db,
	 distfunc&		dist,
	 int*			enrollset,
	 unsigned		enrollsetsize,
	 std::vector< float >&	intra,
	 std::vector< float >&	inter)
{
  std::list< struct s_cmp >*	cmplist = new std::list< struct s_cmp >;
  std::list< struct s_cmp >	tmplist;

  for (unsigned user2 = 0 ; user2 < db.population; ++user2)
    {
      tmplist.clear();
      for (unsigned i = 0; i < enrollsetsize; ++i)
	{
	  if (enrollset[i] < 0)
	    continue;
	  unsigned session2 = unsigned(enrollset[i]);
	  if (session2 < db[user2].nbsession && db[user2][session2].hasfeatures)
	    {
// 	      std::cout << "\t\t" << db[user].idx + 1 << " "
// 			<< session  + 1 << "/" << db[user].nbsession
// 			<< " vs " << db[user2].idx + 1 << " "
// 			<< session2 + 1 << "/" << db[user2].nbsession
// 			<< std::endl;
	      float Dist = dist(db[user][session].feat,
				db[user2][session2].feat);
	        std::cout <<  "test:\t" << shortname2(db[user][session].name) << "\t" << shortname2(db[user2][session2].name) << "\t"  << "dist:\t" << Dist << "\n" ;
	      if (!Dist)
		std::cout << "0 between " << shortname2(db[user][session].name)
			  << " and " << shortname2(db[user2][session2].name) << std::endl;
//	      std::cout << "push: " << Dist << std::endl;
	      tmplist.push_back(s_cmp(Dist, db[user2].idx,
				      shortname2(db[user2][session2].name)));
	    }
	}
      if (tmplist.size() > 0)
	{
	  tmplist.sort();
// 	  std::list< struct s_cmp >::iterator ite;
// 	  std::cout << "\tdists:";
// 	  for (ite = tmplist.begin(); ite != tmplist.end(); ++ite)
// 	    std::cout << " " << ite->dist;
// 	  std::cout << std::endl;
	  s_cmp closest = tmplist.front();
	  cmplist->push_back(closest);
	  if (user == closest.idx)
	    intra.push_back(closest.dist);
	  else
	    inter.push_back(closest.dist);
	}
    }

  cmplist->sort();
  return cmplist;
}


static inline
void
printResult(const unsigned&	cc,
	    const unsigned&	ci,
	    const unsigned&	invalid,
	    const unsigned&	missed,
	    const float&	IP,
	    const float&	VP,
	    const std::string&	name)
{
  std::cout << std::endl;
  std::cout << "----------------------------------- " << name << std::endl;
  std::cout << "Nb of client/client   cmp: " << cc << std::endl;
  std::cout << "Nb of client/impostor cmp: " << ci << std::endl;
  std::cout << "Invalid pictures in test set: " << invalid << std::endl;
  std::cout << " Missed pictures in test set: " << missed << std::endl;
  std::cout << std::endl;
  std::cout << "Recognition Rate :        " << IP << "%" << std::endl;
  std::cout << "Verification Performance: " << VP << "%" << std::endl;
}

unsigned
getRank(unsigned			user,
	std::list< struct s_cmp >*	dists)
{
  // dists is already sort
  std::list< struct s_cmp >::iterator		ite;
  unsigned i = 0;
  for (ite = dists->begin(); ite != dists->end(); ++ite, ++i)
    if (ite->idx == user)
      break;
  return i;
}


template < class MyExtractor  >
void
verification(Filedb< MyExtractor >&	db,
	     const s_options&		opts,
	     distfunc			dist)
{
  std::cout << "First to test: " << opts.firstToTest << std::endl;
  std::vector< float >		intra;
  std::vector< float >		inter;
  unsigned			invalid = 0;
  unsigned			missed = 0;
  unsigned			success = 0;
  unsigned			nbcmp = 0;
  float				IP = 0.f;
  std::list< struct s_cmp >*	dists;
  int*				enrollset = new int[opts.nbToEnroll];
  for (int i = 0; i < (int)opts.nbToEnroll; ++i)
    enrollset[i] = i;

  unsigned*				ranks = new unsigned[db.population];
  for (unsigned i = 0; i < db.population; ++i)
    ranks[i] = 0;

  // for all users in database
  for (unsigned user = 0 ; user < db.population; ++user)
    // for all image in test set (start at firstToTest to end)
    {
      for (unsigned session = opts.firstToTest; session < db[user].nbsession; ++session)
       	// if valid entry
	{
	  nbcmp++;
	  //std::cout << "User: " << user + 1 << " session: " << session + 1 << std::endl;
	  if (db[user].validSet(session, opts.nbToEnroll, enrollset))
	    {
	      dists = getdist2< MyExtractor >(user, session, db, dist,
					      enrollset, opts.nbToEnroll, intra, inter);
	      struct s_cmp& closest = dists->front();
	      if (user == closest.idx)
		success++;
	      else
		{
		  missed++;
// 		  std::cout << "\t" << shortname2(db[user][session].name)
// 			    << " is missed!" << std::endl;
		  std::cout << "\t" << shortname2(db[user][session].name)
			    << " is missed! (closest: " << closest.name << " enroll-set:";
		  for (unsigned i = 0; i < opts.nbToEnroll; ++i)
		    std::cout << ' ' << shortname2(db[user][i].name);
		  std::cout << ')' << std::endl;

		}
	      ranks[getRank(user, dists)]++;
	    }
	  else
	    invalid++;
	}
    }

  std::cout << "success: " << success << " taille: " << nbcmp << std::endl;
  IP = float(success) * (100.f / nbcmp);

  std::cout << std::endl;
  std::cout << "Nb of client/client   cmp: " << intra.size() << std::endl;
  std::cout << "Nb of client/impostor cmp: " << inter.size() << std::endl;
  std::cout << "Invalid pictures in test set: " << invalid << std::endl;
  std::cout << " Missed pictures in test set: " << missed << std::endl;
  std::cout << std::endl;

  std::cout << "Recognition Rate(identification performance): " << IP << "%" << std::endl;
  float VP = generateResult(intra, inter, ranks, opts.resultPrefix, db.population);
  std::cout << "Verification Performance:                     " << VP << "%" << std::endl;
}

template < class MyExtractor >
void
crossValidation(Filedb< MyExtractor >&	db,
		const s_options&	opts,
		distfunc		dist)
{
  std::vector< float >		intra;
  std::vector< float >		inter;
  unsigned			invalid = 0;
  unsigned			missed = 0;
  unsigned			success = 0;
  float				IP = 0.f;
  std::list< struct s_cmp >*	dists;
  unsigned			nbcmp = 0;

  unsigned*				ranks = new unsigned[db.population];
  for (unsigned i = 0; i < db.population; ++i)
    ranks[i] = 0;

  // for all users in database
  for (unsigned user = 0 ; user < db.population; ++user)
    // for all picture of this user
    {
      for (unsigned session = 0; session < db[user].nbsession; ++session)
	{
	  //std::cout << "User: " << user << " session: " << session << std::endl;
	  std::vector< int* >* enrollset = getperm(db[user].nbsession, opts.nbToEnroll);
	  for (unsigned permutation = 0; permutation < enrollset->size(); ++permutation)
	    if (containnot(session, (*enrollset)[permutation], opts.nbToEnroll))
	      {
		nbcmp++;
		//		std::cout << "\tPermut: " << std::endl;
		if (db[user].validSet(session, opts.nbToEnroll, (*enrollset)[permutation]))
		  {
		    dists = getdist2< MyExtractor >(user, session, db, dist,
						   (*enrollset)[permutation],
						   opts.nbToEnroll, intra, inter);
		    struct s_cmp& closest = dists->front();
		    if (user == closest.idx)
		      success++;
		    else
		      {
			missed++;
			std::cout << "\t" << shortname2(db[user][session].name)
				  << " is missed! (closest: " << closest.name << " enroll-set:";
			for (unsigned i = 0; i < opts.nbToEnroll; ++i)
			  if ((*enrollset)[permutation][i] < (int)db[user].nbsession)
			    std::cout << ' ' << shortname2(db[user][(*enrollset)[permutation][i]].name);
			std::cout << ')' << std::endl;
		      }
		    ranks[getRank(user, dists)]++;
		  }
		else
		  invalid++;
	      }
// 	    else
// 	      std::cout << " in permutation " << std::endl;
	}
    }

  IP = float(success) * (100.f / nbcmp);
  std::cout << std::endl;
  std::cout << "Nb of client/client   cmp: " << intra.size() << std::endl;
  std::cout << "Nb of client/impostor cmp: " << inter.size() << std::endl;
  std::cout << "Invalid pictures in test set: " << invalid << std::endl;
  std::cout << " Missed pictures in test set: " << missed << std::endl;
  std::cout << std::endl;
  std::cout << "Recognition Rate:         " << IP << "%" << std::endl;
  float VP = generateResult(intra, inter, ranks, opts.resultPrefix, db.population);
  std::cout << "Verification Performance: " << VP << "%" << std::endl;
}



template < class MyExtractor >
void
compareSet(Filedb< MyExtractor >&	db,
	   const s_options&		opts,
	   distfunc			dist)
{
  std::vector< float >		intra;
  std::vector< float >		inter;
  unsigned			invalid = 0;
  unsigned			missed = 0;
  unsigned			success = 0;
  float				IP = 0.f;
  std::list< struct s_cmp >*	dists;
  unsigned			nbcmp = 0;

  unsigned			limit = opts.limit;
  unsigned*			ranks = new unsigned[db.population];
  for (unsigned i = 0; i < db.population; ++i)
    ranks[i] = 0;

  // for all users in database
  for (unsigned user = 0 ; user < db.population; ++user)
    // for all picture of this user
    {
      for (unsigned session = limit; session < db[user].nbsession; ++session)
	{
	  std::vector< int* >* enrollset = getperm(limit, opts.nbToEnroll);
// 	  std::cout << "User: " << user << " session: " << session << " nb perm: " 
// 		    << enrollset->size() << std::endl;
	  for (unsigned permutation = 0; permutation < enrollset->size(); ++permutation)
	    {
// 	      for (unsigned i = 0; i < opts.nbToEnroll; ++i)
// 		std::cout << " " << (*enrollset)[permutation][i];
// 	      std::cout << std::endl;
	      if (containnot(session, (*enrollset)[permutation], opts.nbToEnroll))
		{
		  nbcmp++;
// 		  std::cout << "\tvs:";
// 		  for (unsigned i = 0; i < opts.nbToEnroll; ++i)
// 		    std::cout << " " << (*enrollset)[permutation][i];
// 		  std::cout << std::endl;
		  if (db[user].validSet(session, opts.nbToEnroll, (*enrollset)[permutation]))
		    {
		      dists = getdist2< MyExtractor >(user, session, db, dist,
						      (*enrollset)[permutation],
						      opts.nbToEnroll, intra, inter);
		      struct s_cmp& closest = dists->front();
		      if (user == closest.idx)
			success++;
		      else
			{
			  missed++;
			  std::cout << "\t" << shortname2(db[user][session].name)
				    << " is missed! (closest: " << closest.name << " enroll-set:";
			  for (unsigned i = 0; i < opts.nbToEnroll; ++i)
			    if ((*enrollset)[permutation][i] < (int)db[user].nbsession)
			      std::cout << ' ' << shortname2(db[user][(*enrollset)[permutation][i]].name);
			  std::cout << ')' << std::endl;
			}
		      ranks[getRank(user, dists)]++;
		    }
		  else
		    invalid++;
		}
// 	      else
// 		std::cout << " in permutation " << std::endl;
	    }
	}
    }

  IP = float(success) * (100.f / nbcmp);
  std::cout << std::endl;
  std::cout << "Nb of client/client   cmp: " << intra.size() << std::endl;
  std::cout << "Nb of client/impostor cmp: " << inter.size() << std::endl;
  std::cout << "Invalid pictures in test set: " << invalid << std::endl;
  std::cout << " Missed pictures in test set: " << missed << std::endl;
  std::cout << std::endl;
  std::cout << "Recognition Rate:         " << IP << "%" << std::endl;
  float VP = generateResult(intra, inter, ranks, opts.resultPrefix, db.population);
  std::cout << "Verification Performance: " << VP << "%" << std::endl;
}



template < class MyExtractor >
void
compare(Filedb< MyExtractor >&	db,
	const s_options&	opts,
	distfunc&		dist)
{
  unsigned			limit = opts.limit;

  enum e_type {bellow, above, mixte, versus};
  std::vector< float >		intra[4];
  std::vector< float >		inter[4];
  unsigned			invalid[4] = {0, 0, 0, 0};
  unsigned			missed[4] = {0, 0, 0, 0};
  unsigned			success[4] = {0, 0, 0, 0};
  unsigned			nbcmp[4] = {0, 0, 0, 0};
  std::string			strtype[4] = {"set1", "set2", "all","Set1VsSet2"};
  std::list< struct s_cmp >*	dists;

  unsigned*				ranks = new unsigned[db.population];
  for (unsigned i = 0; i < db.population; ++i)
    ranks[i] = 0;

  int*				staticenrollset = new int[limit];
  for (int i = 0; i < (int)limit; ++i)
    staticenrollset[i] = i;

  // for all users in database
  for (unsigned user = 0 ; user < db.population; ++user)
    // for all picture of this user
    {
      for (unsigned session = 0; (session < limit) && (session < db[user].nbsession); ++session)
	{
// 	  std::cout << "User: " << user << " (" << shortname2(db[user][session].name)
// 		    << ") session: " << session << std::endl;
	  std::vector< int* >* enrollset = getperm(db[user].nbsession, opts.nbToEnroll);
	  for (unsigned permutation = 0; permutation < enrollset->size(); ++permutation)
	    if (containnot(session, (*enrollset)[permutation], opts.nbToEnroll))
	      {
		unsigned type = getPermType(limit, (*enrollset)[permutation], opts.nbToEnroll);
		nbcmp[type]++;
		//		std::cout << "\tPermut: " << std::endl;
		if (db[user].validSet(session, opts.nbToEnroll, (*enrollset)[permutation]))
		  {
		    dists = getdist2< MyExtractor >(user, session, db, dist,
						   (*enrollset)[permutation],
						   opts.nbToEnroll, intra[type], inter[type]);
		    struct s_cmp& closest = dists->front();

		    if (user == closest.idx)
		      success[type]++;
		    else
		      {
			missed[type]++;
			std::cout << "\t" << shortname2(db[user][session].name)
				  << " is missed! ( " << type << " closest: "
				  << closest.name << " enroll-set:";
			for (unsigned i = 0; i < opts.nbToEnroll; ++i)
			  std::cout << ' ' << shortname2(db[user][(*enrollset)[permutation][i]].name);
			std::cout << ')' << std::endl;
		      }
		  }
		else
		  invalid[type]++;
	      }
// 	    else
// 	      std::cout << " in permutation " << std::endl;
	  delete enrollset;
	}
      for (unsigned session = limit; session < db[user].nbsession; ++session)
	// if valid entry
	{
	  nbcmp[versus]++;
	  if (db[user].valid(session, limit))
	    {
	      dists = getdist2< MyExtractor >(user, session, db, dist, staticenrollset,
					     limit, intra[versus], inter[versus]);
	      struct s_cmp& closest = dists->front();
	      if (user == closest.idx)
		success[versus]++;
	      else
		{
		  missed[versus]++;
		  std::cout << "\t" << shortname2(db[user][session].name)
			    << " is missed! ( " << versus << " closest: "
			    << closest.name << " enroll-set:";
		  for (unsigned i = 0; i < limit; ++i)
		    std::cout << ' ' << shortname2(db[user][i].name);
		  std::cout << ')' << std::endl;
		}
	    }
	  else
	    invalid[versus]++;
	}
    }

  success[mixte] += success[bellow];
  missed[mixte]  += missed[bellow];
  invalid[mixte] += invalid[bellow];
  nbcmp[mixte]   += nbcmp[bellow];
  intra[mixte].insert(intra[mixte].end(), intra[bellow].begin(), intra[bellow].end());
  inter[mixte].insert(inter[mixte].end(), inter[bellow].begin(), inter[bellow].end());

  float	IP;
  float	VP;

  for (unsigned i = 0 ; i < 4 ; ++i)
    if (nbcmp[i])
      {
	IP = float(success[i] * (100.f / nbcmp[i]));
	VP = generateResult(intra[i], inter[i], ranks, opts.resultPrefix + strtype[i] + "-", 
			    db.population);
	printResult(intra[i].size(), inter[i].size(), invalid[i],
		    missed[i], IP, VP, strtype[i]);
      }
}

template < class MyExtractor >
void
twohands(Filedb< MyExtractor >&	db,
	 Filedb< MyExtractor >&	db2,
	 const s_options&	opts,
	 distfunc		dist)
{
  std::cout << "two hands evaluation" << std::endl;

  std::vector< float >		intra;
  std::vector< float >		inter;
  unsigned			invalid = 0;
  unsigned			missed = 0;
  unsigned			success = 0;
  float				IP = 0.f;
  std::list< struct s_cmp >*	dists;
  std::list< struct s_cmp >*	dists2;
  unsigned			nbcmp = 0;

  unsigned*				ranks = new unsigned[db.population];
  for (unsigned i = 0; i < db.population; ++i)
    ranks[i] = 0;

  // for all users in database
  for (unsigned user = 0 ; user < db.population; ++user)
    // for all picture of this user
    {
      for (unsigned session = 0; session < db[user].nbsession; ++session)
	{
	  //std::cout << "User: " << user << " session: " << session << std::endl;
	  std::vector< int* >* enrollset = getperm(db[user].nbsession, opts.nbToEnroll);
	  for (unsigned permutation = 0; permutation < enrollset->size(); ++permutation)
	    if (containnot(session, (*enrollset)[permutation], opts.nbToEnroll))
	      {
		nbcmp++;
		//		std::cout << "\tPermut: " << std::endl;
		if (db[user].validSet(session, opts.nbToEnroll, (*enrollset)[permutation]))
		  {
		    dists = getdist2< MyExtractor >(user, session, db, dist,
						    (*enrollset)[permutation],
						    opts.nbToEnroll, intra, inter);
		    if (db2[user].validSet(session, opts.nbToEnroll, (*enrollset)[permutation]))
		      {
			dists2 = getdist2< MyExtractor >(user, session, db2, dist,
							 (*enrollset)[permutation],
							 opts.nbToEnroll, intra, inter);
			//struct s_cmp& closest1 = dists->front();
			
			//std::cout << "\t" << shortname2(db[user][session].name) << " " << closest1.dist << " (" << dists->size() << ") ";
			dists->insert(dists->end(), dists2->begin(), dists2->end());
			dists->sort();
			//struct s_cmp& closest2 = dists->front();
			//std::cout << closest2.dist << " (" << dists->size() << ") " << std::endl; 
		      }
		    struct s_cmp& closest = dists->front();
		    if (user == closest.idx)
		      success++;
		    else
		      {
			missed++;
			std::cout << "\t" << shortname2(db[user][session].name)
				  << " is missed! (closest: " << closest.name << " enroll-set:";
			for (unsigned i = 0; i < opts.nbToEnroll; ++i)
			  if ((*enrollset)[permutation][i] < (int)db[user].nbsession)
			    std::cout << ' ' << shortname2(db[user][(*enrollset)[permutation][i]].name);
			std::cout << ')' << std::endl;
		      }
		    ranks[getRank(user, dists)]++;
		  }
		else
		  invalid++;
	      }
// 	    else
// 	      std::cout << " in permutation " << std::endl;
	}
    }

  IP = float(success) * (100.f / nbcmp);
  std::cout << std::endl;
  std::cout << "Nb of client/client   cmp: " << intra.size() << std::endl;
  std::cout << "Nb of client/impostor cmp: " << inter.size() << std::endl;
  std::cout << "Invalid pictures in test set: " << invalid << std::endl;
  std::cout << " Missed pictures in test set: " << missed << std::endl;
  std::cout << std::endl;
  std::cout << "Recognition Rate:         " << IP << "%" << std::endl;
  float VP = generateResult(intra, inter, ranks, opts.resultPrefix, db.population);
  std::cout << "Verification Performance: " << VP << "%" << std::endl;
}


template < class Extractor >
void
evaluation(Filedb< Extractor >&		pictureSet,
	   const struct s_options&	opts)
{
  distfunc	dist = selectDist(opts.progname);

  if (opts.onetoone)
    {
      OnetoOnedist<  Extractor >(pictureSet, dist);
      return ;
    }

  switch (opts.mode)
    {
    case COMPARE  : {
      compareSet< Extractor >(pictureSet, opts, dist); break; }
    case VERIF    : {
      verification< Extractor >(pictureSet, opts, dist); break; }
    case MERGE : { 
      crossValidation< Extractor >(pictureSet, opts, dist); break; }
    case EVAL : {
      crossValidation< Extractor >(pictureSet, opts, dist); break; }
    default: {
      std::cerr << "can't recognize mode: " << opts.mode << std::endl;
      exit(-1);
    }
    }
}

template < class Extractor >
void
mergeSet(Filedb< Extractor >&		pictureSet,
	 Filedb< Extractor >&		pictureSet2)
{
  std::cout << "merging features..." << std::endl;
  assert(pictureSet.population == pictureSet2.population);

  for (unsigned user = 0; user < pictureSet.population; ++user)
    for (unsigned session = 0; session < pictureSet[user].nbsession; ++session)
      if (session < pictureSet2[user].nbsession)
	mergeFeatures(pictureSet[user][session].feat, pictureSet2[user][session].feat);
}

template < class Extractor >
void
evaluation(Filedb< Extractor >&		pictureSet,
	   Filedb< Extractor >&		pictureSet2,
	   const struct s_options&	opts)
{
  if (opts.mode == MERGE)
    {
      mergeSet(pictureSet, pictureSet2);
      evaluation< Extractor >(pictureSet, opts);
    }
  else
    {
      distfunc	dist = selectDist(opts.progname);
      twohands< Extractor >(pictureSet, pictureSet2, opts, dist);
    }
}


#endif
