// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// MATLAB Compiler: 4.3 (R14SP3)
// Arguments: "-B" "macro_default" "-W" "cpplib:libbuhand" "-T" "link:lib"
// "preparehand.m" "normalizehand.m" "experimentshands.m" 
//

#ifndef __libbuhand_h
#define __libbuhand_h 1

#if defined(__cplusplus) && !defined(mclmcr_h) && defined(__linux__)
#  pragma implementation "mclmcr.h"
#endif
#include "mclmcr.h"
#include "mclcppclass.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_libbuhand
#define PUBLIC_libbuhand_C_API __global
#else
#define PUBLIC_libbuhand_C_API /* No import statement needed. */
#endif

#define LIB_libbuhand_C_API PUBLIC_libbuhand_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_libbuhand
#define PUBLIC_libbuhand_C_API __declspec(dllexport)
#else
#define PUBLIC_libbuhand_C_API __declspec(dllimport)
#endif

#define LIB_libbuhand_C_API PUBLIC_libbuhand_C_API


#else

#define LIB_libbuhand_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_libbuhand_C_API 
#define LIB_libbuhand_C_API /* No special import/export declaration */
#endif

extern LIB_libbuhand_C_API 
bool libbuhandInitializeWithHandlers(mclOutputHandlerFcn error_handler,
                                     mclOutputHandlerFcn print_handler);

extern LIB_libbuhand_C_API 
bool libbuhandInitialize(void);

extern LIB_libbuhand_C_API 
void libbuhandTerminate(void);


extern LIB_libbuhand_C_API 
void mlxPreparehand(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

extern LIB_libbuhand_C_API 
void mlxNormalizehand(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

extern LIB_libbuhand_C_API 
void mlxExperimentshands(int nlhs, mxArray *plhs[],
                         int nrhs, mxArray *prhs[]);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

/* On Windows, use __declspec to control the exported API */
#if defined(_MSC_VER) || defined(__BORLANDC__)

#ifdef EXPORTING_libbuhand
#define PUBLIC_libbuhand_CPP_API __declspec(dllexport)
#else
#define PUBLIC_libbuhand_CPP_API __declspec(dllimport)
#endif

#define LIB_libbuhand_CPP_API PUBLIC_libbuhand_CPP_API

#else

#if !defined(LIB_libbuhand_CPP_API)
#if defined(LIB_libbuhand_C_API)
#define LIB_libbuhand_CPP_API LIB_libbuhand_C_API
#else
#define LIB_libbuhand_CPP_API /* empty! */ 
#endif
#endif

#endif

extern LIB_libbuhand_CPP_API void preparehand(const mwArray& folder
                                              , const mwArray& population
                                              , const mwArray& output
                                              , const mwArray& flip);

extern LIB_libbuhand_CPP_API void normalizehand(int nargout
                                                , mwArray& NormalizedHandAppearance
                                                , mwArray& NormalizedHandShape
                                                , mwArray& NormalizedPalm
                                                , const mwArray& filename
                                                , const mwArray& flip);

extern LIB_libbuhand_CPP_API void experimentshands(const mwArray& Prefix
                                                   , const mwArray& scheme
                                                   , const mwArray& dim
                                                   , const mwArray& enroll);

#endif

#endif
