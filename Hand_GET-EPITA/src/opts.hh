// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// command line argument
#include <iostream>
#include <string>
#include <unistd.h>
#include <vector>

#ifndef __OPTS__HH__
#define __OPTS__HH__

enum s_mode {EVAL, COMPARE, VERIF, MERGE};

struct s_options
{
  std::string			progname;
  std::string			inputFile;
  std::string			extraInputFile;
  std::string			resultFolder;
  std::string			resultPrefix;
  unsigned			nbToEnroll;
  unsigned			firstToTest;
  unsigned			limit;

  bool				flip;
  bool				forceExtraction;
  bool				extractOnly;
  bool				noextraction;
  bool				quiet;
  bool				extrafile;

  std::vector< std::string >	files;
  unsigned			mode;
  bool				onetoone;

  void
  usage()
  {
    std::cout << "Usage: " << progname
	      <<  " [-options]  image1.pgm [image2.pgm [...]] " << std::endl;
    std::cout << "       or " << std::endl;
    std::cout << "       " << progname
	      <<  " [-options] dbfile " << std::endl << std::endl;
    std::cout << "Options: " << std::endl;
    std::cout << "\t-f        : Flip hand (for right hand database)" << std::endl;
    std::cout << "\t-e #      : Number of session use for enrollment (default 2)"
	      << std::endl;
    std::cout << "\t-t #      : Test set size" << std::endl;
    std::cout << "\t-l        : Limit between sessions when comparing two sets (hand-cmp)"<< std::endl;
    std::cout << "\t-o string : Output directory (default ./features/)" << std::endl;
    std::cout << "\t-h        : Help (this message)" << std::endl;

    std::cout << std::endl;
    std::cout << "Arguments: " << std::endl;
    std::cout << "\t1: image1.pgm [image2.pgm [...]]" << std::endl;
    std::cout << "\t   extract features for each image and compute one to one"
	      << std::endl;
    std::cout << "\t   distance between each hands" << std::endl << std::endl;

    std::cout << "\t2: database file:" << std::endl;
    std::cout << "\t   extract features and experiments to produce verification"
	      << std::endl;
    std::cout << "\t   performances and Recognition Rate." << std::endl;
    std::cout << "\t   File format: each line contain all sessions filenames" << std::endl;
    std::cout << "\t   from a person. Example: " << std::endl;
    std::cout << "\t   ---" << std::endl;
    std::cout << "\t   user1_hand1.pgm user1_hand2.pgm user1_hand3.pgm " << std::endl;
    std::cout << "\t   u2_hand1.pgm u2_hand2.pgm u2_hand3.pgm " << std::endl;
    std::cout << "\t   hnd1_u3.pgm hnd2_u3.pgm hnd3_u3.pgm " << std::endl;
    std::cout << "\t   ---" << std::endl;
    std::cout << std::endl;

    std::cout << "Advanced options:" << std::endl;
    std::cout << "\t-F        : Force feature extraction" << std::endl;
    std::cout << "\t-x        : Extract features only" << std::endl;
    std::cout << "\t-n        : Do not extract features" << std::endl;
    std::cout << "\t-q        : Quiet output during extraction" << std::endl;
    std::cout << "\t-p        : Scores Files Prefix" << std::endl;
    std::cout << std::endl;
}

  unsigned
  selectMode(const std::string&	progname)
  {
    unsigned mode = 0;
    std::cout << "Mode: ";

    //size_type find(const string& s2, size_type pos1);
    unsigned loc = progname.find("-cmp",0);
    if (loc != std::string::npos)
      {
	mode = COMPARE;
	std::cout << " compare" << std::endl;
	return mode;
      }
    loc = progname.find("-eval",0);
    if (loc != std::string::npos)
      {
	mode = EVAL;
	if (extrafile)
	  std::cout << " two-hands (score fusion)" << std::endl;
	else
	  std::cout << " evaluate" << std::endl;
	return mode;
      }
    loc = progname.find("-verif",0);
    if (loc != std::string::npos)
      {
	mode = VERIF;
	std::cout << " verification" << std::endl;
	return mode;
      }
    loc = progname.find("-merge",0);
    if (loc != std::string::npos)
      {
	mode = MERGE;
	std::cout << " two-hands (features fusion)" << std::endl;
	return mode;
      }
    std::cout << " evaluate" << std::endl;
    return mode;
  }

  void
  printCmdLine(int	argc,
	       char**	argv)
  {
    std::cout << "cmd line:";
    for (int i = 0; i < argc; ++i)
      std::cout << " " << argv[i];
    std::cout << std::endl;
  }

  s_options(int		argc,
	    char**	argv)
    : resultFolder ("./features/"), resultPrefix("")
  {
    printCmdLine(argc, argv);
    limit = 3;
    nbToEnroll = 2;
    // start at 0
    firstToTest = 0;

    progname = std::string(argv[0]);

    flip = false;
    forceExtraction = false;
    onetoone = false;
    extractOnly = false;
    noextraction = false;
    quiet = false;
    extrafile = false;

    char		c;
    while ((c = getopt(argc, argv, "e:fFlo:t:s:xnqp:h")) != EOF)
      {
	switch (c)
	  {
	  case 'f':
	    flip = true;
	    std::cout << "flip pictures" << std::endl;
	    break;
	  case 'F':
	    forceExtraction = true;
	    std::cout << "Force features extraction" << std::endl;
	    break;
	  case 'e':
	    nbToEnroll = atoi(optarg);
	    if (nbToEnroll < 1)
	      {
		std::cerr << "Error: Wrong enrollment number: " << nbToEnroll
			  << std::endl << std::endl;
		usage();
		exit(1);
	      }
	    break;
	  case 't':
	    firstToTest = atoi(optarg);
	    if (firstToTest < 1)
	      {
		std::cerr << "Error: Wrong test number: " << firstToTest
			  << std::endl << std::endl;
		usage();
		exit(1);
	      }
	    break;
          case 'l':
            limit = atoi(optarg);
	    if (limit < 1)
            {
                std::cerr << "Error: Wrong limit number: " << limit
                          << std::endl << std::endl;
                usage();
                exit(1);
            }
	  case 'o':
	    {
	      resultFolder  = std::string(optarg);
	      break;
	    }
	  case 'x':
	    {
	      extractOnly = true;
	      break;
	    }
	  case 'n':
	    {
	      noextraction = true;
	      break;
	    }
	  case 'q':
	    {
	      quiet = true;
	      break;
	    }
	  case 'p':
	    {
	      resultPrefix = std::string(optarg) + "-";
	      break;
	    }
	  case 'h' :
	    {
	      usage();
	      exit (0);
	    }
	  }
      }

    if (firstToTest == 0)
      firstToTest = nbToEnroll;

    if (optind == argc)
      {
	std::cerr << "Error: Missing argument" << std::endl << std::endl;
	usage();
	exit (-1);
      }

    if (noextraction && extractOnly)
      {
	std::cerr << "Parameter mismatch: -n conflict with -x" << std::endl << std::endl;
	usage();
	exit (-1);
      }

    inputFile = std::string(argv[optind]);

    unsigned loc = inputFile.find( ".pgm", 0);
    if ((loc != std::string::npos) && (loc == inputFile.length() - 4))
	onetoone = true;
    else
      std::cout << "input file: " << inputFile << std::endl;

    if (onetoone)
      {
	for (; optind < argc; ++optind)
	  files.push_back(argv[optind]);
      }
    else
      {
	if (argc > optind + 1)
	  {
	    // there is another input File
	    extraInputFile = std::string(argv[optind + 1]);
	    extrafile = true;
	    std::cout << "extra input file: " << extraInputFile << std::endl;
	  }
	mode = selectMode(std::string(argv[0]));
      }

    std::cout << "outpur dir: " << resultFolder << std::endl;
  }

};

#endif // __OPTS__HH__
