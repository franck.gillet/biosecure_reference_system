// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#include <vector>
#include <fstream>
#include <string>
#include <dirent.h>

#include "opts.hh"
#include "filenameutils.hh"
#include "extractor.hh"
#include <assert.h>

#ifndef FILEDB__
#define FILEDB__

struct phand
{
  const std::string	name;
  std::vector< double >	feat;
  bool			hasfeatures;

  phand(const std::string&	_name,
	std::vector< double >	*_feat)
    : name (_name)
  {
    setFeat(_feat);
  }

  phand(const std::string&	name)
    : name (name), hasfeatures (hasfeatures)
  {
    hasfeatures = false;
  }

  phand(const struct phand&	orig)
    : name (orig.name), feat (orig.feat), hasfeatures(orig.hasfeatures)
  {
    // do not copy data
    assert(0);
  }

  void
  setFeat(std::vector< double >*	newfeat)
  {
    if (newfeat->size())
      {
	feat = *newfeat;
	hasfeatures = true;
      }
    else
      hasfeatures = false;
  }

};

struct phands
{
  // result folder
  const std::string&		path;
  // index of this image in the file database
  const unsigned 		idx;

  std::vector< struct phand* >	hands;
  unsigned			nbsession;

  phands(const std::string&	_path,
	 const unsigned		_idx)
    : path (_path), idx (_idx)
  {
    nbsession = 0;
  }

  phands(const struct phands&	orig)
    : path (orig.path), idx (orig.idx)
  {
    // do not copy data
    assert(0);
  }

  phand&
  operator[](const unsigned	idx)
  {
    return *(hands[idx]);
  }

  static
  bool
  cmpphand(const phand*	cmp1,
	   const phand*	cmp2)
  {
    if (cmp1->hasfeatures)
      return true;
    else
      if (cmp2->hasfeatures)
	return false;
    return true;
  }

  void
  sort()
  {
    std::sort(hands.begin(), hands.end(), &cmpphand);
  }

  unsigned
  add(const std::string&	name,
      std::vector< double >*	feat)
  {

    hands.push_back(new phand(name, feat));
    return nbsession++;
  }

  unsigned
  add(const std::string&	name)
  {
    hands.push_back(new phand(name));
    return nbsession++;
  }

  bool
  enrollmentfailed(unsigned	enroll)
  {
    for (unsigned i = 0; i < enroll; ++i)
      if (hands[i]->hasfeatures == true)
	return false;
    return true;
  }

  bool
  validEnrollment(unsigned	enroll)
  {
    for (unsigned i = 0; i < enroll; ++i)
      if (hands[i]->hasfeatures == true)
	return true;
    return false;
  }

  bool
  validSet(unsigned	session,
	   int		enrollSetSize,
	   int*		enrollset)
  {
    // a hand is valid if it got features
    if (hands[session]->hasfeatures == false)
      {
	//std::cout << "no features" << std::endl;
	return false;
      }
    // if enroll set does not contain this session
    for (int i = 0; i < enrollSetSize; ++i)
      if (enrollset[i] == int(session))
	{
	  //std::cout << "enroll set contain session" << std::endl;
	  return false;
	}
    // and if at least one picture in enroll set got features
    for (int i = 0; i < enrollSetSize; ++i)
      if (enrollset[i] >= 0)
	if (hands[i]->hasfeatures == true)
	  return true;
    //std::cout << "can't find a valid picture in enroll set " << std::endl;
    return false;
  }

  bool
  valid(unsigned	session,
	int		enroll)
  {
    return validSet(session, 1, &enroll);
  }

  void
  tostring()
  {
  }
};

template < class MyExtractor >
class Filedb
{
  MyExtractor			extractor;
  const s_options&		opts;
  std::string			prefix;

public:
  std::vector< struct phands* >	db;
  unsigned			population;

  phands&
  operator[](unsigned	idx)
  {
    return (*db[idx]);
  }

  /////////////////////////////////////////////////////////////
  // Constructor
  Filedb(const s_options&	opts)
    : opts (opts), prefix (opts.resultFolder)
  {
    population = 0;

    // check if inputfile is a file or a picture
    if (opts.onetoone)
      loadVector();
    else
      loadList(opts.inputFile);

    std::cout << "Extractor: " << extractor.id() << std::endl;
  }

  Filedb(const s_options&	opts,
	 const std::string&	inputFile)
    : opts (opts), prefix (opts.resultFolder)
  {
    population = 0;

    // check if inputfile is a file or a picture
    if (opts.onetoone)
      loadVector();
    else
      loadList(inputFile);

    std::cout << "Extractor: " << extractor.id() << std::endl;
  }

  void
  loadVector()
  {
    std::vector< std::string>::const_iterator	ifile;
    population = 0;

    for (ifile = opts.files.begin(); ifile != opts.files.end(); ++ifile)
      {
	db.push_back(new phands(prefix, population));
	addFile(*ifile, *(db[population++]));
      }
  }

  void
  loadList(const std::string&	inputFile)
  {
    population = 0;
    // first, the filename may just be a picture name
    unsigned loc = inputFile.find( ".pgm", 0);
    if ((loc != std::string::npos) && (loc == inputFile.length() - 4))
      {
	db.push_back(new phands(prefix, population));
	addFile(inputFile, *(db[population++]));
	return;
      }

    // first, the filename may just be a picture name (bmp version)
    loc = inputFile.find( ".bmp", 0);
    if ((loc != std::string::npos) && (loc == inputFile.length() - 4))
      {
	db.push_back(new phands(prefix, population));
	addFile(inputFile, *(db[population++]));
	return;
      }

    // it's not a picture: open it
    std::ifstream	ifs(inputFile.c_str());
    std::string		tmpstr;

    if (!ifs)
      {
	std::cerr << "Can't open file: " << inputFile << std::endl;
	exit (1);
      }

    // check if result folder already exist
    DIR* dir = opendir(prefix.c_str());

    if (dir == NULL)
      {
        std::cout << "folder " << prefix << " does not exist, creating."
		  << std::endl;
	std::string cmd = "mkdir -p " + prefix;
	system(cmd.c_str());
      }
    else
      closedir(dir);

    // read all lines from input file.
    // do it while length is not null since last line has a null length.
    do
      {
	// read line
	std::getline( ifs, tmpstr);

	// preprocessing
	replaceTabBySpace(tmpstr);
	tmpstr = eatWhiteSpace(tmpstr);

	// if line is not empty
	if (tmpstr.length())
	  {
	    // this is a comment: do nothing. (Pattern: ^#)
	    if (tmpstr[0] == '#')
		continue;

	    // folder for result
	    loc = tmpstr.find( "prefix", 0 );
	    if ((loc != std::string::npos) && (loc == 0) && (tmpstr[6] == ' '))
	      {
		prefix = tmpstr.substr(7);
		continue;
	      }

	    // Not a comment neither a directory: we assume this is a picture.
	    db.push_back(new struct phands(prefix, population));

	    // add each filename on the line, but not the last one.
	    do
	      {
		// find if this is the last filename or not
		// (filename are separated by space)
		loc = tmpstr.find_first_of(' ', 0);
		if (loc != std::string::npos)
		  {
		    // add the file.
		    std::string tmpstr2 = tmpstr.substr(0, loc);
		    addFile(tmpstr2, *(db[population]));

		    // remove the added filename from string.
		    while (tmpstr[loc] == ' ')
		      loc++;
		    tmpstr = tmpstr.substr(loc);
		  }
	      }
	    while (loc != std::string::npos);

	    // add the last filename on line.
	    addFile(tmpstr, *(db[population]));
	    population++;
	  }
      }
    while (tmpstr.length());

    ifs.close();
    std::cout << "Population: " << population << std::endl;
  }

  unsigned
  testSetSize(unsigned	enroll)
  {
    unsigned		size = 0;
    for (unsigned p = 0 ; p < population; ++p)
      if (db[p]->enrollmentfailed(enroll) == false)
        size += db[p]->nbsession - enroll;
    return size;
  }

  void
  ExtractFeaturesIfNeeded()
  {
    unsigned	failed = 0;

    // for all people and for all session
    for (unsigned p = 0 ; p < population; ++p)
      {
	for (unsigned s = 0; s < db[p]->nbsession; ++s)
	  {
	    struct phand&	current = (*db[p])[s];
	    if (opts.quiet == false)
	      {
		printf("\rExtraction: %.3d/%.3d %d/%d", p + 1, population, s + 1,
		       db[p]->nbsession);
		fflush(stdout);
	      }

	    // if features still not extracted...
	    if (current.hasfeatures == false)
	      {
		// ...extract features
		current.setFeat(extractor.doit(current.name, db[p]->path,
					       opts.flip));
		// if successful, save features
		if (current.hasfeatures == true)
		  writeFeatureFile(p, s);
		else
		  failed++;
	      }
	  }
	//db[p]->sort();
      }

    std::cout << "\r";
    std::cout.width(25); std::cout.fill(' ');
    std::cout << " " << std::endl;
  }

private:
  void
  writeFeatureFile(unsigned	p,
		   unsigned	s)
  {
    //struct phand* tmp = (*db[p])[s];
    struct phand& tmp = (*db[p])[s];
    writeFeatureFile(tmp.name, db[p]->path, tmp.feat);
  }

  void
  writeFeatureFile(const std::string&		filename,
		   const std::string&		prefix,
		   const std::vector< double >&	vect)
  {
    std::string	str = basename(filename);
    str += ".features";
    //std::string	str = basename(filename) + ".features";
    str = prefix + shortname(str);

    std::ofstream	ofs(str.c_str());
    if (!ofs)
      {
	std::cerr << "Can't open file: " << str << std::endl;
	exit (1);
      }
    std::vector< double >::const_iterator ival;
    for (ival = vect.begin(); ival != vect.end(); ++ival)
      ofs << *ival << std::endl;
    ofs.close();
  }

  bool
  readFeatureFile(struct phand&	p)
  {
    unsigned		loc ;
    std::string		filename = basename(p.name) + ".features";
    if (prefix.length() != 0)
      filename = prefix + '/' + shortname(filename);

    //std::cout << "Read feature file: " << filename << std::endl;
    assert(((loc = filename.find(".features")) != std::string::npos) &&
	   (loc == filename.length() - 9));

    std::ifstream	ifs(filename.c_str());
    if (!ifs)
      return false;

    std::vector< double >* feat = new std::vector< double >();
    std::string			tmpstr;
    double val = 0;
    do
      {
	ifs >> val;
	if (ifs.good())
	  feat->push_back(val);
      }
    while (ifs.good());

    ifs.close();
    p.setFeat(feat);
    return true;
  }

  void
  addFile(const std::string	&file,
	  struct phands&	hands)
  {
    unsigned loc;
    int index = 0;

    if (file == "." || file == ".." )
      return;

    // FIXME: verify there is no space in filename (?)
    loc = file.find(".pgm");
    if ((loc != std::string::npos) && (loc == file.length() - 4))
      {
	unsigned index = hands.add(file);
	if (opts.forceExtraction == false)
	  readFeatureFile(hands[index]);
	return;
      }
    loc = file.find(".bmp");
    if ((loc != std::string::npos) && (loc == file.length() - 4))
      {
	unsigned index = hands.add(file);
	if (opts.forceExtraction == false)
	  readFeatureFile(hands[index]);
	return;
      }
    std::cerr << "Illegal file name (ignore): " << file << std::endl;
    index = -1;
  }
  /*
  void
  readDirectory(std::string	dirname)
  {

    DIR*	dir = opendir(dirname.c_str());

    if (dir == NULL)
      {
        std::cerr << "error: directory can not be opened (ignore)"
		  << std::endl;
        return;
      }

    if (dirname[ dirname.size() - 1 ] != '/')
      dirname += '/';

    struct dirent*	dir_ent = 0;
    while ((dir_ent = readdir(dir)) != NULL)
      {
	std::string filename = dir_ent->d_name;
	if (filename == "." || filename == ".." )
	  continue;
	filename = dirname + filename;
	addFile(filename);
      }
    (void)closedir(dir);
  }
  */

};

#endif // FILEDB__
