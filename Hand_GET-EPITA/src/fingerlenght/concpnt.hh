// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef CONNECTED_COMPONENT
#define CONNECTED_COMPONENT

#include <qgarlib/GenPoint.H>
#include <qgarlib/Component.H>
#include <qgarlib/ConnectedComponents.H>

#include "handinfo.hh"
#include "saveimg.hh"
#include "boundary.hh"
#include "morphomat.hh"
#include "eigenvalues.hh"

class	concpnt
{
public:
  typedef qgar::ConnectedComponents::node_type tnode;

  // input Image
  //qgar::BinaryImage*		resultImg;

  bool				ccfound;
  bool				boundaryfound;

  // connected components
  qgar::ConnectedComponents	cc;
  // object component
  qgar::Component*		bigcomp;

  ////////////////////////////////////////////////////////////////
  // Constructor
  //

  concpnt(struct s_handinfo&	info)
    : cc(*info.shape)
  {
    ccfound = false;
  }

  ////////////////////////////////////////////////////////////////
  // Accessor
  //
  qgar::BoundingBox
  getBBox()
  {
    if (ccfound == false)
      {
	std::cerr << "Find Connected components first!" << std::endl;
	exit (1);
      }
    return bigcomp->accessBoundingBox();
  }

private:

  ////////////////////////////////////////////////////////////////
  // Internal Methods
  //

  // deep parse of hierarchical component tree
  // the purpose is to identifie all the sub-component
  // from the object component
  void
  getsublabel(tnode	*root,
	      unsigned	**lab)
  {
    if (root == NULL)
      return ;
    qgar::Component	*comp = (root)->data();
    //mark the component
    (*lab)[comp->label()] = 2;

    // parse the component first child and his siblings
    tnode* fchild = root->pFirstChild();
    if (fchild != NULL)
      do
	{
	  getsublabel(fchild, lab);
	  fchild = fchild->pRSibling();
	}
      while (fchild != NULL);
  }

  void
  getsublabel(tnode	*root,
	      unsigned	label,
	      unsigned	**labeltab)
  {
    if (root == NULL)
      return ;
    qgar::Component	*comp = (root)->data();
    //mark the component
    (*labeltab)[comp->label()] = label;

    // parse the component first child and his siblings
    tnode* fchild = root->pFirstChild();
    if (fchild != NULL)
      do
	{
	  getsublabel(fchild, label, labeltab);
	  fchild = fchild->pRSibling();
	}
      while (fchild != NULL);
  }

  void
  generateShapeImage(struct s_handinfo&	info,
		     unsigned*		label,
		     unsigned		nblabel,
		     unsigned		nbcomp)
  {
    // generate result image
    const qgar::ConnectedComponents::image_type& compImg =
      cc.accessComponentImage();
    const std::vector< tnode * >& comptab = cc.accessComponentTab();
    assert(comptab.size() > 1);
//     std::vector< tnode * >::const_iterator	icc = comptab.begin();
    unsigned width = compImg.width();
    unsigned height = compImg.height();

    struct stat *stats = new struct stat[nbcomp];

    unsigned link[nblabel];
    for (unsigned i = 0, j = 0; i < nblabel; ++i)
      if (label[i] == 1)
	link[i] = j++;
      else
	link[i] = nbcomp;

    for (unsigned i = 0; i < width; ++i)
      for (unsigned j = 0; j < height; ++j)
	if (link[compImg.pixel(i, j)] < nbcomp)
	  stats[link[compImg.pixel(i, j)]].add(i, j);

    for (unsigned i = 0; i < nbcomp; ++i)
      {
	stats[i].computeCovarMatrix();
	stats[i].compute_eigenvalues();
      }

    unsigned elementsize = compImg.height() * 3 / 100;
    qgar::ConnectedComponents::image_type	*tmp = dilate(compImg, elementsize,
							      link, stats,
							      nbcomp, info,
							      bigcomp->label());
    qgar::Component::label_type*   pMapComp = tmp->pPixMap();
    delete info.shape;
    info.shape = new qgar::BinaryImage(width, height);
    qgar::BinaryImage::value_type* pMapRes = info.shape->pPixMap();

    // generate result image
    int size = width * height;
    for (int iCnt = 0 ; iCnt < size ; ++iCnt, ++pMapComp, ++pMapRes)
      *pMapRes = (label[*pMapComp] > 0) ? 1 : 0;
    //saveImg(*info.shape, info.out + "-cc.pbm", info);
  }

  void
  generateShapeImage(struct s_handinfo&	info,
		     unsigned*		label)
  {
    // generate result image
    const qgar::ConnectedComponents::image_type& compImg =
      cc.accessComponentImage();
    //const std::vector< tnode * >& comptab = cc.accessComponentTab();
    //assert(comptab.size() > 1);
    //std::vector< tnode * >::const_iterator	icc = comptab.begin();
    unsigned width = compImg.width();
    unsigned height = compImg.height();

    qgar::Component::label_type*   pMapComp = compImg.pPixMap();
    delete info.shape;
    info.shape = new qgar::BinaryImage(width, height);
    qgar::BinaryImage::value_type* pMapRes = info.shape->pPixMap();

    // generate result image
    int size = width * height;
    for (int iCnt = 0 ; iCnt < size ; ++iCnt, ++pMapComp, ++pMapRes)
      *pMapRes = (label[*pMapComp] > 0) ? 1 : 0;
    //saveImg(*info.shape, info.out + "-cc.pbm", info);
  }

  void
  FillHoles(unsigned*		label,
	    unsigned*		label2,
	    unsigned		comptabsize)
  {
    for (unsigned i = 0; i < comptabsize; ++i)
      if (label[i] == 0)
	label2[i] = 0;

    // generate result image
    const qgar::ConnectedComponents::image_type& compImg =
      cc.accessComponentImage();
    //const std::vector< tnode * >& comptab = cc.accessComponentTab();
    //assert(comptab.size() > 1);
    //     std::vector< tnode * >::const_iterator	icc = comptab.begin();
    unsigned width = compImg.width();
    unsigned height = compImg.height();

    qgar::Component::label_type*   pMapComp = compImg.pPixMap();

    // generate result image
    int size = width * height;
    for (int iCnt = 0 ; iCnt < size ; ++iCnt, ++pMapComp)
      *pMapComp = label2[*pMapComp];
  }

public:
  ////////////////////////////////////////////////////////////////
  // Public Methods
  //

  /*
    rechercher les composantes connexes
    supprimer les plus petites : taille minimale des composantes ?
    dilater l'image : element structurant ?
    regarder les composantes qui fusionnent
    les inserer dans la main.
   */

  void
  doit(s_handinfo&	info)
  {
    // find connected component
    const std::vector< tnode * >& comptab = cc.accessComponentTab();
    //assert(comptab.size() > 1);
    std::vector< tnode * >::const_iterator	icc = comptab.begin();
    //bool				*label = new bool[comptab.size()];
    unsigned				*label = new unsigned[comptab.size()];
    unsigned				*label2 = new unsigned[comptab.size()];

    for (unsigned i = 0; i < comptab.size(); ++i)
      {
	label[i] = 0;
	label2[i] = i;
      }

    bigcomp = NULL;
    int size = 0;
    std::vector< qgar::Component* > tmpcomp;
    for (icc = comptab.begin(); icc != comptab.end(); ++icc)
      {
	qgar::Component* cur = (*icc)->data();
	if (cur->color() != OBJCOLOR)
	    continue;
	if (cur->areaPixels() > info.getmincompsize())
	  {
	    tmpcomp.push_back(cur);
	    label[cur->label()] = 1;
	    if (cur->areaPixels() > size)
	      {
		bigcomp = cur;
		size = cur->areaPixels();
	      }
	  }
// 	else
// 	  std::cout << "not enough: " << cur->areaPixels() << "/"
// 		    <<  info.getmincompsize() << std::endl;
      }

    if (!tmpcomp.size())
      {
	std::cout << "size: " << comptab.size() << std::endl;
	std::cerr << "can't find a object in picture" << std::endl;
	//FIXME : we should not exit application.
	exit (-1);
      }
    else
      ccfound = true;

    // fill holes in hand component
    label[bigcomp->label()] = 2;

//     std::cout<< "Label: ";
//     for (unsigned i = 0; i < comptab.size(); ++i)
//       std::cout << label[i] << " ";
//     std::cout << std::endl;

//     std::cout<< "Label 2: ";
//     for (unsigned i = 0; i < comptab.size(); ++i)
//       std::cout << label2[i] << " ";
//     std::cout << std::endl;

    // label : keep component or not.
    // label2 : merge components
    //getsublabel(cc.pNode(bigcomp->label()), &label);

//     for (unsigned i = 0; i < tmpcomp.size(); ++i)
//       getsublabel(cc.pNode(tmpcomp[i]->label()), tmpcomp[i]->label(), &label2);

//     std::cout<< "Label: ";
//     for (unsigned i = 0; i < comptab.size(); ++i)
//       std::cout << label[i] << " ";
//     std::cout << std::endl;

//     std::cout<< "Label 2: ";
//     for (unsigned i = 0; i < comptab.size(); ++i)
//       std::cout << label2[i] << " ";
//     std::cout << std::endl;



//     std::cout<< "Label: ";
//     for (unsigned i = 0; i < comptab.size(); ++i)
//       std::cout << label[i] << " ";
//     std::cout << std::endl;

    if (tmpcomp.size() > 1)
      {
//  	std::cout << "cut finger detected ("
//  		  << tmpcomp.size() - 1 << ')' << std::endl;
	for (unsigned i = 0; i < tmpcomp.size(); ++i)
	  getsublabel(cc.pNode(tmpcomp[i]->label()), tmpcomp[i]->label(), &label2);
	FillHoles(label, label2, comptab.size());
	generateShapeImage(info, label, comptab.size(), tmpcomp.size() - 1);
      }
    else
      {
	getsublabel(cc.pNode(bigcomp->label()), &label);
	generateShapeImage(info, label);
      }

    delete []label;
  }

};

#endif
