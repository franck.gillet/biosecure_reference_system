// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "normalizehand.hh"

#include "libbuhand.h"
#include "mclcppclass.h"
#include "filedb.hh"

void
normalizehands::normalizeOneHand(const std::string&     filename,
				 const bool&            flip,
				 qgar::GreyLevelImage	normalized,
				 qgar::BinaryImage&	binimg)
{
  int   nargout = 3;
  mwArray HandAppearance;
  mwArray HandShape;
  mwArray Palm;
  mwArray in1(filename.c_str());
  mwArray in2(flip);

  normalizehand(nargout, HandAppearance, HandShape, Palm, in1,in2);

  int nbdim = HandShape.NumberOfDimensions();
  mwArray dims = HandShape.GetDimensions();

  assert((nbdim == 2) && (dims.NumberOfElements() == 2));
  unsigned      row = dims.Get(1, 1);
  unsigned      col = dims.Get(1, 2);

  for (unsigned i = 1; i <= row; ++i)
    for (unsigned j = 1; j <= col; ++j)
      {
	normalized.setPixel(j - 1, i - 1,
			    HandAppearance.Get(nbdim, i, j));
	binimg.setPixel(j - 1, i - 1,
			HandShape.Get(nbdim, i, j));
      }
}
