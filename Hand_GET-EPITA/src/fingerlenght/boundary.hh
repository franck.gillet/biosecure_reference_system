// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <qgarlib/GenPoint.H>
#include <qgarlib/Component.H>
#include <qgarlib/ConnectedComponents.H>

#include "handinfo.hh"
#include "saveimg.hh"
#include "axis.hh"

#ifndef __BOUNDARY__HH__
#define __BOUNDARY__HH__


#define	BGCOLOR		0
#define	OBJCOLOR	1

// accessor to the pixel from an image needed to take care
// of the border during boundary tracing.
unsigned char
getPixel(const qgar::IPoint		&point,
	 const qgar::BinaryImage	&img)
{
  if ((point.x() < 0) || (point.x() >= img.width()) ||
      (point.y() < 0) || (point.y() >= img.height()))
    {
      fprintf(stderr, "border access\n");
      return BGCOLOR;
      }
  return img.pixel(point.x(), point.y());
}

inline
double
computeRadialDist(const qgar::IPoint s,
		  const qgar::IPoint p,
		  struct s_handinfo&	info)
{
  double res = pow(s.x() - p.x(), 2) + pow(s.y() - p.y(), 2);
  if (res > info.maxdist)
    info.maxdist = res;
  return res;
}

// compute which point, from a 3x3 neightborhood, need
// to be test with the actual direction (4-connectivity)
void
getNextPoints8C(const qgar::IPoint	p,
		const int		dir,
		qgar::IPoint*&		tab)
{
  int i = p.x();
  int j = p.y();
  switch (dir)
    {
    case 0:
    case 1: {
      tab[0].setXY(i + 1, j + 1); tab[1].setXY(i    , j + 1);
      tab[2].setXY(i - 1, j + 1); tab[3].setXY(i - 1, j    );
      break;
    }
    case 2:
    case 3: {
      tab[0].setXY(i - 1, j + 1); tab[1].setXY(i - 1, j    );
      tab[2].setXY(i - 1, j - 1); tab[3].setXY(i    , j - 1);
      break;
    }
    case 4:
    case 5: {
      tab[0].setXY(i - 1, j - 1); tab[1].setXY(i    , j - 1);
      tab[2].setXY(i + 1, j - 1); tab[3].setXY(i + 1,     j);
      break;
    }
    case 6:
    case 7: {
      tab[0].setXY(i + 1, j - 1); tab[1].setXY(i + 1,     j);
      tab[2].setXY(i + 1, j + 1); tab[3].setXY(i    , j + 1);
      break;
    }
    default : { std::cerr << "unknown direction" << std::endl; }
    }
}

double
diff(const qgar::IPoint&	first,
     const qgar::IPoint&	second,
     const qgar::IPoint&	secondlast,
     const qgar::IPoint&	last)
{
  if (computeEuclidiandist(first, secondlast) <= 1)
    if (computeEuclidiandist(second, last) <= 1)
      return true;
  return false;
}

unsigned
selectInitialDirection(const double&		slope,
		       const double&		bias,
		       const qgar::DPoint&	startPoint,
		       const int&		side)
{
  double		*tab = new double[4];
  qgar::DPoint		p(startPoint.x() + 1, (startPoint.x() + 1) * slope + bias);
  qgar::DSegment	seg(startPoint.x(), startPoint.y(), p.x(), p.y());

  tab[0] = seg.distance(qgar::DPoint(startPoint.x() + 1, startPoint.y()));
  tab[1] = seg.distance(qgar::DPoint(startPoint.x() + 1, startPoint.y() + 1));
  tab[2] = seg.distance(qgar::DPoint(startPoint.x()    , startPoint.y() + 1));
  tab[3] = seg.distance(qgar::DPoint(startPoint.x() - 1, startPoint.y() + 1));

  unsigned dir = 0;
  for(unsigned i = 1; i < 4; ++i)
    if (tab[i] < tab[dir])
      dir = i;
  if (side == -1)
    dir += 4;
  delete []tab;
  return dir;
}


qgar::IPoint
selectInitialPoint(struct s_handinfo&		info,
		   const qgar::BinaryImage&	shape)
{
  //qgar::DPoint cur(startPoint);
  //qgar::IPoint result = DPointToIPoint(cur);

  //std::cout << std::endl << "image: " << info.name << std::endl;

//   while (isInObject(cur, *info.shape))
//     {
//       result = DPointToIPoint(cur);
//       cur.setXY(cur.x() + info.absside,
// 		(cur.x() + info.absside) * info.handaxis.slope + info.handaxis.bias);
//     }

  unsigned idx = info.startidx;
  //std::cout << "init here: " << getcolor(info.variation[idx].middle, *info.shape) << std::endl;
  if (getcolor(info.variation[idx].middle, *info.shape) != OBJCOLOR)
    {
      //      std::cout << "color OK" << std::endl;
      while (!isAtBorder(info.variation[idx].middle, *info.shape))
	{
// 	  std::cout << info.variation[idx].middle << " -> (-) " << info.variation[idx + info.side].middle
// 		    << std::endl;
	  idx -= info.side;
	}
    }
  else
    {
      while (!isAtBorder(info.variation[idx].middle, *info.shape))
	{
// 	  std::cout << info.variation[idx].middle << " -> (+) "
// 		    << info.variation[idx + info.side].middle << std::endl;
	  idx += info.side;
	}
    }
//   if (isAtBorder(info.variation[idx].middle, *info.shape))
//     std::cout << info.variation[idx].middle << " is at border" << std::endl;
//   else
//     std::cout << info.variation[idx].middle << " is not at border" << std::endl;

  qgar::IPoint result = DPointToIPoint(info.variation[idx].middle);
  int value = info.shape->pixel(result.x(), result.y());
  if (value != OBJCOLOR)
    {
      std::cout << "error: " << result << " is not an object point" << std::endl;
      savePbmImg(shape, info.out + "-error-boundary.pbm");
      exit(-1);
    }

  //std::cout << "initial : " << DPointToIPoint(startPoint) << " final: " << result << std::endl;
  return result;
}

// boundary tracing (simple version)
// compute the inner and the outer boundary.
// the given point is the bottom right from the
// object bbox.
void
boundaryTracing8C(struct s_handinfo&		info)
{
  std::list< qgar::IPoint >	iBoundary;
  std::list< qgar::IPoint >::iterator first, second, sndlast, last;

  info.oBoundary.clear();
  info.oBoundaryDirection.clear();
  info.dist.clear();

  qgar::IPoint cur = selectInitialPoint(info, *info.shape);
  //unsigned dir = selectInitialDirection(info.handaxis.slope, info.handaxis.bias,
  //					startPoint, info.absside);
  //dir = 1;
  // we need to adapt initial direction in function of hand main line
  // or boundary tracing doesn't work. We need a more accurate
  // initatization here I think.
  unsigned dir = 3;
  if (info.absside == -1)
    dir = 7;
  //int value = 0;
  //std::cout << "dir: " << dir << " side: " << info.absside << std::endl;
  //std::cout << "Freeman::Initial Direction: " << dir << std::endl;

  // initialisation : given point
  //std::cout << "Freeman::Initial Point: " << i << ' ' << j << std::endl;
  //qgar::IPoint cur(i, j) ;
  int	value = 0;
  iBoundary.push_back(cur);

  first = iBoundary.begin();
  last = iBoundary.begin();

  unsigned imageperimeter = (info.shape->width() + info.shape->height()) * 2;

  //std::cout << *cur << " " << **first << std::endl << std::endl;
  qgar::IPoint* tab = new qgar::IPoint[4];
  do
    {
      sndlast = last;
      first = iBoundary.begin();
      //std::cout << "cur: " << cur;
      getNextPoints8C(cur, dir, tab);
      //std::cout << " next: " << tab[0] << " " << tab[1] << " " << tab[2] << " " << tab[3]  << " dir: " << dir << std::endl;
      if ((value = getPixel(tab[0], *info.shape)) == OBJCOLOR)
	{
	  cur = tab[0];
	  dir = ((dir - 1) - (dir % 2)) % 8;
	}
       else
	{
	  info.oBoundary.push_back(qgar::IPoint(tab[0].x(), tab[0].y()));
	  info.oBoundaryDirection.push_back(dir);
	  info.dist.push_back(computeRadialDist(*first, tab[0], info));
	  if ((value = getPixel(tab[1], *info.shape)) == OBJCOLOR)
	    {
	      cur = tab[1];
	      dir = (dir - (dir % 2)) % 8;
	    }
	  else
	    {
	      info.oBoundary.push_back(qgar::IPoint(tab[1].x(), tab[1].y()));
	      info.oBoundaryDirection.push_back(dir);
	      info.dist.push_back(computeRadialDist(*first, tab[1], info));
	      if ((value = getPixel(tab[2], *info.shape)) == OBJCOLOR)
		{
		  cur = tab[2];
		  dir = ((dir + 1) - (dir % 2)) % 8;
		}
	      else
		{
		  info.oBoundary.push_back(qgar::IPoint(tab[2].x(), tab[2].y()));
		  info.oBoundaryDirection.push_back(dir);
		  info.dist.push_back(computeRadialDist(*first, tab[1], info));
		  if ((value = getPixel(tab[3], *info.shape)) == OBJCOLOR)
		    {
		      cur = tab[3];
		      dir = ((dir + 2) - (dir % 2)) % 8;
		    }
		  else
		    {
		      info.oBoundary.push_back(qgar::IPoint(tab[3].x(),
							tab[3].y()));
		      info.oBoundaryDirection.push_back(dir);
		      info.dist.push_back(computeRadialDist(*first, tab[2], info));
		      dir = (dir + 1) % 8;
		    }
		}
	    }
	}

      iBoundary.push_back(qgar::IPoint(cur.x(), cur.y()));
      last = iBoundary.end();
      last--;
      second = first;
      second++;

      if ((iBoundary.size() > 10) && diff(*first, *second, *sndlast, *last))
	break;
      // emergency stop
      if (iBoundary.size() > imageperimeter * 3)
	{
	  std::cerr << "Infinite loop in boundary tracing, exit "
		    << std::endl;
	  break;
	}
    }
  while (1);
  //std::cout << std::endl;
  //std::cout << "boundary size: " << info.oBoundary.size() << std::endl;

  delete []tab;
}

#endif
