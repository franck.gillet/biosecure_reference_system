// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef DISTANCE__HH__
#define DISTANCE__HH__

#include	<extractor.hh>
#include	<qgarlib/GenImage.H>
#include	<iostream>
#include	<vector>

double
computedist(const qgar::IPoint &s,
	    const qgar::IPoint &p);

double
computeEuclidiandist(const qgar::IPoint &s,
		     const qgar::IPoint &p);


double
computeEuclidiandist(const qgar::DPoint &s,
		     const qgar::DPoint &p);



#endif
