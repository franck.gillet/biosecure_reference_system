// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef OTSU_THRES
#define OTSU_THRES

#include "graylevel.hh"

class otsuthres
{
private:
  int threshold;
  static const int HMIN = 0;
  static const int HMAX = 256;

public:

  qgar::BinaryImage *
  doit(const qgar::GreyLevelImage sourceImg)
  {
    int  width = sourceImg.width();
    int height = sourceImg.height();
    int   size = width * height;
    qgar::Histogram	hist(sourceImg, 0, 255, 1);
    float *phist = new float[256];

    for (int i = 0; i < 256 ; ++i)
      phist[i] = ((float) hist[i]) / ((float) size);

    GrayLevel C1(phist, true);
    GrayLevel C2(phist, false);

    float fullMu = C1.getOmega() * C1.getMu() + C2.getOmega() * C2.getMu();
    double sigmaMax = 0;
    int threshold = 0;

    for (int i = 0 ; i < 255 ; i++)
      {
	double sigma = C1.getOmega() * (pow(C1.getMu() - fullMu, 2))
	  + C2.getOmega() * (pow(C2.getMu() - fullMu, 2));

	    if (sigma > sigmaMax)
	      {
		sigmaMax = sigma;
		threshold = C1.getThreshold();
	      }

	    C1.addToEnd();
	    C2.removeFromBeginning();
      }

    qgar::GreyLevelImage::value_type* pMapSource = sourceImg.pPixMap();
    qgar::BinaryImage *resultImg = new qgar::BinaryImage(width, height);
    qgar::BinaryImage::value_type* pMapRes = resultImg->pPixMap();

    for (int iCnt = 0 ; iCnt < size ; ++iCnt, ++pMapRes, ++pMapSource)
      *pMapRes = (*pMapSource < threshold) ? 0 : 1 ;

    delete []phist;
    return resultImg;
  }

};

#endif
