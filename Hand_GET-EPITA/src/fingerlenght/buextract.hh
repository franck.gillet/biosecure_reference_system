// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef __BUEXTRACTOR__HH__
#define __BUEXTRACTOR__HH__

#include "extractor.hh"
#include "libbuhand.h"
#include "mclcppclass.h"
#include "filedb.hh"

class BuExtractor  : public Extractor
{
public:

  std::vector< double >*
  doit(const std::string&	fileName,
       const std::string&	prefix,
       const bool&		flip)
  {
    return new std::vector< double >;
  }

  // FIXME : l'extraction n'est pas faite ici !!!
  // comment séparer l'extraction des calculs suivants ?

  normalizehands norm;

  void
  doit2(Filedb&			db,
	const std::string&	scheme,
	const unsigned&		dim,
	const unsigned&		enroll)
  {
    // FIXME : filedb : génération matrice
    // scheme
    // dim
    // enroll
    bool			flip = false;

    //unsigned	size = 200 * 200;
    unsigned size = db[0][0].feat.size();

    // FIXME : à quel point le code bu est dépendant du schema 3 image
    // par main.
    unsigned totalpopulation = 0;
    for (unsigned p = 0; p < db.population; ++p)
      totalpopulation += db[p].nbsession;
    mwArray	in1(size, totalpopulation, mxDOUBLE_CLASS);

    unsigned cur = 0;
    for (unsigned p = 0; p < db.population; ++p)
      for (unsigned s = 0; s < db[p].nbsession; ++s, ++cur)
	{
	  mwArray img(200,200, mxDOUBLE_CLASS);
	  norm.normalizeOneHand(fileName, flip, normalized, binimg);

	  for (unsigned i = 0; i < 200; ++i)
	    for (unsigned j = 0; j < 200; ++j)
	      in1(cur, i + i * j + 1) = normalized.pixel(i, j);
	}
    mwArray       in2(scheme.c_str());
    mwArray       in3(dim);
    mwArray       in4(enroll);
    experimentshands(in1, in2, in3, in4);
  }

  std::string
  id()
  {
    return "buextract";
  }

};

#endif // __BUEXTRACTOR__HH__
