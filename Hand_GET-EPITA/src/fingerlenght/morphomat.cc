// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include	<vector>
#include	<algorithm>
#include	<list>

// qgar include
#include	<qgarlib/GenImage.H>
#include	<qgarlib/Component.H>
#include	<qgarlib/ConnectedComponents.H>

#include	"handinfo.hh"
#include	"watershed_bienek-1d.hh"
#include	"morphomat.hh"
#include	"dist.hh"
#include	"axis.hh"

static inline
double *
mmClose(const std::vector< double >	&tab,
	const int			&C)
{
  int			size = tab.size();
  double*		resDil  = new double[size];
  double*		res  = new double[size];
  double		min = 1000000000;
  int			i;
  int			j;

  assert(size > C);

  for (i = 0 ; i < size; ++i)
    resDil[i] = res[i] = 0;

  // dilatation
  for (i = C; i < size - C; ++i)
    {
      double max = 0;
      for (j = -C; j < C ; ++j)
	max = std::max(tab[i + j], max);
      resDil[i] = max;
    }

  // erosion
  for (i = C; i < size - C; ++i)
    {
      min = 1000000000;
      for (j = -C; j < C ; ++j)
	min = std::min(resDil[i + j], min);
      res[i] = min;
    }

  delete []resDil;
  return res;
}

static inline
bool
compare_coords(const struct s_extremum&	a,
	       const struct s_extremum&	b)
{
  return a.p.y() < b.p.y();
};

static inline
bool
compare_coords_inv(const struct s_extremum&	a,
		   const struct s_extremum&	b)
{
  return a.p.y() > b.p.y();
};

static inline
bool
compare_index(const struct s_extremum&	a,
	      const struct s_extremum&	b)
{
    return a.idx < b.idx;
};


std::vector< s_extremum >
FindPoints(const struct s_handinfo&		info,
	   const unsigned&			nbtofind)
{
  // FIXME : to adapt with image size
  const int	C = 200;
  double*	resClose = mmClose(info.dist, C);
  unsigned	size = info.dist.size();
  std::vector< qgar::IPoint >::const_iterator ibound = info.oBoundary.begin();

  qgar::DSegment seg = getDSegment(info.handaxis.slope, info.handaxis.bias,
				   *info.shape);
  // apply watershed
  watershed1D ws(resClose, size);
  ws.doit();

  int	*label = ws.getLabel();
  int	cur = label[0];

  std::vector< s_extremum > tmplist;

  // extract extremum
  unsigned count = 0;
  for (unsigned i = 0; i < info.dist.size(); ++i, ++ibound)
    if (label[i] != cur)
      {
	count++;
	qgar::DPoint p(ibound->x(), ibound->y());
	tmplist.push_back(s_extremum(i, resClose[i], seg.project(p)));
	cur = label[i];
      }

  // sort best extremum at beginning
  if (info.absside == 1)
    std::sort(tmplist.begin(), tmplist.end(), compare_coords);
  else
    std::sort(tmplist.begin(), tmplist.end(), compare_coords_inv);

  // remove extra points
  if (tmplist.size() > nbtofind)
    tmplist.erase(tmplist.begin() + nbtofind, tmplist.end());

  // sort extremum by index and fill handinfo structure
  std::sort(tmplist.begin(), tmplist.end(), compare_index);

  delete []resClose;
  return tmplist;
}

std::vector< s_extremum >
FindPointsAuto(const struct s_handinfo&		info,
	       const unsigned&			nbtofind)
{
  qgar::DSegment seg = getDSegment(info.handaxis.slope, info.handaxis.bias,
				   *info.shape);
  unsigned					nbite = 0;
  std::vector< s_extremum >			tmplist;
  double*					resClose = NULL;
  int						C = 200;
  unsigned					size = info.dist.size();
  std::vector< qgar::IPoint >::const_iterator	ibound;
  do
    {
      resClose = mmClose(info.dist, C);

      // apply watershed
      watershed1D ws(resClose, size);
      ws.doit();

      int	*label = ws.getLabel();
      int	cur = label[0];

      // extract extremum
      unsigned count = 0;
      ibound = info.oBoundary.begin();
      tmplist.clear();
      for (unsigned i = 0; i < info.dist.size(); ++i, ++ibound)
	if (label[i] != cur)
	  {
	    count++;
	    qgar::DPoint p(ibound->x(), ibound->y());
	    tmplist.push_back(s_extremum(i, resClose[i], seg.project(p)));
	    cur = label[i];
	  }
      if (tmplist.size() > nbtofind)
	{
//     	  std::cout << "size: " << tmplist.size() << "/" << nbtofind << " C= "
//     		    << C << " +10 ite:" << nbite << std::endl;
	  C += (nbite < 50) ? 10 : 1;
	}
      else if (tmplist.size() < nbtofind)
	{
//    	  std::cout << "size: " << tmplist.size() << "/" << nbtofind << " C= "
//    		    << C << " -10 ite: " << nbite << std::endl;
	  C -= (nbite < 50) ? 10 : 1;
	}
//        else
//    	std::cout << "size: " << tmplist.size() << " C= " << C << " ite: " << nbite << std::endl;
      nbite++;
    }
  while ((tmplist.size() != nbtofind) && (C > 0) && (nbite < 100));

  delete []resClose;
  //  std::cout << "end" << std::endl;
  return tmplist;
}

qgar::BinaryImage*
dilate(const qgar::BinaryImage&	src,
       const int&		size)
{
  int width = src.width();
  int height = src.height();
  qgar::BinaryImage*	result = new qgar::BinaryImage(width, height);

  std::cout << "dilatation" << std::endl;

  for (int i = 0; i < width; ++i)
    for (int j = 0; j < height; ++j)
      {
	result->setPixel(i, j, src.pixel(i, j));
	if (src.pixel(i, j) == BGCOLOR)
	  for (int k = i - size; k < i + size; ++k)
	    {
	      for (int l = j - size; l < j + size; ++l)
		{
		  if ((k > 0) && (k < width) && (l > 0) && (l < height))
		    if (src.pixel(k, l) == OBJCOLOR)
		      {
			result->setPixel(i, j, OBJCOLOR);
			break;
		      }
		}
	      if (result->pixel(i, j) == OBJCOLOR)
		break;
	    }
      }

  return result;
}

qgar::ConnectedComponents::image_type*
dilate(const qgar::ConnectedComponents::image_type&	src,
       const int&					size,
       unsigned						*link,
       struct stat*					stats,
       unsigned						nbcomp,
       struct s_handinfo&				info,
       const unsigned&					handlabel)
{
  int width = src.width();
  int height = src.height();
  qgar::ConnectedComponents::image_type*	result =
    new qgar::ConnectedComponents::image_type(width, height);

  qgar::DPoint p;

  qgar::Component::label_type* pMapComp = src.pPixMap();
  qgar::Component::label_type* pMapRes  = result->pPixMap();

  // generate result image
  int total = width * height;
  for (int iCnt = 0 ; iCnt < total ; ++iCnt, ++pMapComp, ++pMapRes)
    {
      // if point is an OBJECT point, result will be an object point
      // too (but maybe with not the same label)
      if (*pMapComp)
	*pMapRes = *pMapComp;
      if (link[*pMapComp] < nbcomp)
	{
	  int i = iCnt % width;
	  int j = iCnt / width;
	  double segslope = stats[link[src.pixel(i, j)]].slope;
	  double segbias = j - i * segslope;

 	  double segj = j + size * info.absside;
 	  double segi = (segj - segbias) / segslope;
	  qgar::DPoint p(segi, segj);
//  	  double segi = i - size * info.absside;
//  	  double segj = (segi * segslope) + segbias;
//    	  std::cout << "dilate " << i << ", " << j <<  " to " << segi << ", "
//    		    << segj << std::endl;
	  if (isIn(p, src))
	    {   
	      if (src.pixel(int(round(segi)), int(round(segj))) == int(handlabel))
		{
		  qgar::Segment seg(i, j, int(round(segi)), int(round(segj)));
		  result->draw(seg, *pMapComp);
		}
	    }
// 	  else
// 	    std::cout << "dilate outside boundary " << i << ", " << j 
// 		      <<  " to " << segi << ", " << segj << std::endl;
	}
    }

  return result;
}
