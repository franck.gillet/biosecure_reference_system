// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "axis.hh"
#include "saveimg.hh"

bool
sort_lines(const struct s_line&	line1,
	   const struct s_line&	line2)
{
  return line1.bias < line2.bias;
}

bool
isIn(const qgar::DPoint&	p,
     const qgar::BinaryImage&	img)
{
  int ux = unsigned(round(p.x()));
  int uy = unsigned(round(p.y()));

  if ((ux > 0) && (ux < img.width()) &&
      (uy > 0) && (uy < img.height()))
    return true;
  return false;
}

bool
isIn(const qgar::DPoint&				p,
     const qgar::ConnectedComponents::image_type&	img)
{
  int ux = unsigned(round(p.x()));
  int uy = unsigned(round(p.y()));

  if ((ux > 0) && (ux < img.width()) &&
      (uy > 0) && (uy < img.height()))
    return true;
  return false;
}

bool
isInObject(const qgar::DPoint&	p,
	   const qgar::BinaryImage&	img)
{
  int ux = unsigned(round(p.x()));
  int uy = unsigned(round(p.y()));

  if ((ux > 0) && (ux < img.width()) &&
      (uy > 0) && (uy < img.height()) &&
      img.pixel(ux, uy) == OBJCOLOR)
    return true;
  return false;
}

unsigned
getcolor(const qgar::DPoint&		p,
	 const qgar::BinaryImage&	img)
{
  int ux = unsigned(round(p.x()));
  int uy = unsigned(round(p.y()));
  return img.pixel(ux, uy);
}

qgar::IPoint
DPointToIPoint(const qgar::DPoint&	p)
{
  return qgar::IPoint(unsigned(round(p.x())), unsigned(round(p.y())));
}

qgar::DPoint
getnext(const qgar::DPoint	&p,
	const double&		slope,
	const double&		bias,
	const int&		step,
	const unsigned&		axis)
{
  qgar::DPoint res = p;
  if (axis == AXIS_X)
    res.setXY(p.x() + step, (p.x() + step) * slope + bias);
  else
    res.setXY((p.y() + step - bias) / slope, p.y() + step);
  return res;
}

struct s_line
analyseLine(const double&		slope,
	    const double&		bias,
	    const qgar::DPoint&		start,
	    const qgar::BinaryImage&	image,
	    const unsigned		axis)
{
  struct s_line	data(slope, bias, start);

  unsigned		currentcolor = getcolor(start, image);
  unsigned		newcolor;
  int			step = -1;
  unsigned		size = 0;
  unsigned		cpt = 0;

  unsigned		stripewidth = unsigned(floor(image.width() * 8 / 100));

  for (unsigned direction = 0; direction < 2; direction++, step += 2)
    {
      qgar::DPoint	p = start, old = start;
      while ( isInObject(p, image))
	{
	  old = p;
	  p = getnext(p, slope, bias, step, axis);
	}
      data.objborder[direction] = old;

      if  (isIn(p, image))
	currentcolor = getcolor(p, image);
      cpt = 0;
      while (isIn(p, image))
	{
	  cpt++;
	  newcolor = getcolor(p, image);
	  if (newcolor != currentcolor)
	    {
	      if (size > stripewidth)
		data.count += 1;
	      size = 0;
	      currentcolor = newcolor;
	    }
	  else
	    size++;
	  old = p;
	  p = getnext(p, slope, bias, step, axis);
	}
      data.imgborder[direction] = old;
    }
  data.finalize();
  return data;
}

void
witchSide(const std::vector< struct s_line >&	data,
	  const unsigned&			index,
	  s_handinfo&				info)
{
  int side1= 0;
  int side2 = 0;
  unsigned cpt1 = 0;
  unsigned cpt2 = 0;

//   std::cout << "main line bias: " << data[index].bias << std::endl;
//   std::cout << "bias 1: " << data[0].bias << std::endl;

  for (unsigned i = 0; i < data.size(); ++i)
    if (i < index)
      {
	side1 += data[i].count;
	cpt1++;
      }
    else
      {
	side2 += data[i].count;
	cpt2++;
      }
//   std::cout << "bias N: " << data[data.size() - 1].bias << std::endl;
//   std::cout << std::endl << "sides: "
// 	    << side1 << "(1 " << cpt1 << ") " << side2
// 	    << "(-1 " << cpt2 << ")" << std::endl;
  double val = data[0].start.x() * data[index].slope +
    data[index].bias - data[0].start.y();
  if (side1 > side2)
    info.absside = (val > 0) ? 1 : -1;
  else
    info.absside = (val > 0) ? -1 : 1;
  info.side = (side1 > side2) ? 1 : -1;
  //info.absside = info.side;
}

qgar::BinaryImage*
cutWrist(const qgar::BinaryImage&	shape,
	 const struct s_line&		line,
	 const int&			side)
{
  qgar::BinaryImage*	newshape = new qgar::BinaryImage(shape.width(),
							 shape.height());
  qgar::BinaryImage::value_type* pMapShape = shape.pPixMap();
  qgar::BinaryImage::value_type* pMapRes = newshape->pPixMap();

  int width = shape.width();
  int height = shape.height();
  int size = width * height;

  //     std::cout << "cut wrist: slope: " << line.slope
  //       	      << " bias: " << line.bias << std::endl;
  //     std::cout << "\t(0,0): " << ((line.bias * side) > 0 ? 1 : 0) << " (0,H): "
  // 	      << (((line.bias - height) * side) > 0 ? 1 : 0) << std::endl;
  for (int iCnt = 0 ; iCnt < size ; ++iCnt, ++pMapShape, ++pMapRes)
    {
      int i = iCnt % width;
      int j = iCnt / width;
      double val = i * line.slope + line.bias - j;
      if (val  * side > 0)
	*pMapRes = *pMapShape;
      else
	*pMapRes = BGCOLOR;
    }

  return newshape;
}

unsigned
findLargest(const std::vector< struct s_line >&	variation)
{
  unsigned	max = 2;

  // FIXME : I put a margin because some binary
  // pictures got a black line bellow.
  // this line is the largest of the hand
  for (unsigned i = 2; i < variation.size() - 2; ++i)
    if (variation[i].objectwidth > variation[max].objectwidth)
      max = i;
  return max;
}


unsigned
findClosest(const unsigned&			start,
	    const std::vector< struct s_line >&	variation,
	    const struct s_handinfo&		info)
{
  if (isInObject(variation[start].middle, *info.shape))
    return start;
  unsigned newpoint = start;
  while ((newpoint > 0) && (newpoint < variation.size()) &&
	 !isInObject(variation[newpoint].middle, *info.shape))
    newpoint -= info.absside;

  if (!isInObject(variation[newpoint].middle, *info.shape))
    {
      std::cerr << "error: can't find a point in object from ("
		<< variation[start].middle << ')' << std::endl;
      exit (-1);
    }
  return newpoint;
}

unsigned
findWristPoint(const unsigned&				indexmax,
	       const std::vector< struct s_line >&	variation,
	       const struct s_handinfo&			info)
{
  // find wrist line
  unsigned	idx = indexmax;
  unsigned	idxmin = indexmax;

  // stop one line before image border since we need a point
  // of this line in hand and the next one will be the wrist
  // line (in white).
  for (; (idx > 1) && (idx < (variation.size() - 1));
       idx += info.side)
    {
      if (variation[idx].objectwidth < variation[idxmin].objectwidth)
	idxmin = idx;
      if (variation[idx].objectwidth < (variation[indexmax].objectwidth / 2))
	break;
    }

  //std::cout << "idx: " << idx << std::endl;

  if ((idx < 2) || (idx > variation.size() - 2))
    idx = idxmin;

  if (isInObject(variation[idx].middle, *info.shape))
    return idx;
  unsigned closest = idx;
  while ((closest > 0) && (closest < variation.size()) &&
	 !isInObject(variation[closest].middle, *info.shape))
    closest -= info.absside;
  if (!isInObject(variation[closest].middle, *info.shape))
    {
      std::cerr << "error: can't find a point in object from ("
		<< variation[idx].middle << ')' << std::endl;
      exit (-1);
    }
  return closest;
}

bool
isAtBorder(const qgar::DPoint&		p,
	   const qgar::BinaryImage&	shape)
{
  int ix = int(round(p.x()));
  int iy = int(round(p.y()));
  unsigned count = 0;
  unsigned value = shape.pixel(ix, iy);
//   std::cout << std::endl << "pt: ix: " << ix << " iy: " << iy
// 	    << " color: " << value << std::endl;
  if (value != OBJCOLOR)
    {
      //      std::cout << "\tWrong color" << std::endl;
      return false;
    }

  if ((ix == 0) || (iy == 0) ||
      (ix == shape.width()) || (iy == shape.height()))
    {
      //std::cout << "\tImage border" << std::endl;
      return true;
    }

  for (int i = -1; i < 2; ++i)
    for (int j = -1; j < 2; ++j)
      {
	value = shape.pixel(ix + i, iy + j);
	if (value == BGCOLOR)
	  count++;
      }

  //  std::cout << "count: " << count << std::endl;
  if (count > 0)
    return true;
  return false;
}

// find points at image border on the line
// corresponding to a given slope and bias.
qgar::ISegment
getISegment(const double&		slope,
	    const double&		bias,
	    const qgar::BinaryImage&	img)
{
  qgar::DPoint	pts[4];

  // just for drawing purpose
  double tslope = slope;
  if (!slope)
    tslope += EPSILON;

  double W = double(img.width() - 1);
  double H = double(img.height() - 1);

  pts[0].setXY(1, slope + bias);
  pts[1].setXY(W, W * slope + bias);
  pts[2].setXY((1 - bias) / tslope, 1);
  pts[3].setXY((H - bias) / tslope, H);

  unsigned i = 0;
  while ((i < 4) && !isIn(pts[i], img))
    i++;
  assert(i < 4);
  int ux1 = int(round(pts[i].x()));
  int uy1 = int(round(pts[i].y()));
  i++;
  while ((i < 4) && !isIn(pts[i], img))
    i++;
  assert(i < 4);
  int ux2 = int(round(pts[i].x()));
  int uy2 = int(round(pts[i].y()));
  return qgar::ISegment(ux1, uy1, ux2, uy2);
}

// find points at image border on the line
// corresponding to a given slope and bias.
qgar::DSegment
getDSegment(const double&		slope,
	    const double&		bias,
	    const qgar::BinaryImage&	img)
{
  qgar::DPoint	pts[4];

  // just for drawing purpose
  double tslope = slope;
  if (!slope)
    tslope += EPSILON;

  double W = double(img.width() - 1);
  double H = double(img.height() - 1);

  pts[0].setXY(1, slope + bias);
  pts[1].setXY(W, W * slope + bias);
  pts[2].setXY((1 - bias) / tslope, 1);
  pts[3].setXY((H - bias) / tslope, H);

  unsigned i = 0;
  while ((i < 4) && !isIn(pts[i], img))
    i++;
  assert(i < 4);
  double ux1 = pts[i].x();
  double uy1 = pts[i].y();
  i++;
  while ((i < 4) && !isIn(pts[i], img))
    i++;
  assert(i < 4);
  double ux2 = pts[i].x();
  double uy2 = pts[i].y();
  return qgar::DSegment(ux1, uy1, ux2, uy2);
}


void
findDirection(s_handinfo&		info)
{
  qgar::DPoint pmean(info.handaxis.xmean, info.handaxis.ymean);
  // FIXME : we should select the axis directly with the slope,
  // instead of trying each axis :
  struct s_line mainline_x = analyseLine(info.handaxis.slope,
					 info.handaxis.bias, pmean,
					 *info.shape, AXIS_X);
  struct s_line mainline_y = analyseLine(info.handaxis.slope,
					 info.handaxis.bias, pmean,
					 *info.shape, AXIS_Y);
  struct s_line mainline(info.handaxis.slope, info.handaxis.bias, pmean);
  if ((mainline_x.xmax - mainline_x.xmin) >
      ((mainline_y.ymax - mainline_y.ymin)))
    mainline = mainline_x;
  else
    mainline = mainline_y;

  double slope = - 1 / info.handaxis.slope;
  qgar::DPoint p = mainline.start;
  unsigned axis = mainline.largest_dir;
  struct s_line line(0, 0, pmean);

  while (isIn(p, *info.shape))
    {
      double bias = p.y() - slope * p.x();
      if (axis == AXIS_X)
	line = analyseLine(slope, bias, p, *info.shape, AXIS_Y);
      else
	line = analyseLine(slope, bias, p, *info.shape, AXIS_X);
      info.variation.push_back(line);
      p = getnext(p, info.handaxis.slope, info.handaxis.bias, 1, axis);
    }
  info.indexmax = findLargest(info.variation);
  info.largestpalmpt = info.variation[info.indexmax].middle;
  witchSide(info.variation, info.indexmax, info);

  //   std::cout << "info side: " << info.side << " abs side: "
  //     	    << info.absside << std::endl;
  //   std::cout << "largest idx: " << indexmax << " / " << info.variation.size()
  // 	    << std::endl;
}

void
wrist(s_handinfo&		info)
{
  // FIXME : virer le poignet apres suppression avoir boucher les
  // trous sinon les mesures ne sont pas correctes.
  info.startidx = findWristPoint(info.indexmax, info.variation, info);;
  //unsigned idx = findWristPoint(info.indexmax, info.variation, info);
  info.startpoint = info.variation[info.startidx].middle;

//   std::cout << std::endl;
//   std::cout << "\tidxmax: " << info.indexmax << " idx: " << idx << std::endl;
//   std::cout << "\tmiddle: " << info.variation[idx].middle << std::endl;
//   std::cout << "\tmiddle: " << info.variation[idx + info.side].middle << std::endl;

  //saveImg(*info.shape, info.out + "-wrist.pbm", info);
  qgar::BinaryImage *newshape = cutWrist(*info.shape,
    					 info.variation[info.startidx + info.side],
    					 info.absside);
  // replace shape
  delete info.shape;
  info.shape = newshape;
}
