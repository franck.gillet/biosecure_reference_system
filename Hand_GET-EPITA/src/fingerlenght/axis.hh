// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef __AXIS__HH__
#define __AXIS__HH__

#include	<qgarlib/ConnectedComponents.H>

#include "handinfo.hh"
#include "dist.hh"


bool
sort_lines(const struct s_line&	line1,
	   const struct s_line&	line2);

bool
isIn(const qgar::DPoint&	p,
     const qgar::BinaryImage&	img);

bool
isIn(const qgar::DPoint&				p,
     const qgar::ConnectedComponents::image_type&	img);

bool
isInObject(const qgar::DPoint&	p,
	   const qgar::BinaryImage&	img);

unsigned
getcolor(const qgar::DPoint&		p,
	 const qgar::BinaryImage&	img);

bool
isAtBorder(const qgar::DPoint&		p,
	   const qgar::BinaryImage&	shape);

qgar::IPoint
DPointToIPoint(const qgar::DPoint&	p);

qgar::DPoint
getnext(const qgar::DPoint	&p,
	const double&		slope,
	const double&		bias,
	const int&		step,
	const unsigned&	axis);

struct s_line
analyseLine(const double&		slope,
	    const double&		bias,
	    const qgar::DPoint&		start,
	    const qgar::BinaryImage&	image,
	    const unsigned		axis);

void
witchSide(const std::vector< struct s_line >&	data,
	  const unsigned&			index,
	  s_handinfo&				info);

qgar::BinaryImage*
cutWrist(const qgar::BinaryImage&	shape,
	 const struct s_line&		line,
	 const int&			side);

qgar::ISegment
getISegment(const double&		slope,
	    const double&		bias,
	    const qgar::BinaryImage&	img);

qgar::DSegment
getDSegment(const double&		slope,
	    const double&		bias,
	    const qgar::BinaryImage&	img);

void
findDirection(s_handinfo&		info);

void
wrist(s_handinfo&		info);

#endif
