// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <string>
// qgar include
#include <qgarlib/GenImage.H>
#include <qgarlib/PbmFile.H>
#include <qgarlib/PgmFile.H>
#include <qgarlib/QgarApp.H>
#include <qgarlib/Histogram.H>
#include <qgarlib/GenPoint.H>
#include <qgarlib/Component.H>
#include <qgarlib/ConnectedComponents.H>

#include "handinfo.hh"
#include "axis.hh"

static inline
qgar::GreyLevelImage*
prepareImg(const qgar::GreyLevelImage&		sourceImg,
	   unsigned				max)
{
  qgar::GreyLevelImage* outputImg = new qgar::GreyLevelImage(sourceImg);
  qgar::GreyLevelImage::value_type* pMapCont = outputImg->pPixMap();
  unsigned size = sourceImg.width() * sourceImg.height();

  // remove pixel between max and 255
  for (unsigned iCnt = 0 ; iCnt < size ; ++iCnt, ++pMapCont)
    *pMapCont = (*pMapCont > max) ? max : *pMapCont;

  return outputImg;
}

static inline
void
taintImg(qgar::GreyLevelImage&			outputImg,
	 const std::vector< qgar::IPoint >&	points,
	 const unsigned				greylevel)
{
  std::vector< qgar::IPoint >::const_iterator	ipoint = points.begin();
  for (ipoint = points.begin(); ipoint != points.end(); ++ipoint)
    {
      if (ipoint->x() < outputImg.width())
	if (ipoint->y() < outputImg.height())
	  outputImg.setPixel(ipoint->x(), ipoint->y(), greylevel);
    }
}

static inline
void
taintImg(qgar::GreyLevelImage&			outputImg,
	 const std::list< unsigned >&		index,
	 const std::vector< qgar::IPoint >&	points,
	 const unsigned				greylevel)
{
  std::list< unsigned >::const_iterator	iindex = index.begin();
  for (iindex = index.begin(); iindex != index.end(); ++iindex)
    if (*iindex)
      outputImg.setPixel(points[*iindex].x(),
			 points[*iindex].y(),
			 greylevel);
}

void
savePgmImg(const qgar::GreyLevelImage&	outputImg,
	   const std::string&		filename)
{
  // save boundary image
  qgar::PgmFile outputFile(filename.c_str(),
			   outputImg.height(),
			   outputImg.width());
  outputFile.openWONLY();
  outputImg.save(outputFile);
  outputFile.close();
}

void
savePbmImg(const qgar::BinaryImage&	outputImg,
	   const std::string&		filename)
{
  // save boundary image
  qgar::PbmFile outputFile(filename.c_str(),
			   outputImg.height(),
			   outputImg.width());
  outputFile.openWONLY();
  outputImg.save(outputFile);
  outputFile.close();
}

void
saveImg(qgar::BinaryImage&	image,
	const std::string&	filename,
	s_handinfo&		info)
{
  qgar::BinaryImage		outputImg(image);

  // draw hand main line
  qgar::ISegment seg = getISegment(info.handaxis.slope, info.handaxis.bias, image);
  outputImg.draw(seg, BGCOLOR);

  // draw largestpalm line
  double slope =  -1 / info.handaxis.slope;
  double bias = info.largestpalmpt.y() - info.largestpalmpt.x() * slope;
  seg = getISegment(slope, bias, image);
  outputImg.draw(seg, BGCOLOR);

  // draw wrist cut line
  slope =  -1 / info.handaxis.slope;
  bias = info.startpoint.y() - info.startpoint.x() * slope;
  seg = getISegment(slope, bias, image);
  outputImg.draw(seg, BGCOLOR);

  // save boundary image
  qgar::PbmFile outputFile(filename.c_str(), outputImg.height(),
			   outputImg.width());
  outputFile.openWONLY();
  outputImg.save(outputFile);
  outputFile.close();
}

void
saveBoundary(const std::string&	filename,
	     s_handinfo&	info)
{
  qgar::GreyLevelImage* outputImg = prepareImg(*info.appearance, 253);
  taintImg(*outputImg, info.oBoundary, 254);
  savePgmImg(*outputImg, info.out + filename);
  delete outputImg;
}

void
saveImg(const std::string&			filename,
	s_handinfo&				info)
{
  qgar::ISegment	seg;
  qgar::GreyLevelImage* outputImg = prepareImg(*info.appearance, 253);
  taintImg(*outputImg, info.oBoundary, 254);

  // hand main line
  seg = getISegment(info.handaxis.slope, info.handaxis.bias, *outputImg);
  outputImg->draw(seg, 254);

  // largest palm line
  double slope =  -1 / info.handaxis.slope;
  double bias = info.largestpalmpt.y() - info.largestpalmpt.x() * slope;
  seg = getISegment(slope, bias, *outputImg);
  outputImg->draw(seg, 254);

  // wrist line
  bias = info.startpoint.y() - info.startpoint.x() * slope;
  seg = getISegment(slope, bias, *outputImg);
  outputImg->draw(seg, 254);

  std::list< unsigned > points;
  points.push_back(1);
  for (unsigned finger = 0; finger < 5; ++finger)
    {
      points.push_back(info.tips[finger]);
      for (unsigned j = 0; j < 2; ++j)
	points.push_back(info.valley[finger][j]);

      if (info.p1[finger].x() != 0)
	{
	  unsigned x1 = unsigned(info.p1[finger].x());
	  unsigned y1 = unsigned(info.p1[finger].y());
	  unsigned x2 = unsigned(info.p2[finger].x());
	  unsigned y2 = unsigned(info.p2[finger].y());
	  qgar::ISegment seg1(x1, y1, x2, y2);
	  outputImg->draw(seg1, 254);
	}
      qgar::ISegment seg2(info.oBoundary[info.valley[finger][0]].x(),
			  info.oBoundary[info.valley[finger][0]].y(),
			  info.oBoundary[info.valley[finger][1]].x(),
			  info.oBoundary[info.valley[finger][1]].y());
      outputImg->draw(seg2, 254);
    }

  taintImg(*outputImg, points, info.oBoundary, 255);
  savePgmImg(*outputImg, info.out + filename);
  delete outputImg;
}
