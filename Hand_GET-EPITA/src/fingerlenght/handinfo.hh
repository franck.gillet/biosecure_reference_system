// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef __HANDINFO_HH__
#define __HANDINFO_HH__

#define	BGCOLOR		0
#define	OBJCOLOR	1

#define EPSILON	1.2207031250000000e-04

// qgar include
#include <qgarlib/GenImage.H>
#include <qgarlib/PbmFile.H>
#include <qgarlib/PgmFile.H>
#include <qgarlib/QgarApp.H>
#include <qgarlib/Histogram.H>
#include <qgarlib/GenPoint.H>

#include "filenameutils.hh"
#include "eigenvalues.hh"
#include "dist.hh"

#define	AXIS_X	0
#define AXIS_Y	1

struct s_line
{
  qgar::DPoint		middle;
  double		slope;
  double		bias;
  qgar::DPoint		start;
  qgar::DPoint		objborder[2];
  qgar::DPoint		imgborder[2];
  unsigned		count;
  double		xmin;
  double		xmax;
  double		ymin;
  double		ymax;
  unsigned		largest_dir;
  double		objectwidth;

  s_line(double	slope,
	 double	bias,
	 qgar::DPoint	middle)
    : middle (middle), slope (slope), bias (bias)
  {
    count = 0;
  }

  void
  finalize()
  {
    xmin = (imgborder[0].x() < imgborder[1].x()) ?
      imgborder[0].x() : imgborder[1].x();
    xmax = (imgborder[0].x() < imgborder[1].x()) ?
      imgborder[1].x() : imgborder[0].x();
    ymin = (imgborder[0].y() < imgborder[1].y()) ?
      imgborder[0].y() : imgborder[1].y();
    ymax = (imgborder[0].y() < imgborder[1].y()) ?
      imgborder[1].y() : imgborder[0].y();
    if ((xmax - xmin) > (ymax - ymin))
      {
	largest_dir = AXIS_X;
	start = (imgborder[0].x() < imgborder[1].x()) ?
	  imgborder[0] : imgborder[1];
      }
    else
      {
	largest_dir = AXIS_Y;
	start = (imgborder[0].y() < imgborder[1].y()) ?
	  imgborder[0] : imgborder[1];
      }
    objectwidth = computeEuclidiandist(objborder[0], objborder[1]);
  }
};

struct s_handinfo
{
  // FINGERS
  unsigned		tips[5];
  const unsigned	nbtips;
  unsigned		nbtipsfound;

  unsigned		valley[5][2];
  const unsigned	nbvalley;
  unsigned		nbvalleyfound;

  // obsolete
  unsigned		phal_up[5][2];
  unsigned		phal_down[5][2];

  struct stat		stats[6];
  qgar::DPoint		p1[6];
  qgar::DPoint		p2[6];

  static const unsigned	DMAX = 100;
  double		histo[5][DMAX];
  unsigned		histocard[5];

  // HAND
  const std::string&	name;   //input name
  std::string		out;	//output directory + image name without extension
  const std::string&	prefix; //output directory

  qgar::GreyLevelImage*	appearance;
  qgar::BinaryImage*	shape;

  std::vector< qgar::IPoint >	oBoundary;
  std::vector< unsigned >	oBoundaryDirection;
  std::vector< double >		dist;
  double			maxdist;

  struct stat			handaxis;
  qgar::DPoint			largestpalmpt;
  qgar::DPoint			startpoint;

  int				side;
  int				absside;
  std::vector< s_line >		variation;
  unsigned			indexmax;
  unsigned			startidx;

  // minimum size of component to be a finger
  unsigned			mincomp;

  s_handinfo(const std::string&	name,
	     const std::string&	prefix)
    : nbtips (5), nbvalley (10), name (name), prefix (prefix)
  {
    std::string filename = shortname2(name);
    out = filename.substr(0, filename.length() - 4);
    out = prefix + shortname(out);

    maxdist = 0;

    nbtipsfound   = 0;
    nbvalleyfound = 0;

    for (unsigned finger = 0; finger < 5; ++finger)
      {
	tips[finger] = 0;
	for (unsigned j = 0; j < 2; ++j)
	  {
	    valley[finger][j]    = 0;
	    phal_down[finger][j] = 0;
	    phal_up[finger][j]   = 0;
	  }
	for (unsigned i = 0; i < DMAX; ++i)
	  histo[finger][i] = 0;
	histocard[finger] = 0;
      }
    appearance = NULL;
    shape      = NULL;

    mincomp = 0;
  }

  int
  getmincompsize()
  {
    if (mincomp == 0)
      if (appearance != NULL)
	mincomp = int(round((appearance->height() / 8) *
			    (appearance->width() / 10)));
      else
	std::cerr << "Need to load a picture first!" << std::endl;
    return mincomp;
  }

  ~s_handinfo()
  {
    if (appearance != NULL)
      delete appearance;
    if (shape != NULL)
      delete shape;
  }
};

struct s_extremum {
  unsigned	idx;
  float		dist;
  qgar::DPoint	p;

  s_extremum(const unsigned&		_idx,
	     const float&		_dist,
	     const qgar::DPoint&	_p)
    : idx ( _idx ), dist ( _dist ), p ( _p )
  {
    ;
  }
};

#endif
