// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// qgar include
#include <qgarlib/GenImage.H>
//#include <qgarlib/PbmFile.H>
//#include <qgarlib/PgmFile.H>
//#include <qgarlib/QgarApp.H>
//#include <qgarlib/Histogram.H>

#include <vector>
#include <assert.h>

class MRF
{
  const std::vector< qgar::IPoint >&	contour;
  const std::vector< unsigned >&	dir;
  const std::vector< unsigned >&	points;
  std::vector< unsigned >		result;

  inline
  double
  geodesicDist(const unsigned&	p1,
	       const unsigned&	p2)
  {
    unsigned pmin = (p1 < p2) ? p1 : p2;
    unsigned pmax = (p1 < p2) ? p2 : p1;
    assert(pmin != pmax);
    double	inc = sqrt(2);
  
    double result = 0;
    for (unsigned i = pmin; i < pmax; ++i)
      result += (dir[i] % 2) ? inc : 1;
    return result;
  }

  inline
  double
  euclidianDist(const unsigned &s,
		       const unsigned &p)
  {
    return sqrt(pow(contour[s].x() - contour[p].x(), 2) + 
		pow(contour[s].y() - contour[p].y(), 2));
  }

  inline
  double
  energy(const unsigned&	p)
  {
    unsigned			pbefore = p - 1;
    unsigned			pafter = p + 1;

    const double A = 1;
    // FIXME : point pas glop
    double e = geodesicDist(points[p], result[p]);
    e += std::fabs(euclidianDist(result[p], result[pbefore]) - 
		   euclidianDist(result[p], result[pafter]));
    e += A * std::fabs(geodesicDist(result[p], result[pbefore]) - 
		       geodesicDist(result[p], result[pafter]));
    return e;
  }

public:

  MRF(const std::vector< qgar::IPoint >&	contour,
      const std::vector< unsigned >&		dir,
      const std::vector< unsigned >&		points)
    : contour (contour), dir (dir), points (points)
  {
    // initialization with 'points' vector argument.
    result.assign(points.begin(), points.end());
  }

  std::vector< unsigned >&
  doit()
  {
    // select site
    const double NBSITE = 9.0;
    unsigned site = 1 + (unsigned)(NBSITE * rand() / (RAND_MAX + 1.0));
    double e_before = energy(result[site]);

    unsigned max = contour.size();
    unsigned newplace = 1 + (unsigned) (max * rand()/(RAND_MAX + 1.0));
    double e_new = energy(newplace);

    return result;
  }
};
