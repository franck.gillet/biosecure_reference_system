// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <assert.h>
#include <queue>

const int	N = 256;
const int	PLATEAU = -10000*10000+1;

class watershed1D
{

  int*				P;
  double*&			dist;
  int				size;

public:

  watershed1D(double*&		_dist,
	      unsigned&		_size)
    : dist ( _dist )
  {
    size = _size;
    P = new int[size];

    for (int p = 0; p < size; ++p)
      P[p] = int(dist[p]);
  }

  ~watershed1D()
  {
    delete []P;
  }

  int *
  getLabel()
  {
    return P;
  }

private:

  int
  tarjan_find_root(const int	i)
  {
    int root;
    int trail;
    int lead;

    assert(P[i] >= 0);

    for (root = i ;P[root] != root;)
      {
	assert(P[root] >= 0);
	assert(dist[root] >= dist[P[root]]);
	root = P[root];
      }
    for (trail = i; trail != root; trail = lead)
      {
	lead = P[trail];
	P[trail] = root;
      }
    return root;
  }

public:

  void
  doit()
  {

    for (int p = 0; p < size; ++p)
      {
        int q = p;
        for (int np = -1; np < 2; np += 2)
          {
            int p_prime = p + np;
	    if ((p_prime >= 0) && (p_prime < size))
	      if (dist[p_prime] < dist[p])
		if (dist[p_prime] < dist[q])
		  q = p_prime;
          }
        if (p != q)
          P[p] = q;
        else
          P[p] = PLATEAU;
      }

    std::queue<int>	Queue;
    for (int p = 0; p < size; ++p)
      if (P[p] == PLATEAU)
	for (int np = -1; np < 2; np += 2)
	  {
	    int p_prime = p + np;
	    if ((p_prime >= 0) && (p_prime < size))
	      if (P[p_prime] != PLATEAU)
		if (dist[p_prime] == dist[p])
		  {
		    Queue.push(p_prime);
		    break;
		  }
	  }

    while (Queue.empty() == false)
      {
	int p = Queue.front();
	Queue.pop();
	for (int np = -1; np < 2; np += 2)
	  {
	    int p_prime = p + np;
	    if ((p_prime >= 0) && (p_prime < size))
	      if (P[p_prime] == PLATEAU)
		if (dist[p_prime] == dist[p])
		  {
		    P[p_prime] = p;
		    Queue.push(p_prime);
		  }
	  }
      }

    for (int p = 0; p < size; ++p)
      if (P[p] == PLATEAU)
	{
	  P[p] = p;
	  int p_prime = p - 1;
	  if (p_prime >= 0)
	    if (dist[p_prime] == dist[p])
	      P[p] = P[p_prime] = std::min(tarjan_find_root(p),
					   tarjan_find_root(p_prime));
	}

    for (int p = 0; p < size; ++p)
      P[p] = tarjan_find_root(p);
  } // doit

}; // class
