// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include	"dist.hh"
#include	<extractor.hh>
#include	<qgarlib/GenImage.H>
#include	<iostream>
#include	<vector>

double
computedist(const qgar::IPoint &s,
	    const qgar::IPoint &p)
{
  return pow(s.x() - p.x(), 2) + pow(s.y() - p.y(), 2);
}


double
computeEuclidiandist(const qgar::IPoint &s,
		     const qgar::IPoint &p)
{
  return sqrt(pow(s.x() - p.x(), 2) + pow(s.y() - p.y(), 2));
}


double
computeEuclidiandist(const qgar::DPoint &s,
		     const qgar::DPoint &p)
{
  return sqrt(pow(s.x() - p.x(), 2) + pow(s.y() - p.y(), 2));
}

double
KL5f(const std::vector< double >&	v1,
     const std::vector< double >&	v2)
{
  assert(v1.size() == v2.size());
  double result = 0;
  for (unsigned i = 0; i < v1.size(); ++i)
    {
      if (v2[i] && v1[i])
	result += v1[i] * log(v1[i] / v2[i]) + v2[i] * log(v2[i] / v1[i]);
    }
  return result;
}

double
KL4f(const std::vector< double >&	v1,
       const std::vector< double >&	v2)
{
  if ((v1.size() == 0) || (v2.size() == 0))
    return 1000;
  assert(v1.size() == v2.size());
  unsigned DMAX = v1.size() / 5;
  double result = 0;
  // start at DMAX to dump thumb
  for (unsigned i = DMAX; i < v1.size(); ++i)
    {
      if (v2[i] && v1[i])
	result += v1[i] * log(v1[i] / v2[i]) + v2[i] * log(v2[i] / v1[i]);
      //       else
      // 	result += 1000;
    }
  return result;
}

double
KL2f(const std::vector< double >&	v1,
     const std::vector< double >&	v2)
{
  if ((v1.size() == 0) || (v2.size() == 0))
    return 1000;
  assert(v1.size() == v2.size());
  unsigned DMAX = v1.size() / 5;
  double result = 0;
  // start at 2 * DMAX to dump thumb and index
  // stop at size - DMAX to dump little finger.
  for (unsigned i = DMAX * 2; i < v1.size() - DMAX; ++i)
    {
      if (v2[i] && v1[i])
	result += v1[i] * log(v1[i] / v2[i]) + v2[i] * log(v2[i] / v1[i]);
      //       else
      // 	result += 1000;
    }
  return result;
}

double
KLbest3(const std::vector< double >&	v1,
	const std::vector< double >&	v2)
{
  const unsigned nbfing = 3;
  assert(v1.size() == v2.size());
  unsigned DMAX = v1.size() / 5;
  double tmpresult = 0;
  std::vector< double >	result;
  unsigned i = 0;
  unsigned max = 0;

  for (unsigned finger = 0; finger < 5; ++finger)
    {
      i = DMAX * finger;
      max = (DMAX * finger + DMAX);
      tmpresult = 0;
      for (; i < max; ++i)
	if (v2[i] && v1[i])
	  tmpresult += v1[i] * log(v1[i] / v2[i]) + v2[i] * log(v2[i] / v1[i]);
      result.push_back(tmpresult);
    }
  std::sort(result.begin(), result.end());
  if (result[0] >= result[4])
    {
      for (unsigned finger = 0; finger < 5; ++finger)
	std::cout << result[finger] << " ";
      std::cout << std::endl;
    }
  assert(result[0] <= result[4]);

  tmpresult = 0;
  for (unsigned i = 0; i < nbfing; ++i)
    tmpresult += result[i];

  return tmpresult / nbfing;
}

double
KLbestNP(const std::vector< double >&	v1,
	 const std::vector< double >&	v2,
	 const unsigned			N,
	 const unsigned			P)
{
  const unsigned nbfing = N;
  assert(v1.size() == v2.size());
  unsigned DMAX = v1.size() / P;
  double tmpresult = 0;
  std::vector< double >	result;
  unsigned i = 0;
  unsigned max = 0;

  for (unsigned finger = 0; finger < P; ++finger)
    {
      i = DMAX * finger;
      max = (DMAX * finger + DMAX);
      tmpresult = 0;
      for (; i < max; ++i)
	if (v2[i] && v1[i])
	  tmpresult += v1[i] * log(v1[i] / v2[i]) + v2[i] * log(v2[i] / v1[i]);
      result.push_back(tmpresult);
    }
  std::sort(result.begin(), result.end());

  tmpresult = 0;
  i = 0;
  while (result[i] == 0)
    i++;
  unsigned count = nbfing;
  for (; i < P && count > 0; --count, ++i)
      tmpresult += result[i];

  return tmpresult / nbfing;
}

double
KLbest103(const std::vector< double >&	v1,
	  const std::vector< double >&	v2)
{
  return KLbestNP(v1, v2, 3, 10);
}

// double
// KLbest3(const std::vector< double >&	v1,
// 	const std::vector< double >&	v2)
// {
//   return KLbestNP(v1, v2, 3, 5);
// }

double
KLbest105(const std::vector< double >&	v1,
	  const std::vector< double >&	v2)
{
  return KLbestNP(v1, v2, 5, 10);
}

double
KLbest107(const std::vector< double >&  v1,
          const std::vector< double >&  v2)
{
  return KLbestNP(v1, v2, 7, 10);
}



void
mergeFeatures(std::vector< double >&	v1,
	      std::vector< double >&	v2)
{
//   if (v1.size() == 0)
//     {
//       v1.assign(v2.begin(), v2.end());
//       return ;
//     }
//   if (v2.size() == 0)
//     return;
//   assert(v1.size() == v2.size());
//   for (unsigned i = 0; i < v1.size(); ++i)
//     v1[i] = (v1[i] + v2[i]) / 2;

//   if ((v1.size() == 0) && (v2.size() == 0))
//     std::cout << "v1 et v2 nuls" << std::endl;
  unsigned DMAX = 100;
  if (v1.size() == 0)
    {
      v1.resize(DMAX * 5,0);
      //      std::cout << "v1 nul" << std::endl;
    }
  if (v2.size() == 0)
    {
      v2.resize(DMAX * 5,0);
      //std::cout << "v2 nul" << std::endl;
    }
  v1.insert(v1.end(), v2.begin(), v2.end());
}

distfunc
selectDist(const std::string&	progname)
{
  std::cout << "Dist: ";

  //size_type find(const string& s2, size_type pos1);
  unsigned loc = progname.find("-klbest3",0);
  if (loc != std::string::npos)
    {
      std::cout << "Kullback-Leibler (3 best fingers from 5)" << std::endl;
      return &KLbest3;
    }
  loc = progname.find("-kl5f",0);
  if (loc != std::string::npos)
    {
      std::cout << "Kullback-Leibler (5 fingers)" << std::endl;
      return &KL5f;
    }
  loc = progname.find("-kl4f",0);
  if (loc != std::string::npos)
    {
      std::cout << "Kullback-Leibler (4 fingers without thumb)" << std::endl;
      return &KL4f;
    }
  loc = progname.find("-kl2f",0);
  if (loc != std::string::npos)
    {
      std::cout << "Kullback-Leibler (middle and ring)" << std::endl;
      return &KL2f;
    }
  loc = progname.find("-klbest103",0);
  if (loc != std::string::npos)
    {
      std::cout << "Kullback-Leibler (3 best from 10)" << std::endl;
      return &KLbest103;
    }
  loc = progname.find("-klbest105",0);
  if (loc != std::string::npos)
    {
      std::cout << "Kullback-Leibler (5 best from 10)" << std::endl;
      return &KLbest105;
    }
  loc = progname.find("-klbest107",0);
  if (loc != std::string::npos)
    {
      std::cout << "Kullback-Leibler (7 best from 10)" << std::endl;
      return &KLbest107;
    }
  // default distance
  std::cout << "Kullback-Leibler (3 best fingers from 5)" << std::endl;
  return &KLbest3;
}
