// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef __MORPHOMAT_HH__
#define __MORPHOMAT_HH__

#include "handinfo.hh"
#include "eigenvalues.hh"

std::vector< s_extremum >
FindPoints(const struct s_handinfo&	info,
	   const unsigned&		nbtofind);

std::vector< s_extremum >
FindPointsAuto(const struct s_handinfo&	info,
	       const unsigned&		nbtofind);

qgar::BinaryImage*
dilate(const qgar::BinaryImage&	src,
       const int&		size);

qgar::ConnectedComponents::image_type*
dilate(const qgar::ConnectedComponents::image_type&	src,
       const int&					size,
       unsigned						*label,
       struct stat*					stats,
       unsigned						nbcomp,
       struct s_handinfo&				info,
       const unsigned&					handlabel);

#endif // __MORPHOMAT_HH__
