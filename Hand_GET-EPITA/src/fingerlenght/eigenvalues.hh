// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef __EIGEN__HH__
#define __EIGEN__HH__

#include <vector>
#include <complex>

typedef std::complex<double> complex;

struct stat
{
  double	covar[2][2];
  double	xmean;
  double	ymean;
  double	card;

  double	eigenval[2];
  double	slope;
  double	bias;

  std::vector< qgar::IPoint >	points;

  stat()
  {
    for (unsigned i = 0; i < 2; ++i)
      {
	for (unsigned j = 0; j < 2; ++j)
	  covar[i][j] = 0.0;
	eigenval[i] = 0.0;
      }
    xmean = 0;
    ymean = 0;
    card = 0;
    slope = 0;
    bias = 0;
  }

  void
  add(unsigned&	i,
      unsigned&	j)
  {
    xmean += i;
    ymean += j;
    card++;
    points.push_back(qgar::IPoint(i, j));
  }

  void
  computeCovarMatrix()
  {
    if (card <= 1)
      std::cerr << "Division by zero" << std::endl;
    xmean /= card;
    ymean /= card;
    std::vector<qgar::IPoint>::iterator		p;
    for (p = points.begin(); p != points.end(); ++p)
      {
	covar[0][0] += (p->x() - xmean) * (p->x() - xmean);
	covar[1][1] += (p->y() - ymean) * (p->y() - ymean);
	covar[0][1] += (p->x() - xmean) * (p->y() - ymean);
      }

    covar[0][0] /= (card - 1);
    covar[1][1] /= (card - 1);
    covar[0][1] /= card;
    covar[1][0] = covar[0][1];
  }


  bool
  complexEigenvalues()
  {
    double t = covar[0][0] - covar[1][1];
    return (t * t < -4.0 * covar[0][1] * covar[1][0]);
  }

  void
  computeEigenvalues(complex		e[])
  {
    double u1 = 1;
    double u2 = covar[0][0] + covar[1][1];
    double u3 = covar[0][0] * covar[1][1] - covar[0][1] * covar[1][0];
    double D = u2 * u2 - 4 * (u1 * u3);

    if (D > 0)
      {
	// real and unequal
	double m = covar[0][0] + covar[1][1];
	double n = sqrt(D);
	e[0] = 0.5 * (m + n);
	e[1] = 0.5 * (m - n);
      }
    else if (D < 0)
      {
	// complex conjugate
	double m = 0.5 * (covar[0][0] + covar[1][1]);
	double n = 0.5 * sqrt(-D);
	e[0] = complex(m, n);
	e[1] = complex(m, -n);
      }
    else
      {
	// real and equal
	e[0] = e[1] = 0.5 * (covar[0][0] + covar[1][1]);
      }
  }

  int
  compute_eigenvalues()
  {
    if (complexEigenvalues())
      return 1;
    else
      {
 	complex e[2];
 	computeEigenvalues(e);
 	eigenval[0] = e[0].real();
 	eigenval[1] = e[1].real();

	assert(covar[0][1] != 0);
 	slope = (eigenval[0] - covar[0][0]) / covar[0][1];
 	bias = ymean - xmean * slope;
// 	std::cout << "slope = " << slope << std::endl;
// 	std::cout << "xmean = " << xmean << std::endl;
// 	std::cout << "ymean = " << ymean << std::endl;
// 	std::cout << "bias = " << bias << std::endl;
      }
    return 0;
  }

};

#endif
