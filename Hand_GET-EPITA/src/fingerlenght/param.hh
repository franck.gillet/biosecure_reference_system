// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef __PARAM__FILE__
#define __PARAM__FILE__

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include "filenameutils.hh"

struct s_param {
  // Verification:
  //  - training;
  //  - enrollment;
  //  - test;

  // Identification:
  //  - training;
  //  - client/enrollement files;
  //  - test composed by:
  //    - client/testfiles;
  //    - impostor;

  // Verification and Identification
  std::string trainingSetFileName;
  // Verification and Identification
  //std::string enrollmentSetFileName;
  // Verification and Identification
  std::string testSetFileName;
  // Identification only
  std::string impostorSetFileName;
  // Repartition betweeen enrollment and test
  unsigned		nbFileToEnroll;
  // Mode (verification or identification)
  bool ident;

  s_param()
  {
    ident = false;
    nbFileToEnroll = 1;
  }

  void
  toString()
  {
    std::cout << "Protocol files details:" << std::endl;
    std::cout << "\tMode = " << ((ident == true) ? "identification" : "verification")
	      << std::endl;
    std::cout << "\tTraining Set:   " << trainingSetFileName   << std::endl;
    std::cout << "\tTest Set:       " << testSetFileName       << std::endl;
    std::cout << "\tNb to enroll: " << nbFileToEnroll << std::endl;
    if (ident)
      std::cout << "\tImpostor Set:   " << impostorSetFileName  << std::endl;
  }

  void
  readInputFile(const std::string&	filename)
  {
    std::ifstream	ifs(filename.c_str());
    std::string		tmpstr;
    std::string		arg;
    std::string		value;
    unsigned		pos;

    if (!ifs)
      {
	std::cerr << "Error, Can't open file: " << filename << " exiting..."
		  << std::endl;
	exit (-1);
      }

    do
      {
	// read line
	std::getline(ifs, tmpstr);

	// preprocessing
	replaceTabBySpace(tmpstr);

	// if line is not empty
	if (!tmpstr.length())
	  continue;

	// this is a comment: do nothing. (Pattern: ^#)
	if (tmpstr[0] == '#')
	  continue;

	pos = tmpstr.find(' ',0);
	if (pos != std::string::npos)
	  {
	    arg = tmpstr.substr(0, pos);
	    value = tmpstr.substr(pos);
	    value = eatWhiteSpace(value);
	  }
	else
	  continue;

	std::stringstream strm(value);
	if (arg == "training")
	  strm >> trainingSetFileName;
	else if (arg == "test")
	  strm >> testSetFileName;
	else if (arg == "impostor")
	  strm >> impostorSetFileName;
	else if (arg == "nbtoenroll")
	  {
	    strm >> nbFileToEnroll;
	  }
	else if (arg == "mode")
	  {
	    if (value == "identification")
	      ident = true;
	  }
	else
	  {
	    std::cerr << "Unknown parameter: " << arg << std::endl;
	    exit (-1);
	  }
      }
    while (!ifs.eof());

    ifs.close();
  };

};

#endif // __PARAM__FILE__
