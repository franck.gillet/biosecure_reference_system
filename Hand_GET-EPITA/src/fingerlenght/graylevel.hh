// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef GRAYLVL__
#define GRAYLVL__

class GrayLevel
{
private :
  float *phist;
  int index;
  float omega;
  float mu;

public :

  GrayLevel(float*	_phist,
	    bool	first)
    : phist ( _phist )
  {
    if (first)
      {
	index = 1;
	omega = phist[index - 1];
	if (omega == 0)
	  mu = 0;
	else
	  mu =  1 * phist[index - 1] / omega;
      }
    else
      {
	index = 2;
	omega = 0;
	mu = 0;
	for (int i = index; i < 256 ; ++i)
	  {
	    omega +=  phist[i - 1];
	    mu +=  phist[i - 1] * i;
	  }
	if (omega == 0)
	  mu = 0;
	else
	  mu /= omega;
      }
  }

  void removeFromBeginning()
  {
    index++;
    mu = 0;
    omega = 0;

    for (int i = index; i < 256 ; ++i)
      {
	omega +=  phist[i - 1];
	mu +=  i * phist[i - 1];
      }
    if (omega == 0)
      mu = 0;
    else
      mu /= omega;
  }

  void addToEnd()
  {
    index++;
    mu = 0;
    omega = 0;
    for (int i = 1; i < index ; i++)
      {
	omega +=  phist[i - 1];
	mu +=  i * phist[i - 1];
      }
    if (omega == 0)
      mu = 0;
    else
      mu /= omega;
  }


  float getMu()
  {
    return mu;
  }

  float getOmega()
  {
    return omega;
  }

  int getThreshold()
  {
    return index;
  }
};

#endif
