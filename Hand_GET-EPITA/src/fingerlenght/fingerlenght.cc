// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "extractor.hh"

// qgar include
#include <qgarlib/qgarTypes.H>
#include <qgarlib/globalFunctions.H>
#include <qgarlib/GenImage.H>
#include <qgarlib/PbmFile.H>
#include <qgarlib/PgmFile.H>
#include <qgarlib/QgarApp.H>
#include <qgarlib/Histogram.H>
#include <qgarlib/GenQgarSegment.H>

#include <qgarlib/Component.H>
#include <qgarlib/ConnectedComponents.H>

// TOOLS:
// Otsu thresholding
#include "otsuthres.hh"
// Connected components
#include "concpnt.hh"
// Watershed
#include "watershed_bienek-1d.hh"
// Morphomat filter
#include "morphomat.hh"
// hand normalization process
#include "normalizehand.hh"
#include "mrf.hh"

#include "dist.hh"

// debug
#include "saveimg.hh"
#include "filenameutils.hh"

#include "handinfo.hh"
#include "eigenvalues.hh"
#include "axis.hh"

struct s_pair
{
  unsigned idx;
  unsigned size;

  s_pair(unsigned	idx,
	 unsigned	size)
    : idx (idx), size (size)
  {}

};

bool
sort_pair(const struct s_pair&	p1,
	  const struct s_pair&	p2)
{
  return p1.size > p2.size;
}

class FingerLenght : public Extractor
{

  private:

  bool
  findTips(s_handinfo&				info)
  {
    std::vector< s_extremum > tmp = FindPointsAuto(info, info.nbtips);
    info.nbtipsfound = tmp.size();

    for (unsigned i = 0; ((i < info.nbtipsfound) && (i < info.nbtips)); ++i)
      info.tips[i] = tmp[i].idx;

    if (info.nbtipsfound != info.nbtips)
      {
	std::cout << "\r        " << info.name << ": "
		  << "Wrong tips number:" << info.nbtipsfound << std::endl;
	return false;
      }
    return true;
  }

  bool
  findValleys(s_handinfo&				info)
  {
    //std::vector< s_extremum > tmp = FindPoints(info, 4);
    std::vector< s_extremum > tmp = FindPointsAuto(info, 6);
    info.nbvalleyfound = tmp.size() - 2;

    if (info.nbvalleyfound > 0)
      {
	info.valley[0][1] = tmp[1].idx;

	for (unsigned i = 1; i < info.nbvalleyfound; ++i)
	  info.valley[i][1] = info.valley[i + 1][0] = tmp[i + 1].idx;
      }

    if (info.nbvalleyfound != 4)
      {
	std::cout << "\r        " << info.name << ": "
		  << "Wrong valley number:" << info.nbvalleyfound << std::endl;
	return false;
      }
    // some points are merge
    info.nbvalleyfound = 7;
    return true;
  }

  // for index and little finger : we need to find a third point which
  // is not an inter-finger point. Find the boundary point with the
  // same euclidian dist from the initial point and the tips.
  unsigned
  findSymetricPoint(const unsigned&			tips,
		    const unsigned&			inter,
		    const std::vector< qgar::IPoint >&	boundary)
  {
    unsigned result = 0;
    double dist = computedist(boundary[tips], boundary[inter]);

    if (inter > tips)
      for (result = tips - 1; result > 0; --result)
	{
	  double rdist = computedist(boundary[tips], boundary[result]);
	  if (rdist > dist)
	    break;
	}
    else
      for (result = tips + 1; result < boundary.size(); ++result)
	{
	  double rdist = computedist(boundary[tips], boundary[result]);
	  if (rdist > dist)
	    break;
	}
    return result;
  }

  bool
  computeRemainingExtremum(struct s_handinfo&	info)
  {
    info.valley[0][0] = findSymetricPoint(info.tips[0],
					  info.valley[0][1],
					  info.oBoundary);
    info.valley[1][0] = findSymetricPoint(info.tips[1],
					  info.valley[1][1],
					  info.oBoundary);
    info.valley[4][1] = findSymetricPoint(info.tips[4],
					  info.valley[4][0],
					  info.oBoundary);

    if ((info.valley[0][0] == 0) ||
	(info.valley[0][0] >= info.tips[0]) ||
	(info.valley[0][1] > info.valley[1][0]) ||
	(info.valley[1][0] >= info.tips[1]) ||
	(info.valley[4][1] <= info.tips[4]) ||
	(info.valley[4][1] == info.oBoundary.size()))
      {
	std::cout << "\r        " << info.name << ": "
		  << "Error in point extrapolation."
		  << std::endl;
	return false;
      }
    info.nbvalleyfound = 10;
    return true;
  }

  void
  doMrfRegulation(struct s_handinfo&	info)
  {
    unsigned *tab = new unsigned[12];
    tab[1]  = info.tips[0];
    tab[4]  = info.tips[1];
    tab[6]  = info.tips[2];
    tab[8]  = info.tips[3];
    tab[10] = info.tips[4];

    tab[0]  = info.valley[0][0];
    tab[2]  = info.valley[0][1];
    tab[3]  = info.valley[1][0];
    tab[5]  = info.valley[1][1];
    tab[7]  = info.valley[2][1];
    tab[9]  = info.valley[3][1];
    tab[11] = info.valley[4][1];

    // MAP for points of interest
    MRF	mrf(info.oBoundary, info.oBoundaryDirection, tab);
    unsigned*	res = mrf.doit();

    info.tips[0] = res[1];
    info.tips[1] = res[4];
    info.tips[2] = res[6];
    info.tips[3] = res[8];
    info.tips[4] = res[10];

    info.valley[0][0] = res[0];
    info.valley[0][1] = res[2];
    info.valley[1][0] = res[3];
    info.valley[1][1] = info.valley[2][0] = res[5];
    info.valley[2][1] = info.valley[3][0] = res[7];
    info.valley[3][1] = info.valley[4][0] = res[9];
    info.valley[4][1] = res[11];
  }

  qgar::IPoint
  getMiddlePoint(const qgar::IPoint	&p1,
		 const qgar::IPoint	&p2)
  {
    qgar::IPoint p = p1;

    int dx = abs(p1.x() - p2.x()) / 2;
    int dy = abs(p1.y() - p2.y()) / 2;

    p.setX(std::min(p1.x(), p2.x()) + dx);
    p.setY(std::min(p1.y(), p2.y()) + dy);
    return p;
  }

  // find the point in the middle of two inter-finger points
  // and compute euclidian distance between this point and the
  // same finger tips.
  inline
  void
  computeFingerLength(struct s_handinfo&	info,
		      std::vector< double >&	features)
  {
    for (unsigned i = 0 ; i < 5; ++i)
      {
	qgar::IPoint left   = (info.oBoundary)[info.valley[i][0]];
	qgar::IPoint right  = (info.oBoundary)[info.valley[i][1]];
	qgar::IPoint top    = (info.oBoundary)[info.tips[i]];
	qgar::IPoint bottom = getMiddlePoint(left, right);
	features.push_back(computeEuclidiandist(bottom, top));

	qgar::IPoint ph_up_left = (info.oBoundary)[info.phal_up[i][0]];
	qgar::IPoint ph_up_right = (info.oBoundary)[info.phal_up[i][1]];
	features.push_back(computeEuclidiandist(ph_up_left, ph_up_right));

	qgar::IPoint ph_down_left = (info.oBoundary)[info.phal_down[i][0]];
	qgar::IPoint ph_down_right = (info.oBoundary)[info.phal_down[i][1]];
	features.push_back(computeEuclidiandist(ph_down_left, ph_down_right));
      }
  }

  double
  geodesicDist(const unsigned&				p1,
	       const unsigned&				p2,
	       const std::vector< unsigned >&		bounddir)
  {
    unsigned start = p1 < p2 ? p1 : p2;
    unsigned stop  = p1 < p2 ? p2 : p1;

    double dist = 0;
    for (unsigned p = start; p < stop; ++p)
      {
	unsigned dir = bounddir[p];
	dist += 1.0 + (sqrt(2.0) * double(dir % 2));
      }
    return dist;
  }

  unsigned
  geodesicFind(const unsigned&				start,
	       const double&				dist,
	       const std::vector< unsigned >&		bounddir)
  {
    unsigned p = start;
    double curdist = 0;
    while (curdist < dist)
      {
	unsigned dir = bounddir[p++];
	curdist += 1.0 + (sqrt(2.0) * double(dir % 2));
      }
    return p;
  }

  unsigned
  geodesicFindReverse(const unsigned&				start,
		      const double&				dist,
		      const std::vector< unsigned >&		bounddir)
  {
    unsigned p = start;
    double curdist = 0;
    while (curdist < dist)
      {
	unsigned dir = bounddir[p--];
	curdist += 1.0 + (sqrt(2.0) * double(dir % 2));
      }
    return p;
  }

  void
  findPhalanges(struct s_handinfo&		info)
  {
    double const ph1ratio = 0.35;
    double const ph2ratio = 0.70;

    for (unsigned i = 0; i < 5; ++i) // nbfinger
      {
	double dist = geodesicDist(info.tips[i], info.valley[i][0],
				   info.oBoundaryDirection);
	double ph1dist = ph1ratio * dist;
	double ph2dist = ph2ratio * dist;
	info.phal_down[i][0] = geodesicFind(info.valley[i][0],
					    ph1dist, info.oBoundaryDirection);
	info.phal_up[i][0] = geodesicFind(info.phal_down[i][0],
					  ph2dist - ph1dist,
					  info.oBoundaryDirection);

	dist = geodesicDist(info.tips[i], info.valley[i][1],
			    info.oBoundaryDirection);
	ph1dist = ph1ratio * dist;
	ph2dist = ph2ratio * dist;
	info.phal_down[i][1] = geodesicFindReverse(info.valley[i][1],
						   ph1dist,
						   info.oBoundaryDirection);
	info.phal_up[i][1] = geodesicFindReverse(info.phal_down[i][1],
						 ph2dist - ph1dist,
						 info.oBoundaryDirection);
	}
  }

  bool
  findPoints(s_handinfo&			info)
  {
    // FIXME: hard limit, to adapt with image size
    if (info.oBoundary.size() < 1000)
      {
	std::cout << "\r        " << info.name << ": "
		  << "Error in boundary tracing"
		  << std::endl;
	saveBoundary("-error-bt.pgm", info);
	return false;
      }

    findTips(info);

    // invert radial distance curve
    for (std::vector< double >::iterator idist = info.dist.begin();
	 idist != info.dist.end(); ++idist)
      *idist = info.maxdist - *idist;

    findValleys(info);

    if ((info.nbtipsfound   != info.nbtips) ||
	(info.nbvalleyfound != info.nbvalley - 3))
      {
	std::cout << "\tWrong point number." << std::endl;
	saveImg("-error-point.pgm", info);
	return false;
      }

    // find remaining points
    // fixme : we need to do this after mrf !

    if (computeRemainingExtremum(info) == false)
      {
	std::cout << "\tWrong point number." << std::endl;
	saveImg("-error-point.pgm", info);
	return false;
      };

//     for (unsigned finger = 0; finger < 5; ++finger)
//       std::cout << "\tfinger: " << finger << " v0: " 
// 		<< info.oBoundary[info.valley[finger][0]] << " tips: " 
// 		<< info.oBoundary[info.tips[finger]] << " v1: " 
// 		<< info.oBoundary[info.valley[finger][1]] << std::endl;
    return true;
  }

  bool
  isIn(const qgar::IPoint	p,
       const qgar::IPoint&	topleft,
       const qgar::IPoint&	bottomRight)
  {
    if ((p.x() >= topleft.x() - 10) && (p.x() <= bottomRight.x() + 10))
      if ((p.y() >= topleft.y() - 10) && (p.y() <= bottomRight.y() + 10))
	return true;
    return false;
  }

  // giving a bounding box, find  a tips which is in this bounding box
  // in order to assign a finger to the bounding box.
  unsigned
  findWhichFinger2(const s_handinfo&			info,
		   const qgar::BoundingBox&		bbox,
		   const std::vector< qgar::IPoint >&	boundary)
  {
    qgar::IPoint topleft = bbox.topLeft();
    qgar::IPoint BottomRight = bbox.bottomRight();
    std::cout << "topleft: " << topleft << " bottomright: "
	      << BottomRight << std::endl;

    for (unsigned i = 0; i < 5; ++i)
      {
	std::cout << "finger " << i << " " << boundary[info.tips[i]]
		  << " " << boundary[info.valley[i][0]] << " "
		  << boundary[info.valley[i][1]] << std::endl;


	if (isIn(boundary[info.tips[i]], topleft, BottomRight) &&
	    isIn(boundary[info.valley[i][0]], topleft, BottomRight) &&
	    isIn(boundary[info.valley[i][1]], topleft, BottomRight))
	  return i;
      }
    return 5;
  }

  // giving a bounding box, find  a tips which is in this bounding box
  // in order to assign a finger to the bounding box.
  unsigned
  findWhichFinger(const s_handinfo&			info,
		  const qgar::BoundingBox&		bbox,
		  const std::vector< qgar::IPoint >&	boundary)
  {
    qgar::IPoint topleft = bbox.topLeft();
    qgar::IPoint BottomRight = bbox.bottomRight();
//     std::cout << "topleft: " << topleft << " bottomright: "
// 	      << BottomRight << std::endl;
    unsigned middle = unsigned(round((BottomRight.x() - topleft.x()) / 2));
    qgar::IPoint m1(topleft.x() + middle, topleft.y());
    qgar::IPoint m2(topleft.x() + middle, BottomRight.y());
    qgar::ISegment seg(m1.x(), m1.y(), m2.x(), m2.y());

    qgar::ISegment seg1(topleft.x(), topleft.y(), BottomRight.x(), BottomRight.y());
    qgar::ISegment seg2(topleft.x(), BottomRight.y(), BottomRight.x(), topleft.y());

    double	dist[5];
    double	distmin = 10000000;
    unsigned	idxmin = 0;
    for (unsigned i = 0; i < 5; ++i)
      {
	dist[i] = seg1.distance(boundary[info.tips[i]]);
	dist[i] += seg2.distance(boundary[info.tips[i]]);
	dist[i] += seg1.distance(boundary[info.valley[i][0]]);
	dist[i] += seg2.distance(boundary[info.valley[i][0]]);
	dist[i] += seg1.distance(boundary[info.valley[i][1]]);
	dist[i] += seg2.distance(boundary[info.valley[i][1]]);
	if (dist[i] < distmin)
	  {
	    distmin = dist[i];
	    idxmin = i;
	  }
	//std::cout << i << " " << dist[i]  << std::endl;
      }

    return idxmin;
  }

  typedef qgar::ConnectedComponents::node_type ccnode;
  typedef qgar::ConnectedComponents::image_type ccimg;

  // cut fingers from hand on shape image.
  void
  cutFingers(s_handinfo&			info)
  {
    qgar::BinaryImage shapecpy(*info.shape);
    unsigned width = (unsigned)shapecpy.width();
    unsigned height = (unsigned)shapecpy.height();

    // draw segment to cut fingers on shape image
    for (unsigned i = 0; i < 5; ++i)
      {
	qgar::IPoint p1 = (info.oBoundary)[info.valley[i][0]];
	qgar::IPoint p2 = (info.oBoundary)[info.valley[i][1]];
 	qgar::ISegment seg(p1.x(), p1.y(), p2.x(), p2.y());
	// Should cut the finger.
        // sometimes create new connected components
        // thus it should be fixed.
	shapecpy.draw(seg, BGCOLOR);
	qgar::ISegment seg2(p1.x(), p1.y() + info.absside,
			    p2.x(), p2.y() + info.absside);
	shapecpy.draw(seg2, BGCOLOR);
      }

    //savePbmImg(shapecpy, info.out + "-cut.pbm");

    // search for connected components
    qgar::ConnectedComponents		ccs(shapecpy);
    std::vector< ccnode * >		comptab = ccs.componentTab();
    std::vector< ccnode * >::iterator	iccs;
    ccimg compImg = ccs.accessComponentImage();

    std::vector< s_pair > vect;

    // identify component, delete background or artefact components
    // and assign bounding box to each finger
    unsigned* link = new unsigned[comptab.size()];
    unsigned cur = 0;
    for (iccs = comptab.begin(); iccs !=  comptab.end(); ++iccs, ++cur)
      {
 	qgar::Component* comp = (*iccs)->data();
 	if (comp->color() != OBJCOLOR)
	  continue;
	vect.push_back(s_pair(cur, comp->areaPixels()));
      }
    std::sort(vect.begin(), vect.end(), sort_pair);
    //    std::cout << std::endl;
//     for (unsigned i = 0; i < vect.size(); i++)
//       std::cout << vect[i].idx << " size: " << vect[i].size << std::endl;
    assert(vect.size() >= 6);
    link[vect[0].idx] = 5;
    for (unsigned i = 1; i < 6; ++i)
      {
	if (i == 0) // biggest comp. = palm
	  link[vect[0].idx] = 5;
	else if (i < 6) // fingers
	  {
	    qgar::Component* comp = comptab[vect[i].idx]->data();
	    qgar::BoundingBox bbox = comp->accessBoundingBox();
	    link[vect[i].idx] = findWhichFinger(info, bbox, info.oBoundary);
	    //std::cout << "assign finger: " << link[vect[i].idx] << std::endl;
	  }
	else
	  link[vect[i].idx] = 6;

      }

    // for each connected component, compute mean for x and y
    // and store points in a vector.
    for (unsigned i = 0; i < width; ++i)
      for (unsigned j = 0; j < height; ++j)
	{
	  unsigned finger = link[compImg.pixel(i, j)];
	  if (finger < 5)
	    info.stats[finger].add(i, j);
	}
    delete []link;

    // for each finger, compute covariance matrix, find singular
    // values and the line supporting the first singular vector
    for (unsigned finger = 0; finger < 5; ++finger)
      {
	info.stats[finger].computeCovarMatrix();
	info.stats[finger].compute_eigenvalues();
      }
  }

  void
  computeHisto(s_handinfo&			info)
  {
    for (unsigned finger = 0; finger < 5; ++finger)
      {
	// initialize segment
	qgar::DPoint p1(info.stats[finger].xmean,
			info.stats[finger].ymean);
	qgar::DPoint p2(info.stats[finger].xmean + 1,
		       (info.stats[finger].xmean + 1) *
			info.stats[finger].slope +
			info.stats[finger].bias);
	qgar::DSegment seg(p1.x(), p1.y(), p2.x(), p2.y());
	double hist[info.DMAX];
	for (unsigned i = 0; i < info.DMAX; ++i)
	  hist[i] = 0;

	for (unsigned i = info.valley[finger][0];
	     i < info.valley[finger][1]; ++i)
	  {
	    double x = (info.oBoundary)[i].x();
	    double y = (info.oBoundary)[i].y();
	    qgar::DPoint tmp(x, y);
	    qgar::DPoint p = seg.project(tmp);
	    if (p.x() < p1.x())
	      p1 = p;
	    else if (p.x() > p2.x())
	      p2 = p;

	    unsigned dist = unsigned(round(seg.distance(tmp)));
	    if (dist >= info.DMAX)
	      dist = info.DMAX - 1;
	    hist[dist]++;
	    info.histocard[finger]++;
	  }
	info.p1[finger] = p1;
	info.p2[finger] = p2;
	for (unsigned i = 0; i < info.DMAX; ++i)
	  hist[i] /= info.histocard[finger];

	const float sigma = 0.2;

	info.histo[finger][0] = hist[0] * (1 - sigma) +
	  hist[1] * sigma;

	for (unsigned i = 1; i < info.DMAX - 1; ++i)
	  info.histo[finger][i] =
	    hist[i - 1] * sigma +
	    hist[i]     * (1 - 2 * sigma) +
	    hist[i + 1] * sigma;

	info.histo[finger][info.DMAX - 1] = hist[info.DMAX - 2] * sigma +
	  hist[info.DMAX - 1] * (1 - sigma);
      }
  }

  void
  computeHisto2(s_handinfo&			info)
  {
    double maxima = 0;

    //std::cout << std::endl;
    for (unsigned finger = 0; finger < 5; ++finger)
      {
	// initialize segment
	qgar::DPoint p1(info.stats[finger].xmean,
			info.stats[finger].ymean);
	qgar::DPoint p2(info.stats[finger].xmean + 1,
		       (info.stats[finger].xmean + 1) *
			info.stats[finger].slope +
			info.stats[finger].bias);
	qgar::DSegment seg(p1.x(), p1.y(), p2.x(), p2.y());
	double hist[info.DMAX];
	for (unsigned i = 0; i < info.DMAX; ++i)
	  hist[i] = 0;

	qgar::DPoint tmp;
	qgar::DPoint q;
	unsigned j;

	unsigned count1 = 0;
	unsigned count2 = 0;
	for (unsigned i = info.valley[finger][0];
	     i < info.valley[finger][1]; ++i)
	  {
	    // project a boundary point on main line
	    tmp.setXY((info.oBoundary)[i].x(),
		      (info.oBoundary)[i].y());
	    qgar::DPoint p = seg.project(tmp);

	    // extend segment (just for drawing purpose)
	    if (p.x() < p1.x())
	      p1 = p;
	    else if (p.x() > p2.x())
	      p2 = p;

	    // extend segment to cross boundary
	    qgar::DSegment seg2(tmp.x() - 10 * (tmp.x() - p.x()),
				tmp.y() - 10 * (tmp.y() - p.y()),
				p.x() + 10 * (tmp.x() - p.x()),
				p.y() + 10 * (tmp.y() - p.y()));

	    qgar::DSegment segref(tmp.x(), tmp.y(), p.x(), p.y());

	    if (i < info.tips[finger])
	      {
		j = info.valley[finger][1];
		q.setXY((info.oBoundary)[j].x(),
			(info.oBoundary)[j].y());
		while ((j > info.tips[finger]) && (!seg2.contains(q)))
		  {
		    j--;
		    q.setXY((info.oBoundary)[j].x(),
			    (info.oBoundary)[j].y());
		  }
	      }
	    else
	      {
		j = info.valley[finger][0];
		q.setXY((info.oBoundary)[j].x(),
		  (info.oBoundary)[j].y());
		while ((j < info.tips[finger]) && (!seg2.contains(q)))
		  {
		    j++;
		    q.setXY((info.oBoundary)[j].x(),
			    (info.oBoundary)[j].y());
		  }
	      }

	    if (j != info.tips[finger])
	      {
		unsigned dist = unsigned(round(seg.distance(tmp))) +
		  unsigned(round(seg.distance(q)));
		if (dist > maxima)
		  maxima = dist;
		if (dist >= info.DMAX)
		  dist = info.DMAX - 1;
		hist[dist]++;
		info.histocard[finger]++;
		//std::cout << "glopglop i: " << i << " j: " << j << std::endl;
		count1++;
	      }
	    else
	      {
		//std::cout << "not glop i: " << i << " j: " << j << std::endl;
		count2++;
	      }
	  }

// 	std::cout << "\tfinger: " << finger << " ok: " << count1
// 		  << " out: " << count2 << std::endl;

	info.p1[finger] = p1;
	info.p2[finger] = p2;
	for (unsigned i = 0; i < info.DMAX; ++i)
	  hist[i] /= info.histocard[finger];

	const double sigma = 0.2;

	info.histo[finger][0] = hist[0] * (1 - sigma) +
	  hist[1] * sigma;

	for (unsigned i = 1; i < info.DMAX - 1; ++i)
	  info.histo[finger][i] =
	    hist[i - 1] * sigma +
	    hist[i]     * (1 - 2 * sigma) +
	    hist[i + 1] * sigma;

	info.histo[finger][info.DMAX - 1] = hist[info.DMAX - 2] * sigma +
	  hist[info.DMAX - 1] * (1 - sigma);
      }

    //std::cout << "Maxima: " << maxima << " DMAX: " << info.DMAX << std::endl;
  }


  // flip right hand to left hand
  void
  flipImage(qgar::GreyLevelImage& input)
  {
    unsigned	row = input.height();
    unsigned	col = input.width();

    unsigned	max = ((col / 2) + (col % 2));
    for (unsigned i = 0; i < row; ++i)
      {
	qgar::QGTuChar* prow = input.pRow(i);
	for (unsigned j = 0; j < max; ++j)
	  {
	    qgar::QGTuChar tmp = prow[col - 1 - j];
	    prow[col - 1 - j] = prow[j];
	    prow[j] = tmp;
	  }
      }
  }


  void
  readAndBinarizeImage(struct s_handinfo&	info,
		       bool			doFlipImage,
		       bool			doNormalization)
  {
    if (doNormalization)
      {
// 	info.appearance = new qgar::GreyLevelImage(200,200);
// 	info.shape      = new qgar::GreyLevelImage(200,200);
// 	normalizehands		norm;
// 	norm.normalizeOneHand(info.name, doFlipImage,
// 			      *info.appearance, *info.shape);
      }
    else
      {
	qgar::PgmFile sourceFile((char*) info.name.c_str());
	info.appearance = new qgar::GreyLevelImage(sourceFile);
	sourceFile.close();
	if (doFlipImage)
	  {
	    flipImage(*info.appearance);
	    //savePgmImg(*info.appearance, info.out + "-flip.pgm");
	  }
	otsuthres	ot;
	info.shape = ot.doit(*info.appearance);
      }
    //
    //savePbmImg(*info.shape, info.out + "-bin.pbm");
  }

  void
  computeHandMainLine(struct s_handinfo&	info)
  {
    unsigned width = info.shape->width();
    unsigned height = info.shape->height();
    for (unsigned i = 0; i < width; ++i)
      for (unsigned j = 0; j < height; ++j)
	if (info.shape->pixel(i, j) == OBJCOLOR)
	  info.handaxis.add(i,j);
    info.handaxis.computeCovarMatrix();
    info.handaxis.compute_eigenvalues();
//     std::cout << "slope: " << info.handaxis.slope
// 	      << " bias: " << info.handaxis.bias << std::endl;
    qgar::DPoint pmean(info.handaxis.xmean, info.handaxis.ymean);

  }

public:

  std::vector< double >*
  doit(const std::string&	fileName,
       const std::string&	prefix,
       const bool&		doFlip);

  std::string
  id();

};

std::vector< double >*
FingerLenght::doit(const std::string&	fileName,
		   const std::string&	prefix,
		   const bool&		doFlip)
{
  s_handinfo			info(fileName, prefix);
  // do bogazici ref. sys. hand normalization ?
  // doesn't work anymore, need to recompile with matlab
  bool			doNormalization = false;
  readAndBinarizeImage(info, doFlip, doNormalization);

  computeHandMainLine(info);
  findDirection(info);

  // extract the main component and remove the others
  concpnt	cc(info);
  cc.doit(info);

  // find wrist line and cut hand
  wrist(info);
  //
  //saveImg(*info.shape, info.out + "-cc.pbm", info);

  // compute boundary tracing
  //qgar::BoundingBox bbox = cc.getBBox();
  boundaryTracing8C(info);

  // re-compute boundary tracing
  //boundaryTracing8C(info);

  // find tips and valley points
  std::vector< double >*	features = new std::vector< double >();
  if (findPoints(info) == false)
    return features; // if something goes wrong, return a null vector

  cutFingers(info);
  computeHisto(info);

  for (unsigned finger = 0; finger < 5; ++finger)
    for (unsigned i = 0; i < info.DMAX; ++i)
      features->push_back(info.histo[finger][i]);

  //doMrfRegulation(cc, info);
  //findPhalanges(cc, info);
  //
  //saveImg("-cont.pgm", info);

  //computeFingerLength(cc, info, *features);

  return features;
}

std::string
FingerLenght::id()
{
  return std::string("FingerLenght");
}
