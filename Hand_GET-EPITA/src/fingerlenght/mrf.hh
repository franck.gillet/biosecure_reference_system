// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef __MRF__HH__
#define __MRF__HH__

// qgar include
#include <qgarlib/GenImage.H>
//#include <qgarlib/PbmFile.H>
//#include <qgarlib/PgmFile.H>
//#include <qgarlib/QgarApp.H>
//#include <qgarlib/Histogram.H>

#include <vector>
#include <assert.h>
#include <math.h>

class MRF
{
  const unsigned NBSITE;
  const unsigned FIRSTSITE;
  double				T;
  const std::vector< qgar::IPoint >&	contour;
  std::vector< unsigned >&		dir;
  unsigned*&				points;
  unsigned*				RESULT;
  std::vector< double >			ENERGY;


  // input: indexes in direction (and boundary) vector
  // output: geodesic distance
  inline
  double
  geodesicDist(const unsigned&	p1,
	       const unsigned&	p2)
  {
    if (p1 == p2)
      return 0;
    unsigned pmin = (p1 < p2) ? p1 : p2;
    unsigned pmax = (p1 < p2) ? p2 : p1;

    double	inc = sqrt(2);
    double	result = 0;
    for (unsigned i = pmin; i < pmax; ++i)
      result += (dir[i] % 2) ? inc : 1;
    return result;
  }

  // input: indexes in contour (and direction) vector
  // output: euclidian distance
  inline
  double
  euclidianDist(const unsigned &s,
		const unsigned &p)
  {
    return sqrt(pow(contour[s].x() - contour[p].x(), 2) +
		pow(contour[s].y() - contour[p].y(), 2));
  }

  inline
  double
  energy(const unsigned&	p,
	 const unsigned&	place)
  {
    // if p == FIRSTSITE then prec doesn't exist
    // if p == NBSITE - 1 then next doesn't exist
    if ((p == FIRSTSITE) || (p == NBSITE - 1))
      return geodesicDist(points[p], place);;

    unsigned			pbefore = p - 1;
    unsigned			pafter = p + 1;

    // distance from initialization
    double e = geodesicDist(points[p], place);
    const double A = 1;

    e += std::fabs(euclidianDist(place, RESULT[pbefore]) -
		   euclidianDist(place, RESULT[pafter]));
    e += A * std::fabs(geodesicDist(place, RESULT[pbefore]) -
		       geodesicDist(place, RESULT[pafter]));
    return e;
  }

  double
  systemEnergy()
  {
    double total = 0;
    for (unsigned i = FIRSTSITE; i < NBSITE; ++i)
      total += ENERGY[i];
    return total;
  }

public:

  MRF(const std::vector< qgar::IPoint >&	contour,
      std::vector< unsigned >&			dir,
      unsigned*&				points)
    : NBSITE (12), FIRSTSITE (3), T (10), contour (contour), dir (dir), points (points)
  {
    // initialization with 'points' vector argument.
    RESULT = new unsigned[NBSITE];
    for (unsigned i = 0; i < NBSITE; ++i)
      RESULT[i] = points[i];

    ENERGY.reserve(NBSITE);
    for (unsigned i = FIRSTSITE; i < NBSITE; ++i)
      ENERGY[i] = energy(i, RESULT[i]);
  }

  unsigned*
  doit()
  {
    //unsigned nbite = 0;
    double sysenergy = 0;
    double newenergy = systemEnergy();
    do
      {
	sysenergy = newenergy;

	for (unsigned i = FIRSTSITE; i < NBSITE; ++i)
	  {
	    // select site
	    unsigned site = FIRSTSITE + (rand() % (NBSITE - FIRSTSITE));

	    // compute range
	    unsigned minidx = 0;
	    unsigned maxidx = 0;
	    if (site == FIRSTSITE)
	      maxidx = RESULT[site + 1];
	    else if (site == NBSITE - 1)
	      {
		minidx = RESULT[site - 1] + 1;
		maxidx = contour.size();
	      }
	    else
	      {
		assert((site > FIRSTSITE) && (site < (NBSITE - 1)));
		minidx = RESULT[site - 1] + 1;
		maxidx = RESULT[site + 1] - 1;
	      }

	    unsigned newplace = minidx + (rand() % (maxidx - minidx + 1));
	    double e_new = energy(site, newplace);

	    if (e_new < ENERGY[site])
	      {
		ENERGY[site] = e_new;
		RESULT[site] = newplace;
	      }
	    else
	      {
		double proba = exp(-(e_new - ENERGY[site]) / T);
		double tirage = rand() / double(RAND_MAX);

		if (tirage <= proba)
		  {
		    ENERGY[site] = e_new;
		    RESULT[site] = newplace;
		  }
	      }
	  }
	newenergy = systemEnergy();
	T = T * 0.999;
      }
    while ((T > 0.0001));

    return RESULT;
  }
};

#endif //__MRF__HH__
