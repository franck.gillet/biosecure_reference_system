// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef RESULT__
#define RESULT__

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

  unsigned
  getcard(const std::vector< float >&	vect,
	  const float&			thres,
	  const float&			min,
	  const float&			max)
  {
    unsigned idx = 0;
    std::vector< float >::const_iterator	ite;

    if (thres > min)
      if (thres < max)
	{
	  for (ite = vect.begin(); ite != vect.end(); ++ite, ++idx)
	    if (*ite >= thres)
	      break;
	}
      else
	idx = vect.size();
    return idx;
  }

  void
  rocfile(const std::vector< float >&	vfar,
	  const std::vector< float >&	vgar,
	  const std::string&		filename)
  {
    std::vector< float >::const_iterator	ite;
    std::ofstream			ofs(filename.c_str());

    ofs << "FAR=[" << std::endl;
    assert (vfar.size() == vgar.size());
    for (ite = vfar.begin(); ite != vfar.end(); ++ite)
      ofs << *ite << std::endl;
    ofs << "];"                    << std::endl;
    ofs << "GAR=["                 << std::endl;
    for (ite = vgar.begin(); ite != vgar.end(); ++ite)
      ofs << *ite << std::endl;
    ofs << "];"                    << std::endl;

    ofs << "semilogx (FAR, GAR);"  << std::endl;
    ofs << "axis([0.0001 1 0 1]);" << std::endl;
    ofs << "title ('ROC Curve');"  << std::endl;
    ofs << "xlabel ('False Acceptance Rate');"   << std::endl;
    ofs << "ylabel ('Genuine Acceptance Rate');" << std::endl;
    ofs << "saveas(gcf,'roc.fig')" << std::endl;
    ofs << "print -f1 -dpng roc"   << std::endl;
    ofs << "print -f1 -depsc roc"  << std::endl;
    ofs.close();
  }

  void
  histofile(const std::vector< float >&	intra,
	    const std::vector< float >&	inter,
	    const float&		err,
	    const float&		ter,
	    const std::string&		filename)
  {
    std::vector< float >::const_iterator	ite;
    std::ofstream			ofs(filename.c_str());

    ofs << "INTRA=["  << std::endl;
    for (ite = intra.begin(); ite != intra.end(); ++ite)
      ofs << *ite << std::endl;
    ofs << "];"  << std::endl;
    ofs << "INTER=[" << std::endl;
    for (ite = inter.begin(); ite != inter.end(); ++ite)
      ofs << *ite << std::endl;
    ofs << "];"  << std::endl;

    ofs << "X=[" << std::endl;
    unsigned NBSTEPINTER = 100;
    float STEPINTER = inter.back() / NBSTEPINTER;

    unsigned count = 0;
    for (float i = 0.0 ; i < inter.back() ; i += STEPINTER)
      {
	ofs << i << std::endl;
	count++;
      }
    ofs << "];"  << std::endl;
    NBSTEPINTER = count;

    ofs << "Y=[" << std::endl;

    unsigned NBSTEPINTRA = 20;
    float STEPINTRA = intra.back() / NBSTEPINTRA;
    count = 0;
    for (float i = 0.0 ; i < intra.back(); i += STEPINTRA)
      {
	ofs << i << std::endl;
	count ++;
      }
    ofs << "];"  << std::endl;
    NBSTEPINTRA = count;

    ofs << "D=histc(INTRA,Y);"           << std::endl;
    ofs << "E=histc(INTER,X);"           << std::endl;
    ofs << "for I = 1:" << NBSTEPINTRA << ", D(I) = D(I) / "
	<< intra.size() << "; end" << std::endl;
    ofs << "for I = 1:" << NBSTEPINTER << ", E(I) = E(I) / "
	<< inter.size() << "; end" << std::endl;
    ofs << "plot (Y,D,'b');"         << std::endl;
    ofs << "hold on"                 << std::endl;
    ofs << "plot (X,E,'r');"         << std::endl;
    ofs << "xlabel('distance hand/hand');"     << std::endl;
    ofs << "ylabel('cardinal');"     << std::endl;
    ofs << "title ('histograms client/client(blue) and client/impostor (red)');" << std::endl;
    ofs << "ERRX=[" << err           << std::endl;
    ofs << err << "];"               << std::endl;
    ofs << "TERX=[" << ter           << std::endl;
    ofs << ter << "];"               << std::endl;
    ofs << "MAXINTRA=max(D);"        << std::endl;
    ofs << "MAXINTER=max(E);"        << std::endl;
    ofs << "MAX=max(MAXINTRA,MAXINTER);" << std::endl;
    ofs << "ERRY=[0"                 << std::endl;
    ofs << "MAX];"                   << std::endl;
    ofs << "plot(ERRX, ERRY, 'g--');"  << std::endl;
    ofs << "plot(TERX, ERRY, 'm--');"  << std::endl;
    ofs << "legend ('client/client','client/impostor','ERR','TER');" << std::endl;

    ofs << "saveas(gcf,'histo.fig')" << std::endl;
    ofs << "print -f1 -dpng histo"   << std::endl;
    ofs << "print -f1 -depsc histo"  << std::endl;
    ofs.close();
  }

void
generateScoreFile(const std::vector< float >&	vect,
		  const std::string&		filename,
		  const double&			max)
{
  std::vector< float >::const_iterator	ite;
  std::ofstream			ofs(filename.c_str());

  double umax = round(max) + 1;

  for (ite = vect.begin(); ite != vect.end(); ++ite)
      ofs << umax - *ite << std::endl;
  ofs.close();
}

void
generateScoreFile(const unsigned*	vect,
		  const std::string&	filename,
		  const unsigned	population)
{
  std::vector< float >::const_iterator	ite;
  std::ofstream			ofs(filename.c_str());

  unsigned count = 0;
  for (unsigned i = 0; i < population; ++i)
    {
      count += vect[i];
      ofs << count << std::endl;
    }
  
  ofs.close();
}

  void
  farfrrfile(const std::vector< float >& far,
	     const std::vector< float >& gar,
	     const float&		 max,
	     const std::string&		 filename)
  {
    std::vector< float >::const_iterator	ite;
    std::ofstream			ofs(filename.c_str());

    assert(far.size() == gar.size());

    ofs << "X=[" << std::endl;
    for (float i = 0; i < max; i += 0.1)
      ofs << i << std::endl;
    ofs << "];" << std::endl;

    ofs << "FAR=[" << std::endl;
    for (ite = far.begin(); ite != far.end(); ++ite)
      ofs << *ite << std::endl;
    ofs << "];" << std::endl;

    ofs << "FRR=[" << std::endl;
    for (ite = gar.begin(); ite != gar.end(); ++ite)
      ofs << 1.f - *ite << std::endl;
    ofs << "];" << std::endl;

    ofs << "plot(X,FAR,'r');" << std::endl;
    ofs << "hold on"                 << std::endl;
    ofs << "plot(X,FRR,'b');" << std::endl;
    ofs << "title ('FAR and FRR');"  << std::endl;
    ofs << "xlabel ('Threshold');"   << std::endl;
    ofs << "axis([0 100 0 1]);"     << std::endl;
    ofs << "saveas(gcf,'farfrr.fig')" << std::endl;
    ofs << "print -f1 -dpng farfrr"   << std::endl;
    ofs << "print -f1 -depsc farfrr"  << std::endl;

    ofs.close();
  }

float
computeERR(const std::vector< float >&	intra,
	   const std::vector< float >&	inter)
{
  float intra_min = intra.front();
  float inter_min = inter.front();
  float intra_max = intra.back();
  float inter_max = inter.back();

  bool  found = false;

  for (float thres = 0; thres < inter_max; thres += 0.1)
    {
      unsigned tmpfrr = getcard(intra, thres, intra_min, intra_max);
      unsigned tmpfar = getcard(inter, thres, inter_min, inter_max);

      float frr = (float)(intra.size() - tmpfrr) / (float)intra.size();
      float far = (float)tmpfar / (float)inter.size();

      if ((frr <= far) && !found)
	return thres;
    }
  assert(1);
  return inter_max;
}

float
computeTER(const std::vector< float >&	intra,
	   const std::vector< float >&	inter)
{
  float intra_min = intra.front();
  float inter_min = inter.front();
  float intra_max = intra.back();
  float inter_max = inter.back();

  float	ter_val = 1000000;
  float	ter_thres = 0;

  for (float thres = 0; thres < inter_max; thres += 0.1)
    {
      unsigned tmpfrr = getcard(intra, thres, intra_min, intra_max);
      unsigned tmpfar = getcard(inter, thres, inter_min, inter_max);

      float frr = (float)(intra.size() - tmpfrr) / (float)intra.size();
      float far = (float)tmpfar / (float)inter.size();

      if ((frr + far) < ter_val)
	{
	  ter_val = far + frr;
	  ter_thres = thres;
	}
    }
  return ter_thres;
}

float
generateResult(std::vector< float >&	intra,
	       std::vector< float >&	inter,
	       const unsigned*		ranks,
	       const std::string&	prefix,
	       const unsigned&		population)
{
  std::sort(intra.begin(), intra.end());
  std::sort(inter.begin(), inter.end());

  float intra_min = intra.front();
  float inter_min = inter.front();
  float intra_max = intra.back();
  float inter_max = inter.back();

  bool  found = false;

  float err_val = 0;
  float err_thres = 0;
  float ter_val = 1000000000;
  float ter_thres = 0;


  std::vector< float > vfar;
  std::vector< float > vgar;

  for (float thres = 0; thres < inter_max; thres += 0.001)
    {
      unsigned tmpfrr = getcard(intra, thres, intra_min, intra_max);
      unsigned tmpfar = getcard(inter, thres, inter_min, inter_max);

      float frr = (float)(intra.size() - tmpfrr) / (float)intra.size();
      float far = (float)tmpfar / (float)inter.size();

      //std::cout << "frr: " << frr << " far: " << far << std::endl;

      vfar.push_back(far);
      vgar.push_back(1.f - frr);
      if ((frr <= far) && !found)
	{
	  err_val = frr;
	  err_thres = thres;
	  found = true;
	}

      if ((far + frr) < ter_val)
	{
	  ter_val = far + frr;
	  ter_thres = thres;
	}
    }

  generateScoreFile(intra, prefix + "genuine-score.txt", inter_max);
  generateScoreFile(inter, prefix + "impostor-score.txt", inter_max);
  generateScoreFile(ranks, prefix + "cmc.txt", population);
  rocfile(vfar, vgar, prefix + "roc.m");
  histofile(intra, inter, err_thres, ter_thres, prefix + "histo.m");
  //farfrrfile(vfar, vgar, inter_max, "farfrr.m");
  return (1.f - err_val)  * 100.f;
}


#endif
