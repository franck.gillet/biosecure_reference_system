// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef __EXTRACTOR__HH__
#define __EXTRACTOR__HH__

#include	<string>
#include	<vector>
#include	<iostream>

typedef double(*distfunc)(const std::vector< double >&,
			  const std::vector< double >&);

void
mergeFeatures(std::vector< double >&	v1,
	      std::vector< double >&	v2);

distfunc
selectDist(const std::string&	progname);

class Extractor
{
public:
  virtual
  std::vector< double >*
  doit(const std::string&	fileName,
       const std::string&	prefix,
       const bool&		flip) = 0;

  virtual
  std::string
  id() = 0;

};

#endif // __EXTRACTOR__HH__
