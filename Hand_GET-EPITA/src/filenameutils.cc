// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <string>


void
replaceTabBySpace(std::string&	str)
{
  std::string::iterator		ite;
  for (ite = str.begin(); ite != str.end(); ++ite)
    if (*ite == '\t')
      *ite = ' ';
}

std::string
eatWhiteSpace(const std::string&	str)
{
  // Eat whitespace
  unsigned	loc = 0;
  while (str[loc] == ' ')
    loc++;
  if (loc)
    return str.substr(loc);
  return str;
}

std::string
basename(const std::string&	str)
{
  std::string		res = str;
  unsigned		loc = res.find_last_of('.');
  if (loc != std::string::npos)
    return res.substr(0, loc);
  return res;
}

std::string
dirname(const std::string&	str)
{
  std::string		res = str;
  unsigned		loc = res.find_last_of('/');
  if (loc != std::string::npos)
    return res.substr(0, loc);
  return res;
}

std::string
shortname(const std::string&	str)
{
  std::string		res = str;
  unsigned		loc = res.find_last_of('/');
  if (loc != std::string::npos)
    return res.substr(loc);
  return res;
}

std::string
shortname2(const std::string&	str)
{
  std::string		res = str;
  unsigned		loc = res.find_last_of('/');
  if (loc != std::string::npos)
    return res.substr(loc + 1);
  return res;
}
