// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef __PERMUTATION_HH__
#define __PERMUTATION_HH__

int permcard[7] = {0,0,2,6,14,30,62};
int permsize[7] = {0,0,1,2,3,4,5};

int enrollset2[2][1] = {{0},{1}};

int enrollset3[6][2] = {
  {0,-1}, {1,-1}, {2,-1}, {0,1}, {0,2}, {1,2}
};

int enrollset4[14][3] = {
  {0,-1,-1}, {1,-1,-1}, {2,-1,-1}, {3,-1,-1}, {0, 1,-1}, {0, 2,-1}, {1, 2,-1},
  {0, 3,-1}, {1, 3,-1}, {2, 3,-1}, {0, 1, 2}, {0, 1, 3}, {0, 2, 3}, {1, 2, 3}
};

int enrollset5[30][4] = {
  // taille 1 (5)
  {0,-1,-1,-1}, {1,-1,-1,-1}, {2,-1,-1,-1}, {3,-1,-1,-1}, {4,-1,-1,-1},
  // taille 2 (10)
  {0,1,-1,-1}, {0,2,-1,-1}, {1,2,-1,-1}, {0,3,-1,-1}, {1,3,-1,-1},
  {2,3,-1,-1}, {0,4,-1,-1}, {1,4,-1,-1}, {2,4,-1,-1}, {3,4,-1,-1},
  // taille 3 (10)
  {0,1,2,-1},{0,1,3,-1},{0,1,4,-1},{0,2,3,-1},{0,2,4,-1},
  {0,3,4,-1},{1,2,3,-1},{1,2,4,-1},{1,3,4,-1},{2,3,4,-1},
  // taille 4 (5)
  {0,1,2,3},{0,1,2,4},{0,1,3,4},{0,2,3,4},{1,2,3,4}
};

int enrollset6[62][5] = {
  // taille 1 (6)
  {0,-1,-1,-1,-1}, {1,-1,-1,-1,-1}, {2,-1,-1,-1,-1}, {3,-1,-1,-1,-1}, {4,-1,-1,-1,-1}, {5,-1,-1,-1,-1},
  // taille 2 (15)
  {0,1,-1,-1,-1}, {0,2,-1,-1,-1}, {0,3,-1,-1,-1}, {0,4,-1,-1,-1}, {0,5,-1,-1,-1},
  {1,2,-1,-1,-1}, {1,3,-1,-1,-1}, {1,4,-1,-1,-1}, {1,5,-1,-1,-1}, {2,3,-1,-1,-1},
  {2,4,-1,-1,-1}, {2,5,-1,-1,-1}, {3,4,-1,-1,-1}, {3,5,-1,-1,-1}, {4,5,-1,-1,-1},
  // taille 3 (20)
  {0,1,2,-1,-1},{0,1,3,-1,-1},{0,1,4,-1,-1},{0,1,5,-1,-1},{0,2,3,-1,-1},
  {0,2,4,-1,-1},{0,2,5,-1,-1},{0,3,4,-1,-1},{0,3,5,-1,-1},{0,4,5,-1,-1},
  {1,2,3,-1,-1},{1,2,4,-1,-1},{1,2,5,-1,-1},{1,3,4,-1,-1},{1,3,5,-1,-1},
  {1,4,5,-1,-1},{2,3,4,-1,-1},{2,3,5,-1,-1},{2,4,5,-1,-1},{3,4,5,-1,-1},
  // taille 4 (15)
  {0,1,2,3,-1},{0,1,2,4,-1},{0,1,2,5,-1},{0,1,3,4,-1},{0,1,3,5,-1},
  {0,1,4,5,-1},{0,2,3,4,-1},{0,2,3,5,-1},{0,2,4,5,-1},{0,3,4,5,-1},
  {1,2,3,4,-1},{1,2,3,5,-1},{1,2,4,5,-1},{1,3,4,5,-1},{2,3,4,5,-1},
  // taille 5 (6)
  {0,1,2,3,4},{0,1,2,3,5},{0,1,2,4,5},{0,1,3,4,5},{0,2,3,4,5},{1,2,3,4,5}};

unsigned
permutationSize(int*		permutation,
		const int&	maxsize)
{
  unsigned	count = 0;
  for (int i = 0; i < maxsize; ++i, ++count)
    if (permutation[i] == -1)
      return count;
  return count;
}

bool
containnot(const unsigned&	session,
	   int*			permutations,
	   const int&		permsize)
{
  for (int i = 0; i < permsize; ++i)
    if (permutations[i] == (int)session)
      return false;
  return true;
}

bool
bellowlimit(const unsigned&	limit,
	    int*		permutations,
	    const int&		permsize)
{
  for (int i = 0; i < permsize; ++i)
    if (permutations[i] >= (int)limit)
      return false;
  return true;
}

bool
abovelimit(const unsigned&	limit,
	   int*			permutations,
	   const int&		permsize)
{
  for (int i = 0; i < permsize; ++i)
    if (permutations[i] < (int)limit)
      return false;
  return true;
}

unsigned
getPermType(const unsigned&	limit,
	    int*		permutations,
	    const int&		permsize)
{
  if (bellowlimit(limit, permutations, permsize))
    return 0;
  else
    if (abovelimit(limit, permutations, permsize))
      return 1;
    else
      return 2;
  return 0;
}

std::vector< int* >*
getperm(const unsigned&	nbsession,
	const unsigned&	enrollSetSize)
{
  assert(nbsession > 1);
  std::vector< int* >* result = new std::vector<int *>();
  if (nbsession >= 7)
    {
      std::cerr << "Can't compute permutation for more than 6 sessions! exit" << std::endl;
      exit(-1);
    }
  int PermMaxSize = permsize[nbsession];
//   std::cout << "\tnbsession: " << nbsession << " enrollSetSize: " << enrollSetSize
//  	    << " nbPerm: " << permcard[nbsession] << " PermMaxSize: " << PermMaxSize << std::endl;

  switch (nbsession)
    {
    case 2: {
      for (int i = 0; i < permcard[nbsession]; ++i)
	if (permutationSize(enrollset2[i], PermMaxSize) == enrollSetSize)
	  result->push_back(enrollset2[i]);
      break;
    }
    case 3: {
      for (int i = 0; i < permcard[nbsession]; ++i)
	if (permutationSize(enrollset3[i], PermMaxSize) == enrollSetSize)
	  result->push_back(enrollset3[i]);
      break;
    }
    case 4: {
      for (int i = 0; i < permcard[nbsession]; ++i)
	if (permutationSize(enrollset5[i], PermMaxSize) == enrollSetSize)
	  result->push_back(enrollset5[i]);
      break;
    }
    case 5: {
      for (int i = 0; i < permcard[nbsession]; ++i)
	if (permutationSize(enrollset5[i], PermMaxSize) == enrollSetSize)
	  result->push_back(enrollset5[i]);
      break;
    }
    case 6: {
      for (int i = 0; i < permcard[nbsession]; ++i)
	if (permutationSize(enrollset6[i], PermMaxSize) == enrollSetSize)
	  result->push_back(enrollset6[i]);
      break;
    }
    default:
      {
	std::cerr << "wrong number of permutation: " << nbsession << std::endl;
	exit (-1);
      }
    }

  return result;
}


#endif
