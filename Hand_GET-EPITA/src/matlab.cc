// Copyright (c) 2007 GET-EPITA
// Authors Geoffroy Fouquier, Laurence Likformann, Jerome Darbon
//
// This file is part of the BioSecure - GET-EPITA reference system
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <iostream>
#include <mclcppclass.h>

#include "libbuhand.h"

void
initializeMatlabRuntime()
{
  /*
   * Call the mclInitializeApplication routine. Make sure that
   * the application was initialized properly by checking the
   * return status. This initialization has to be done before
   * calling any MATLAB APIs or MATLAB Compiler generated
     * shared library functions.
     */
  if( !mclInitializeApplication(NULL,0) )
    {
      fprintf(stderr, "Could not initialize the application\n");
      exit(1);
    }
  // Call the library intialization routine and make sure that
  // the library was initialized properly.
  if (!libbuhandInitialize())
    {
      fprintf(stderr, "Could not initialize the library, exit");
      exit(1);
    }
}

void
closeMatlabRuntime()
{
  // Call the library termination routine.
  libbuhandTerminate();
  mclTerminateApplication();
}
