#/bin/zsh

[ $# -lt 1 ] && {
	echo "usage: "$0" listfile"
	exit -1
}

# which binary ?
BINARY=$HND_BIN_EXTRACT

# which list of picture ?
LISTFILE=$1

# how many jobs to launch at the same time
DEFAULTNBNODES=5

# where to find extract.sh and finalyze.sh
EXTRACT=${SCRIPTDIR}/internals/extract.sh
EXTRACTPART=${SCRIPTDIR}/internals/extract-split.sh
FINALIZE=${SCRIPTDIR}/internals/finalize-extraction.sh

#############################################################################

source ${SCRIPTDIR}/internals/functions.sh

checkScript $BINARY

NBNODES=1
# split extraction or not
echo $0 | grep -q "\-split" && {
		NBNODES=$DEFAULTNBNODES
}

EXTOPTIONS=''
# flip picture or not
echo $0 | grep -q "\-flip" && {
	EXTOPTIONS='-f'
}

[[ $NBNODES -gt 1 ]] && {
		checkScript $EXTRACTPART
		checkScript $FINALIZE
	} || {
		checkScript $EXTRACT
}

ROOTDIR=`pwd`
cd $ROOTDIR
TMPDIR=${ROOTDIR}/tmp
mkdir $TMPDIR 2> /dev/null

BINARY2=`basename $BINARY`
LISTFILE2=`basename $LISTFILE`
NAME2=`echo $LISTFILE2 | cut -d'-' -f 2-`

PARTLOG=${TMPDIR}/log.extr.${BINARY2}.${LISTFILE2}
LOGFILE=${ROOTDIR}/log.extr.${BINARY2}.${LISTFILE2}

##############################################################################
# check for grid engine environment

#if [ x$SGE_ROOT = x ]; then
#   SGE_ROOT=/usr/SGE
#fi
#if [ ! -d $SGE_ROOT ]; then
#   echo "error: SGE_ROOT directory $SGE_ROOT does not exist"
#   exit 1
#fi

#ARC=`$SGE_ROOT/util/arch`

#QSUB=$SGE_ROOT/bin/$ARC/qsub
#QALTER=$SGE_ROOT/bin/$ARC/qalter

#if [ ! -x $QSUB ]; then
#   echo "error: cannot execute qsub command under $QSUB"
#   exit 1
#fi

#if [ ! -x $QALTER ]; then
#   echo "error: cannot execute qalter command under $QALTER"
#   exit 1
#fi

##############################################################################
# cut file

[[ $NBNODES -gt 1 ]] && {

	NBUSER=`cat $LISTFILE | wc -l | xargs`
	(( STEP = NBUSER / NBNODES  + 1 ))
	echo "user: " $NBUSER" NBNODES: "$NBNODES" STEP:" $STEP

	(( i = 1 ))
	while [[ i -le $NBNODES ]]
	do
		(( stop = $i * $STEP - 1 ))
		(( start = $stop - $STEP ))
		[[ -f ${TMPDIR}/${LISTFILE2}.$i ]] && rm ${TMPDIR}/${LISTFILE2}.$i
		[[ $i -lt  $NBNODES ]] && {
			cat ${LISTFILE} | head -n $stop | tail -n $STEP >> ${TMPDIR}/${LISTFILE2}.$i
		} || {
			(( stop = $NBUSER - ($i - 1) * $STEP + 1))
			cat ${LISTFILE} | tail -n $stop  >> ${TMPDIR}/${LISTFILE2}.$i
		}
		(( i = $i + 1 ))
	done

	# test file spliting
	(( i = 1 ))
	while [[ i -le $NBNODES ]]
	do
		cat ${TMPDIR}/${LISTFILE2}.$i
		(( i = $i + 1 ))
	done > ${TMPDIR}/${LISTFILE2}.all
	diff -q ${TMPDIR}/${LISTFILE2}.all ${LISTFILE} > /dev/null 2> /dev/null || {
		echo "file spliting failed, exit"
		exit
	}
}

##############################################################################
# launch process

#[[ $NBNODES -gt 1 ]] && {

#	jobid_a=`$QSUB -m n -t 1-$NBNODES -r y -N "P ${NAME2}-split" ${EXTRACTPART} $BINARY ${TMPDIR}/$LISTFILE2 $PARTLOG $ROOTDIR $EXTOPTIONS | cut -f3 -d" " | cut -f1 -d.`

#	$QSUB -hold_jid $jobid_a -N "F ${NAME2}" ${FINALIZE} $ROOTDIR $LOGFILE $PARTLOG $NBNODES
#} || {
#	$QSUB -N "Ext. ${NAME2}" ${EXTRACT} $BINARY $LISTFILE $LOGFILE $ROOTDIR $EXTOPTIONS
#}

${EXTRACT} $BINARY $LISTFILE $LOGFILE $ROOTDIR $EXTOPTIONS
