#!/bin/zsh

[ $# -lt 2 ] && {
        echo "usage: "$0" binary listfile"
        exit -1
}

# which binary ?
BINARY=$1

# where to find extract.sh and finalyze.sh
EVALUATE=${SCRIPTDIR}/internals/evaluate-db.sh
EVALUATEPART=${SCRIPTDIR}/internals/evaluate-db-split.sh
FINALIZE=${SCRIPTDIR}/internals/finalize-evaluation.sh
FINALIZEMEAN=${SCRIPTDIR}/internals/finalize-evaluation-mean.sh

#############################################################################

source ${SCRIPTDIR}/internals/functions.sh

[[ $# -eq 1 ]] && {
    checkScript $EVALUATE
} || {
    checkScript $EVALUATEPART
    echo $0 | grep -q "\-mean" && {
	FINALIZESCRIPT=$FINALIZEMEAN
    } || {
	FINALIZESCRIPT=$FINALIZE
    }
    checkScript $FINALIZESCRIPT
}


ROOTDIR=`pwd`
LOGDIR=${ROOTDIR}/log
SCOREDIR=${ROOTDIR}/score
mkdir $LOGDIR $SCOREDIR 2> /dev/null

BINARY2=`basename $BINARY`
NAME2=Eval-dbs

PARTLOG=${TMPDIR}/log.eval.${BINARY2}
LOGFILE=${ROOTDIR}/log.eval.${BINARY2}

(( NBJOBS = $# - 1 ))

##############################################################################
# check for grid engine environment

#if [ x$SGE_ROOT = x ]; then
#   SGE_ROOT=/usr/SGE
#fi
#if [ ! -d $SGE_ROOT ]; then
#   echo "error: SGE_ROOT directory $SGE_ROOT does not exist"
#   exit 1
#fi

#ARC=`$SGE_ROOT/util/arch`

#QSUB=$SGE_ROOT/bin/$ARC/qsub
#QALTER=$SGE_ROOT/bin/$ARC/qalter

#if [ ! -x $QSUB ]; then
#   echo "error: cannot execute qsub command under $QSUB"
#   exit 1
#fi

#if [ ! -x $QALTER ]; then
#   echo "error: cannot execute qalter command under $QALTER"
#   exit 1
#fi

##############################################################################
# copy list files

[[ $NBJOBS -gt 1 ]] && {

	[[ -f list.txt ]] && rm list.txt

	for (( i = 2 ; i <= $# ; i += 1 ))
	do
		(( j = i - 1 ))
		echo $j" "$argv[i] >> list.txt
	done
}

##############################################################################

#[[ $NBJOBS -gt 1 ]] && {
#	jobid_a=`$QSUB -m n -t 1-$NBJOBS -r y -N "${NAME2}-split" ${EVALUATEPART} $BINARY $ROOTDIR $EVALOPTIONS | cut -f3 -d" " | cut -f1 -d.`
#	$QSUB -hold_jid $jobid_a -m n -N "F ${NAME2}" ${FINALIZESCRIPT} $ROOTDIR $LOGFILE $NBJOBS
#} || {
#	$QSUB -m n -N "Eval `basename $2`" ${EVALUATE} $BINARY $ROOTDIR $2 $EVALOPTIONS
#}


##############################################################################

${EVALUATE} $BINARY $ROOTDIR $2 $EVALOPTIONS
