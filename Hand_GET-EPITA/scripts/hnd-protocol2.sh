#!/bin/zsh

BINARY=$HND_BIN_VERIF
DBLISTS=$HND_BU_RIGHT

export EVALOPTIONS="-e 1"

source ${SCRIPTDIR}/internals/functions.sh

################################################################################

checkScript $BINARY
checkRep `pwd`/features
checkFiles `echo $DBLISTS | xargs`

${SCRIPTDIR}/hnd-evaluate-dbs.sh $BINARY ${DBLISTS}
