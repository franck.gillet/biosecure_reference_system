#!/bin/zsh

#DRYRUN=echo

case "$1" in
  bu)
  	BASEREP=$HND_BU_ROOT
	echo "Bogazici database in "$BASEREP
	BASEID=bu
	BASETYPE=bmp
	BASESUBDIR='lapse left right total'
  ;;
  
  enst)
  	BASEREP=$HND_ENST_ROOT
	echo "ENST database in "$BASEREP
	BASEID=enst
	BASETYPE=pnm
	BASESUBDIR='150 400 600'
  ;;

  biomet)
  	BASEREP=$HND_BIOMET_ROOT
	echo "Biomet database in "$BASEREP
	BASEID=biomet
	BASETYPE=bmp
	BASESUBDIR='camp03'
  ;;
  
  *)
  	echo "usage "$0" bu|biomet|enst [sizefactor (%)]"
	exit -1
	;;
esac

######################################################################################"

[[ -d $BASEREP ]] || {
	echo $BASEREP" does not exist, exit"
	exit -1
}
$DRYRUN cd $BASEREP

[[ $# -gt 1 ]] && {
        CONVERTOPT=" -geometry ${2}% "
	DESTREP=$BASEREP/pgm-${2}%
} || DESTREP=$BASEREP/pgm

[[ -d $DESTREP ]] || { 
	$DRYRUN mkdir $DESTREP || {
		echo "error: can't create output directory, fix permission"
		exit
	}
}

getlastimage() {
	[[ $1 == "bu" ]] && LASTIMG=`ls 1*${BASETYPE} | sort -n -t'(' -k 2 | tail -n 1 | cut -d '(' -f 2 | cut -d ')' -f 1`
	[[ $1 == "enst" ]] && LASTIMG=`ls main*${BASETYPE} | sort -n -t '_' -k 2 | tail -n 1 | cut -d '_' -f 2`
	[[ $1 == "biomet" ]] && LASTIMG=`ls HND*${BASETYPE}  | sort -n | tail -n 1 | cut -d'_' -f 1 | cut -d'D' -f 2`
	echo $LASTIMG
}

getlastsession() {
	[[ $1 == "bu" ]] && LASTSESSION=`ls *" (1).${BASETYPE}" | sort -n -t'(' -k 2 | tail -n 1 | cut -d ' ' -f 1 | xargs`
	[[ $1 == "enst" ]] && LASTSESSION=`ls main*${BASETYPE} | sort -n -t '_' -k 3 | tail -n 1 | cut -d '_' -f 3 | cut -d'.' -f 1 | xargs`
	[[ $1 == "biomet" ]] && LASTSESSION=`ls HND*${BASETYPE} | sort -n -t 'v' -k 2 | tail -n 1 | cut -d 'v' -f 2 | cut -d'.' -f 1 | xargs` 
        echo $LASTSESSION
}

getsrcname() {
	USER=$2
	SESSION=$3
	[[ $1 == "bu" ]] && echo "$SESSION ($USER).${BASETYPE}"
	[[ $1 == "enst" ]] && echo "main_`printf "%.3d" ${USER}`_${SESSION}.${BASETYPE}"
	[[ $1 == "biomet" ]] && echo HND`printf "%.3d" ${USER}`_v`printf "%.2d" ${SESSION}`.${BASETYPE}
}

foreach rep (`echo $BASESUBDIR`)

	# set directory and create output directory
	INPUT=${BASEREP}/${rep}
	OUTPUT=${DESTREP}/${rep}
	[[ -d $OUTPUT ]] || {
		$DRYRUN mkdir $OUTPUT
	}

	# list file
	[[ $# -gt 1 ]] && {
		LISTFILE=${DESTREP}/list-${BASEID}-${rep}-${2}%
	} || {
		LISTFILE=${DESTREP}/list-${BASEID}-${rep}
	}
	[[ -f $LISTFILE ]] && rm $LISTFILE 

	# convert image
	cd ${INPUT}
	LASTIMG=`getlastimage $BASEID`
	LASTSESSION=`getlastsession $BASEID`
	echo ${rep}" last: "$LASTIMG" session: "$LASTSESSION
	for i in {1..$LASTIMG} ; {
		for j in {1..$LASTSESSION} ; {
			SRCNAME="`getsrcname $BASEID $i $j`"
			[[ -f $SRCNAME ]] && {
				DESTNAME=`echo ${BASEID} | tr '[:lower:]' '[:upper:]'`_`printf "%.3d" ${i}`_${rep}_`printf "%.1d" ${j}`.pgm
				[[ -f ${OUTPUT}/${DESTNAME} ]] && {
					#$DRYRUN echo ${OUTPUT}/${DESTNAME}" already exist"
				} || {
					echo '\t'${rep}" "`printf "%.3d" $i`/$LASTIMG $j/$LASTSESSION
					$DRYRUN convert `echo $CONVERTOPT` ${INPUT}/"${SRCNAME}" ${OUTPUT}/${DESTNAME}
				}
				[[ $DRYRUN == "echo" ]] || { echo -n ${OUTPUT}/${DESTNAME}" " >> $LISTFILE }
			} || echo "\t"$SRCNAME" is missing"
		}
		[[ $DRYRUN == "echo" ]] || { echo >> $LISTFILE }
	}

	cat $LISTFILE | grep -v "^$" | sed 's/ $//g' > /tmp/list.${BASEID}.tmp
	mv /tmp/list.${BASEID}.tmp $LISTFILE
end

echo "Done."
