
############################################################################
# Path to binary and scripts
#
#export SCRIPTDIR=/tsi/cluster/fouquier/svn/hand/trunk/reference-systems/get-enst/scripts
#export SCRIPTDIR=/cluster/lrde/prof/fouqui_g/svn/hand/trunk/reference-systems/get-enst/scripts
export SCRIPTDIR=/home/get-epita/scripts
export PATH=$PATH:$SCRIPTDIR
export HND_BIN_EXTRACT=$SCRIPTDIR/hand
export HND_BIN_EVALUATE=$SCRIPTDIR/hand
export HND_BIN_COMPARE=$SCRIPTDIR/hand-cmp
export HND_BIN_VERIF=$SCRIPTDIR/hand-verif
export HND_BIN_MERGE=$SCRIPTDIR/hand-merge

############################################################################
# Path to database list
#
#export HND_DB_ROOT=/tsi/biosecure/hands
#export HND_DB_ROOT=/data/biosecure/hands
export HND_DB_ROOT=/home

# Biomet Database
export HND_BIOMET_ROOT=${HND_DB_ROOT}/biomet-hand-v2
export HND_BIOMET_DB=${HND_BIOMET_ROOT}/pgm/list-biomet-camp03

# Enst Database
export HND_ENST_ROOT=${HND_DB_ROOT}/ENST-v2
export  HND_ENST_150=${HND_ENST_ROOT}/pgm/list-enst-150
export  HND_ENST_400=${HND_ENST_ROOT}/pgm/list-enst-400
export  HND_ENST_600=${HND_ENST_ROOT}/pgm/list-enst-600

# Bogazici Database
export  HND_BU_ROOT=${HND_DB_ROOT}/bogazici-v2
export  HND_BU_LEFT=${HND_BU_ROOT}/pgm/list-bu-left
export HND_BU_LAPSE=${HND_BU_ROOT}/pgm/list-bu-lapse
export HND_BU_RIGHT=${HND_BU_ROOT}/pgm/list-bu-right
export HND_BU_TOTAL=${HND_BU_ROOT}/pgm/list-bu-total


############################################################################
# Which database in protocols
#
export HND_PROT_ALL_DBS="$HND_BU_TOTAL $HND_BU_LEFT $HND_BU_RIGHT \
	                 $HND_BU_LAPSE $HND_ENST_150 $HND_BIOMET_DB"

export HND_PROT_DBSIZE_DBS="$HND_BU_TOTAL"
export HND_PROT_DBSIZE_STEPS="100 200 450 600 750"

export HND_PROT_ENROLLSIZE_DBS="$HND_BU_TOTAL"
export HND_PROT_ENROLLSIZE_MAX=2

export HND_PROT_LEFTRIGHT_DBS="$HND_BU_LEFT $HND_BU_RIGHT"

export HND_PROT_LAPSE_DBS="$HND_BU_LAPSE"

export HND_PROT_IMGSIZE_DBS="$HND_BU_TOTAL"
export HND_PROT_IMGSIZE_FACTORS="80 60 40 30 20"

export HND_PROT_FEATSIZE_DBS="$HND_BU_TOTAL"
export HND_PROT_FEATSIZE_DISTS="kl2f kl4f kl5f klbest3"
