#!/bin/zsh

#$ -S /bin/zsh
#$ -V
#$ -cwd
#$ -o sge-evaluate-log
#$ -j y

BINARY=$1
ROOTDIR=$2
cd $ROOTDIR
LISTFILE=$3
OPTIONS=$4

LOGFILE=evaluation.`basename ${LISTFILE}`
${BINARY} -n -p "score/`basename ${LISTFILE}`" `echo ${OPTIONS}` `echo ${LISTFILE} | xargs` > ${ROOTDIR}/log/${LOGFILE} 2>&1
