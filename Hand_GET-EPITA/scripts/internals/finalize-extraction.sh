#!/bin/zsh

#$ -S /bin/zsh
#$ -V
#$ -cwd
#$ -o sge-finalize-log
#$ -j y

ROOTDIR=$1
LOGFILE=$2
PARTLOG=$3
NBNODES=$4

cd $ROOTDIR
[[ -f $LOGFILE ]] && rm $LOGFILE

(( i = 1 ))
while [[ i -le $NBNODES ]]
do
        cat ${PARTLOG}.$i >> ${LOGFILE}
        (( i = $i + 1 ))
done
