#!/bin/zsh

#$ -S /bin/zsh
#$ -V
#$ -cwd
#$ -o sge-finalize-log
#$ -j y
#$ -m n

ROOTDIR=$1
LOGFILE=$2
NBNODES=$3

GENUINESCR=${ROOTDIR}/score/all-genuine-score.txt
IMPOSTORSCR=${ROOTDIR}/score/all-impostor-score.txt

############################################################

#$ -V
#$ -cwd
#$ -o sge-finalize-log
#$ -j y

source ${SCRIPTDIR}/internals/functions.sh

removeIfExist $LOGFILE ${GENUINESCR} ${IMPOSTORSCR}

cd $ROOTDIR

(( VPMEAN =     0.0 ))
(( VPMAX  =     0.0 ))
(( VPMIN  = 10000.0 ))
(( IPMEAN =     0.0 ))
(( IPMAX  =     0.0 ))
(( IPMIN  = 10000.0 ))
(( COUNT  =     0   ))

for (( i = 1 ; i < $NBNODES + 1 ; i += 1 ))
do
	LISTFILE=`grep "^$i " list.txt | cut -d' ' -f 2`
	PARTLOG=evaluation.`basename $LISTFILE`
        cat log/${PARTLOG} | grep -v "is missed" >> ${LOGFILE}

	cat log/$PARTLOG | grep -q "^Verification Performance:" && {
		# grep Verification and identification performances
		VP=`cat log/$PARTLOG | grep "^Verification Performance:" | cut -d':' -f 2 | cut -d'%' -f 1 | xargs`
		IP=`cat log/$PARTLOG | grep "^Recognition Rate:" | cut -d':' -f 2 | cut -d'%' -f 1 | xargs`

		# add to counter
		(( IPMEAN = $IPMEAN + $IP ))
		(( VPMEAN = $VPMEAN + $VP ))

		# keep min and max values
		[[ $VP -gt $VPMAX ]] && VPMAX=$VP
		[[ $IP -gt $IPMAX ]] && IPMAX=$IP
		[[ $VPMIN -gt $VP ]] && VPMIN=$VP
		[[ $IPMIN -gt $IP ]] && IPMIN=$IP

		# merge score
		cat score/`basename $LISTFILE`-genuine-score.txt >> ${GENUINESCR}
		cat score/`basename $LISTFILE`-impostor-score.txt >> ${IMPOSTORSCR}

		(( COUNT += 1 ))
	}

	echo >> ${LOGFILE}
	echo '####################################' >> ${LOGFILE}
	echo >> ${LOGFILE}
done

(( VPMEAN = $VPMEAN / $COUNT ))
(( IPMEAN = $IPMEAN / $COUNT ))

[[ $NBNODES -ne $COUNT ]] && {
	(( NBPBS = $NBNODES - $COUNT ))
	echo "Problem with: " $NBPBS" evaluations" >> ${LOGFILE}
}

# print result
echo "Min  Recognition Rate:         "`printScore $IPMIN`  >> ${LOGFILE}
echo "Mean Recognition Rate:         "`printScore $IPMEAN` >> ${LOGFILE}
echo "Max  Recognition Rate:         "`printScore $IPMAX`  >> ${LOGFILE}
echo                                          >> ${LOGFILE}
echo "Min  Verification Performance: "`printScore $VPMIN`  >> ${LOGFILE}
echo "Mean Verification Performance: "`printScore $VPMEAN` >> ${LOGFILE}
echo "Max  Verification Performance: "`printScore $VPMAX`  >> ${LOGFILE}
echo                                          >> ${LOGFILE}

# clean
#rm list.txt
