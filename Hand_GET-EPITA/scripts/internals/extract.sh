#/bin/zsh

#$ -S /bin/zsh
#$ -V
#$ -cwd
#$ -o sge-extraction-log
#$ -j y

BINARY=$1
LISTFILE=$2
LOGFILE=$3
ROOTDIR=$4
OPTIONS=$5

cd $ROOTDIR

# split extraction or not
echo $JOB_NAME | grep -q "\-split" && {
	echo "extract split" $JOB_NAME
	${BINARY} -qx ${OPTIONS} ${LISTFILE}.${SGE_TASK_ID} > ${LOGFILE}.${SGE_TASK_ID} 2>&1
} || {
	echo "extract pas split" $JOB_NAME
	${BINARY} -qx ${OPTIONS} ${LISTFILE} > ${LOGFILE} 2>&1
}
