#!/bin/zsh

#$ -S /bin/zsh
#$ -V
#$ -cwd
#$ -o sge-evaluate-log$TASK_ID
#$ -j y

BINARY=$1
ROOTDIR=$2
cd $ROOTDIR
OPTIONS=$3
LISTFILE=`grep "^${SGE_TASK_ID} " ${ROOTDIR}/list.txt | cut -d' ' -f 2`

LOGFILE=evaluation.`basename ${LISTFILE}`
${BINARY} -n -p "score/`basename ${LISTFILE}`" `echo ${OPTIONS}` ${LISTFILE} > ${ROOTDIR}/log/${LOGFILE} 2>&1
