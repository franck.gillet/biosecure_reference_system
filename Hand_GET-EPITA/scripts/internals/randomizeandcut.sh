#!/bin/zsh

OUTPUTDIR=`pwd`/out

CUTPROG=${SCRIPTDIR}/internals/cut
RANDOMPROG=${SCRIPTDIR}/internals/rando

###############################################################################

[[ $# -eq 2 ]] && {
	LISTFILE=$1
	STEP=$2
} || {
	echo "usage: "$0" file card"
	exit -1
}

mkdir $OUTPUTDIR
cd $OUTPUTDIR

LISTCARD=`cat $1 | wc -l | xargs`
(( NBSTEP = $LISTCARD / STEP ))
echo "Nb pictures: " $LISTCARD" step: "$STEP" nbstep: "$NBSTEP

for (( i = 1 ; i < $NBSTEP + 1 ; i += 1 ))
do
    (( FILESIZE = $i * $STEP ))
    $RANDOMPROG $LISTFILE > list-randomized-${FILESIZE}
    $CUTPROG list-randomized-${FILESIZE} $FILESIZE 4
    rm list-randomized-${FILESIZE}
done
