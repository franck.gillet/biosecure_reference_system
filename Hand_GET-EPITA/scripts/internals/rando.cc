
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

int
main(int	argc,
     char**	argv)
{

  if (argc != 2)
    {
      std::cerr << "Usage: " << argv[0] << " filetoshuffle" << std::endl;
      exit (-1);
    }

  std::ifstream ifs(argv[1]);

  if (!ifs)
    {
      std::cerr << "Error, Can't open file " << argv[1] << std::endl;
      exit (-2);
    }

  std::vector< std::string > v;
  std::string str;

  do
    {
      std::getline( ifs, str);
      if (str.length())
	v.push_back(str);
    }
  while (str.length());


  random_shuffle(v.begin(), v.end());

  for (unsigned i = 0; i < v.size(); ++i)
    std::cout << v[i] << std::endl;
}
