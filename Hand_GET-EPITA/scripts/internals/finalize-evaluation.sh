#!/bin/zsh

#$ -S /bin/zsh
#$ -V
#$ -cwd
#$ -o sge-finalize-log
#$ -j y
#$ -m n

ROOTDIR=$1
cd $ROOTDIR
LOGFILE=$2
NBNODES=$3

source ${SCRIPTDIR}/internals/functions.sh

removeIfExist $LOGFILE

for (( i = 1 ; i < $NBNODES + 1 ; i += 1 ))
do
	LISTFILE=`grep "^$i " list.txt | cut -d' ' -f 2`
	PARTLOG=evaluation.`basename $LISTFILE`
        cat log/${PARTLOG} | grep -v "is missed" >> ${LOGFILE}
	echo >> ${LOGFILE}
	echo '####################################' >> ${LOGFILE}
	echo >> ${LOGFILE}
done

rm list.txt
