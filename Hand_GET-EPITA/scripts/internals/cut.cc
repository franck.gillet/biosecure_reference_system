
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

#include <math.h>

std::string
shortname2(const char*	str)
{
  std::string           res(str);
  unsigned              loc = res.find_last_of('/');
  if (loc != std::string::npos)
    return res.substr(loc + 1);
  return res;
}

int
main(int	argc,
     char**	argv)
{
  if (argc != 4)
    {
      std::cerr << "Usage: " << argv[0] << " filetocut card nbfile" << std::endl;
      exit (-1);
    }

  std::ifstream ifs(argv[1]);
  unsigned	card = atoi(argv[2]);
  unsigned	nbfile = atoi(argv[3]);
  assert((card > 0) && (nbfile > 0));

  if (!ifs)
    {
      std::cerr << "Error, Can't open file " << argv[1] << std::endl;
      exit (-2);
    }

  std::vector< std::string > v;
  std::string str;

  do
    {
      std::getline( ifs, str);
      if (str.length())
        v.push_back(str);
    }
  while (str.length());

  for (unsigned i = 0; i < nbfile; ++i)
    {
      std::string outputname = shortname2(argv[1]);
      outputname += ".";
      // beurk :
      assert(i < 10);
      char c = (char)i + 48;
      outputname += c;
      std::ofstream ofs(outputname.c_str());
      if (!ofs.is_open())
	{
	  std::cerr << "can't open file: " << outputname << std::endl;
	  exit (-1);
	}
      random_shuffle(v.begin(), v.end());
      for (unsigned j = 0; j < card && j < v.size(); ++j)
	ofs << v[j] << std::endl;
      ofs.close();
    }
}
